# Glamping aggregator

## Project resources

  - [Task manager](https://trello.com/b/B32cy1cw)
  - [Bug tracker](https://bitbucket.org/IliyVas/torgservice/issues)
  - [Project documents](https://drive.google.com/drive/folders/0Byv4F70exq0GOG9MTEJJd1FoMkU)

## Test server

Coming soon

### ssh/ftp
| Server            | Login           | Password  |
| ----------------- |-----------------| --------- |
| - | - | - |

### Database
| Host            | User/DB          | Password |
| --------------- |------------------| -------- |
| - | - | - |