<?php

use Illuminate\Database\Seeder;
use App\Models\Glamping\Amenity;

class AmenitiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $amenities = [
            [
                'icon' => 'flaticon-fridge',
                'description:en' => 'Fridge',
                'name:en' => 'Fridge',
                'description:ru' => 'Холодильник',
                'name:ru' => 'Холодильник',
            ],
            [
                'icon' => 'flaticon-telephone',
                'description:en' => 'Telephone',
                'name:en' => 'Telephone',
                'description:ru' => 'Телефон',
                'name:ru' => 'Телефон',
            ],
            [
                'icon' => 'flaticon-oven',
                'description:en' => 'Oven',
                'name:en' => 'Oven',
                'description:ru' => 'Духовой шкаф',
                'name:ru' => 'Духовка',
            ],
            [
                'icon' => 'flaticon-fan',
                'description:en' => 'Fan',
                'name:en' => 'Fan',
                'description:ru' => 'Вентилятор',
                'name:ru' => 'Вентилятор'
            ],
        ];

        foreach ($amenities as $key => $value) {
           Amenity::create($value);
        }
    }
}
