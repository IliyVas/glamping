<?php

use Illuminate\Database\Seeder;
use App\Models\News\News;

class NewsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 1; $i <= 5; $i++) {
            $news = [
                [
                    'manager_id' => 1,
                    'name:ru' => 'Первая новость',
                    'name:en' => 'First news',
                    'description:ru' => 'Описание для первой новости',
                    'description:en' => 'Description for first news',
                    'title:ru' => 'Первая новость',
                    'title:en' => 'The first news',
                    'keywords:ru' => 'первая, новость',
                    'keywords:en' => 'first, news',
                    'seo_description:ru' => 'Описание для сео новости',
                    'seo_description:en' => 'Description for seo news',
                ],
                [
                    'manager_id' => 1,
                    'name:ru' => 'Вторая новость',
                    'name:en' => 'The second news',
                    'description:ru' => 'Описание для второй новости',
                    'description:en' => 'Description for second page',
                    'title:ru' => 'Вторая новость',
                    'title:en' => 'The second news',
                    'keywords:ru' => 'вторая, новость',
                    'keywords:en' => 'second, news',
                    'seo_description:ru' => 'Описание для сео новости',
                    'seo_description:en' => 'Description for seo news',
                ]
            ];

            foreach ($news as $key => $value) {
                $article = News::create($value);

                $article->addMediaFromUrl('http://richwenews.com/images/article.jpeg')
                    ->toMediaLibrary('NewsImages');
            }
        }
    }
}
