<?php

use Illuminate\Database\Seeder;
use App\Models\Glamping\Impression;

class ImpressionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $impressions = [
            [
                'icon' => 'http://cyclepedia.ru/images/imagecache/post_pictures/bike-icon.png',
                'name:en' => 'Biking',
                'name:ru' => 'Езда на велосипеде',
                'description:en' => 'There are plenty of things to do right here at the camp, such as biking.',
                'description:ru' => 'Здесь есть чем заняться, например, можно покататься на велосипеде.',
            ],
            [
                'icon' => 'http://rusticker.ru/files/collections/19705.png?t=1411761600',
                'name:en' => 'Fishing',
                'name:ru' => 'Рыбалка',
                'description:en' => 'There are plenty of things to do right here at the camp, such as fishing.',
                'description:ru' => 'Здесь есть чем заняться, например, можно порыбачить.',
            ],
            [
                'icon' => 'http://s1.iconbird.com/ico/2014/1/597/w512h5121390846284bird512.png',
                'name:en' => 'Bird watching',
                'name:ru' => 'Наблюдение за птицами',
                'description:en' => 'There are plenty of things to do right here at the camp, such as bird watching.',
                'description:ru' => 'Здесь есть чем заняться, например, можно наблюдать за птицами.',
            ],
            [
                'icon' => 'https://maxcdn.icons8.com/wp-content/uploads/2014/04/trekking-128.png',
                'name:en' => 'Hiking',
                'name:ru' => 'Пеший туризм',
                'description:en' => 'There are plenty of things to do right here at the camp, such as hiking.',
                'description:ru' => 'Здесь есть чем заняться, например, можно отправиться в пеший поход.',
            ],
        ];

        foreach ($impressions as $key => $value) {
            Impression::create($value);
        }
    }
}
