<?php

use App\Models\Auth\Role;
use App\Models\Auth\Permission;
use Illuminate\Database\Seeder;
use App\Models\Auth\Manager;
use App\Models\Auth\User;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(RoleTableSeeder::class);
        $this->call(PermissionTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(CountriesTableSeeder::class);
        $this->call(GlampingGroupsTableSeeder::class);
        $this->call(ImpressionsTableSeeder::class);
        $this->call(AmenitiesTableSeeder::class);
        $this->call(ServicesTableSeeder::class);
        $this->call(GlampingsTableSeeder::class);
        $this->call(BuildingTypesTableSeeder::class);
        $this->call(BuildingsTableSeeder::class);
        $this->call(BookBuildingsTableSeeder::class);
        $this->call(MessagesTableSeeder::class);
        $this->call(PageSeeder::class);
        $this->call(NewsTableSeeder::class);

        //admin
        $adminUser = Manager::where('first_name', 'Master')->first();
        $adminPermission = Permission::where('name', 'manage-project')->first();
        $adminRole = Role::where('name', 'admin')->first();
        //assign role to user
        $adminUser->attachRole($adminRole->id); // id only
        //assign permission to role
        $adminRole->attachPermission($adminPermission->id); // id only

        //owners
        $owners = [
            Manager::where('first_name', 'Loges')->first(),
            Manager::where('first_name', 'Eco')->first(), 
            ];
        $ownerPermission = Permission::where('name', 'leave-application')->first();
        $ownerRole = Role::where('name', 'owner')->first();
        //assign permission to role
        $ownerRole->attachPermission($ownerPermission->id); // id only

        foreach ($owners as $owner) {
            //assign role to user
            $owner->attachRole($ownerRole->id); // id only
        }

//        $testUser = User::where('first_name', 'Walter')->first();
//        $testUserRole = Role::where('name', 'user')->first();
//        //assign role to user
//        $testUser->attachRole($testUserRole->id); // id only
    }
}
