<?php

use Illuminate\Database\Seeder;
use App\Models\Auth\Role;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = [
            [
                'name' => 'admin',
                'display_name' => 'Project admin',
                'description' => 'User can create, edit all data.'
            ],
            // [
            //     'name' => 'moderator',
            //     'display_name' => 'Junior admin',
            //     'description' => 'User can revise and publish some information.'
            // ],
            [
                'name' => 'owner',
                'display_name' => 'Glamping owner',
                'description' => 'User can leave application for create glamping, edit the glamping data.'
            ],
        ];

        foreach ($roles as $key => $value) {
            Role::create($value);
        }
    }
}
