<?php

use Illuminate\Database\Seeder;
use App\Models\Communication\Country;

class CountriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $countries = [
            [
                'iso_code' => 'RU',
                'title:en' => 'Russian Federation',
                'title:ru' => 'Российская Федерация',
            ],
            [
                'iso_code' => 'ES',
                'title:en' => 'Spain',
                'title:ru' => 'Испания',
            ],
            [
                'iso_code' => 'IT',
                'title:en' => 'Italy',
                'title:ru' => 'Италия',
            ],
            [
                'iso_code' => 'FR',
                'title:en' => 'France',
                'title:ru' => 'Франция',
            ],
            [
                'iso_code' => 'BY',
                'title:en' => 'Republic of Belarus',
                'title:ru' => 'Республика Беларусь',
            ],
        ];

        foreach ($countries as $key => $value) {
            Country::create($value);
        }
    }
}
