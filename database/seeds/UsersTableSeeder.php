<?php

use Illuminate\Database\Seeder;
use App\Models\Auth\Manager;
use App\Models\Auth\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = [
            [
                'first_name' => 'Walter',
                'last_name' => 'White',
                'patronymic' => 'Middle',
                'birth_date' => date_create('26-11-1992')->format('Y-m-d'),
                'activated' => true,
                'email_token' => base64_encode('walter@white.com'),
                'email' => 'walter@white.com',
                'phone' => '8(963)995-24-27',
                'password' => bcrypt('walter'),
            ]
        ];

        foreach ($users as $key => $value) {
            User::create($value);
        }

        $managers = [
            [
                'first_name' => 'Master',
                'last_name' => 'Senior-admin',
                'email' => 'master@master.com',
                'password' => bcrypt('master'),
                'activated' => true,
                'accepted' => true,
                'email_token' => base64_encode('master@master.com'),
            ],
            [
                'first_name' => 'Loges',
                'patronymic' => 'Middle',
                'last_name' => 'Loges',
                'email' => 'loges@owner.com',
                'phone' => '8(963)995-24-27',
                'password' => bcrypt('loges'),
                'activated' => true,
                'accepted' => true,
                'email_token' => base64_encode('loges@owner.com'),
            ],
            [
                'first_name' => 'Eco',
                'last_name' => 'Loges',
                'phone' => '8(963)995-24-27',
                'patronymic' => 'Mind',
                'email' => 'eco_loges@owner.com',
                'password' => bcrypt('eco_loges'),
                'email_token' => base64_encode('eco_loges@owner.com'),
            ],
        ];

        foreach ($managers as $key => $value) {
            $manager = Manager::create($value);

            if ($manager->email === 'loges@owner.com')
                $manager->company()->create(['name' => 'Loges Company', 'registration_number' => '159GT78']);
        }
    }
}