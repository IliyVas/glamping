<?php

use Illuminate\Database\Seeder;
use App\Models\Booking\Booking;
use App\Models\Building\Building;
use App\Http\Controllers\Glamping\BookController;
use App\Models\Auth\User;

class BookBuildingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $names = ['Natasha', 'Sasha', 'Alina', 'Dima', 'Victor'];
        $lastNames = ['Drobko', 'Gud', 'Minsk', 'Kribko', 'Borisenko'];
        $countries = ['RU', 'ES', 'IT', 'BY', 'FR'];

        for ($i = 1; $i < 5; $i++) {
            $build = Building::find($i);
            $count = rand(1, $build->count);
            $user = User::find(1);

            $buildingBookings = [
                [
                    'booking' =>
                        [
                            'name' => $names[rand(0, (count($names) - 1))] . ' ' . $lastNames[rand(0, (count($lastNames) - 1))],
                            'from_date' => date('Y-m-d', time() - (rand(0, 3)*24*60*60)),
                            'to_date' => date('Y-m-d', time() + (rand(4, 10)*24*60*60)),
                            'adults' => $build->capacity * $count,
                            'country_code' => $countries[rand(0, (count($countries) - 1))],
                            'children' => rand(1,3),
                            'manager_id' => 2,
                            'email' => 'email@email.com',
                            'glamping_id' => $build->glamping_id,
                        ],
                    'book_buildings' => ['building_id' => $i, 'count' => $count]
                ],
                [
                    'booking' =>
                        [
                            'from_date' => date('Y-m-d', time() + (rand($i, $i + 3)*24*60*60)),
                            'to_date' => date('Y-m-d', time() + (rand($i + 4, $i + 10)*24*60*60)),
                            'adults' => $build->capacity,
                            'country_code' => $countries[rand(0, (count($countries) - 1))],
                            'children' => rand(1,3),
                            'manager_id' => 2,
                            'user_id' => $user->id,
                            'name' => $user->first_name . ' ' . $user->last_name,
                            'email' => 'email@email.com',
                            'glamping_id' => $build->glamping_id,
                        ],
                    'book_buildings' => ['building_id' => $i, 'count' => 1]
                ],
            ];

            foreach ($buildingBookings as $buildingBooking) {
                $booking = Booking::create($buildingBooking['booking']);
                $booking->bookBuildings()->create($buildingBooking['book_buildings']);
                (new BookController)->generateBookingPDF($buildingBooking['booking'], $build->glamping, $booking);
            }
        }
    }
}
