<?php

use Illuminate\Database\Seeder;
use App\Models\Auth\Permission;

class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permissions = [
            [
                'name' => 'manage-project',
                'display_name' => 'Manage the Project',
                'description' => 'User can create, edit all data'
            ],
            [
                'name' => 'leave-application',
                'display_name' => 'Leave Application',
                'description' => 'User can leave application'
            ]
        ];

        foreach ($permissions as $key => $value) {
            Permission::create($value);
        }
    }
}
