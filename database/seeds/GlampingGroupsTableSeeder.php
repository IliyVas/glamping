<?php

use Illuminate\Database\Seeder;
use App\Models\Glamping\GlampingGroup;

class GlampingGroupsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $glampingGroups = [
            [
                'icon' => 'https://maxcdn.icons8.com/Share/icon/Animals//cat_footprint1600.png',
                'name:en' => 'Pet-friendly',
                'name:ru' => 'Разрешены домашние животные',
                'description:en' => 'Pet-friendly',
                'description:ru' => 'Разрешены домашние животные',
            ],
            [
                'icon' => 'http://vignette2.wikia.nocookie.net/absurdopedia/images/c/c8/Fire_Icon.png/revision/latest?cb=20100720194021',
                'name:en' => 'Making a campfire allowed',
                'name:ru' => 'Разрешено палить кастер',
                'description:en' => 'Making a campfire allowed',
                'description:ru' => 'Разрешено палить кастер',
            ],
            [
                'icon' => 'http://abali.ru/wp-content/uploads/2013/12/znak-kurit-zapresheno.png',
                'name:en' => 'No smoking',
                'name:ru' => 'Курение запрещено',
                'description:en' => 'No smoking',
                'description:ru' => 'Курение запрещено',
            ],
            [
                'icon' => 'http://www.clker.com/cliparts/m/p/r/5/O/4/meeting-hi.png',
                'name:en' => 'For large companies',
                'name:ru' => 'Разрешены большие компании',
                'description:en' => 'For large companies',
                'description:ru' => 'Разрешены большие компании',
            ],
        ];

        foreach ($glampingGroups as $key => $value) {
            GlampingGroup::create($value);
        }
    }
}