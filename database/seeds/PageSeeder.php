<?php

use Illuminate\Database\Seeder;
use App\Models\Page\Page;

class PageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $pages = [
            [
                'name:ru' => 'О нас',
                'name:en' => 'About us',
                'title:ru' => 'О нас',
                'title:en' => 'About us',
                'slug' => 'about',
                'keywords:ru' => 'всё о нас, информация о компании, гламп',
                'keywords:en' => 'all about us, information about company, glump',
                'seo_description:ru' => 'Описание для сео страницы',
                'seo_description:en' => 'Description for seo page',
                'description:ru' => 'Описание для странички',
                'description:en' => 'Description for page',
            ],
            [
                'name:ru' => 'Контакты',
                'name:en' => 'Contacts',
                'title:ru' => 'Контакты',
                'title:en' => 'Contacts',
                'slug' => 'contact',
                'keywords:ru' => 'контакты, телефон, адрес',
                'keywords:en' => 'contacts, phone, address',
                'seo_description:ru' => 'Описание для сео страницы',
                'seo_description:en' => 'Description for seo page',
                'description:ru' => 'Описание для странички контактов',
                'description:en' => 'Description for page contacts',
            ]
        ];

        foreach ($pages as $key => $value) {
            Page::create($value);
        }
    }
}
