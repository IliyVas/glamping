<?php

use Illuminate\Database\Seeder;
use App\Models\Glamping\Glamping;

class GlampingsTableSeeder extends Seeder
{
    const COUNT = 10;
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $ruNames = ['Глэмпинг', 'Ферма', 'Домик', 'Эко-дом', 'Палатки'];
        $enNames = ['Glamping', 'Farmhouse', 'Loges', 'Eco-house', 'Tents'];
        $enCountries = ['Russian Federation', 'Republic of Belarus', 'Italy', 'Spain', 'France'];
        $ruCountries = ['Российская Федерация', 'Республика Беларусь', 'Италия', 'Испания', 'Франция'];
        $enRegion = ['Moscow', 'Mogilev', 'Milan', 'Barcelona', 'Paris'];
        $ruRegion = ['Москва', 'Могилев', 'Милан', 'Барселона', 'Париж'];

        for ($i = 1; $i <= self::COUNT; $i++) {
            $glampings = [
                [
                    'latitude' => '55.76638',
                    'longitude' => '37.46734',
                    'max_child_age' => rand(1, 6),
                    'name:en' => $enNames[rand(0, (count($enNames) - 1))] . '-' . $i,
                    'name:ru' => $ruNames[rand(0, (count($ruNames) - 1))] . '-' . $i,
                    'description:en' => 'Farmhouse',
                    'description:ru' => 'Ферма',
                    'country:en' => $enCountries[rand(0, (count($enCountries) - 1))],
                    'country:ru' => $ruCountries[rand(0, (count($ruCountries) - 1))],
                    'region:en' => $enRegion[rand(0, (count($enRegion) - 1))],
                    'region:ru' => $ruRegion[rand(0, (count($ruRegion) - 1))],
                    'arrival_time' => rand(12, 24) . ':' . rand(10, 59),
                    'check_out_time' => rand(10, 24) . ':' . rand(10, 59),
                    'free_return' => rand(1, 5),
                ],
                [
                    'latitude' => '53.07870',
                    'longitude' => '29.14871',
                    'max_child_age' => rand(1, 6),
                    'name:en' => $enNames[rand(0, (count($enNames) - 1))] . ' №' . $i,
                    'name:ru' => $ruNames[rand(0, (count($ruNames) - 1))] . ' №' . $i,
                    'description:en' => 'Farmhouse2',
                    'description:ru' => 'Ферма2',
                    'country:en' => $enCountries[rand(0, (count($enCountries) - 1))],
                    'country:ru' => $ruCountries[rand(0, (count($ruCountries) - 1))],
                    'region:en' => $enRegion[rand(0, (count($enRegion) - 1))],
                    'region:ru' => $ruRegion[rand(0, (count($ruRegion) - 1))],
                    'arrival_time' => rand(12, 24) . ':' . rand(10, 59),
                    'check_out_time' => rand(12, 24) . ':' . rand(10, 59),
                    'free_return' =>  rand(1, 5),
                ]
            ];

            foreach ($glampings as $key => $value) {
                $glamping = Glamping::create($value);

                //set rating for glamping
                $glamping->comments()->create(['rating' => rand(0, 5), 'user_id' => 1,
                    'comment' => 'Привет! Я комментарий к глэмпингу с id равным ' . $glamping->id]);

                //activate and accept some glampings
                if ($glamping->id <= self::COUNT)
                    $glamping->update(['active' => true, 'accepted' => true]);

                //set glamping image
                $glamping->addMediaFromUrl('http://assets.inhabitat.com/wp-content/blogs.dir/1/files/2016/08/Autonomous-Tent-by-Harry-Gesner-7.jpg')
                    ->toMediaLibrary('GlampingImages');

                $glamping->glampingGroups()->attach(rand(1, 4));
                $glamping->impressions()->attach(rand(1, 4));
                $glamping->amenities()->attach(rand(1, 4));
                $glamping->services()->attach(rand(1, 4));
                $glamping->managers()->attach(2);
            }
        }
    }
}
