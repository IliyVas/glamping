<?php

use Illuminate\Database\Seeder;
use App\Models\Building\BuildingType;

class BuildingTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $buildingTypes = [
            [
                'image' => 'http://www.thankyouforbeingsophisticated.com/uploads/1/0/8/1/10814696/1873382.jpg',
                'icon' => 'https://www.glamping.com/static/media/uploads/icons/glamping/type/cabinneering.svg',
                'description:en' => "Cabineering gives adventure travelers the comforts of home and the special amenities of a luxury hotel in some of the most remote and breathtaking areas around the globe. It's a cozy way to have a memorable vacation.",
                'name:en' => 'Cabins',
                'description:ru' => 'Хижина предлагает приключенческим путешественникам комфорт дома и особые удобства роскошного отеля в самых отдаленных и захватывающих дух районах по всему миру. Это уютный способ провести незабываемые каникулы.',
                'name:ru' => 'Хижина'
            ],
            [
                'image' => 'http://www.thankyouforbeingsophisticated.com/uploads/1/0/8/1/10814696/1873382.jpg',
                'icon' => 'https://www.glamping.com/static/media/uploads/icons/glamping/type/cabinneering.svg',
                'description:en' => 'Many resorts or lodges have been built around the world with eco-friendliness in mind. From the building materials used to the energy and resource consumption, eco lodges try to embody a full destination expereince with as little enviornmental impact as possible while allowing guests to stay in complete luxury.',
                'name:en' => 'Eco Lodges',
                'description:ru' => 'Многие курорты или домики построены по всему миру с учетом экологичности. Из строительных материалов, используемых для энергопотребления и энергопотребления, эко-домики пытаются воплотить в жизнь все направления с минимальным воздействием на окружающую среду, позволяя гостям оставаться в полной роскоши.',
                'name:ru' => 'Эко-домики'
            ],
            [
                'image' => 'http://www.thankyouforbeingsophisticated.com/uploads/1/0/8/1/10814696/1873382.jpg',
                'icon' => 'https://www.glamping.com/static/media/uploads/icons/glamping/type/cabinneering.svg',
                'description:en' => 'Lodges give travelers the comforts of home and the special amenities of a luxury hotel in some of the most remote and breathtaking areas around the globe. It is a cozy way to have a memorable vacation.',
                'name:en' => 'Lodges',
                'description:ru' => 'Домики предлагает путешественникам комфорт дома и особые удобства роскошного отеля в самых отдаленных и захватывающих дух районах по всему миру. Это уютный способ провести незабываемые каникулы.',
                'name:ru' => 'Домики'
            ],
            [
                'image' => 'http://www.thankyouforbeingsophisticated.com/uploads/1/0/8/1/10814696/1873382.jpg',
                'icon' => 'https://www.glamping.com/static/media/uploads/icons/glamping/type/cabinneering.svg',
                'description:en' => 'We have dedicated an entire category to Tents. It is a cozy way to have a memorable vacation.',
                'name:en' => 'Tents',
                'description:ru' => 'Мы посвятили целую категорию палаткам. Это уютный способ провести незабываемый отпуск.',
                'name:ru' => 'Палатки'
            ],
            [
                'image' => 'http://www.thankyouforbeingsophisticated.com/uploads/1/0/8/1/10814696/1873382.jpg',
                'icon' => 'https://www.glamping.com/static/media/uploads/icons/glamping/type/cabinneering.svg',
                'description:en' => 'Trailers are a cozy way to have a memorable vacation.',
                'name:en' => 'Trailers',
                'description:ru' => 'Трейлеры - это удобный способ провести незабываемый отпуск.',
                'name:ru' => 'Трейлеры'
            ]
        ];

        foreach ($buildingTypes as $key => $value) {
            BuildingType::create($value);
        }
    }
}