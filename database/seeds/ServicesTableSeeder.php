<?php

use Illuminate\Database\Seeder;
use App\Models\Glamping\Service;

class ServicesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $services = [
            [
                'icon' => 'flaticon-telephone',
                'name:en' => 'Concierge',
                'description:en' => 'We offer concierge service.',
                'name:ru' => 'Консьерж',
                'description:ru' => 'Мы предлагаем услуги консъержа.',
            ],
            [
                'icon' => 'flaticon-telephone',
                'name:en' => 'Safe deposit boxes',
                'description:en' => 'Only you, and whomever you designate, can open your safety deposit box.',
                'name:ru' => 'Сейф',
                'description:ru' => 'Только вы и тот, кого вы укажете, можете открыть сейф.',
            ],
            [
                'icon' => 'flaticon-telephone',
                'name:en' => 'Currency exchange',
                'description:en' => 'Currency exchange.',
                'name:ru' => 'Обмен валют',
                'description:ru' => 'Обмен валют.',
            ],
            [
                'icon' => 'flaticon-telephone',
                'name:en' => 'Business center',
                'description:en' => 'We have a fully staffed business center.',
                'name:ru' => 'Бизнес-центр',
                'description:ru' => 'У нас есть полностью укомплектованный бизнес-центр.',
            ],
        ];

        foreach ($services as $key => $value) {
            Service::create($value);
        }
    }
}
