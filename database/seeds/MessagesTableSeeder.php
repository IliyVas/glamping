<?php

use Illuminate\Database\Seeder;
use App\Models\Communication\Message;

class MessagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 1; $i <= 10; $i++) {
            $message = [
                'name' => 'User №' . $i,
                'email' => 'user@' . $i . '.com',
                'message' => 'Hello! I\'m interested in registering a glamping on your site. How can i do this?',
            ];

           Message::create($message);
        }
    }
}
