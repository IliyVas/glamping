<?php

use Illuminate\Database\Seeder;
use App\Models\Building\Building;
use App\Models\Building\BuildingOption;
use App\Models\Building\BuildingPrice;
use App\Models\Communication\Application;
use App\Models\Glamping\Glamping;
use App\Models\Communication\ApplicationStatus;
use App\Models\Communication\ApplicationType;

class BuildingsTableSeeder extends Seeder
{
    const COUNT = 30;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 1; $i <= self::COUNT; $i++) {
            //get glamping id
            $glampingId = $i;
            if ($glampingId > GlampingsTableSeeder::COUNT * 2)
                $glampingId = 1;
            if ($glampingId <= GlampingsTableSeeder::COUNT) $st = true; else $st = false;

            $buildings = [
                [
                    'glamping_id' => $glampingId,
                    'building_type_id' => rand(1, 5),
                    'capacity' => rand(1, 5),
                    'count' => rand(1, 10),
                    'main_price' => rand(500, 1000),
                    'description:en' => 'Cabin',
                    'active' => $st,
                    'accepted' => $st,
                    'name:en' => 'Cabin',
                    'description:ru' => 'Хижина',
                    'name:ru' => 'Хижина',

                ],
                [
                    'glamping_id' =>  $glampingId,
                    'building_type_id' => rand(1, 5),
                    'capacity' => rand(1, 7),
                    'count' => rand(1, 5),
                    'main_price' => rand(500, 700),
                    'min_nights_count' => rand(1, 5),
                    'active' => $st,
                    'accepted' => $st,
                    'description:en' => 'Cabin room',
                    'name:en' => 'Cabin room',
                    'description:ru' => 'Комната в хижине',
                    'name:ru' => 'Комната в хижине',
                ]
            ];

            foreach ($buildings as $key => $value) {
                $building = Building::create($value);

                if ($building->id <= 10) {
                    $priceData = [
                        'from' => date('Y-m-d', time() + (rand(0, 5) * 24 * 60 * 60)),
                        'to' => date('Y-m-d', time() + (7 * 24 * 60 * 60)),
                        'price' => rand(50, 525)
                    ];

                    $price = BuildingPrice::create($priceData);
                    $building->prices()->attach($price->id);
                }

                    $glamping = Glamping::find($building->glamping_id);

                if ($building->glamping_id > 15 && !count($glamping->applications) > 0) {
                    $app = new Application;
                    $app->forceFill([
                        'type' => ApplicationType::CREATE,
                        'status' => ApplicationStatus::PENDING,
                    ]);
                    $glamping->applications()->save($app);
                }

                if ($building->id < 5) {
                    $options = [
                        [
                            'building_id' => $building->id,
                            'price' => rand(5, 25),
                            'name:en' => 'Breakfasts',
                            'name:ru' => 'Завтраки',
                            'description:ru' => 'Включены завтраки.',
                            'description:en' => 'Breakfasts included.'
                        ],
                        [
                            'building_id' => $building->id,
                            'price' => rand(5,25),
                            'name:en' => 'Lunches',
                            'name:ru' => 'Обеды',
                            'description:ru' => 'Включены обеды.',
                            'description:en' => 'Lunches included.'
                        ],
                        [
                            'building_id' => $building->id,
                            'price' => rand(7, 15),
                            'name:en' => 'Breakfasts + Lunches',
                            'name:ru' => 'Завтраки + Обеды',
                            'description:ru' => 'Включены завтраки и обеды .',
                            'description:en' => 'Breakfasts and Lunches included.'
                        ]
                    ];

                    foreach ($options as $optionKey => $optionValue) {
                        BuildingOption::create($optionValue);
                    }

                    if ($building->glamping_id <= GlampingsTableSeeder::COUNT && !count($glamping->applications) > 0) {
                        $app = new Application;
                        $app->forceFill([
                            'type' => ApplicationType::CREATE,
                            'status' => ApplicationStatus::ACCEPTED,
                            'manager_id' => 1,
                            'is_viewed' => true
                        ]);
                        $glamping->applications()->save($app);
                    }
                }

                //set building amenities
                $building->amenities()->attach([rand(1, 4)]);

                //set building image
                $building->addMediaFromUrl('http://richwenews.com/images/article.jpeg')
                    ->toMediaLibrary('BuildingImages');

                //set rating
                $building->comments()->create(['rating' => rand(0, 5), 'user_id' => 1,
                    'comment' => 'Привет! Я комментарий к помещению с id равным ' . $building->id]);
            }
        }
    }
}
