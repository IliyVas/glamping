<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateApplicationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('applications', function (Blueprint $table) {
            $table->increments('id');
            $table->string('status');
            $table->string('type');
            $table->string('answer')->nullable();
            $table->integer('manager_id')->unsigned()->nullable();
            $table->boolean('is_viewed')->default(false);
            $table->integer('applicationable_id')->unsigned();
            $table->string('applicationable_type');
            $table->timestamps();

            $table->foreign('manager_id')->references('id')->on('managers');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('applications');
    }
}
