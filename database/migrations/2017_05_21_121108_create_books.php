<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBooks extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //create table for bookings
        Schema::create('bookings', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('manager_id')->unsigned();
            $table->integer('glamping_id')->unsigned();
            $table->string('status');
            $table->string('country_code');
            $table->date('from_date');
            $table->date('to_date');
            $table->integer('user_id')->nullable()->unsigned();
            $table->string('name')->nullable();
            $table->string('email')->nullable();
            $table->integer('adults');
            $table->integer('children');

            $table->timestamps();
            $table->foreign('glamping_id')->references('id')->on('glampings');
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('manager_id')->references('id')->on('managers');
        });

        //create table for booking buildings
        Schema::create('book_buildings', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('booking_id')->unsigned();
            $table->integer('building_id')->unsigned();
            $table->integer('count');

            $table->timestamps();
            $table->foreign('booking_id')->references('id')->on('bookings');
            $table->foreign('building_id')->references('id')->on('buildings');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('book_buildings');
        Schema::dropIfExists('bookings');
    }
}
