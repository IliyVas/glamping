<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBuildingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        //create table for building types
        Schema::create('building_types', function (Blueprint $table) {
            $table->increments('id');
            $table->text('icon')->nullable();
            $table->longText('image')->nullable();
            $table->timestamps();
        });

        //create table for building type translations
        Schema::create('building_type_translations', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('building_type_id')->unsigned()->nullable();
            $table->string('name');
            $table->mediumText('description');
            $table->string('locale')->index();

            $table->unique(['building_type_id','locale']);
            $table->foreign('building_type_id')->references('id')->on('building_types')->onDelete('cascade');
        });

        //create table for buildings
        Schema::create('buildings', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('building_id')->nullable()->unsigned();
            $table->boolean('active')->default(false);
            $table->boolean('accepted')->default(false);
            $table->integer('building_type_id')->unsigned();
            $table->integer('glamping_id');
            $table->integer('capacity');
            $table->integer('count');
            $table->integer('min_nights_count')->nullable();
            $table->integer('main_price');
            $table->timestamps();

            $table->foreign('building_id')->references('id')->on('buildings');
            $table->foreign('building_type_id')->references('id')->on('building_types');
        });

        //create table for building translations
        Schema::create('building_translations', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('building_id')->unsigned();
            $table->string('name');
            $table->mediumText('description');
            $table->mediumText('other_information')->nullable();
            $table->string('locale')->index();

            $table->unique(['building_id','locale']);
            $table->foreign('building_id')->references('id')->on('buildings')->onDelete('cascade');
        });

        //create table for buildings options
        Schema::create('building_options', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('building_id')->unsigned();
            $table->integer('price');
            $table->timestamps();

            $table->foreign('building_id')->references('id')->on('buildings')->onDelete('cascade');
        });

        //create table for buildings option translations
        Schema::create('building_option_translations', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('building_option_id')->unsigned();
            $table->string('name');
            $table->mediumText('description');
            $table->string('locale')->index();

            $table->unique(['building_option_id','locale']);
            $table->foreign('building_option_id')->references('id')->on('building_options')->onDelete('cascade');
        });

        //create table for buildings count
        Schema::create('building_counts', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('building_id')->unsigned();
            $table->date('from_date');
            $table->date('to_date');
            $table->integer('count');
            $table->timestamps();

            $table->foreign('building_id')->references('id')->on('buildings')->onDelete('cascade');
        });

        // Create table for associating buildings to amenities (Many-to-Many)
        Schema::create('building_amenity', function (Blueprint $table) {
            $table->integer('building_id')->unsigned();
            $table->integer('amenity_id')->unsigned();

            $table->foreign('building_id')->references('id')->on('buildings')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('amenity_id')->references('id')->on('amenities')
                ->onUpdate('cascade')->onDelete('cascade');

            $table->primary(['building_id', 'amenity_id']);
        });

        // Create table for building prices
        Schema::create('building_prices', function (Blueprint $table) {
            $table->increments('id');
            $table->date('from');
            $table->date('to');
            $table->integer('price');

            $table->timestamps();
        });

        // Create table for building prices
        Schema::create('building_price_building', function (Blueprint $table) {
            $table->integer('building_id')->unsigned();
            $table->integer('building_price_id')->unsigned();

            $table->foreign('building_id')->references('id')->on('buildings')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('building_price_id')->references('id')->on('building_prices')
                ->onUpdate('cascade')->onDelete('cascade');

            $table->primary(['building_id', 'building_price_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //buildings
        Schema::dropIfExists('building_amenity');
        //buildings types
        Schema::dropIfExists('building_type_translations');
        Schema::dropIfExists('building_types');
        //building_prices
        Schema::dropIfExists('building_price_building');
        Schema::dropIfExists('building_prices');
        //building options
        Schema::dropIfExists('building_option_translations');
        Schema::dropIfExists('building_options');
        //buildings;
        Schema::dropIfExists('building_counts');
        Schema::dropIfExists('building_translations');
        Schema::dropIfExists('buildings');
    }
}
