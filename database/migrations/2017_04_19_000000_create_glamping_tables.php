<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGlampingTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //create table for glamping groups
        Schema::create('glamping_groups', function (Blueprint $table) {
            $table->increments('id');
            $table->text('icon')->nullable();
            $table->timestamps();
        });

        //create table for glamping group translations
        Schema::create('glamping_group_translations', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('glamping_group_id')->unsigned();
            $table->string('name');
            $table->mediumText('description');
            $table->string('locale')->index();

            $table->unique(['glamping_group_id','locale']);
            $table->foreign('glamping_group_id')->references('id')->on('glamping_groups')->onDelete('cascade');
        });

        //create table for impressions
        Schema::create('impressions', function (Blueprint $table) {
            $table->increments('id');
            $table->text('icon')->nullable();
            $table->timestamps();
        });

        //create table for impression translations
        Schema::create('impression_translations', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('impression_id')->unsigned();
            $table->string('name');
            $table->mediumText('description');
            $table->string('locale')->index();

            $table->unique(['impression_id','locale']);
            $table->foreign('impression_id')->references('id')->on('impressions')->onDelete('cascade');
        });

        //create table for amenities
        Schema::create('amenities', function (Blueprint $table) {
            $table->increments('id');
            $table->text('icon');
            $table->timestamps();
        });

        //create table for amenity translations
        Schema::create('amenity_translations', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('amenity_id')->unsigned();
            $table->string('name');
            $table->mediumText('description');
            $table->string('locale')->index();

            $table->unique(['amenity_id','locale']);
            $table->foreign('amenity_id')->references('id')->on('amenities')->onDelete('cascade');
        });

        //create table for services
        Schema::create('services', function (Blueprint $table) {
            $table->increments('id');
            $table->text('icon');
            $table->timestamps();
        });

        //create table for service translations
        Schema::create('service_translations', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('service_id')->unsigned();
            $table->string('name');
            $table->mediumText('description');
            $table->string('locale')->index();

            $table->unique(['service_id','locale']);
            $table->foreign('service_id')->references('id')->on('services')->onDelete('cascade');
        });

        //create table for glampings
        Schema::create('glampings', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('glamping_id')->nullable()->unsigned();
            $table->string('latitude');
            $table->string('longitude');
            $table->integer('max_child_age');
            $table->integer('commission')->default(12);
            $table->boolean('active')->default(false);
            $table->boolean('accepted')->default(false);
            $table->string('arrival_time')->nullable();
            $table->string('check_out_time')->nullable();
            $table->integer('free_return');
            $table->timestamps();

            $table->foreign('glamping_id')->references('id')->on('glampings');
        });

        //create table for glamping translations
        Schema::create('glamping_translations', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('glamping_id')->unsigned();
            $table->string('name')->unique();
            $table->mediumText('description');
            $table->string('country');
            $table->string('region');
            $table->text('glamping_address')->nullable();
            $table->mediumText('other_information')->nullable();
            $table->string('locale')->index();

            $table->unique(['glamping_id','locale']);
            $table->foreign('glamping_id')->references('id')->on('glampings')->onDelete('cascade');
        });

        // Create table for associating glampings to services (Many-to-Many)
        Schema::create('glamping_service', function (Blueprint $table) {
            $table->integer('glamping_id')->unsigned();
            $table->integer('service_id')->unsigned();

            $table->foreign('glamping_id')->references('id')->on('glampings')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('service_id')->references('id')->on('services')
                ->onUpdate('cascade')->onDelete('cascade');

            $table->primary(['glamping_id', 'service_id']);
        });

        // Create table for associating glampings to glamping_groups (Many-to-Many)
        Schema::create('glamping_glamping_group', function (Blueprint $table) {
            $table->integer('glamping_id')->unsigned();
            $table->integer('glamping_group_id')->unsigned();

            $table->foreign('glamping_id')->references('id')->on('glampings')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('glamping_group_id')->references('id')->on('glamping_groups')
                ->onUpdate('cascade')->onDelete('cascade');

            $table->primary(['glamping_id', 'glamping_group_id']);
        });

        // Create table for associating glampings to amenities (Many-to-Many)
        Schema::create('glamping_amenity', function (Blueprint $table) {
            $table->integer('glamping_id')->unsigned();
            $table->integer('amenity_id')->unsigned();

            $table->foreign('glamping_id')->references('id')->on('glampings')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('amenity_id')->references('id')->on('amenities')
                ->onUpdate('cascade')->onDelete('cascade');

            $table->primary(['glamping_id', 'amenity_id']);
        });

        // Create table for associating glampings to impressions (Many-to-Many)
        Schema::create('glamping_impression', function (Blueprint $table) {
            $table->integer('glamping_id')->unsigned();
            $table->integer('impression_id')->unsigned();

            $table->foreign('glamping_id')->references('id')->on('glampings')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('impression_id')->references('id')->on('impressions')
                ->onUpdate('cascade')->onDelete('cascade');

            $table->primary(['glamping_id', 'impression_id']);
        });

         // Create table for associating glampings to managers (Many-to-Many)
        Schema::create('glamping_manager', function (Blueprint $table) {
            $table->integer('glamping_id')->unsigned();
            $table->integer('manager_id')->unsigned();

            $table->foreign('glamping_id')->references('id')->on('glampings')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('manager_id')->references('id')->on('managers')
                ->onUpdate('cascade')->onDelete('cascade');

            $table->primary(['glamping_id', 'manager_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //services
        Schema::dropIfExists('service_translations');
        Schema::dropIfExists('glamping_service');
        Schema::dropIfExists('services');
        //glamping groups
        Schema::dropIfExists('glamping_group_translations');
        Schema::dropIfExists('glamping_glamping_group');
        Schema::dropIfExists('glamping_groups');
        //amenities
        Schema::dropIfExists('amenity_translations');
        Schema::dropIfExists('glamping_amenity');
        Schema::dropIfExists('amenities');
        //impressions
        Schema::dropIfExists('impression_translations');
        Schema::dropIfExists('glamping_impression');
        Schema::dropIfExists('impressions');
        //glampings
        Schema::dropIfExists('glamping_translations');
        Schema::dropIfExists('glampings');
    }
}