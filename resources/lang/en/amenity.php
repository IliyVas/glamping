<?php

return [

    //grid
    'createButton' => 'Create new amenity',
    'gridName' => 'Amenities grid',

    //form
    'formName' => 'Amenities form',

];