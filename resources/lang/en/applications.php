<?php

return [

    //grid
    'gridName' => 'Applications grid',
    'glamping_create' => 'Application for the creation of glamping',
    'glamping_update' => 'Application for the change of glamping',
    'building' => 'Application for the creation/change of building',
    'owner_register' => 'Application for the registration as owner',
    'owner_edit_profile' => 'Application for the change the profile data',

    //types
    'glamping_create_type' => 'Glamping creation',
    'glamping_update_type' => 'Glamping change',
    'building_type' => 'Building creation/change',
    'registration' => 'Owner registration',
    'edit_profile' => 'Owner profile change',

    //status
    'pending' => 'Pending',
    'rejected' => 'Rejected',
    'accepted' => 'Accepted',

    //btn
    'publish' => 'Publish',
    'reject' => 'Reject',
    'accept' => 'Accept',

    'answer' => 'Answer'
];