<?php

return [
    //booking confirmation
    'bookingConfirmation' => 'Booking confirmation',
    'welcome' => 'Welcome',
    'welcomeMessage' => 'Thank you for trusting our service. Do not forget to leave comment about the visited glamping on our website.
        Glamping will automatically send you a confirmation as soon as everything is ready. Have a nice holiday!',

    'yourBooking' => 'Your booking',
    'bookNumber' => 'Booking number',
    'bookDate' => 'Booking date',
    'bookFullName' => 'Full name',
    'bookEmail' => 'Email',
    'bookPhoneNumber' => 'Phone number',
    'bookType' => 'Booking type',
    'bookPayMethod' => 'Payment method',

    'selectedGlamping' => 'Selected glamping',
    'timeIn' => 'Arrival time',

    'buildingsInfo' => 'Buildings info',
    'buildingNumber' => 'Building number',
    'buildingGuests' => 'Number of guests',
    'buildingChildren' => 'Number of children',
    'arrival' => 'Arrival',
    'departure' => 'Departure',
    'buildingPrice' => 'Price',

    'taxesIn' => 'Taxes includes',
    'amenities' => 'Amenities',
    'services' => 'Services',
    'impressions' => 'Impressions',

    'finalMessage' => 'Glampster - aggregator of glampings. Available for communication 24 hours a day. 
        Phone: +7 (800) 888-22-11, e-mail: info@glampster.ru. Central office: Moscow, Russia. 
        Terms of use of the service are available on the site.',

    'bookTypeGuarantee' => 'Guaranteed reservation',
    'payCard' => 'Payment by card',
];