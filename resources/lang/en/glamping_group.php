<?php

return [

    //grid
    'createButton' => 'Create new glamping group',
    'gridName' => 'Glamping groups grid',

    //form
    'formName' => 'Glamping groups form',

];