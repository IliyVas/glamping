<?php

return [

    //grid
    'createButton' => 'Create new service',
    'gridName' => 'Services grid',

    //form
    'formName' => 'Services form',

];