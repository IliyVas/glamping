<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    //errors
    'failed' => 'These credentials do not match our records.',
    'failedPassword' => 'Password do not match.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',
    'already_account' => 'You already have an account.',

    //messages
    'editSuccess' => 'Your profile updated successfully.',
    'passwordChangeSuccess' => 'Password changed successfully.',
    'emailChangedSuccess' => 'You need to go to your mail :email and activate it.',
    'emailChanged' => 'Your mail updated successfully. Your new mail - :email',
    'emailVerified' => 'Your mail :email verified successfully.',
    'verifyTitle' => 'E-mail confirmation',
    'verifyMessage' => 'Click on the link to confirm your email address',
    'sendVerifyMessage' => 'A letter with activation has been sent to you by mail',
    'captcha' => 'Enter the code from the image',
    'resubmit' => 'Resubmit.',
    'nonActiveTitle' => 'You did not activate your email address',
    'nonAcceptedTitle' => 'Your application for registration/change profile has not been considered yet.',

    //blocks
    'changePassword' => 'Change password',
    'changeEmail' => 'Change email',
    'personalData' => 'Personal data',
    'companyData' => 'Company data',

    //forms
    'first_name' => 'First name',
    'name' => 'Name',
    'last_name' => 'Last name',
    'email' => 'E-Mail Address',
    'new_email' => 'New e-mail Address',
    'password' => 'Password',
    'confirm_password' => 'Password Confirmation',
    'new_password' => 'New password',
    'confirm_new_password' => 'New password confirmation',
    'phone' => 'Phone Number',
    'glamping_description' => 'Glamping description',
    'company' => 'Company',
    'birth_date' => 'Date of Birth',
    'patronymic' => 'Patronymic',

    //btn
    'btn_signin' => 'SIGN IN',
    'btn_signup' => 'SIGN UP',
    'owner_btn' => 'Submit',
    'logout' => 'Logout',

    //auth page
    'welcome' => 'Welcome',
    'sign_in' => 'Sign in to start your session',
    'auth_text' => 'It\'s login page. You can either sing in or go to glamping list page',
    'remember' => 'Remember Me',
    'forgot_pass' => 'Forgot Password?',
    'have_account' => 'You already have a membership?',
    'not necessary' => 'Not necessary',
];
