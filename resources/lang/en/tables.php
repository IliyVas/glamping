<?php

return [

    'glampingsListId' => 'ID',
    'glampingsListCaption' => 'Name',
    'glampingsListPlace' => 'Place',
    'glampingsListConfirmed' => 'Confirmed',
    'glampingsListGlampingsSum' => 'At all',
    'glampingsListAddBuilding' => 'Add building',
    'freePlaces' => 'Free places',
    'registrationDate' => 'Date of registration',
    'rating' => 'Rating',
    'receipts' => 'Receipts',
    'bookingCount' => 'Number of Booking',
    'canceledBookingCount' => 'Number of canceled Booking',
    'clientsCount' => 'Number of clients',
    'bookingTime' => 'Time from order to arrival',
    'clients' => 'Clients'

];