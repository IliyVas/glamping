<?php

return [

    //grid
    'createButton' => 'Create new impression',
    'gridName' => 'Impressions grid',

    //form
    'formName' => 'Impressions form'

];