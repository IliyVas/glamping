<?php

return [

    'welcome' => 'Hello',

    //building type form messages
    'createBuildingTypeSuccessMessage' => 'Building type created successfully.',
    'updateBuildingTypeSuccessMessage' => 'Building type updated successfully.',
    'deleteBuildingTypeSuccessMessage' => 'Building type deleted successfully.',
    'notExistBuildingTypeError' => 'Building type does not exist.',
    'noImagesFound' => 'No images were found.',

    //common form messages
    'dangerMessage' => 'Error! Try again.',

    //empty grid text
    'emptyGridText' => 'Currently the table is empty.',

    //glamping group form messages
    'createGlampingGroupSuccessMessage' => 'Glamping group created successfully.',
    'updateGlampingGroupSuccessMessage' => 'Glamping group updated successfully.',
    'deleteGlampingGroupSuccessMessage' => 'Glamping group deleted successfully.',
    'notExistGlampingGroupError' => 'Glamping group does not exist.',

    //impression form messages
    'createImpressionSuccessMessage' => 'Impression created successfully.',
    'updateImpressionSuccessMessage' => 'Impression updated successfully.',
    'deleteImpressionSuccessMessage' => 'Impression deleted successfully.',
    'notExistImpressionError' => 'Impression does not exist.',

    //amenities form messages
    'createAmenitySuccessMessage' => 'Amenity created successfully.',
    'updateAmenitySuccessMessage' => 'Amenity updated successfully.',
    'deleteAmenitySuccessMessage' => 'Amenity deleted successfully.',
    'notExistAmenityError' => 'Amenity does not exist.',

    //services form messages
    'createServiceSuccessMessage' => 'Service created successfully.',
    'updateServiceSuccessMessage' => 'Service updated successfully.',
    'deleteServiceSuccessMessage' => 'Service deleted successfully.',
    'notExistServiceError' => 'Service does not exist.',

    //glampings form messages
    'createGlampingSuccessMessage' => 'Glamping created successfully.',
    'updateGlampingSuccessMessage' => 'Glamping updated successfully.',
    'updateGlampingAppSuccessMessage' => 'Application for the create/change glamping updated successfully.',
    'updateChangedGlampingError' => 'You can not edit a previously modified glamping.',
    'notExistGlampingError' => 'Glamping does not exist.',
    'noRightsToThisGlamping' => 'You haven\'t enough rights to edit this glamping.',
    'delImgError' => 'You can not delete all the images of the glamping and do not choose a new one.',
    'maxImgCountError' => 'You can not upload images more than ',
    'emptyBuildingList' => 'This glamping has no buildings yet.',
    'noRightsToEditThisGlamping' => 'You can not edit glamping before considering the last application for creating/editing the glamping.',
    'noRightsToEditThisBuilding' => 'You can not edit building before considering the last application for creating/editing the building.',

    //building form messages
    'createBuildingSuccessMessage' => 'Building created successfully.',
    'changeBuildingSuccessMessage' => 'Application for the changing of the building was sent successfully.',
    'editBuildingSuccessMessage' => 'Building was successfully edited.',
    'notExistBuilding' => 'This building doesn\'t exist.',
    'wrongGlampingOfBuilding' => 'These building and glamping don\'t connected.',

    //price to dates
    'priceDatesImposed' => 'Your dates are imposed by each other.',
    'messages.priceDatesBroken' => '\'From\' can\'t be earlier than \'to\'.',
    'priceDatesWrongPrice' => 'Price should be more than zero.',

    //auth forms
    'registerOwnerSuccessMessage' => 'Application for the adding to the system was sent successfully.',
    'registerUserSuccessMessage' => 'User was registered successfully.',
    'loginNoAccessMessage' => 'No access! Your application for the adding to the system is not yet verified.',
    //'loginNoAccessRejectMessage' => 'No access! Your application for the adding to the system was rejected.',
        
    //applications
    'notExistApplicationError' => 'Application №:id not found. Close this page.',
    'rejectApplicationMessage' => 'Application rejected.',
    'approveApplicationMessage' => 'Application approved.',
    'emptyDenyMessage' => 'Deny message is required.',
    'unknownApplicationStatus' => 'Unknown application status.',

    //bookings
    'cancelBookingSuccessMessage' => 'Booking canceled successfully.',
    'confirmBookingStatusSuccessMessage' => 'confirmed successfully.',
    'noSelectedBuildings' => 'Please, select some buildings.',
    'notExistBookingError' => 'Booking does not exist.',
    'noBookings' => 'You do not have bookings.',
    'createBookingSuccessMessage' => 'Booking created successfully.',
    'updateBookingSuccessMessage' => 'Booking updated successfully.',
    'noGlampingList' => 'You do not have a single glamping, please create it first.',
    'notEnoughRightsForBooking' => 'You do not have enough rights for edit this booking.',
    'bookingNotActive' => 'This booking is not active.',
    'confirmCancelBooking' => 'You try to cancel booking too late. We will charge you a commission in one day. 
        Do you agree?',

    //user account
    'userAccountMessage' => 'This is main page of your account, ' .
        'here you can find all the information about your services.',

    //public glamping list
    'notExistPublishedGlampings' => "Unfortunately, at the moment we don't have any published glamping.",
    'nothingFoundFilterResult' => 'Nothing found.',
    'sendSuccessMessage' => 'Message sent successfully.',

    //grid
    'feedbackMessagesGrid' => 'Feedback messages grid',
    'message' => 'Message',

    //Answer
    'ur_answer' => 'Ваш ответ',
    'btn_answer' => 'Ответить'
];
