<?php

return [

    //grid
    'createButton' => 'Create new building type',
    'gridName' => 'Building types grid',

    //form
    'formName' => 'Building types form'

];