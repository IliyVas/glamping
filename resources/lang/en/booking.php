<?php

return [

    //grid
    'createButton' => 'Add booking',
    'gridName' => 'Bookings grid',
    'building' => 'Building',

    //booking status
    'confirmedStatus' => 'Confirmed',
    'notConfirmedStatus' => 'Not confirmed',
    'canceledStatus' => 'Canceled',

    //form
    'createFormName' => 'Add booking',
    'editFormName' => 'Edit booking',
    'maxChildAgeInf' => 'The age at which the minor takes a separate place',

    'allBookings' => 'Total bookings'

];