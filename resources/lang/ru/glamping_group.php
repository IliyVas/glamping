<?php

return [

    //grid
    'createButton' => 'Создать новую группу глэмпингов',
    'gridName' => 'Таблица групп глэмпингов',

    //form
    'formName' => 'Форма создания/редактирования групп глэмпингов',

];