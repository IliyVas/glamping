<?php

return [

    'glampingsListId' => 'ID',
    'glampingsListCaption' => 'Название',
    'glampingsListPlace' => 'Место',
    'glampingsListConfirmed' => 'Подтвержден',
    'glampingsListGlampingsSum' => 'Всего',
    'glampingsListAddBuilding' => 'Добавиь помещение',
    'freePlaces' => 'Свободные места',
    'registrationDate' => 'Дата регистрации',
    'rating' => 'Рейтинг',
    'receipts' => 'Выручка',
    'bookingCount' => 'Количество броней',
    'canceledBookingCount' => 'Количество отмененных броней',
    'clientsCount' => 'Количество клиентов',
    'bookingTime' => 'Время от заказа до заезда',
    'clients' => 'Клиенты'

];