<?php

return [

    //grid
    'gridName' => 'Таблица заявок',
    'glamping_create' => 'Заявка на создание глэмпинга',
    'glamping_update' => 'Заявка на изменение глэмпинга',
    'building' => 'Заявка на создание/изменение помещения глэмпинга',
    'owner_register' => 'Заявка на регистрацию в качестве владельца',
    'owner_edit_profile' => 'Заявка на редактирование профиля',

    //types
    'glamping_create_type' => 'Создание глэмпинга',
    'glamping_update_type' => 'Изменение глэмпинга',
    'building_type' => 'Сзодание/изменение помещения',
    'registration' => 'Регистрация владельца',
    'edit_profile' => 'Изменение профиля',

    //status
    'pending' => 'На рассмотрении',
    'rejected' => 'Отклонена',
    'accepted' => 'Одобрена',

    //btn
    'publish' => 'Опубликовать',
    'reject' => 'Отклонить',
    'accept' => 'Одобрить',

    'answer' => 'Ответ'
];