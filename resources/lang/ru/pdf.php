<?php

return [
    //booking confirmation
    'bookingConfirmation' => 'Подвтерждение бронирования',
    'welcome' => 'Здравствуйте',
    'welcomeMessage' => 'Спасибо за доверие к нашему сервису. Не забудьте оставить комментарии о посещенном глэмпинге на сайте.
        Глэмпинг автоматически отправит вам подтверждение, как только всё будет готово. Хорошего вам отдыха!',

    'yourBooking' => 'Ваше бронирование',
    'bookNumber' => 'Номер брони',
    'bookDate' => 'Дата бронирования',
    'bookFullName' => 'ФИО',
    'bookEmail' => 'Email',
    'bookPhoneNumber' => 'Номер телефона',
    'bookType' => 'Тип бронирования',
    'bookPayMethod' => 'Метод оплаты',

    'selectedGlamping' => 'Выбранный глэмпинг',
    'timeIn' => 'Время прибытия',

    'buildingsInfo' => 'Информация о помещениях',
    'buildingNumber' => 'Номер помещения',
    'buildingGuests' => 'Количество гостей',
    'buildingChildren' => 'Количество детей',
    'arrival' => 'Прибытие',
    'departure' => 'Отъезд',
    'buildingPrice' => 'Стоимость',

    'taxesIn' => 'НДС включён в стоимость',
    'amenities' => 'Удобства',
    'services' => 'Услуги',
    'impressions' => 'Впечатления',

    'finalMessage' => 'Glampster - агрегатор глэмпингов. Доступен для связи 24 часа в сутки. 
        Телефон: +7(800)888-22-11, e-mail: info@glampster.ru. Центральный офис: Москва, Россия. 
        Условия использования сервиса доступны на сайте.',

    'bookTypeGuarantee' => 'Гарантированное бронирование',
    'payCard' => 'Оплата картой',
];