<?php

return [

    'welcome' => 'Привет',

    //building type form messages
    'createBuildingTypeSuccessMessage' => 'Тип помещения создан успешно.',
    'updateBuildingTypeSuccessMessage' => 'Тип помещения изменен успешно.',
    'deleteBuildingTypeSuccessMessage' => 'Тип помещения удалён успешно.',
    'notExistBuildingTypeError' => 'Данного типа помещения не существет.',
    'noImagesFound' => 'Не обнаружены картинки.',

    //common form messages
    'dangerMessage' => 'Ошибка. Попробуйте повторить попытку.',

    //empty grid text
    'emptyGridText' => 'В настоящий момент в таблице нет ни одной записи.',

    //glamping group form messages
    'createGlampingGroupSuccessMessage' => 'Группа глэмпингов создана успешно.',
    'updateGlampingGroupSuccessMessage' => 'Группа глэмпингов изменена успешно.',
    'deleteGlampingGroupSuccessMessage' => 'Группа глэмпингов удалена успешно.',
    'notExistGlampingGroupError' => 'Данной группы глэмпингов не существует.',

    //impression form messages
    'createImpressionSuccessMessage' => 'Впечатление создано успешно.',
    'updateImpressionSuccessMessage' => 'Впечатление изменено успешно.',
    'deleteImpressionSuccessMessage' => 'Впечатление удалено успешно.',
    'notExistImpressionError' => 'Данного впечатления не существует.',

    //amenities form messages
    'createAmenitySuccessMessage' => 'Удобство создано успешно.',
    'updateAmenitySuccessMessage' => 'Удобство изменено успешно.',
    'deleteAmenitySuccessMessage' => 'Удобство удалено успешно.',
    'notExistAmenityError' => 'Данного удобства не существует.',

    //services form messages
    'createServiceSuccessMessage' => 'Услуга создана успешно.',
    'updateServiceSuccessMessage' => 'Услуга изменена успешно.',
    'deleteServiceSuccessMessage' => 'Услуга удалена успешно.',
    'notExistServiceError' => 'Данная услуга не существует.',

    //glampings form messages
    'createGlampingSuccessMessage' => 'Глэмпинг создан успешно.',
    'updateGlampingSuccessMessage' => 'Глэмпинг изменен успешно.',
    'updateGlampingAppSuccessMessage' => 'Заявка на создание/изменение глэмпинга изменена успешно.',
    'updateChangedGlampingError' => 'Вы не можете редактировать ранее измененный глэмпинг.',
    'notExistGlampingError' => 'Данного глэмпинга не существует.',
    'noRightsToThisGlamping' => 'У вас нет прав для доступа к этому глэмпингу.',
    'delImgError' => 'Вы не можете удалить все изображения глэмпинга и не выбрать новое.',
    'maxImgCountError' => 'Вы не можете загрузить изображений больше ',
    'emptyBuildingList' => 'У данного глэмпинга пока нет помещений.',
    'noRightsToEditThisGlamping' => 'Вы не можете редактировать глэмпинг до рассмотрения последней заявки на создание/редактирование глэмпинга.',
    'noRightsToEditThisBuilding' => 'Вы не можете редактировать помещение до рассмотрения последней заявки на создание/редактирование помещения.',

    //building form messages
    'createBuildingSuccessMessage' => 'Помещение создано успешно.',
    'changeBuildingSuccessMessage' => 'Заявка на изменение помещения подана успешно.',
    'editBuildingSuccessMessage' => 'Помещение успешно отредактировано.',
    'notExistBuilding' => 'Такого помещения не существует.',
    'wrongGlampingOfBuilding' => 'Помещение не соответствует глэмпингу.',

    //price to dates
    'priceDatesImposed' => 'Ваши даты накладываются друг на друга.',
    'messages.priceDatesBroken' => '\'До\' не может быть раньше чем \'от\'.',
    'priceDatesWrongPrice' => 'Цена должна быть больше 0.',

    //auth forms
    'registerOwnerSuccessMessage' => 'Заявка на добавление в систему отправлена успешно.',
    'registerUserSuccessMessage' => 'Пользователь успешно зарегистрирован.',
    'loginNoAccessMessage' => 'Нет доступа! Ваша заявка на добавление в систему ещё не проверена.',
    //'loginNoAccessRejectMessage' => 'Нет доступа! Ваша заявка на добавление систему была отклонена.',

    //applications
    'notExistApplicationError' => 'Данной заявки не существует.',
    'rejectApplicationMessage' => 'Заявка отклонена.',
    'approveApplicationMessage' => 'Заявка одобрена.',
    'emptyDenyMessage' => 'Необходимо указать причину отклонения заявки.',
    'unknownApplicationStatus' => 'Неизвестный статус заявки.',

    //bookings
    'cancelBookingSuccessMessage' => 'Бронирование отменено успешно.',
    'confirmBookingStatusSuccessMessage' => 'подтвержден успешно.',
    'noSelectedBuildings' => 'Пожалуйста, выберите помещения.',
    'notExistBookingError' => 'Данного бронирование не существует.',
    'noBookings' => 'У Вас пока нет бронирований.',
    'createBookingSuccessMessage' => 'Бронирование успешно оформлено.',
    'updateBookingSuccessMessage' => 'Бронирование изменено успешно.',
    'noGlampingList' => 'У Вас нет ни одного глэмпинга, пожалуйста, создайте сначала глэмпинг.',
    'notEnoughRightsForBooking' => 'У вас недостаточно прав для редактирования этого бронирования.',
    'bookingNotActive' => 'Это бронирование не активно.',
    'confirmCancelBooking' => 'Так как срок бесплатной отмены прошёл, с вас будет списана комиссия за одни сутки. 
        Вы согласны отменить бронь?',

    //user account
    'userAccountMessage' => 'Это главная страница вашего аккаунта, ' .
    'здесь вы можете узнать всю информацию о ваших услугах.',

    //public glamping list
    'notExistPublishedGlampings' => "К сожалению, в настоящий момент у нас нет опубликованных глэмпингов.",
    'nothingFoundFilterResult' => 'Ничего не найдено.',
    'sendSuccessMessage' => 'Сообщение отправлено успешно.',

    //grid
    'feedbackMessagesGrid' => 'Таблица сообщений с формы обратной связи',
    'message' => 'Сообщение',


    //Answer
    'ur_answer' => 'Ваш ответ',
    'btn_answer' => 'Ответить'

];
