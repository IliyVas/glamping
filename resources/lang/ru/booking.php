<?php

return [

    //grid
    'createButton' => 'Добавить бронирование',
    'gridName' => 'Таблица бронирований',
    'building' => 'Помещение',

    //booking status
    'confirmedStatus' => 'Подтверждено',
    'notConfirmedStatus' => 'Не подтверждено',
    'canceledStatus' => 'Отменено',

    //form
    'createFormName' => 'Добавить бронирование',
    'editFormName' => 'Редактировать бронирование',
    'maxChildAgeInf' => 'Возраст, по достижению которого несовершеннолетний занимает отдельное место',

    'allBookings' => 'Всего закакзов'

];