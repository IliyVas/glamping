<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    //errors
    'failed' => 'Введеные данные не соответствуют существующим записям.',
    'failedPassword' => 'Пароль введень неверно.',
    'throttle' => 'Слишком много попыток авторизации. Повторите попытку через :seconds секунд.',
    'already_account' => 'У вас уже есть аккаунт.',

    //messages
    'editSuccess' => 'Ваш профиль обновлен успешно.',
    'passwordChangeSuccess' => 'Пароль изменен успешно.',
    'emailChangedSuccess' => 'Вам необходимо перейти на свою почту :email и активировать её.',
    'emailChanged' => 'Адрес Вашей электронной почты  изменен успешно.Ваш новый адрес - :email',
    'emailVerified' => 'Адрес Вашей электронной почты :email подтвержден успешно.',
    'verifyTitle' => 'Подтверждение адреса электронной почты',
    'verifyMessage' => 'Нажмите на ссылку для подтверждения Вашего адреса электронной почты',
    'sendVerifyMessage' => 'Письмо с активацией выслано Вам на почту.',
    'captcha' => 'Введите код с картинки',
    'resubmit' => 'Отправить ещё раз.',
    'nonActiveTitle' => 'Вы не активировали свой email адресс',
    'nonAcceptedTitle' => 'Ваша заявка на регистрацию/изменение профиля ещё не рассмотрена.',

    //blocks
    'changePassword' => 'Сменить пароль',
    'changeEmail' => 'Сменить e-mail',
    'personalData' => 'Личные данные',
    'companyData' => 'Данные о компании',

    //glamping owner
    'first_name' => 'Имя',
    'last_name' => 'Фамилия',
    'name' => 'Имя',
    'email' => 'Адрес электронной почты',
    'new_email' => 'Новый адрес электронной почты',
    'password' => 'Пароль',
    'confirm_password' => 'Подтверждение пароля',
    'new_password' => 'Новый пароль',
    'confirm_new_password' => 'Подтверждение нового пароля',
    'phone' => 'Номер телефона',
    'glamping_description' => 'Описание глэмпинга',
    'company' => 'Компания',
    'owner_btn' => 'Отправить',
    'birth_date' => 'День Рождения',
    'patronymic' => 'Отчество',

    //btn
    'btn_signin' => 'Войти',
    'btn_signup' => 'Создать',
    'logout' => 'Выйти',

    //auth page
    'welcome' => 'Добро пожаловать',
    'sign_in' => 'Выполните вход, чтобы начать работу',
    'auth_text' => 'Это страница входа, вы можете выполнить вход или перейти на страницу со списком глэмпингов',
    'remember' => 'Запомнить меня',
    'forgot_pass' => 'Забыли пароль?',
    'have_account' => 'У вас уже есть аккаунт?',
    'not necessary' => 'Не обязательно',
];
