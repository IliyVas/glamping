@extends('layouts.app')

@section ('title'){{ trans('common_elements.glampings') }}@endsection

@section('content')
    <div class="row public-page-container">
        {{--Glamping filter block--}}
        <div class="col-xs-12 col-lg-3 filter-block">
            <form onsubmit="sendAjaxFilter(event, this)" method="post" action="">
                {{-- name block --}}
                <div class="form-group">
                    <h4>{{ trans('common_elements.nameInputLabel') }}</h4>
                    <input class="form-control" id="name" name="name" type="text" autofocus value="{{ $name or null }}">
                </div>

                {{-- country block --}}
                <div class="form-group">
                    <h4>{{ trans('common_elements.countryInputLabel') }}:</h4>
                    <input id="country" class="form-control" name="country" type="text" value="{{ $country or null }}">
                </div>

                {{-- region block --}}
                <div class="form-group">
                    <h4>{{ trans('common_elements.regionInputLabel') }}</h4>
                    <input id="region" class="form-control" name="region" type="text" value="{{ $region or null }}">
                </div>

                {{-- arrival date block --}}
                <div class="form-group">
                    <h4>{{ trans('common_elements.bookDateIn') }}:</h4>
                    <input id="arrival_date" min="{{ date('Y-m-d', time()) }}"
                           class="form-control" name="from_date" type="date"
                           value="{{ $fromDate or null }}">
                </div>

                {{-- county date block --}}
                <div class="form-group">
                    <h4>{{ trans('common_elements.bookDateOut') }}:</h4>
                    <input min="{{ date('Y-m-d', time() + (24*60*60)) }}"
                           class="form-control" id="county_date" name="to_date"
                           type="date" value="{{ $toDate or null }}">
                </div>

                {{-- price block --}}
                <div class="row form-group">
                    <div class="col-xs-7">
                        <h4>{{ trans('common_elements.bookTablePrice') }}:</h4>
                    </div>
                    <div class="col-xs-5">
                        <input class="form-control" name="price" type="number" min="0" value="{{ old('price') }}">
                    </div>
                </div>

                {{-- adults, children and buildings count block --}}
                {{--Adults--}}
                <div class="row form-group">
                    <div class="col-xs-7">
                        <h4>{{ trans('common_elements.bookAdultsCount') }}:</h4>
                    </div>
                    <div class="col-xs-5">
                        <input class="form-control" name="adults" type="number" value="{{ $adults }}"
                               min="0">
                    </div>
                </div>
                {{--Children--}}
                <div class="row form-group">
                    <div class="col-xs-7">
                        <h4>{{ trans('common_elements.bookChildrenCount') }}:</h4>
                    </div>
                    <div class="col-xs-5">
                        <input class="form-control" name="children" type="number" value="{{ old('children') }}"
                               min="0">
                    </div>
                </div>
                {{--Buildings--}}
                <div class="row form-group">
                    <div class="col-xs-7">
                        <h4>{{ trans('common_elements.buildings') }}:</h4>
                    </div>
                    <div class="col-xs-5">
                        <input class="form-control" name="buildings" type="number" value="{{ old('buildings') }}"
                               min="0">
                    </div>
                </div>

                {{--Building types--}}
                @if (isset($buildingTypes) && count($buildingTypes) > 0)
                    <div class="form-group">
                        <h4 class="page-header">{{ trans('common_elements.buildingTypes') }}:</h4>
                        @foreach ($buildingTypes as $buildingType)
                            @if (isset($_GET['building_types']) && !empty($_GET['building_types']) &&
                             in_array($buildingType->id,$_GET['building_types'])) @php $check = 'checked'; @endphp
                            @else @php $check = ''; @endphp @endif
                            <input {{ $check }} type="checkbox" name="building_types[]" value="{{$buildingType->id }}">
                            <span>{{ $buildingType->name }}</span><Br>
                        @endforeach
                    </div>
                @endif

                {{--Impressions--}}
                @if (isset($impressions) && count($impressions) > 0)
                    <div class="form-group">
                        <h4 class="page-header">{{ trans('common_elements.impressions') }}:</h4>
                        @foreach ($impressions as $impression)
                            @if (isset($_GET['impressions']) && !empty($_GET['impressions']) &&
                             $_GET['impressions'] == $impression->id) @php $check = 'checked'; @endphp
                            @else @php $check = ''; @endphp @endif
                            <input {{ $check }} type="checkbox" name="impressions[]" value="{{ $impression->id }}">
                            <span>{{ $impression->name }}</span><Br>
                        @endforeach
                    </div>
                @endif

                {{--Glamping groups--}}
                @if (isset($glampingGroups) && count($glampingGroups) > 0)
                    <div class="form-group">
                        <h4 class="page-header">{{ trans('common_elements.groups') }}:</h4>
                        @foreach ($glampingGroups as $glampingGroup)
                            @if (isset($_GET['glamping_groups']) && !empty($_GET['glamping_groups']) &&
                             $_GET['glamping_groups'] == $glampingGroup->id) @php $check = 'checked'; @endphp
                            @else @php $check = ''; @endphp @endif
                            <input {{ $check }} type="checkbox" name="glamping_groups[]" value="{{$glampingGroup->id }}">
                            <span>{{ $glampingGroup->name }}</span><Br>
                        @endforeach
                    </div>
                @endif

                {{-- buttons block --}}
                <div class="form-group pull-right">
                    <input class="btn btn-primary" type="submit" name="submit" value="{{ trans('common_elements.btnApply') }}">
                    <a href="{{ route('filterPage') }}" class="btn btn-danger">{{ trans('common_elements.btnReset') }}</a>
                </div>
            </form>
        </div>

        {{--Glamping data block--}}
        <div class="col-xs-12 col-lg-9 pull-right">
            <div class="row" id="ajax-content">
                @include('templates.glampingsData')
            </div>
        </div>
    </div>

    {{--Scripts--}}
    <script type="text/javascript">
        //autocomplete filter by name input
        $('#name').autocomplete({
            source: function  (request, response) {
                autocompleteFilterInputs(request, response, ['name']);
            }
        });

        //autocomplete filter by country input
        $('#country').autocomplete({
            source: function  (request, response) {
                autocompleteFilterInputs(request, response, ['country']);
            }
        });

        //autocomplete filter by region input
        $('#region').autocomplete({
            source: function  (request, response) {
                autocompleteFilterInputs(request, response, ['region']);
            }
        });
    </script>
@endsection
