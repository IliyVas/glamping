<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="UTF-8">
    <title>@yield('title')</title>

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Styles -->
    <link rel="stylesheet" type="text/css" href="{{ mix('css/app.css') }}">
    <link type="text/css" href="{{ mix('css/all.css') }}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ mix('css/booking.css')}}">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

    <!-- Scripts -->
    <script src="{{ mix('js/jQ.js') }}" type="text/javascript"></script>
    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>
</head>
<body>
@include('templates.messages_block')
@yield('content')

<div class="subscribe">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                <h1 class="h1-subscribe text-center">{{ trans('common_elements.subscriptionBlockTitle') }}</h1>
                <h3 class="h3-subscribe text-center">{{ trans('common_elements.subscriptionBlockDescription') }}</h3>
                <form action="" class="subscribe-form transparency">
                    <input type="text" name="mail" class="subscribe-mail transparency"
                           placeholder="{{ trans('common_elements.subscriptionPlaceHolder') }}">
                    <input type="submit" name="submit" value="{{ trans('common_elements.btnSubscription') }}"
                           class="subscribe-btn transparency">
                </form>
            </div>
        </div>
    </div>
</div>
<div id="footer" class="footer">
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <p class="footer-header">{{ trans('common_elements.resources') }}</p>
                <a href="#" class="default-link link-footer">{{ trans('common_elements.destinations') }}</a>
                <a href="#" class="default-link link-footer">{{ trans('common_elements.kindOfGlampings') }}</a>
                <a href="#" class="default-link link-footer">{{ trans('common_elements.glampingQuestion') }}</a>
                <a href="#" class="default-link link-footer">{{ trans('common_elements.guidebooks') }}</a>
            </div>
            <div class="col-md-3">
                <p class="footer-header">{{ trans('common_elements.about_us') }}</p>
                <a href="{{route('page.show',['id' => 'about'])}}" class="default-link link-footer">{{ trans('common_elements.wordAbout') . ' Glampster' }}</a>
                <a href="#" class="default-link link-footer">{{ trans('common_elements.newsTitle') }}</a>
                <a href="#" class="default-link link-footer">{{ trans('common_elements.termsAndPrivacy') }}</a>
                <a href="{{route('page.show',['id' => 'contact'])}}" class="default-link link-footer">{{ trans('common_elements.contact') }}</a>
            </div>
            <div class="col-md-3">
                <p class="footer-header">{{ trans('common_elements.glampings') }}</p>
                <a href="{{ route('filterPage') }}" class="default-link link-footer">{{ trans('common_elements.glamping_list') }}</a>
                <a href="{{ route('filterPage', ['country' => trans('common_elements.russia') ]) }}"
                   class="default-link link-footer">{{ trans('common_elements.russianGlampings') }}</a>
                <a href="{{ route('filterPage', ['glamping_groups' => 1]) }}" class="default-link link-footer">{{ trans('common_elements.petFriendly') }}</a>
                <a href="#" class="default-link link-footer">{{ trans('common_elements.familyGlampings') }}</a>
            </div>
            <div class="col-md-3">
                <p class="footer-header">{{ trans('common_elements.infAndFAQ') }}</p>
                <a href="#" class="default-link link-footer">{{ trans('common_elements.review') }}</a>
                <a href="{{ route('feedbackForm') }}" class="default-link link-footer">{{ trans('common_elements.support') }}</a>
                @if (!MultiAuth::check())
                    <a href="{{ route('manager_register_page') }}"
                       class="default-link link-footer">{{ trans('common_elements.owner_register') }}</a>
                @endif
                <a href="#" class="default-link link-footer">{{ trans('common_elements.frequentlyAskedQuestions') }}</a>
            </div>
            <div class="col-md-12 copyright">
                <p class="copyright-text">Copyright © 2017 Glampster.com™. {{ trans('common_elements.allRightsReserved') }}.</p>
            </div>
        </div>
    </div>
</div>

<!-- Scripts --><div id="app"></div>
<script type="text/javascript" src="{{ mix('js/all.js') }}"></script>
<script type="text/javascript" src="{{ mix('js/app.js') }}"></script>
@yield('additional_scripts')
</body>
</html>
