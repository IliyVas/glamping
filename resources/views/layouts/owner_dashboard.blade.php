<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <title>{{ config('app.name', 'glamping') }}</title>
    @include('templates.page_elements.head')
    @yield('additional_styles')
</head>
<body class="theme-cyan">
<!-- Page Loader -->
@include('templates.page_elements.page_loader')
<!-- Overlay For Sidebars -->
<div class="overlay"></div>
<!-- Top Bar -->
@include('templates.page_elements.users_top_bar')

<!-- Left & Right bar menu -->
<section>
    <!-- Left Sidebar -->
    <aside id="leftsidebar" class="sidebar">
        <!-- User Info -->
        <div class="user-info">
            <div class="image">
                @if (!is_null(Auth::user()->media()->first()))
                    <img src="{{ Auth::user()->media()->first()->getUrl() }}"
                         height="48" alt="User">
                @else
                    <img src="{{ asset('images/account.png') }}" height="48" alt="User">
                @endif
            </div>
            <div class="info-container">
                <div class="name" data-toggle="dropdown">
                    {{ Auth::user()->first_name . ' ' . Auth::user()->last_name }}</div>
                <div class="email">{{ Auth::user()->email }}</div>
                <div class="btn-group user-helper-dropdown"> <i class="material-icons" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">keyboard_arrow_down</i>
                    <ul class="dropdown-menu pull-right">
                        <li>
                            <a href="{{ route('logout') }}"
                               onclick="event.preventDefault();
                                                         document.getElementById('logout-form').submit();">
                                <i class="material-icons">input</i>{{ trans('auth.logout') }}
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" class="hidden">
                                {{ csrf_field() }}
                            </form>
                        </li>
                        <li>
                            <a href="{{ route('manager.account.edit') }}">
                                <i class="material-icons">edit</i>{{ trans('common_elements.editProfileButton') }}
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div> <!-- #User Info -->

        <!-- Menu -->
        <div class="menu">
            <ul class="list">
                <li class="@yield('account_page_status')">
                    <a href="{{ route('manager.owner.profile') }}">
                        <span>{{ trans('common_elements.userProfile') }}</span>
                    </a>
                </li>
                <li class="@yield('glamping_form_page_status')">
                    <a href="{{ route('manager.glampings.create') }}">
                        <span>{{ trans('common_elements.createGlamping') }}</span>
                    </a>
                </li>
                <li  class="@yield('glamping_list_page_status')">
                    <a href="{{ route('manager.glampings.index') }}">
                        <span>{{ trans('common_elements.showUsersGlampings') }}</span>
                    </a>
                </li>
                <li  class="@yield('booking_page_status')">
                    <a href="{{ route('manager.bookings.index') }}">
                        <span>{{ trans('common_elements.ownerBookings') }}</span>
                    </a>
                </li>
            </ul>
        </div><!-- #Menu -->
    </aside>

    <!-- Right Sidebar -->
    <aside id="rightsidebar" class="right-sidebar">
        <ul class="nav nav-tabs tab-nav-right" role="tablist">
            <li role="presentation" class="active">
                <a href="#settings" data-toggle="tab">{{ trans('common_elements.settings') }}</a>
            </li>
        </ul>
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane fade  in active  in active" id="settings">
                <div class="demo-settings">
                    <p class="text-left">{{ mb_strtoupper(trans('common_elements.lang')) }}</p>
                    <ul class="setting-list">
                        @foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties)
                            <li>
                                <a rel="alternate" hreflang="{{ $localeCode }}" href="{{ LaravelLocalization::getLocalizedURL($localeCode, null, [], true) }}">
                                    {{ $properties['native'] }}
                                </a>
                            </li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </aside>
</section>

<!-- Main Content -->
<section class="content">
    @include('templates.messages_block')

    <div class="block-header">
        <div class="row">
            <div class="col-md-7 col-sm-6 col-xs-12">
                <div class="h-left clearfix">
                    {{-- Page name --}}
                    <h2>@yield('page_name')</h2>
                    {{-- Page information --}}
                    <small>@yield('page_information')</small>
                    {{-- Bread crumbs --}}
                    <ol class="breadcrumb breadcrumb-col-pink p-l-0">
                        @yield('bread_crumbs')
                    </ol>
                </div>
            </div>
            <div class="col-md-5 col-sm-6 col-xs-12">
                <div class="h-right">
                    <div class="row">
                        <div class="col-xs-6">
                            <div class="sparkline" data-type="bar" data-width="97%"
                                 data-height="62px" data-bar-Width="3" data-bar-Spacing="4"
                                 data-bar-Color="rgb(96, 125, 139)">6,7,8,5,3,6,7,8,5,2</div>
                            <p class="m-b-0 p-t-10 text-center">{{ trans('common_elements.viewsCount') }}</p>
                        </div>
                        <div class="col-xs-6">
                            <div class="sparkline" data-type="bar" data-width="97%"
                                 data-height="62px" data-bar-Width="3" data-bar-Spacing="4"
                                 data-bar-Color="rgb(120, 184, 62)">4,2,8,6,7,6,7,8,5,2</div>
                            <p class="m-b-0 p-t-10 text-center">{{ trans('common_elements.bookingCount') }}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    @yield('content_name')
                    <div class="body">
                        @yield('content')
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- Jquery Core Js -->
@include('templates.page_elements.basic_scripts')
@yield('additional_scripts')
</body>
</html>