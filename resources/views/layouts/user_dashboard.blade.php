<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <title>{{ config('app.name', 'glamping') }}</title>
    @include('templates.page_elements.head')
    @yield('additional_styles')
</head>
<body class="theme-cyan">
<!-- Page Loader -->
@include('templates.page_elements.page_loader')
<!-- Overlay For Sidebars -->
<div class="overlay"></div>
<!-- Top Bar -->
@include('templates.page_elements.users_top_bar')

<!-- Left & Right bar menu -->
<section>
    <!-- Left Sidebar -->
    <aside id="leftsidebar" class="sidebar">
        <!-- User Info -->
        <div class="user-info">
            <div class="image">
                @if (!is_null(Auth::user()->media()->first()))
                    <img src="{{ Auth::user()->media()->first()->getUrl() }}"
                         height="48" alt="User">
                @else
                    <img src="{{ asset('images/account.png') }}" height="48" alt="User">
                @endif
            </div>
            <div class="info-container">
                <div class="name" data-toggle="dropdown">
                    {{ Auth::user()->first_name . ' ' . Auth::user()->last_name }}</div>
                <div class="email">{{ Auth::user()->email }}</div>
                <div class="btn-group user-helper-dropdown"> <i class="material-icons" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">keyboard_arrow_down</i>
                    <ul class="dropdown-menu pull-right">
                        <li>
                            <a href="/logout"
                               onclick="event.preventDefault();
                                                         document.getElementById('logout-form').submit();">
                                <i class="material-icons">input</i>{{ trans('auth.logout') }}
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" class="hidden">
                                {{ csrf_field() }}
                            </form>
                        </li>
                        <li>
                            <a href="{{ route('user.account.edit') }}">
                                <i class="material-icons">edit</i>{{ trans('common_elements.editProfileButton') }}
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div> <!-- #User Info -->

        <!-- Menu -->
        <div class="menu">
            <ul class="list">
                <li class="@yield('user_account_page_status')">
                    <a href="{{ route('user.account') }}">
                        <span>{{ trans('common_elements.userAccount') }}</span>
                    </a>
                </li>
                <li  class="@yield('user_bookings_page_status')">
                    <a href="{{ route('user.bookings') }}">
                        <span>{{ trans('common_elements.userYourBookings') }}</span>
                    </a>
                </li>
            </ul>
        </div><!-- #Menu -->
    </aside>

    <!-- Right Sidebar -->
    <aside id="rightsidebar" class="right-sidebar">
        <ul class="nav nav-tabs tab-nav-right" role="tablist">
            <li role="presentation" class="active">
                <a href="#settings" data-toggle="tab">{{ trans('common_elements.settings') }}</a>
            </li>
        </ul>
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane fade  in active  in active" id="settings">
                <div class="demo-settings">
                    <p class="text-left">{{ mb_strtoupper(trans('common_elements.lang')) }}</p>
                    <ul class="setting-list">
                        @foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties)
                            <li>
                                <a rel="alternate" hreflang="{{ $localeCode }}" href="{{ LaravelLocalization::getLocalizedURL($localeCode, null, [], true) }}">
                                    {{ $properties['native'] }}
                                </a>
                            </li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </aside>
</section>

<!-- Main Content -->
<section class="content">
    @include('templates.messages_block')
    <div class="block-header">
        <div class="row">
            <div class="col-md-8 col-sm-7 col-xs-12">
                <div class="h-left clearfix">
                    <h2>@yield('page_name')</h2>
                    <small>@yield('page_description')</small>
                    {{-- Bread crumbs --}}
                    <ol class="breadcrumb breadcrumb-col-pink p-l-0">
                        @yield('bread_crumbs')
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    @yield('content_name')
                    <div class="body">
                        @yield('content')
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- Jquery Core Js -->
@include('templates.page_elements.basic_scripts')
@yield('additional_scripts')
</body>
</html>