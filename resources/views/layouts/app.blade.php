<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Laravel</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link type="text/css" href="{{ mix('css/app.css') }}" rel="stylesheet">
    <link type="text/css" href="{{ mix('css/all.css') }}" rel="stylesheet">
    @yield('additional_styles')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

    <!-- Scripts -->
    <script src="{{ mix('js/jQ.js') }}" type="text/javascript"></script>
    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>
</head>
<body>
<nav class="navbar navbar-default navbar-static-top" role="navigation">
    <div class="container">
        <div class="navbar-header">
            <!-- Branding Image -->
            <a class="navbar-brand" href="{{ route('homepage') }}">
                {{ config('app.name', 'Laravel') }}
            </a>
        </div>
        <div class="navbar-collapse">
            <ul class="nav navbar-nav navbar-right">
                <!-- Lang block -->
                <li class="dropdown">
                    <a href="#" data-toggle="dropdown" role="button" aria-expanded="false">
                        {{ trans('common_elements.lang') }}<span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu" role="menu">
                        @foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties)
                            <li>
                                <a rel="alternate" hreflang="{{ $localeCode }}" href="{{ LaravelLocalization::getLocalizedURL($localeCode, null, [], true) }}">
                                    {{ $properties['native'] }}
                                </a>
                            </li>
                        @endforeach
                    </ul>
                </li>

                @if (!MultiAuth::check())
                    <li><a href="{{ route('user_login_page') }}">{{ trans('common_elements.login') }}</a></li>
                    <li><a href="{{ route('manager_register_page') }}">{{ trans('common_elements.owner_register') }}</a></li>
                @else
                    <li><a href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            {{ trans('common_elements.logout') }}
                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>
                    </li>
                @endif

                <li>
                    <a href="{{ route('feedbackForm') }}">{{ trans('common_elements.feedbackFormName') }}</a>
                </li>
                <li>
                    <a href="{{route('page.show',['id' => 'about'])}}">{{ trans('common_elements.about_us') }}</a>
                </li>
                <li>
                    <a href="{{route('page.show',['id' => 'contact'])}}">{{ trans('common_elements.contact') }}</a>
                </li>
            </ul>
        </div>
    </div> {{-- end container--}}
</nav>
<div class="container">
    @include('templates.messages_block')
    @yield('content')
</div>

<!-- Scripts --><div id="app"></div>
<script type="text/javascript" src="{{ mix('js/all.js') }}"></script>
<script type="text/javascript" src="{{ mix('js/app.js') }}"></script>
@yield('additional_scripts')
</body>
</html>
