<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <title>{{ config('app.name', 'glamping') }}</title>
    @include('templates.page_elements.head')
    @yield('additional_styles')
    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>
</head>
<body class="theme-cyan">
<div class="authentication">
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-9 col-md-8 col-xs-12">
                <div class="l-detail">
                    <h5>{{ trans('auth.welcome') }}</h5>
                    <h1>Glampste<span>R</span></h1>
                    <h3>{{ trans('auth.sign_in') }}</h3>
                    <p>{{ trans('auth.auth_text') }}</p>                            
                    <ul class="list-unstyled l-social">
                        <li><a href="#"><i class="zmdi zmdi-facebook-box"></i></a></li>                                
                        <li><a href="#"><i class="zmdi zmdi-linkedin-box"></i></a></li>
                        <li><a href="#"><i class="zmdi zmdi-pinterest-box"></i></a></li>
                        <li><a href="#"><i class="zmdi zmdi-twitter"></i></a></li>
                        <li><a href="#"><i class="zmdi zmdi-youtube-play"></i></a></li>
                        <li><a href="#"><i class="zmdi zmdi-google-plus-box"></i></a></li>
                        <li><a href="#"><i class="zmdi zmdi-behance"></i></a></li>
                        <li><a href="#"><i class="zmdi zmdi-dribbble"></i></a></li>
                        <li><a href="#"><i class="zmdi zmdi-instagram"></i></a></li>
                    </ul>
                    <ul class="list-unstyled l-menu">
                        <li><a href="{{ route('page.show',['id' => 'contact']) }}">{{ trans('common_elements.contact') }}</a></li>
                        <li><a href="{{ route('page.show',['id' => 'about']) }}">{{ trans('common_elements.about_us') }}</a></li>
                        <li><a href="{{ route('filterPage') }}">{{ trans('common_elements.glamping_list') }}</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-3 col-md-4 col-xs-12 bg-white">
                @yield('auth_form')
            </div>
        </div>
    </div>
</div>

<script src="{{ asset('theme/assets/bundles/libscripts.bundle.js') }}"></script> <!-- Lib Scripts Plugin Js -->
<script src="{{ asset('theme/assets/bundles/vendorscripts.bundle.js') }}"></script> <!-- Lib Scripts Plugin Js -->
<script src="{{ asset('theme/assets/plugins/css-gradientify/gradientify.min.js') }}"></script><!-- Gradientify Js -->
<script src="{{ asset('theme/assets/plugins/jquery-validation/jquery.validate.js') }}"></script> <!-- Jquery Validation Plugin Css --> 
<script src="{{ asset('theme/assets/bundles/mainscripts.bundle.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $("body").gradientify({
            gradients: [
                { start: [49,76,172], stop: [242,159,191] },
                { start: [255,103,69], stop: [240,154,241] },
                { start: [33,229,241], stop: [235,236,117] }
            ]
        });
    });
</script>
@yield('additional_scripts')
</body>
</html>
