<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ trans('common_elements.adminTitle') }}</title>
    @include('templates.page_elements.head')

    <link type="text/css" href="{{ mix('css/flaticon.css') }}" rel="stylesheet">

    @yield('additional_styles')
</head>
<body class="theme-cyan">

<!-- Page Loader -->
@include('templates.page_elements.page_loader')

<!-- Overlay For Sidebars -->
<div class="overlay"></div>

<!-- Top Bar -->
@include('templates.page_elements.users_top_bar')

<!-- Left & Right bar menu -->
<section>
    <!-- Left Sidebar -->
    <aside id="leftsidebar" class="sidebar">
        <!-- User Info -->
        <div class="user-info">
            <div class="image">
                @if (!is_null(Auth::user()->media()->first()))
                    <img src="{{ Auth::user()->media()->first()->getUrl() }}"
                         height="48" alt="User">
                @else
                    <img src="{{ asset('images/account.png') }}" height="48" alt="User">
                @endif
            </div>
            <div class="info-container">
                <div class="name" data-toggle="dropdown">
                    {{ Auth::user()->first_name . ' ' . Auth::user()->last_name }}</div>
                <div class="email">{{ Auth::user()->email }}</div>
                <div class="btn-group user-helper-dropdown"> <i class="material-icons" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">keyboard_arrow_down</i>
                    <ul class="dropdown-menu pull-right">
                        <li>
                            <a href="{{ route('logout') }}"
                               onclick="event.preventDefault();
                                                         document.getElementById('logout-form').submit();">
                                <i class="material-icons">input</i>{{ trans('auth.logout') }}
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" class="hidden">
                                {{ csrf_field() }}
                            </form>
                        </li>
                        <li>
                            <a href="{{ route('manager.admin-account.edit') }}">
                                <i class="material-icons">edit</i>{{ trans('common_elements.editProfileButton') }}
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div> <!-- #User Info -->

        <!-- Menu -->
        <div class="menu">
            <ul class="list">
                <li  class="@yield('glamping_list_page_status')">
                    <a href="{{ route('manager.glampings.index') }}">{{ trans('common_elements.glampings') }}</a>
                </li>
                <li @yield('lists')><a href="javascript:void(0);" class="menu-toggle">{{ trans('common_elements.listManagement') }}</a>
                    <ul class="ml-menu">
                        <li @yield('BuildingType')>
                            <a href="{{ route('manager.building_types.index') }}">{{ trans('common_elements.buildingTypes') }}</a>
                        </li>
                        <li @yield('GlampingGroup')>
                            <a href="{{ route('manager.glamping_groups.index') }}">{{ trans('common_elements.glampingGroups') }}</a>
                        </li>
                        <li @yield('Impression')>
                            <a href="{{ route('manager.impressions.index') }}">{{ trans('common_elements.impressions') }}</a>
                        </li>
                        <li @yield('Amenity')>
                            <a href="{{ route('manager.amenities.index') }}">{{ trans('common_elements.amenities') }}</a>
                        </li>
                        <li @yield('Service')>
                            <a href="{{ route('manager.services.index') }}">{{ trans('common_elements.services') }}</a>
                        </li>
                    </ul>
                </li>
                <li @yield('applications')><a href="javascript:void(0);" class="menu-toggle">{{ trans('common_elements.applications') }}</a>
                    <ul class="ml-menu">
                        <li @yield('Application')>
                            <a href="{{ route('manager.applications.index') }}">{{ trans('common_elements.new') }}</a>
                        </li>
                        <li @yield('ApplicationArchive')>
                            <a href="{{ route('manager.applications.archive') }}">{{ trans('common_elements.archive') }}</a>
                        </li>
                    </ul>
                </li>
                <li class="@yield('pages_status')">
                    <a href="{{route('manager.pages')}}">{{ trans('common_elements.pages') }}</a>
                </li>
                <li  class="@yield('news_page_status')">
                    <a href="{{route('manager.news.index')}}">{{ trans('common_elements.newsTitle') }}</a>
                </li>
                <li @yield('Feedback')>
                    <a href="{{ route('manager.feedbackMessages') }}">{{ trans('common_elements.feedbackMessages') }}</a>
                </li>
            </ul>
        </div><!-- #Menu -->
    </aside>

    <!-- Right Sidebar -->
    <aside id="rightsidebar" class="right-sidebar">
        <ul class="nav nav-tabs tab-nav-right" role="tablist">
            <li role="presentation" class="active">
                <a href="#settings" data-toggle="tab">{{ trans('common_elements.settings') }}</a>
            </li>
        </ul>
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane fade  in active  in active" id="settings">
                <div class="demo-settings">
                    <p class="text-left">{{ mb_strtoupper(trans('common_elements.lang')) }}</p>
                    <ul class="setting-list">
                        @foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties)
                            <li>
                                <i class="material-icons text-success">label</i><a rel="alternate" hreflang="{{ $localeCode }}" href="{{ LaravelLocalization::getLocalizedURL($localeCode, null, [], true) }}">
                                    {{ $properties['native'] }}
                                </a>
                            </li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </aside>
</section>

<!-- Main Content -->
<section class="content">
    @include('templates.messages_block')

    <div class="block-header">
        <div class="row">
            <div class="col-md-7 col-sm-6 col-xs-12">
                <div class="h-left clearfix">
                    {{-- Page name --}}
                    <h2>@yield('page_name')</h2>

                    {{-- Page information --}}
                    <small>@yield('page_information')</small>

                    {{-- Bread crumbs --}}
                    <ol class="breadcrumb breadcrumb-col-pink p-l-0">
                        @yield('bread_crumbs')
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    @yield('content_name')
                    <div class="body">
                        @yield('content')
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- Jquery Core Js -->
@include('templates.page_elements.basic_scripts')
@yield('additional_scripts')

@if (!empty($sortable))
<script>
    $(document).ready(function() {
        $('.js-basic-example').DataTable()
                .order([@foreach ($sortable as $sort_column => $sort_value)
                [{{ $sort_column }}, "{{ $sort_value }}"]
                @endforeach])
                .draw();
    } );
</script>
@endif
</body>
</html>
