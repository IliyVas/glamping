@extends('layouts.public_dashboard')

@section ('title'){{ trans('common_elements.home') }}@endsection

@section('content')
    <div class="header">
        @include('templates.page_elements.frontend_top_bar')

        <div class="header-content">
            <div class="container-fluid">
                <div class="row">
                    <h4 class="h4">{{ trans('common_elements.glampingDescription') }}</h4>
                    <h1 class="h1">{{ trans('common_elements.findTheBestDeals') }}</h1>
                    <div class="form-find">
                        {{--Check prices block--}}
                        <form method="get" action="{{ route('filterPage') }}" class="find">
                            <div class="wrapper-find-form">
                                {{--Name, country or region block--}}
                                <div class="wrapper-input">
                                    <input type="text" id="additional_data" class="place"
                                           placeholder="{{ trans('common_elements.checkPricesPlaceHolder') }}" name="additional_data">
                                    <input id="additional_data_type" name="additional_data_type" type="hidden" value="name">
                                </div>
                                {{--Arrival date block--}}
                                <div class="wrapper-input">
                                    <label for="arrival_date">{{ trans('common_elements.arrivalDate') }}:</label>
                                    <input min="{{ date('Y-m-d', time()) }}" id="arrival_date" type="date" name="from_date">
                                </div>
                                {{--County date block--}}
                                <div class="wrapper-input">
                                    <label for="county_date">{{ trans('common_elements.countyDate') }}:</label>
                                    <input min="{{ date('Y-m-d', time() + (2*24*60*60)) }}" type="date" name="to_date"
                                           id="county_date">
                                </div>
                                {{--Adults--}}
                                <div class="wrapper-input">
                                    <label>{{ trans('common_elements.quantity') }}:</label>
                                    <select name="adults" id="guests">
                                        @for ($i = 1; $i <= $maxQuantity; $i++)<option value="{{ $i }}">{{ $i }}</option>@endfor
                                    </select>
                                </div>
                            </div>
                            {{--Buttons block--}}
                            <a href="{{ route('filterPage') }}" class="extended-find">{{ trans('common_elements.btnAdvancedSearch') }}</a>
                            <button type="submit" name="submit" class="btn-find">{{ trans('common_elements.btnCheckPrices') }}</button>
                        </form>
                    </div>
                </div>
            </div>
            <div class="wrapper-issue">
               <div class="issues-position">
                    <div class="issues">
                        <p>{{ trans('common_elements.glampingQuestion') }}</p>
                        <div class="question-img">
                            <img src="{{ asset('images/question-sign.png') }}" alt="/">
                        </div>
                    </div>
                </div> 
            </div>  
        </div>
        {{--Impressions block--}}
        <div class="header-menu-bottom">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-6">
                        <div class="popular-place">
                            <p class="head-tags">{{ trans('common_elements.popularImpressions') }}:</p>
                            <div class="tags">
                                @if(count($popularImpressions) > 0)
                                    @foreach($popularImpressions as $popularImpression)
                                        <span style="cursor: pointer" class="wrapper-tag" data-impression-name="{{ $popularImpression->name }}"
                                              onclick="setImpression(this)" data-impression-id="{{ $popularImpression->id }}">
                                            <img src="{{ $popularImpression->icon }}" alt="tag" class="img-tag">
                                            {{ $popularImpression->name }}
                                        </span>
                                    @endforeach
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <form method="get" action="{{ route('filterPage') }}" class="search-form">
                            @if(count($popularImpressions) > 0)
                                @php $impression = $popularImpressions->first()->name; @endphp
                            @else
                                @php $impression = 'Рыбалка'; @endphp
                            @endif
                            <input style="width:70%;" id="impressions_name" type="text" name="impressions_name"
                                   placeholder="{{ trans('common_elements.impressionFilterPlaceHolder') . '"' .
                                    $impression . '"' }}">
                            <input id="impressions" name="impressions" type="hidden">
                            <button class="btn-text" name="submit" type="submit">{{ trans('common_elements.btnSearch') }}</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="page">
        <div class="container">
            <div class="row">
                {{--Glampings block--}}
                <div class="col-md-7">
                    <div class="row">
                        <div class="col-md-12 inder">
                            <h3 class="h3 indent">{{ trans('common_elements.ourOffers') }}</h3>
                        </div>
                        @if (count($glampings) > 0)
                            @foreach($glampings as $glamping)
                                <div class="offers row">
                                    @php $discountData = $glamping->getDiscountData(); @endphp
                                    <div class="col-md-5">
                                        @if (is_null($glamping->media()->first()))
                                            <img src="{{ asset('images/offer.png') }}" alt="offer" class="offer-img">
                                        @else
                                            <img src="{{ $glamping->media()->first()->getUrl() }}" alt="offer" class="offer-img">
                                        @endif
                                        @if(!is_null($discountData))
                                            <div class="discount">
                                                <h3 class="discount-value">{{ $discountData['sale'] }}%</h3>
                                                <p>{{ trans('common_elements.sale') }}!</p>
                                            </div>
                                        @endif
                                    </div>
                                    <div class="col-md-7">
                                        <h3 class="h3 offer-name">{{ $glamping->name . ', ' . $glamping->country }}</h3>
                                        <p class="text-offer">{{ $glamping->description }}</p>
                                        <div class="price-block">
                                            <div class="old">
                                                <h3 class="old-price">{{ $discountData['price'] }}</h3>
                                            </div>
                                            <img src="{{ asset('images/discount.png') }}" alt="d">
                                            <div class="new">
                                                <h3 class="h3">{{ $glamping->min_price }}</h3>
                                                <p>{{ trans('common_elements.perNight') }}!</p>
                                            </div>
                                        </div>
                                        <a href="{{ route('viewGlamping', ['id' => $glamping->id]) }}"
                                           class="btn-purple gradient btn-hover-green">{{ trans('common_elements.seeOffer') }}</a>
                                    </div>
                                </div>
                            @endforeach
                        @endif
                    </div>
                </div>
                <div class="col-md-5">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="about-glamping under">
                                <h3 class="h3 indent">{{ trans('common_elements.glampingQuestion') }}</h3>
                                <p class="description">{{ trans('common_elements.glampingAnswer') }}</p>
                                <a href="#">{{ trans('common_elements.readMore') }}</a>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="registration-glamping under">
                                <p class="low-text">{{ trans('common_elements.ownerSubscriptionTitle') }}</p>
                                <h3 class="h3">{!! trans('common_elements.ownerSubscriptionDescription') !!}</h3>
                                <p class="any-text">{!! trans('common_elements.ownerSubscriptionAdditionalDescription') !!}</p>
                                <a href="#" class="btn-purple">{{ trans('common_elements.readMore') }}</a>
                            </div>
                        </div>
                        {{--News block--}}
                        @if (count($news) > 0)
                            <div class="col-md-12 last-entry-header">
                                <h3 class="h3">{{ trans('common_elements.newsTitle') }}</h3>
                            </div>
                            <div class="col-md-12">
                                @foreach($news as $article)
                                    <div class="last-entry">
                                        <div class="row">
                                            <div class="col-md-5 img-entry">
                                                @if (is_null($article->media()->first()))
                                                    <img src="{{ asset('images/entry.png') }}" alt="entry" class="img-entry">
                                                @else
                                                    <img src="{{ $article->media()->first()->getUrl() }}" alt="entry" class="img-entry">
                                                @endif
                                            </div>
                                            <div class="col-md-7 text-entry">
                                                <img src="{{ asset('images/clock.png') }}" alt="date">
                                                <p class="date-entry">{{ date_create($article->created_at)->format('d.m.Y H:i') }}</p>
                                                <p class="name-entry">{{ $article->title }} </p>
                                                <p class="description-entry">{{ $article->description . '...'}}</p>
                                                <a href="#" class="link-entry">{{ trans('common_elements.readMore') }}</a>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{--Scripts--}}
    <script type="text/javascript">
        //autocomplete filter by additional_data input
        $('#additional_data').autocomplete({
            source: function  (request, response) {
                autocompleteFilterInputs(request, response, ['name', 'country', 'region']);
            },
            select: function( event, ui ) {
                $( "#additional_data" ).val( ui.item.value );
                $( "#additional_data_type" ).val( ui.item.type );

                return false;
            }
        });
        //autocomplete impressions input
        $('#impressions_name').autocomplete({
            source: function  (request, response) {
                autocompleteImpressions(request, response);
            },
            select: function( event, ui ) {
                $( "#impressions" ).val( ui.item.id );
                $( "#impressions_name" ).val( ui.item.value );

                return false;
            }
        });

        function setImpression(elem) {
            var impressionName = elem.getAttribute('data-impression-name');
            var impressionId = elem.getAttribute('data-impression-id');
            $( "#impressions" ).val( impressionId );
            $( "#impressions_name" ).val( impressionName );
        }
    </script>
@endsection