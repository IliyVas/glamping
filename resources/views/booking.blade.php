@extends('layouts.public_dashboard')

@section ('title'){{ trans('common_elements.home') }}@endsection

@section('content')

<div class="wrapper-border">
	@include('templates.page_elements.frontend_top_bar')
</div>
<div class="path-to-page">
	<span class="path">
	<a href="#" class="default-link">Главная<img src="images/next.png"></a>
	<a href="#" class="default-link">Россия<img src="images/next.png"></a>
	<a href="#" class="default-link">Москва<img src="images/next.png"></a>
	<a href="#" class="default-link">Гуляй Город</a>
	</span>
</div>
<div class="page">
	<div class="container">
		<div class="row">
			<div class="col-md-10">
				<div class="name-place">
					<h1>«Гуляй-город», Россия</h1>
					<p>Палаточный отель на берегу Оки в часе езды от МКАД</p>
				</div>
			</div>
			<div class="col-md-2">
				<p class="ranked-name">Рейтинг глэмпинга:</p>
			</div>
			<div class="col-md-8">
				<div class="carousel">
					
				</div>
				<div class="dignity">
					<div class="row">
						<div class="col-md-3 dignity-elem">
							<img src="images/tent.png" alt="tent">
							<p>Палатки</p>
						</div>
						<div class="col-md-3 dignity-elem">
							<img src="images/teamwork.png" alt="team">
							<p>20 человек - макс. вместительность</p>
						</div>
						<div class="col-md-3 dignity-elem">
							<img src="images/calendar.png" alt="calendar">
							<p>2 дня - мин. время
							<br>стоянки</p>
						</div>
						<div class="col-md-3 dignity-elem">
							<img src="images/jack-russell-terrier.png" alt="pet">
							<p>Можно
							<br>с животными</p>
						</div>
					</div>
				</div>
				<div class="description-glamp">
					<div class="row">
						<div class="col-md-12">
							<h3 class="h3">Отдых для тех, кто любит комфорт, но хочет отдыхать на природе. <br>Забудьте о снаряжении, спальниках и холодной воде.</h3>
							<p class="desc">Здесь вас ждут удобства для гигиены, горячая вода, мягкие кровати, wi-fi и розетки.
							Всего в 100 км от Москвы, вблизи к заповедной зоне наслаждайтесь свежим воздухом, зелеными 
							лужайками, рощами и песчаным пляжем. Исследуйте днем местные достопримечательности, а вечером 
							располагайтесь у мангала или с удочкой на пирсе.</p>

							<p class="desc">У нас разрешено купаться в реке, а песок на пляже чистый, без мусора и осколков. Устройте парный 
							заплыв или сыграйте в волейбол. А потом располагайтесь на лежаке под зонтиком, потягивая 
							освежающий мохито или свежевыжатый сок.</p>
							<div class="impression">
								<img src="images/smile.png" alt="smile">
								<p>Впечатления:</p>
								<a href="#" class="default-link impression-tag">Купание в чистой реке</a>
								<a href="#" class="default-link impression-tag">Костры на закате</a>
								<a href="#" class="default-link impression-tag">Пение птиц</a>
								<a href="#">Посмотреть все</a>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="row">
					<div class="col-md-12">
						<div class="price-glamp">
							<h3 class="h3">От <span class="bold">3600 руб.</span>/ночь</h3>
							<button class="btn-booking" href="#">Забронировать</button>
							<p>Ваше бронирование на 100% безопасно и конфиденциально.</p>
						</div>
					</div>
					<div class="col-md-12 can-find">
					<div class="wrapper-border-bottom">
						<h3 class="h3">Тут вы точно сможете:</h3>
						<ul class="find-list">
							<li>выкопать яму</li>
							<li>насладиться природой</li>
							<li>погладить медведя</li>
						</ul>
						<img src="images/chat.png"><a href="#footer" id="link-review">Читать отзывы</a>
					</div>
					</div>
					<div class="col-md-12 map">
						
					</div>
					<div class="col-md-12">
						<div class="social">
							<p class="header-social">Поделиться глэмпингом:</p>
							<img src="images/facebook.png" alt="fb">
							<img src="images/vk.png" alt="vk">
							<img src="images/google.png" alt="g+">
							<img src="images/twitter.png" alt="tw">
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h3 class="h3">Параметры заезда:</h3>
				</div>
			</div>
		</div>
		<div class="parameters">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<form class="form-parameters">
							<div class="wrapper-parameter">
								<img src="images/calendar-yellow.png" alt="calendar">
								<p>Дата пребывания:</p>
							</div>
							<div class="wrapper-parameter">
								<div class="input-animation">
									<input type="text" placeholder="19.07.2017" name="start-date">
								</div>
								<p>-</p>
								<div class="input-animation">
									<input type="text" placeholder="21.07.2017" name="start-date">
								</div>
							</div>
							<div class="wrapper-parameter">
								<img src="images/group.png" alt="group">
								<p>Кол-во гостей:</p>
							</div>
							<div class="wrapper-parameter">
								<div class="input-animation">
									<select>
										<option value="1">1</option>
										<option value="2">2</option>
										<option value="3">3</option>
									</select>
								</div>
							</div>
							<div class="wrapper-parameter">
								<img src="images/home.png" alt="home">
								<p>Помещений</p>
							</div>
							<div class="wrapper-parameter">
								<select>
									<option value="1">1</option>
									<option value="2">2</option>
									<option value="3">3</option>
								</select>
							</div>
							<div class="wrapper-parameter">
								<button class="btn-parameters">Задать параметры</button>
							</div>
							
						</form>
						
					</div>
				</div>
				
			</div>
		</div>
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<table class="select-place">
						<thead>
							<tr>
								<th>Номер</th>
								<th>Вмещает</th>
								<th>Цена на выбранные даты</th>
								<th>Выберите номера</th>
								<th>Подтвердите бронирование</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td class="description-place">
									<h3 class="h3">Название помещения</h3>
									<p class="description-text">Краткое описание этого замечательного помещения</p>
									<ul>
										<li class="header-list">Удобства</li>
										<li class="wifi">wi-fi</li>
										<li class="shower">отдельный душ</li>
										<li class="towel">полотенца</li>
										<li class="linens">постельное белье</li>
									</ul>
									<div class="preview-place">
										<img src="images/place1.png" alt="/">
										<img src="images/place2.png" alt="/">
										<img src="images/place3.png" alt="/">
									</div>
								</td>
								<td class="value"><img src="images/user.png">
								<img src="images/user.png"></td>
								<td><h3 class="h3 price">9 000 руб</h3>
								<p>За 2 ночи</p></td>
								<td>
									<select>
										<option value="1">1</option>
										<option value="2">2</option>
										<option value="3">3</option>
									</select>
								</td>
								<td><p>1 номер за:</p>
										<h3 class="h3 price">9 000 руб.</h3>
										<p class="attention">Вам повезло! <br>
										Это последний номер на нашем сайте!</p>
										<button class="btn-booking">Забронировать</button>
										<p class="low-text">Возможно оплата на месте.</p>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
</div>

<div class="subscribe">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-6 col-md-offset-3">
				<h1 class="h1-subscribe text-center">Сэкономьте время и деньги</h1>
				<h3 class="h3-subscribe text-center">Подпишитесь и мы вышлем вам лучшие предложения</h3>
				<form action="" class="subscribe-form transparency">
					<input type="text" name="mail" class="subscribe-mail transparency" placeholder="Напишите свой электронный адрес">
					<input type="submit" name="submit" value="Подписаться" class="subscribe-btn transparency">
				</form>
			</div>
		</div>
	</div>
</div>
<div id="footer" class="footer">
	<div class="container">
		<div class="row">
			<div class="col-md-3">
				<p class="footer-header">Ресурсы</p>
				<a href="#" class="default-link link-footer">Места</a>
				<a href="#" class="default-link link-footer">Виды глэмпингов</a>
				<a href="#" class="default-link link-footer">Что такое глэмпинг?</a>
				<a href="#" class="default-link link-footer">Путеводители</a>
			</div>
			<div class="col-md-3">
				<p class="footer-header">О нас</p>
				<a href="#" class="default-link link-footer">О Glampster</a>
				<a href="#" class="default-link link-footer">Блог</a>
				<a href="#" class="default-link link-footer">Условия и конфиденциальность</a>
				<a href="#" class="default-link link-footer">Контакты</a>
			</div>
			<div class="col-md-3">
				<p class="footer-header">Глэмпинги</p>
				<a href="#" class="default-link link-footer">Все глэмпинги</a>
				<a href="#" class="default-link link-footer">Глэмпинги России</a>
				<a href="#" class="default-link link-footer">Можно с животными</a>
				<a href="#" class="default-link link-footer">Гэмпинги для всей семьи</a>
			</div>
			<div class="col-md-3">
				<p class="footer-header">Информация и FAQ</p>
				<a href="#" class="default-link link-footer">Отзывы</a>
				<a href="#" class="default-link link-footer">Поддержка</a>
				<a href="#" class="default-link link-footer">Glampster для бизнеса</a>
				<a href="#" class="default-link link-footer">Часто задаваемые вопросы</a>
			</div>
			<div class="col-md-12 copyright">
				<p class="copyright-text">Copyright © 2017 Glampster.com™. Все права защищены.</p>
			</div>
		</div>
	</div>
</div>
@endsection