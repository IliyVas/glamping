@if ($user->hasRole('admin'))
    @php $role = 'admin'; @endphp
@else
    @php $role = 'owner'; @endphp
@endif

@extends('layouts.' . $role . '_dashboard')

@section('additional_styles')
    <!-- JQuery DataTable Css -->
    <link href="{{ asset('theme/assets/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css') }}" rel="stylesheet">
@endsection

@section('glamping_list_page_status') active @endsection

@section('page_name')
    @if ($user->hasRole('admin'))
        {{ trans('common_elements.glampings') }}
    @else
        {{ trans('common_elements.myGlampings') }}
    @endif
@endsection

@section('page_information')
    {{ trans('tables.glampingsListGlampingsSum') }}: {{sizeof($glampings)}}
@endsection

@section('bread_crumbs')
    @if ($user->hasRole('admin'))
        <li><a href="{{ route('manager.admin_dashboard') }}">{{ trans('common_elements.home') }}</a></li>
        <li class="active">{{ trans('common_elements.glampings') }}</li>
    @else
        <li><a href="{{ route('manager.owner_dashboard') }}">{{ trans('common_elements.home') }}</a></li>
        <li class="active">{{ trans('common_elements.showUsersGlampings') }}</li>
    @endif
@endsection

@section('content_name')
    <div class="header">
        <h2>
            @if ($user->hasRole('admin'))
                {{ trans('common_elements.glampings') }}
            @else
                {{ trans('common_elements.myGlampings') }}
            @endif
        </h2>
    </div>
@endsection

@section('content')
    <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
        <thead>
        <tr>
            @include('manager.owner.templates.grid.glamping_list_column_names')
        </tr>
        </thead>
        <tfoot>
        <tr>
            @include('manager.owner.templates.grid.glamping_list_column_names')
        </tr>
        </tfoot>
        <tbody>
        @foreach ($glampings as $glamping)
            @if (!count($glamping->applications) > 0 || is_null($glamping->glamping_id) &&
             $glamping->lastApplicationType() !== ApplicationType::CHANGE || $glamping->accepted == true)
                <tr>
                    <td>
                        @if ($glamping->changedGlamping)
                            <a href="{{ route('manager.glampings.show', ['id' => $glamping->changedGlamping->id]) }}"
                               title="{{ trans('common_elements.btnView') }}" class="text-info">
                                {{$glamping->changedGlamping->name }}
                            </a>
                            <a href="{{ route('manager.glampings.show', ['id' => $glamping->id]) }}"
                               title="{{ trans('common_elements.btnView') }}"
                            @if ($glamping->accepted == false) class="text-info" @endif>
                                ({{$glamping->name}})
                            </a>
                        @else
                            <a href="{{ route('manager.glampings.show', ['id' => $glamping->id]) }}"
                               title="{{ trans('common_elements.btnView') }}"
                            @if ($glamping->accepted == false) class="text-info" @endif>
                                {{$glamping->name}}
                            </a>
                        @endif
                    </td>
                    <td>{{$glamping->country}}, {{$glamping->region}}</td>
                    <td>
                        @if (count($glamping->buildings()->get()) > 0)
                            <ul>
                                @foreach ($glamping->buildings()->get() as $build)
                                    @if (!$build->changedBuilding)
                                        <li class="list-unstyled">
                                            @if ($build->accepted == false && !is_null($build->building_id))
                                                <a href="{{ route('manager.buildings.view', ['glamping_id' => $glamping->id, 'building_id' => $build->id]) }}"
                                                   title="{{ trans('common_elements.btnView') }}" class="text-info">
                                                    {{ $build->name }} </a>
                                                @php $changedBuild = $glamping->buildings()->find($build->building_id); @endphp
                                                <a href="{{ route('manager.buildings.view', ['glamping_id' => $glamping->id, 'building_id' => $changedBuild->id]) }}"
                                                   title="{{ trans('common_elements.btnView') }}"> ({{ $changedBuild->name }})</a>
                                            @elseif($build->accepted == false)
                                                <a href="{{ route('manager.buildings.view', ['glamping_id' => $glamping->id, 'building_id' => $build->id]) }}"
                                                   title="{{ trans('common_elements.btnView') }}" class="text-info">
                                                    {{ $build->name }} </a>
                                            @else
                                                <a href="{{ route('manager.buildings.view', ['glamping_id' => $glamping->id, 'building_id' => $build->id]) }}"
                                                   title="{{ trans('common_elements.btnView') }}">
                                                    {{ $build->name }} </a>
                                            @endif
                                        </li>
                                    @endif
                                @endforeach
                            </ul>

                            <a class="pull-right text-primary" title="{{ trans('common_elements.viewAll') }}"
                               href="{{ route('manager.buildings.show', ['id' => $glamping->id]) }}">
                                <i class="material-icons text-info">visibility</i>
                            </a><br>
                        @endif
                    </td>
                    {{--<td></td>--}}
                    {{--<td></td>--}}
                    {{--<td></td>--}}
                    {{--<td></td>--}}
                    <td>
                        @if (!count($glamping->applications) > 0)
                            @if ($glamping->glamping && $glamping->accepted)
                                {{ trans('common_elements.wordYes') }}
                                @else
                                &mdash;
                            @endif
                        @else
                            @php $req_status = $glamping->lastApplicationStatus(); @endphp
                            @if ($req_status === ApplicationStatus::PENDING)
                                {{ trans('common_elements.wordInProgress') }}
                            @elseif ($req_status === ApplicationStatus::ACCEPTED)
                                {{ trans('common_elements.wordYes') }}
                            @else
                                {{ trans('common_elements.wordNo') }}
                            @endif
                        @endif
                    </td>
                    <td>
                        <a href="{{ route('manager.glampings.show', ['id' => $glamping->id]) }}"
                           title="{{ trans('common_elements.btnView') }}">
                            <i class="material-icons text-warning">visibility</i>
                        </a>
                        <a href="{{ route('manager.glampings.statistics', ['id' => $glamping->id]) }}"
                               title="{{ trans('common_elements.btnViewStatistics') }}">
                            <i class="material-icons text-pink">assessment</i>
                        </a>
                        @if (!count($glamping->applications) > 0 || $user->hasRole('admin') ||
                         $glamping->lastApplicationStatus() !== ApplicationStatus::PENDING)
                            <a href="{{ route('manager.glampings.edit', ['id' => $glamping->id]) }}"
                               title="{{ trans('common_elements.btnEdit') }}">
                                <i class="material-icons text-info">create</i>
                            </a>
                            @if ($user->hasRole('owner'))
                                @if (!count($glamping->applications) > 0 && count($glamping->buildings) > 0 && !$glamping->glamping ||
                                $glamping->accepted == true && count($glamping->buildings()->where('accepted', false)->get()) > 0 ||
                                $glamping->changedGlamping && $glamping->lastApplicationStatus() !== ApplicationStatus::PENDING)
                                    <a href="{{ route('manager.applications.send', ['glamping_id' => $glamping->id]) }}"
                                       title="{{ trans('common_elements.btnSendApp') }}">
                                        <i class="material-icons text-danger">send</i></a>
                                @endif
                                <a title="{{ trans('common_elements.editBusyness') }}" href="{{ route('manager.glampings.busyness', ['id' => $glamping->id]) }}">
                                    <i class="material-icons text-info">developer_board</i>
                                </a>
                                <a title="{{ trans('tables.glampingsListAddBuilding') }}"
                                   href="{{ route('manager.buildings.create', ['glamping_id' => $glamping->id]) }}">
                                    <i class="material-icons text-success">add box</i>
                                </a>
                            @endif
                        @endif
                    </td>
                </tr>
            @endif
        @endforeach
        </tbody>
    </table>
@endsection

@section('additional_scripts')
    <!-- Jquery DataTable Plugin Js -->
    <script src="{{ asset('theme/assets/plugins/jquery-datatable/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('theme/assets/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js') }}"></script>
    <script src="{{ asset('theme/assets/plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('theme/assets/plugins/jquery-datatable/extensions/export/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('theme/assets/plugins/jquery-datatable/extensions/export/jszip.min.js') }}"></script>
    <script src="{{ asset('theme/assets/plugins/jquery-datatable/extensions/export/pdfmake.min.js') }}"></script>
    <script src="{{ asset('theme/assets/plugins/jquery-datatable/extensions/export/vfs_fonts.js') }}"></script>
    <script src="{{ asset('theme/assets/plugins/jquery-datatable/extensions/export/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('theme/assets/plugins/jquery-datatable/extensions/export/buttons.print.min.js') }}"></script>
    <script src="{{ asset('theme/assets/js/pages/tables/jquery-datatable.js') }}"></script>
@endsection

