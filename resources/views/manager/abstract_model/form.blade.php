@extends('layouts.admin_dashboard')

@section('page_name'){{ $pageName or NULL }}@endsection

@section('bread_crumbs')
	@php 
    $role = Auth::guard('manager')->user()->hasRole('owner') ? 'owner' : 'admin'; 
    @endphp
    <li>
    	<a href="{{ route('manager.' . $role . '_dashboard') }}">
    		{{ trans('common_elements.home') }}
    	</a>
    </li>
    <li><a href="{{ route($modelIndexPath) }}">{{ trans('common_elements.' . $breadCrumbsActive) }}</a></li>
    <li class="active">{{ $pageName or NULL }}</li>
@endsection

@section('page_information')
    @if (isset($model))
        {{ $model->name }}
    @endif
@endsection

@section('content')
    @include('templates.form')
@endsection