@if (count($app->applicationable->buildings) > 0)
    <div class="row">
        <div class="header">
            <h2>
                {{ trans('common_elements.buildings') }}
            </h2>
        </div>
    </div>

    {{--Lang tabs--}}
    <ul class="nav nav-tabs" role="tablist">
        <li role="presentation" class="active">
            <a href=".build_ru" data-toggle="tab">RU</a>
        </li>
        <li role="presentation">
            <a href=".build_en" data-toggle="tab">EN</a>
        </li>
    </ul>
    @foreach($app->applicationable->buildings as $build)
        @if (!$build->building || !$build->building->accepted)
            <div class="row">
                <div class="col-sm-6">
                    <!-- Tab panes -->
                    <div class="tab-content">
                        <!--Tabs  ENGLISH --------------------------->
                        <div role="tabpanel" class="tab-pane fade build_en">
                            {{-- Name --}}
                            {{ trans('common_elements.nameInputLabel') }}
                            @if ($app->type === ApplicationType::CHANGE && $build->changedBuilding &&
                             $build->getTranslation('en')->name !== $build->changedBuilding ->getTranslation('en')
                             ->name)
                                <strong class="font-line-through text-danger"> {{ $build->getTranslation('en')
                                ->name or '&mdash;' }}</strong>
                                <strong> {{ $build->changedBuilding->getTranslation('en')->name or '&mdash;' }}
                                </strong>
                            @else
                                <strong> {{ $build->getTranslation('en')->name or '&mdash;' }}</strong>
                            @endif
                            <br>
                            {{-- Descriprion --}}
                            {{ trans('common_elements.descriptionInputLabel') }}
                            @if ($app->type === ApplicationType::CHANGE && $build->changedBuilding
                             && $build->getTranslation('en')->description !== $build->changedBuilding
                             ->getTranslation('en')->description)
                                <strong class="font-line-through text-danger"> {{ $build->getTranslation('en')
                                ->description or '&mdash;' }}</strong>
                                <strong> {{ $build->changedBuilding->getTranslation('en')->description or
                                 '&mdash;' }}</strong>
                            @else
                                <strong> {{ $build->getTranslation('en')->description or '&mdash;' }}</strong>
                            @endif
                            <br>
                            {{-- Other information --}}
                            {{ trans('common_elements.otherInformationInputLabel') }}
                            @if ($app->type === ApplicationType::CHANGE && $build->changedBuilding &&
                             $build->getTranslation('en')->other_information !== $build->changedBuilding
                            ->getTranslation('en')->other_information)
                                <strong class="font-line-through text-danger"> {{ $build->getTranslation('en')
                                ->other_information or '&mdash;' }}</strong>
                                <strong> {{ $build->changedBuilding->getTranslation('en')->other_information or
                                 '&mdash;' }}</strong>
                            @else
                                <strong> {{ $build->getTranslation('en')->other_information or '&mdash;' }}
                                </strong>
                            @endif
                            <br>
                        </div>

                        <!--Tabs RUSSIAN----------------------------->
                        <div role="tabpanel" class="tab-pane fade build_ru in active">
                            {{-- Name --}}
                            {{ trans('common_elements.nameInputLabel') }}
                            @if ($app->type === ApplicationType::CHANGE && $build->changedBuilding &&
                             $build->getTranslation('ru')->name !== $build->changedBuilding ->getTranslation('ru')
                             ->name)
                                <strong class="font-line-through text-danger"> {{ $build->getTranslation('ru')
                                ->name or '&mdash;' }}</strong>
                                <strong> {{ $build->changedBuilding->getTranslation('ru')->name or '&mdash;' }}
                                </strong>
                            @else
                                <strong> {{ $build->getTranslation('ru')->name or '&mdash;' }}</strong>
                            @endif
                            <br>
                            {{-- Descriprion --}}
                            {{ trans('common_elements.descriptionInputLabel') }}
                            @if ($app->type === ApplicationType::CHANGE && $build->changedBuilding
                             && $build->getTranslation('ru')->description !== $build->changedBuilding
                             ->getTranslation('ru')->description)
                                <strong class="font-line-through text-danger"> {{ $build->getTranslation('ru')
                                ->description or '&mdash;' }}</strong>
                                <strong> {{ $build->changedBuilding->getTranslation('ru')->description or
                                 '&mdash;' }}</strong>
                            @else
                                <strong> {{ $build->getTranslation('ru')->description or '&mdash;' }}</strong>
                            @endif
                            <br>
                            {{-- Other information --}}
                            {{ trans('common_elements.otherInformationInputLabel') }}
                            @if ($app->type === ApplicationType::CHANGE && $build->changedBuilding &&
                             $build->getTranslation('ru')->other_information !== $build->changedBuilding
                            ->getTranslation('ru')->other_information)
                                <strong class="font-line-through text-danger"> {{ $build->getTranslation('ru')
                                ->other_information or '&mdash;' }}</strong>
                                <strong> {{ $build->changedBuilding->getTranslation('ru')->other_information or
                                 '&mdash;' }}</strong>
                            @else
                                <strong> {{ $build->getTranslation('ru')->other_information or '&mdash;' }}
                                </strong>
                            @endif
                            <br>
                        </div>
                    </div>

                    @if ($app->type === ApplicationType::CHANGE && $build->changedBuilding)
                        @php
                        $buildType = $build->changedBuilding->building_type_id;
                        $amenities = $build->changedBuilding->amenities;
                        $capacity = $build->changedBuilding->capacity;
                        $count = $build->changedBuilding->count;
                        $options = $build->changedBuilding->buildingOptions;
                        $minNightsCount = $build->changedBuilding->min_nights_count;
                        $price = $build->changedBuilding->main_price;
                        @endphp
                    @else
                        @php
                        $buildType = $build->building_type_id;
                        $amenities = $build->amenities;
                        $capacity = $build->capacity;
                        $count = $build->count;
                        $options = $build->buildingOptions;
                        $minNightsCount = $build->min_nights_count;
                        $price = $build->main_price;
                        @endphp
                    @endif

                    {{-- Building Type --}}
                    {{ trans('common_elements.buildingType') }}:
                    <strong> {{ $buildType or '&mdash;' }}</strong><br>

                    {{-- Amenity block--}}
                    @if (count($amenities) > 0)
                        {{ trans('common_elements.amenities') }}:
                        @foreach($amenities as $amenity)
                            <strong> {{ $amenity->name }};</strong>
                        @endforeach
                        <br>
                    @endif

                    {{--Capacity block--}}
                    {{ trans('common_elements.bookTableCapacity') }}:
                    <strong> {{ $capacity or '&mdash;' }}</strong><br>

                    {{--Count block--}}
                    {{ trans('common_elements.bookTableCount') }}:
                    <strong> {{ $count or '&mdash;' }}</strong><br>

                    {{--Min nights count block--}}
                    {{ trans('common_elements.minNightsCountInputLabel') }}:
                    <strong> {{ $minNightsCount or '&mdash;' }}</strong><br>

                    {{--Main price block--}}
                    {{ trans('common_elements.mainPriceBuildingInputLabel') }}:
                    <strong> {{ $price or '&mdash;' }}</strong><br>

                    {{--Options block--}}
                    @if (count($options) > 0)
                        <br>
                        <table class="table table-bordered table-striped table-hover">
                            <thead>
                            <tr>
                                <th>{{ trans('common_elements.nameColumnName') }}</th>
                                <th class="sortable-false">{{ trans('common_elements.descriptionColumnName') }}</th>
                                <th>{{ trans('common_elements.bookTablePrice') }}</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($options as $option)
                                <tr>
                                    <td>
                                        @if ($option->hasTranslation('en'))
                                            {{ $option->getTranslation('en')->name }}
                                        @endif
                                        @if ($option->hasTranslation('ru'))
                                            | {{ $option->getTranslation('ru')->name }}
                                        @endif
                                    </td>
                                    <td>
                                        @if ($option->hasTranslation('en'))
                                            {{ $option->getTranslation('en')->description }}
                                        @endif
                                        @if ($option->hasTranslation('ru'))
                                            | {{ $option->getTranslation('ru')->description }}
                                        @endif
                                    </td>
                                    <td>{{ $option->price }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    @endif
                </div>

                {{-- Image block --}}
                @if ($app->type === ApplicationType::CHANGE && $build->changedBuilding)
                    @php $images = $build->changedBuilding->media()->get(); @endphp
                @else
                    @php $images = $build->media()->get(); @endphp
                @endif
                <div class="col-sm-6">
                    @if (count($images) > 0)
                        @foreach($images as $image)
                            <img class="glamping-image" src="{{ asset($image->getUrl()) }}">
                        @endforeach
                    @endif
                </div>
            </div>
            <div class="col-xs-12 bg-info"></div>
        @endif
    @endforeach
@endif