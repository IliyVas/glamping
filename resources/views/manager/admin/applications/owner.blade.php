<div class="member-card verified">
    <div class="s-profile">
        {{--Personal Data--}}
        {{ trans('auth.first_name') . ': ' . $model->first_name }}<br>
        {{ trans('auth.patronymic') . ': ' . $model->patronymic }}<br>
        {{ trans('auth.last_name') . ': ' . $model->last_name }}<br>
        {{ trans('auth.phone') . ': ' . $model->phone }}<br>
        {{ trans('auth.email') . ': ' . $model->email }}<br>

        {{--Company--}}
        @if (!is_null(Auth::user()->company))
            {{ trans('common_elements.nameInputLabel') . ' ' . Auth::user()->company->name }}<br>
            {{ trans('common_elements.mainStateRegisterNumber') . ': ' . Auth::user()->company->registration_number }}<br>
        @endif
    </div>
</div>