<div class="row">
    <div class="col-xs-12">
        {{--Lang tabs--}}
        <ul class="nav nav-tabs" role="tablist">
            @php $enActive=''; $ruActive='';@endphp
            @if ($app->applicationable->hasTranslation('en') && !$app->applicationable->hasTranslation('ru'))
                <li role="presentation" class="active">
                    <a href="#home_with_icon_title" data-toggle="tab">EN</a>
                </li>
                @php $enActive='in active'; @endphp
            @endif
            @if ($app->applicationable->hasTranslation('ru') && !$app->applicationable->hasTranslation('en'))
                @php $ruActive='in active'; @endphp
                <li role="presentation" class="active">
                    <a href="#profile_with_icon_title" data-toggle="tab">RU</a>
                </li>
            @endif
            @if ($app->applicationable->hasTranslation('en') && $app->applicationable->hasTranslation('ru'))
                @php $ruActive='in active'; @endphp
                <li role="presentation" class="active">
                    <a href="#profile_with_icon_title" data-toggle="tab">RU</a>
                </li>
                <li role="presentation">
                    <a href="#home_with_icon_title" data-toggle="tab">EN</a>
                </li>
            @endif
        </ul>
    </div>

    <div class="col-sm-4">
        <!-- Tab panes -->
        <div class="tab-content">
            <!--Tabs  ENGLISH --------------------------->
            <div role="tabpanel" class="tab-pane fade {{ $enActive }}" id="home_with_icon_title">
                {{-- Name --}}
                {{ trans('common_elements.nameInputLabel') }}
                @if ($app->type === ApplicationType::CHANGE && $app->applicationable
                ->changedGlamping && $app->applicationable->getTranslation('en')->name !==
                 $app->applicationable->changedGlamping ->getTranslation('en')->name)
                    <strong class="font-line-through text-danger"> {{ $app->applicationable
                    ->getTranslation('en')->name or '&mdash;' }}</strong>
                    <strong> {{ $app->applicationable->changedGlamping->getTranslation('en')->name or
                     '&mdash;' }}</strong>
                @else
                    <strong> {{ $app->applicationable->getTranslation('en')->name or '&mdash;' }}</strong>
                @endif
                <br>
                {{-- Descriprion --}}
                {{ trans('common_elements.descriptionInputLabel') }}
                @if ($app->type === ApplicationType::CHANGE && $app->applicationable
                ->changedGlamping && $app->applicationable->getTranslation('en')->description
                 !== $app->applicationable->changedGlamping->getTranslation('en')->description)
                    <strong class="font-line-through text-danger"> {{ $app->applicationable
                    ->getTranslation('en')->description or '&mdash;' }}</strong>
                    <strong> {{ $app->applicationable->changedGlamping->getTranslation('en')->description
                     or '&mdash;' }}</strong>
                @else
                    <strong> {{ $app->applicationable->getTranslation('en')->description or
                     '&mdash;' }}</strong>
                @endif
                <br>
                {{-- Other information --}}
                {{ trans('common_elements.otherInformationInputLabel') }}
                @if ($app->type === ApplicationType::CHANGE && $app->applicationable
                ->changedGlamping && $app->applicationable->getTranslation('en')
                ->other_information !== $app->applicationable->changedGlamping
                ->getTranslation('en')->other_information)
                    <strong class="font-line-through text-danger"> {{ $app->applicationable
                    ->getTranslation('en')->other_information or '&mdash;' }}</strong>
                    <strong> {{ $app->applicationable->changedGlamping->getTranslation('en')
                    ->other_information or '&mdash;' }}</strong>
                @else
                    <strong> {{ $app->applicationable->getTranslation('en')->other_information
                     or '&mdash;' }}</strong>
                @endif
                <br>
                {{-- Country --}}
                {{ trans('common_elements.countryInputLabel') }}:
                @if ($app->type === ApplicationType::CHANGE && $app->applicationable
                ->changedGlamping && $app->applicationable
                    ->getTranslation('en')->country !== $app->applicationable
                    ->changedGlamping->getTranslation('en')->country)
                    <strong class="font-line-through text-danger"> {{ $app->applicationable
                    ->getTranslation('en')->country or '&mdash;' }}</strong>
                    <strong> {{ $app->applicationable->changedGlamping->getTranslation('en')
                    ->country or '&mdash;' }}</strong>
                @else
                    <strong> {{ $app->applicationable->getTranslation('en')->country
                     or '&mdash;' }}</strong>
                @endif
                <br>
                {{-- Region --}}
                {{ trans('common_elements.regionInputLabel') }}
                @if ($app->type === ApplicationType::CHANGE && $app->applicationable
                ->changedGlamping && $app->applicationable
                    ->getTranslation('en')->region !== $app->applicationable
                    ->changedGlamping->getTranslation('en')->region)
                    <strong class="font-line-through text-danger"> {{ $app->applicationable
                    ->getTranslation('en')->region or '&mdash;' }}</strong>
                    <strong> {{ $app->applicationable->changedGlamping->getTranslation('en')
                    ->region or '&mdash;' }}</strong>
                @else
                    <strong> {{ $app->applicationable->getTranslation('en')->region
                     or '&mdash;' }}</strong>
                @endif
                <br>
                {{-- Glamping adress --}}
                {{ trans('common_elements.glampingAddressInputLabel') }}
                @if ($app->type === ApplicationType::CHANGE && $app->applicationable
                ->changedGlamping && $app->applicationable
                    ->getTranslation('en')->glamping_address !== $app->applicationable
                    ->changedGlamping->getTranslation('en')->glamping_address)
                    <strong class="font-line-through text-danger"> {{ $app->applicationable
                    ->getTranslation('en')->glamping_address or '&mdash;' }}</strong>
                    <strong> {{ $app->applicationable->changedGlamping->getTranslation('en')
                    ->glamping_address or '&mdash;' }}</strong>
                @else
                    <strong> {{ $app->applicationable->getTranslation('en')->glamping_address
                     or '&mdash;' }}</strong>
                @endif
                <br>
            </div>

            <!--Tabs RUSSIAN----------------------------->
            <div role="tabpanel" class="tab-pane fade {{ $ruActive }}" id="profile_with_icon_title">
                {{-- Name --}}
                {{ trans('common_elements.nameInputLabel') }}
                @if ($app->type === ApplicationType::CHANGE && $app->applicationable
                ->changedGlamping && $app->applicationable
                    ->getTranslation('ru')->name !== $app->applicationable->changedGlamping
                    ->getTranslation('ru')->name)
                    <strong class="font-line-through text-danger"> {{ $app->applicationable
                    ->getTranslation('ru')->name or '&mdash;' }}</strong>
                    <strong> {{ $app->applicationable->changedGlamping->getTranslation('ru')->name or
                     '&mdash;' }}</strong>
                @else
                    <strong> {{ $app->applicationable->getTranslation('ru')->name or '&mdash;' }}</strong>
                @endif
                <br>
                {{-- Descriprion --}}
                {{ trans('common_elements.descriptionInputLabel') }}
                @if ($app->type === ApplicationType::CHANGE && $app->applicationable
                ->changedGlamping && $app->applicationable
                    ->getTranslation('ru')->description !== $app->applicationable
                    ->changedGlamping->getTranslation('ru')->description)
                    <strong class="font-line-through text-danger"> {{ $app->applicationable
                    ->getTranslation('ru')->description or '&mdash;' }}</strong>
                    <strong> {{ $app->applicationable->changedGlamping->getTranslation('ru')->description
                     or '&mdash;' }}</strong>
                @else
                    <strong> {{ $app->applicationable->getTranslation('ru')->description or
                     '&mdash;' }}</strong>
                @endif
                <br>
                {{-- Other information --}}
                {{ trans('common_elements.otherInformationInputLabel') }}
                @if ($app->type === ApplicationType::CHANGE && $app->applicationable
                ->changedGlamping && $app->applicationable
                    ->getTranslation('ru')->other_information !== $app->applicationable
                    ->changedGlamping->getTranslation('ru')->other_information)
                    <strong class="font-line-through text-danger"> {{ $app->applicationable
                    ->getTranslation('ru')->other_information or '&mdash;' }}</strong>
                    <strong> {{ $app->applicationable->changedGlamping->getTranslation('ru')
                    ->other_information or '&mdash;' }}</strong>
                @else
                    <strong> {{ $app->applicationable->getTranslation('ru')->other_information
                     or '&mdash;' }}</strong>
                @endif
                <br>
                {{-- Country --}}
                {{ trans('common_elements.countryInputLabel') }}:
                @if ($app->type === ApplicationType::CHANGE && $app->applicationable
                ->changedGlamping && $app->applicationable
                    ->getTranslation('en')->country !== $app->applicationable
                    ->changedGlamping->getTranslation('ru')->country)
                    <strong class="font-line-through text-danger"> {{ $app->applicationable
                    ->getTranslation('ru')->country or '&mdash;' }}</strong>
                    <strong> {{ $app->applicationable->changedGlamping->getTranslation('ru')
                    ->country or '&mdash;' }}</strong>
                @else
                    <strong> {{ $app->applicationable->getTranslation('ru')->country
                     or '&mdash;' }}</strong>
                @endif
                <br>
                {{-- Region --}}
                {{ trans('common_elements.regionInputLabel') }}
                @if ($app->type === ApplicationType::CHANGE && $app->applicationable
                ->changedGlamping && $app->applicationable
                    ->getTranslation('ru')->region !== $app->applicationable
                    ->changedGlamping->getTranslation('ru')->region)
                    <strong class="font-line-through text-danger"> {{ $app->applicationable
                    ->getTranslation('ru')->region or '&mdash;' }}</strong>
                    <strong> {{ $app->applicationable->changedGlamping->getTranslation('ru')
                    ->region or '&mdash;' }}</strong>
                @else
                    <strong> {{ $app->applicationable->getTranslation('ru')->region
                     or '&mdash;' }}</strong>
                @endif
                <br>
                {{-- Glamping adress --}}
                {{ trans('common_elements.glampingAddressInputLabel') }}
                @if ($app->type === ApplicationType::CHANGE && $app->applicationable
                ->changedGlamping && $app->applicationable
                    ->getTranslation('ru')->glamping_address !== $app->applicationable
                    ->changedGlamping->getTranslation('ru')->glamping_address)
                    <strong class="font-line-through text-danger"> {{ $app->applicationable
                    ->getTranslation('ru')->glamping_address or '&mdash;' }}</strong>
                    <strong> {{ $app->applicationable->changedGlamping->getTranslation('ru')
                    ->glamping_address or '&mdash;' }}</strong>
                @else
                    <strong> {{ $app->applicationable->getTranslation('ru')->glamping_address
                     or '&mdash;' }}</strong>
                @endif
                <br>
            </div>

            @if ($app->type === ApplicationType::CHANGE && $app->applicationable
            ->changedGlamping)
                @php $long = $app->applicationable->changedGlamping->longitude;
                $lat = $app->applicationable->changedGlamping->latitude;@endphp
            @else
                @php $long = $app->applicationable->longitude;
                $lat = $app->applicationable->latitude; @endphp
            @endif

            <input type="hidden" id="longitude_input" value="{{ $long }}">
            <input type="hidden" id="latitude_input" value="{{ $lat }}">

            {{-- Latitude --}}
            {{ trans('common_elements.latitudeInputLabel') }}
            @if ($app->type === ApplicationType::CHANGE && $app->applicationable
                ->changedGlamping && $app->applicationable->latitude !==
                 $app->applicationable->changedGlamping->latitude)
                <strong class="font-line-through text-danger"> {{ $app->applicationable
                ->latitude or '&mdash;' }}</strong>
                <strong> {{ $app->applicationable->changedGlamping->latitude or '&mdash;' }}
                </strong>
            @else
                <strong> {{ $app->applicationable->latitude or '&mdash;' }}</strong>
            @endif
            <br>

            {{-- Longtitude --}}
            {{ trans('common_elements.longitudeInputLabel')}}
            @if ($app->type === ApplicationType::CHANGE && $app->applicationable
                ->changedGlamping && $app->applicationable->longitude !==
                 $app->applicationable->changedGlamping->longitude)
                <strong class="font-line-through text-danger"> {{ $app->applicationable
                ->longitude or '&mdash;' }}</strong>
                <strong> {{ $app->applicationable->changedGlamping->longitude or '&mdash;' }}
                </strong>
            @else
                <strong> {{ $app->applicationable->longitude or '&mdash;' }}</strong>
            @endif
            <br>

            {{-- Free retutn --}}
            {{ trans('common_elements.freeReturnDays')}}:
            @if ($app->type === ApplicationType::CHANGE && $app->applicationable
                ->changedGlamping && $app->applicationable->free_return !==
                 $app->applicationable->changedGlamping->free_return)
                <strong class="font-line-through text-danger"> {{ $app->applicationable
                ->free_return or '&mdash;' }}</strong>
                <strong> {{ $app->applicationable->changedGlamping->free_return or '&mdash;' }}
                </strong>
            @else
                <strong> {{ $app->applicationable->free_return or '&mdash;' }}</strong>
            @endif
            <br>

            {{-- Max age free child --}}
            {{ trans('common_elements.maxChildAgeInputLabel') }}
            @if ($app->type === ApplicationType::CHANGE && $app->applicationable
                ->changedGlamping && $app->applicationable->max_child_age !==
                 $app->applicationable->changedGlamping->max_child_age)
                <strong class="font-line-through text-danger"> {{ $app->applicationable
                ->max_child_age or '&mdash;' }}</strong>
                <strong> {{ $app->applicationable->changedGlamping->max_child_age or '&mdash;' }}
                </strong>
            @else
                <strong> {{ $app->applicationable->max_child_age or '&mdash;' }}</strong>
            @endif
            <br>

            {{-- Arrival time --}}
            {{ trans('common_elements.arrivalTimeInputLabel') }}
            @if ($app->type === ApplicationType::CHANGE && $app->applicationable
                ->changedGlamping && $app->applicationable->arrivel_time !==
                 $app->applicationable->changedGlamping->arrivel_time)
                <strong class="font-line-through text-danger"> {{ $app->applicationable
                ->arrivel_time or '&mdash;' }}</strong>
                <strong> {{ $app->applicationable->changedGlamping->arrivel_time or '&mdash;' }}
                </strong>
            @else
                <strong> {{ $app->applicationable->arrivel_time or '&mdash;' }}</strong>
            @endif
            <br>

            {{-- Check out time --}}
            {{ trans('common_elements.checkOutTimeInputLabel') }}
            @if ($app->type === ApplicationType::CHANGE && $app->applicationable
                ->changedGlamping && $app->applicationable->check_out_time !==
                 $app->applicationable->changedGlamping->check_out_time)
                <strong class="font-line-through text-danger"> {{ $app->applicationable
                ->check_out_time or '&mdash;' }}</strong>
                <strong> {{ $app->applicationable->changedGlamping->check_out_time or '&mdash;' }}
                </strong>
            @else
                <strong> {{ $app->applicationable->check_out_time or '&mdash;' }}</strong>
            @endif
            <br>
        </div>
    </div>

    <div class="col-sm-8">
        <div id="map" style="height:300px; width: 100%" ></div>
    </div>
</div>

@if ($app->type === ApplicationType::CHANGE && $app->applicationable->changedGlamping )
    @php $model =  $app->applicationable->changedGlamping; @endphp
@else
    @php $model =  $app->applicationable; @endphp
@endif

{{-- Glamping group block--}}
@if (count($model->glampingGroups()->get()) > 0)
    {{ trans('common_elements.glampingGroups') }}:
    @foreach($model->glampingGroups()->get() as $glampingGroup)
        <strong> {{ $glampingGroup->name }};</strong>
    @endforeach
    <br>
@endif

{{-- Impression block--}}
@if (count($model->impressions()->get()) > 0)
    {{ trans('common_elements.impressions') }}:
    @foreach($model->impressions()->get() as $impression)
        <strong> {{ $impression->name }};</strong>
    @endforeach
    <br>
@endif

{{-- Amenity block--}}
@if (count($model->amenities()->get()) > 0)
    {{ trans('common_elements.amenities') }}:
    @foreach($model->amenities()->get() as $amenity)
        <strong> {{ $amenity->name }};</strong>
    @endforeach
    <br>
@endif

{{-- Service block--}}
@if (count($model->services()->get()) > 0)
    {{ trans('common_elements.services') }}:
    @foreach($model->services()->get() as $service)
        <strong> {{ $service->name }};</strong>
    @endforeach
    <br>
@endif
