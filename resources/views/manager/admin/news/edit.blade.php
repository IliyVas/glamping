@extends('layouts.admin_dashboard')

@section('additional_styles')
    <!-- JQuery DataTable Css -->
    <link href="{{ asset('theme/assets/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css') }}" rel="stylesheet">
@endsection

@section('page_name'){{ trans('common_elements.newsTitle') }}@endsection

@section('bread_crumbs')
    <li><a href="{{ route('manager.admin_dashboard') }}">{{ trans('common_elements.home') }}</a></li>
    <li>
        <a href="{{route('manager.news.index')}}">
            {{ trans('common_elements.newsTitle') }}
        </a>
    </li>
    <li class="active">{{$news->name}}</li>
@endsection


@section('content_name')
    <div class="header">
        <h2>{{ $news->name }}</h2>
    </div>
@endsection

@section('content')
    <div class="wrapper-show-page">
        <form action="{{route('manager.news.update',['id' => $news->id])}}" method="post" enctype="multipart/form-data">
            {{csrf_field()}}
            {{method_field('PUT')}}
            <ul class="nav nav-tabs tab-nav-right" role="tablist">
                <li role="presentation" class="active">
                    <a href="#rus" data-toggle="tab" aria-expanded="true">Rus</a>
                </li>
                <li role="presentation" class="">
                    <a href="#eng" data-toggle="tab" aria-expanded="false">Eng</a>
                </li>
            </ul>
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane fade active in" id="rus">
                    <div class="row">
                        <div class="col-sm-4 col-xs-12">
                            <div class="form-group form-float">
                                <div class="form-line focused">
                                    <input type="text" class="form-control" name="name:ru" value="{{$news->translate('ru')->name}}">
                                    <label class="form-label">{{trans('common_elements.newsName')}}</label>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4 col-xs-12">
                            <div class="form-group form-float">
                                <div class="form-line focused">
                                    <input type="text" class="form-control" name="title:ru" value="{{$news->translate('ru')->title}}">
                                    <label class="form-label">{{trans('common_elements.newsNameTitle')}}</label>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4 col-xs-12">
                            <div class="form-group form-float">
                                <div class="form-line focused">
                                    <input type="text" class="form-control" name="keywords:ru" value="{{$news->translate('ru')->keywords}}">
                                    <label class="form-label">{{trans('common_elements.newsKeywords')}}</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6 col-xs-12">
                            <div class="form-group form-float">
                                <div class="form-line focused">
                                    <label class="form-label">{{trans('common_elements.newsDescription')}}</label>
                                    <textarea name="description:ru" class="form-control" id="desc_ru" required>{{$news->translate('ru')->description }}</textarea>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6 col-xs-12">
                            <div class="form-group form-float">
                                <div class="form-line focused">
                                    <label class="form-label">{{trans('common_elements.newsSeoDescription')}}</label>
                                    <textarea id="seo_desc_ru" class="form-control" name="seo_description:ru" required>{{  $news->translate('ru')->seo_description}}</textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div role="tabpanel" class="tab-pane fade" id="eng">
                    <div class="row">
                        <div class="col-sm-4 col-xs-12">
                            <div class="form-group form-float">
                                <div class="form-line focused">
                                    <input type="text" class="form-control" name="name:en" value="{{$news->translate('en')->name}}" required>
                                    <label class="form-label">{{trans('common_elements.newsName')}}</label>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4 col-xs-12">
                            <div class="form-group form-float">
                                <div class="form-line focused">
                                    <input type="text" class="form-control" name="title:en" value="{{$news->translate('en')->title}}" required>
                                    <label class="form-label">{{trans('common_elements.newsNameTitle')}}</label>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4 col-xs-12">
                            <div class="form-group form-float">
                                <div class="form-line focused">
                                    <input type="text" class="form-control" name="keywords:en" value="{{$news->translate('en')->keywords}}">
                                    <label class="form-label">{{trans('common_elements.newsKeywords')}}</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6 col-xs-12">
                            <div class="form-group form-float">
                                <div class="form-line focused">
                                    <label class="form-label">{{trans('common_elements.newsDescription')}}</label>
                                    <textarea id="desc_en" class="form-control" name="description:en">{{$news->translate('en')->description}}</textarea>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6 col-xs-12">
                            <div class="form-group form-float">
                                <div class="form-line focused">
                                    <label class="form-label">{{trans('common_elements.newsSeoDescription')}}</label>
                                    <textarea id="seo_desc_en" class="form-control" name="seo_description:en">{{$news->translate('en')->seo_description}}</textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <div class="form-group">

                        <img id="prev" src="{{!is_null($thumb) ? asset($thumb->getUrl()) : '#'}}"
                        @if(!is_null($thumb))
                            class="active"
                        @endif
                        >
                        <input type="file" name="mainImg" class="fileInput" id="imgInp">
                    </div>
                </div>
            </div>


            <div>
                <button class="btn btn-raised waves-effect bg-indigo" type="submit">
                    Обновить
                </button>
            </div>
        </form>
    </div>
@endsection

@section('additional_scripts')
    <!-- Jquery DataTable Plugin Js -->
    <script src="{{ asset('theme/assets/plugins/jquery-datatable/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('theme/assets/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js') }}"></script>
    <script src="{{ asset('theme/assets/plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('theme/assets/plugins/jquery-datatable/extensions/export/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('theme/assets/plugins/jquery-datatable/extensions/export/jszip.min.js') }}"></script>
    <script src="{{ asset('theme/assets/plugins/jquery-datatable/extensions/export/pdfmake.min.js') }}"></script>
    <script src="{{ asset('theme/assets/plugins/jquery-datatable/extensions/export/vfs_fonts.js') }}"></script>
    <script src="{{ asset('theme/assets/plugins/jquery-datatable/extensions/export/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('theme/assets/plugins/jquery-datatable/extensions/export/buttons.print.min.js') }}"></script>
    <script src="{{ asset('theme/assets/js/pages/tables/jquery-datatable.js') }}"></script>
    <script src="{{ mix('js/confirm_alert.js') }}"></script>
    <script src="{{asset('js/news/create.js')}}"></script>
@endsection
