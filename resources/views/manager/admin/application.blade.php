@extends('layouts.admin_dashboard')

@section('page_name'){{ $app->getType() }}@endsection

@section('bread_crumbs')
    <li><a href="{{ route('manager.admin_dashboard') }}">{{ trans('common_elements.home') }}</a></li>
    @if (!$app->is_viewed)
        <li><a href="{{ route('manager.applications.index') }}">{{ trans('common_elements.applications') }}</a></li>
    @else
         <li><a href="{{ route('manager.applications.archive') }}">{{ trans('common_elements.applicationsArchive') }}</a></li>
    @endif
    <li class="active">{{ $app->getType() }}</li>
@endsection

@section('page_information')
    {{ trans('common_elements.applicationDateColumnName') . ': ' .
     date_create($app->created_at)->format('d.m.Y H:i') }}
@endsection

@section('content_name')
    <div class="header">
        <h2>{{ $app->getDescription() }}</h2>
    </div>
@endsection

@section('content')
    @if ($app->type !== ApplicationType::REGISTER && $app->type !== ApplicationType::EDIT_PROFILE)
        @if ($app->applicationable->changedGlamping && $app->type === ApplicationType::CHANGE)
            @php $model = $app->applicationable->changedGlamping; @endphp
        @else
            @php $model = $app->applicationable; @endphp
        @endif
    @else
        @php $model = $app->applicationable; @endphp
    @endif

     {{--Image block --}}
    @if (count($model->media()->get()) > 0)
        <div class="col-sm-8">
            <div class="row">
                @foreach($model->media()->get() as $image)
                    <img class="glamping-image" src="{{ asset($image->getUrl()) }}">
                @endforeach
            </div>
        </div>
    @endif

    {{-- Applicatiopn information block --}}
    <div class="col-sm-4 pull-right alert bg-blue-grey">
        <strong>{{ trans('common_elements.applicationDateColumnName') . ': '}}</strong>
        {{ date_create($app->created_at)->format('d.m.Y H:i') }}<br>
        @if ($app->is_viewed)
            <strong>{{ trans('common_elements.reviewDateColumnName') . ': '}}</strong>
            {{ date_create($app->updated_at)->format('d.m.Y H:i') }}<br>
            <strong>{{ trans('common_elements.reviewerColumnName') . ': '}}</strong>
            {{ $reviewer->first_name . ' ' . $reviewer->last_name }}<br>
            <strong>{{ trans('common_elements.statusColumnName') . ': '}}</strong>
            {{ trans('applications.' . $app->status) }}
            @if (!empty($app->answer) )
                <br><strong>{{ trans('applications.answer') . ':'}}</strong>
                {{ $app->answer }}
            @endif
        @endif
    </div>

    @if ($app->type !== ApplicationType::REGISTER && $app->type !== ApplicationType::EDIT_PROFILE)
        @include('manager.admin.applications.glamping')
        @include('manager.admin.applications.building')
    @else
        @include('manager.admin.applications.owner')
    @endif

    @if (!$app->is_viewed)
        <form class="form" method="post" action="{{ route('manager.applications.update', ['id' => $app->id]) }}">
            {{ csrf_field() }}
            {{ method_field('PATCH') }}

            <div class="form-group">
                <div class="form-line">
                    <textarea name="answer" placeholder="{{ trans('applications.answer') }}" class="form-control" rows="4"></textarea>
                </div>
                <input type="submit" name="approve" value="{{ trans('applications.accept') }}"
                       class="btn btn-raised btn-success m-t-15 waves-effect pull-right">
                <input type="submit" value="{{ trans('applications.reject') }}" name="reject"
                       class="btn btn-raised btn-danger m-t-15 waves-effect pull-right">
            </div>
        </form>
    @endif
@endsection

@section('additional_scripts')
    <script src="//api-maps.yandex.ru/2.1/?lang={{ trans('common_elements.langValue') }}" type="text/javascript"></script>
    <script src="{{ mix('js/map.js') }}" type="text/javascript"></script>
@endsection