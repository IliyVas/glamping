@extends('layouts.owner_dashboard')

@section('bread_crumbs')
    <li><a href="{{ route('manager.admin_dashboard') }}">{{ trans('common_elements.home') }}</a></li>
    <li class="active">{{ trans('common_elements.editProfileButton') }}</li>
@endsection

@section('page_name'){{ trans('common_elements.editProfileButton') }}@endsection

@section('page_description'){{ trans('common_elements.userAccountMessage') }}@endsection

@section('content')
    @php $star = '<strong class="text-danger">*</strong>'; @endphp
    <div class="row">
        <div class="col-sm-6">
            <div class="row">
                <div class="header">
                    <h2> {{ trans('common_elements.basicInformationFormBlock') }}:</h2>
                </div>
            </div>
            {!! Form::model(Auth::user(), ['route' => 'manager.admin-account.update', 'enctype' => 'multipart/form-data',]) !!}
            {{-- Image block --}}
            <div class="row form-group">
                <div class="col-xs-6">
                    <div class="thumb-xl member-thumb">
                        <img class="img-circle" id="mainImg" alt="Image" src="
                        @if(!is_null(Auth::user()->media()->first())) {{ Auth::user()->media()->first()->getUrl() }} @else
                        {{ asset('images/account.png') }} @endif ">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    {!! Form::file('image', ['id' => 'image']) !!}
                </div>
            </div>
            {{-- First name --}}
            <div class="form-group">
                {!! Form::label('first_name', trans('auth.first_name')) !!}:
                {!! $star !!}
                <div class="form-line">
                    {!! Form::text('first_name', null, ['class' => 'form-control', 'required']) !!}
                </div>
            </div>

            {{-- Patronymic --}}
            <div class="form-group">
                {!! Form::label('patronymic', trans('auth.patronymic')) !!}:
                <div class="form-line">
                    {!! Form::text('patronymic', null, ['class' => 'form-control']) !!}
                </div>
            </div>

            {{-- Last name --}}
            <div class="form-group">
                {!! Form::label('last_name', trans('auth.last_name')) !!}:
                {!! $star !!}
                <div class="form-line">
                    {!! Form::text('last_name', null, ['class' => 'form-control', 'required']) !!}
                </div>
            </div>

            {!! Form::submit(trans('common_elements.btnSave'), ['class' => 'btn btn-raised btn-info m-t-15 waves-effect pull-right']) !!}
            {!! Form::close() !!}
        </div>

        <div class="col-sm-6">
            <div class="row">
                <div class="header">
                    <h2> {{ trans('auth.changeEmail') }}:</h2>
                </div>
            </div>
            {!! Form::open(['route' => 'manager.admin-email.change']) !!}
            {{-- Email --}}
            <div class="form-group">
                {!! Form::label('email', trans('auth.new_email')) !!}:
                {!! $star !!}
                <div class="form-line">
                    {!! Form::email('email', null, ['class' => 'form-control', 'required']) !!}
                </div>
            </div>

            {!! Form::submit(trans('common_elements.btnSave'), ['class' => 'btn btn-raised btn-info m-t-15 waves-effect pull-right']) !!}
            {!! Form::close() !!}
        </div>
    </div>
@endsection

@section('additional_scripts')
    <script src="{{ mix('js/icon.js') }}" type="text/javascript"></script>
@endsection