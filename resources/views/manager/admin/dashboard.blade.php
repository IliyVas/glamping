@extends('layouts.admin_dashboard')

@section('page_name')
    {{ Auth::user()->first_name . ' ' .  Auth::user()->last_name}}
@endsection

@section('page_information')
    {{ Auth::user()->email }}
@endsection

@section('content_name')
    <div class="header">
        <h2>{{ trans('messages.welcome') . ', ' . Auth::user()->first_name }}!</h2>
    </div>
@endsection