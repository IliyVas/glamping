@if ($user->hasRole('admin'))
    @php $role = 'admin'; @endphp
@else
    @php $role = 'owner'; @endphp
@endif

@section('additional_styles')
    <!-- JQuery DataTable Css -->
    <link href="{{ asset('theme/assets/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css') }}" rel="stylesheet">
@endsection

@extends('layouts.' . $role . '_dashboard')

@section('page_name'){{ trans('common_elements.statistics') }}@endsection

@section('page_information')
    @if (isset($glamping))
        {{ $glamping->name . ' (' . $glamping->country . ', '  . $glamping->region . ')'}}
    @endif
@endsection

@section('bread_crumbs')
    @if ($user->hasRole('admin'))
        <li><a href="{{ route('manager.admin_dashboard') }}">{{ trans('common_elements.home') }}</a></li>
        <li><a href="{{ route('manager.glampings.index') }}">{{ trans('common_elements.glampings') }}</a></li>
    @else
        <li><a href="{{ route('manager.owner_dashboard') }}">{{ trans('common_elements.home') }}</a></li>
        <li><a href="{{ route('manager.glampings.index') }}">{{ trans('common_elements.showUsersGlampings') }}</a></li>
    @endif
    @if (isset($glamping))
        <li><a href="{{ route('manager.glampings.show', ['id' => $glamping->id]) }}">{{ $glamping->name }}</a></li>
        <li class="active">{{ trans('common_elements.statistics') }}</li>
    @endif
@endsection

@section('content_name')
    <div class="header">
        <h2>
            @if (isset($glamping))
                {{ $glamping->name }}
            @endif
        </h2>
    </div>
@endsection

@section('content')
    @if (isset($glamping))
        <div class="row">
            {{-- Commission block --}}
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="card info-box-4 bg-red hover-zoom-effect">
                    <div class="icon"> <i class="material-icons col-blue">attach_money</i> </div>
                    <div class="content">
                        <div class="text">{{ mb_strtoupper(trans('common_elements.commissionInputLabel')) }}</div>
                        <div class="number">{{ $glamping->commission . '%' }}</div>
                    </div>
                </div>
            </div>
            {{-- Receipts block --}}
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="card info-box-2 bg-grey hover-expand-effect">
                    <div class="content">
                        <div class="text">{{ mb_strtoupper(trans('tables.receipts')) }}</div>
                        <div class="number">{{ $glamping->getReceipts($fromDate, $toDate) }}</div>
                    </div>
                </div>
            </div>
            {{-- Booking rating block --}}
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="card info-box-2 bg-light-green hover-expand-effect">
                    <div class="content">
                        <div class="text">{{ mb_strtoupper(trans('tables.rating')) }}</div>
                        <div class="number">{{ $glamping->getAverageRating() }}</div>
                    </div>
                </div>
            </div>
            {{-- Booking count block --}}
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="card info-box hover-expand-effect">
                    <div class="icon bg-red"><i class="zmdi zmdi-shopping-cart"></i></div>
                    <div class="content">
                        <div class="text">{{ mb_strtoupper(trans('booking.allBookings')) }}</div>
                        <div class="number count-to" data-from="0" data-to="125" data-speed="1000" data-fresh-interval="20">
                            {{ count($glamping->bookings) }}
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-6 pull-right text-right">
                {!! Form::open(['route' => ['manager.glampings.statistics', $glamping->id]]) !!}
                {{-- From date --}}
                {!! Form::label('from_date', trans('common_elements.wordFrom')) !!}:
                {!! Form::date('from_date', $fromDate) !!}
                {{-- To date --}}
                {!! Form::label('to_date', trans('common_elements.wordTo')) !!}:
                {!! Form::date('to_date', $toDate) !!}
                {!! Form::submit(trans('common_elements.btnApply'), ['class' => 'btn btn-raised btn-info btn-sm']) !!}
                {!! Form::close() !!}
            </div>
        </div>

        {{--Building table--}}
        <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
            <thead><tr> @include('manager.templates.statistic_build_grid_th') </tr></thead>
            <tfoot><tr> @include('manager.templates.statistic_build_grid_th') </tr></tfoot>
            <tbody>
            @if (count($glamping->buildings) > 0)
                @foreach ($glamping->buildings as $building)
                    <tr>
                        <td>{{ $building->name }}</td>
                        <td>{{ $building->getBuildingConfirmedBookingCount($fromDate, $toDate) }}</td>
                        <td>{{ $building->getBuildingReceipts($fromDate, $toDate) }}</td>
                        <td>{{ $building->getBuildingCanceledBookingCount($fromDate, $toDate) }}</td>
                        <td>{{ $building->getAverageRating() }}</td>
                    </tr>
                @endforeach
            @endif
            </tbody>
        </table>

        {{--Client table--}}
        @if ($glamping->getClientCountries() !== null)
        <div class="row"><div class="header"><h2>{{ trans('tables.clients') }}</h2></div></div>
        <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
            <thead><tr> @include('manager.templates.statistics_client_grid_th') </tr></thead>
            <tfoot><tr> @include('manager.templates.statistics_client_grid_th') </tr></tfoot>
            <tbody>
            @foreach ($glamping->getClientCountries() as $code => $country)
                <tr>
                    <td>{{ $country->title }}</td>
                    <td>{{ count($glamping->bookings()->where('country_code', $country->iso_code)) }}</td>
                    <td>{{ $glamping->getCountryReceipts($country->iso_code) }}</td>
                    <td>{{ $glamping->getCountryTimeFromOrderToArrival($country->iso_code) }}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
        @endif
    @endif
@endsection

@section('additional_scripts')
    <!-- Jquery DataTable Plugin Js -->
    <script src="{{ asset('theme/assets/plugins/jquery-datatable/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('theme/assets/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js') }}"></script>
    <script src="{{ asset('theme/assets/plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('theme/assets/plugins/jquery-datatable/extensions/export/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('theme/assets/plugins/jquery-datatable/extensions/export/jszip.min.js') }}"></script>
    <script src="{{ asset('theme/assets/plugins/jquery-datatable/extensions/export/pdfmake.min.js') }}"></script>
    <script src="{{ asset('theme/assets/plugins/jquery-datatable/extensions/export/vfs_fonts.js') }}"></script>
    <script src="{{ asset('theme/assets/plugins/jquery-datatable/extensions/export/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('theme/assets/plugins/jquery-datatable/extensions/export/buttons.print.min.js') }}"></script>
    <script src="{{ asset('theme/assets/js/pages/tables/jquery-datatable.js') }}"></script>
@endsection