@if ($user->hasRole('admin'))
    @php $role = 'admin'; @endphp
@else
    @php $role = 'owner'; @endphp
@endif

@extends('layouts.' . $role . '_dashboard')

@section('additional_styles')
    <!-- Dropzone Css -->
    <link href="{{ asset('theme/assets/plugins/dropzone/dropzone.css') }}" rel="stylesheet">
@endsection

@section('page_name')
    @if (isset($existBuilding))
        {{ trans('common_elements.btnEdit') . ' ' . mb_strtolower(trans('common_elements.building')) }}
    @else
        {{ trans('tables.glampingsListAddBuilding') }}
    @endif
@endsection

@section('page_information')
    @if (isset($glamping))
        @if (isset($existBuilding))
            {{ $existBuilding->name . ' (' .$glamping->name . ', ' . $glamping->country . ', '  . $glamping->region . ')' }}
        @else
            {{ $glamping->name . ' (' . $glamping->country . ', '  . $glamping->region . ')' }}
        @endif
    @endif
@endsection

@section('bread_crumbs')
    @if ($user->hasRole('admin'))
        <li><a href="{{ route('manager.admin_dashboard') }}">{{ trans('common_elements.home') }}</a></li>
        <li><a href="{{ route('manager.glampings.index') }}">{{ trans('common_elements.glampings') }}</a></li>
    @else
        <li><a href="{{ route('manager.owner_dashboard') }}">{{ trans('common_elements.home') }}</a></li>
        <li><a href="{{ route('manager.glampings.index') }}">{{ trans('common_elements.showUsersGlampings') }}</a></li>
    @endif
    @if (isset($glamping))
        <li><a href="{{ route('manager.glampings.show', ['id' => $glamping->id]) }}">{{ $glamping->name }}</a></li>
    @endif
    @if (isset($existBuilding))
        <li><a href="{{ route('manager.buildings.show', ['id' => $existBuilding->glamping_id]) }}">{{ trans('common_elements.buildings') }}</a></li>
        <li>
            <a href="{{ route('manager.buildings.view', ['glamping_id' => $existBuilding->glamping_id,
             'building_id' => $existBuilding->id]) }}">{{ $existBuilding->name }}</a></li>
        <li class="active">
            {{ trans('common_elements.btnEdit') }}
        </li>
    @else
        <li class="active">{{ trans('tables.glampingsListAddBuilding') }}</li>
    @endif
@endsection

@section('content_name')
    <div class="header">
        <h2>
            @if (isset($existBuilding))
                {{ trans('common_elements.editBuildingForm') }}
            @else
                {{ trans('common_elements.createBuildingForm') }}
            @endif
        </h2>
    </div>
@endsection

@section('content')
    {{--Open form--}}
    @php
    $route = 'manager.buildings.store';
    $btn = trans('common_elements.btnCreate');
    $star = '<strong class="text-danger">*</strong>';
    $capacity = null;
    $count = null;
    $mainPrice = null;
    $minNightsCount = null;
    $method = 'POST';
    $btnWithSendApp = trans('common_elements.btnCreateAndSend')
    @endphp

    @if (isset($existBuilding))
        @php
        $route = ['manager.buildings.update', $existBuilding->id];
        $btn = trans('common_elements.btnSave');
        $capacity = $existBuilding->capacity;
        $count = $existBuilding->count;
        $mainPrice = $existBuilding->main_price;
        $minNightsCount = $existBuilding->min_nights_count;
        $method = 'PUT';
        $btnWithSendApp = trans('common_elements.btnSaveAndSend')
        @endphp
    @endif

    {!! Form::open([ 'route' => $route, 'enctype' => 'multipart/form-data', 'class' => 'add-item']) !!}
    {{ method_field($method) }}
    {{-- Images --}}
    @if (isset($existBuilding))
        @foreach ($existBuilding->media()->get() as $image)
            <label class="glamping-image-label">
                <img class="glamping-image" src="{{$image->getUrl()}}" />
            </label>
        @endforeach
    @endif

    @if (isset($glamping))
        @for ($i = 0; $i < sizeof($glamping->media()->get()); $i++)
            <label class="current-image-label inactive-block" id="{{$i}}">
                <img class="glamping-image" src="{{$glamping->media()->get()[$i]->getUrl()}}">
            </label>
        @endfor
    @endif

    <div id="frmFileUpload" class="dropzone">
        {{ csrf_field() }}
        <div class="dz-message">
            <div class="drag-icon-cph">
                <i class="material-icons">touch_app</i>
            </div>
            <h3>{{ trans('common_elements.dropzoneMessage') }}</h3>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12">
            {{--Lang tabs--}}
            <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="active">
                    <a href=".profile_with_icon_title" data-toggle="tab">RU</a>
                </li>
                <li role="presentation">
                    <a href=".home_with_icon_title" data-toggle="tab">EN</a>
                </li>
            </ul>
        </div>
        <div class="col-sm-6">
            <!-- Tab panes -->
            <div class="tab-content">
                <!--Tabs  ENGLISH --------------------------->
                @php $name = null; $description = null; $otherInformation = null; @endphp

                {{--Get en-translations--}}
                @if (isset($existBuilding) && $existBuilding->hasTranslation('en'))
                    @php $name = $existBuilding->getTranslation('en')->name;
                    $description = $existBuilding->getTranslation('en')->description;
                    $otherInformation = $existBuilding->getTranslation('en')->other_information; @endphp
                @endif

                <div role="tabpanel" class="tab-pane fade home_with_icon_title">
                    {{-- Name --}}
                    <div class="form-group">
                        {!! Form::label('name_en', trans('common_elements.nameInputLabel')) !!}
                        {!! $star !!}
                        <div class="form-line">
                            {!! Form::text('name:en', $name, ['class' => 'form-control', 'id' => 'name_en']) !!}
                        </div>
                    </div>
                    {{-- Descriprion --}}
                    <div class="form-group">
                        {!! Form::label('description_en', trans('common_elements.descriptionInputLabel')) !!}
                        {!! $star !!}
                        <div class="form-line">
                            {!!  Form::textarea('description:en', $description, ['class' => 'form-control',
                            'id' => 'description_en', 'rows' => 3]) !!}
                        </div>
                    </div>
                    {{-- Other information --}}
                    <div class="form-group">
                        {!! Form::label('other_information_en', trans('common_elements.otherInformationInputLabel')) !!}
                        <div class="form-line">
                            {!!  Form::textarea('other_information:en', $otherInformation, ['class' => 'form-control',
                            'id' => 'other_information_en', 'rows' => 3]) !!}
                        </div>
                    </div>
                </div> <!--#Tabs  ENGLISH -->

                <!--Tabs RUSSIAN----------------------------->
                @php $name = null; $description = null; $otherInformation = null; @endphp

                {{--Get ru-translations--}}
                @if (isset($existBuilding) &&$existBuilding->hasTranslation('ru'))
                    @php $name = $existBuilding->getTranslation('ru')->name;
                    $description = $existBuilding->getTranslation('ru')->description;
                    $otherInformation = $existBuilding->getTranslation('ru')->other_information; @endphp
                @endif
                <div role="tabpanel" class="tab-pane fade profile_with_icon_title in active">
                    {{-- Name --}}
                    <div class="form-group">
                        {!! Form::label('name_ru', trans('common_elements.nameInputLabel')) !!}
                        {!! $star !!}
                        <div class="form-line">
                            {!! Form::text('name:ru', $name, ['class' => 'form-control', 'id' => 'name_ru']) !!}
                        </div>
                    </div>
                    {{-- Descriprion --}}
                    <div class="form-group">
                        {!! Form::label('description_ru', trans('common_elements.descriptionInputLabel')) !!}
                        {!! $star !!}
                        <div class="form-line">
                            {!!  Form::textarea('description:ru', $description, ['class' => 'form-control',
                            'id' => 'description_ru', 'rows' => 3]) !!}
                        </div>
                    </div>
                    {{-- Other information --}}
                    <div class="form-group">
                        {!! Form::label('other_information_ru', trans('common_elements.otherInformationInputLabel')) !!}
                        <div class="form-line">
                            {!!  Form::textarea('other_information:ru', $otherInformation, ['class' => 'form-control',
                            'id' => 'other_information_ru', 'rows' => 3]) !!}
                        </div>
                    </div>
                </div><!-- #Tabs RUSSIAN -->
            </div><!-- #Tab panes -->
        </div>
        <div class="col-sm-6">
            @if (isset($glamping)){!! Form::hidden('glamping_id', $glamping->id) !!}@endif
            {{-- Capacity --}}
            <div class="row">
                <div class="form-group">
                    <div class="col-md-5 col-sm-6 col-xs-8">
                        {!! Form::label('capacity_input', trans('common_elements.capacityBuildingInputLabel')) !!}
                        {!! $star !!}
                    </div>
                    <div class="col-sm-3 col-xs-4">
                        <div class="form-line">
                            {!! Form::number('capacity', $capacity, ['class' => 'form-control',
                            'id' => 'capacity_input', 'required', 'min' => '1']) !!}
                        </div>
                    </div>
                </div>
            </div>
            {{-- Count --}}
            <div class="row">
                <div class="form-group">
                    <div class="col-md-5 col-sm-6 col-xs-8">
                        {!! Form::label('count_input', trans('common_elements.countBuildingInputLabel')) !!}
                        {!! $star !!}
                    </div>
                    <div class="col-sm-3 col-xs-4">
                        <div class="form-line">
                            {!! Form::number('count', $count, ['class' => 'form-control',
                            'id' => 'count_input', 'required', 'min' => '1']) !!}
                        </div>
                    </div>
                </div>
            </div>

            {{-- The minimum count of nights here --}}
            <div class="row">
                <div class="form-group">
                    <div class="col-md-5 col-sm-6 col-xs-8">
                        {!! Form::label('min_nights_count', trans('common_elements.minNightsCountInputLabel')) !!}
                    </div>
                    <div class="col-sm-3 col-xs-4">
                        <div class="form-line">
                            {!! Form::number('min_nights_count', $minNightsCount, ['class' => 'form-control',
                            'id' => 'min_nights_count', 'min' => '0']) !!}
                        </div>
                    </div>
                </div>
            </div>

            {{-- Main price --}}
            <div class="row">
                <div class="form-group">
                    <div class="col-md-5 col-sm-6 col-xs-8">
                        {!! Form::label('main_price_input', trans('common_elements.mainPriceBuildingInputLabel')) !!}
                        {!! $star !!}
                    </div>
                    <div class="col-sm-3 col-xs-4">
                        <div class="form-line">
                            {!! Form::number('main_price', $mainPrice, ['class' => 'form-control',
                            'id' => 'main_price_input', 'required', 'min' => '0']) !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12 form-group">
            {!!  Form::label('options', trans('common_elements.options')) !!}:
            <table class="table-bordered">
                <thead>
                <tr>
                    <th>{{ trans('common_elements.nameColumnName') }}</th>
                    <th>{{ trans('common_elements.descriptionColumnName') }}</th>
                    <th>{{ trans('common_elements.bookTablePrice') }}</th>
                    <th>{{ trans('common_elements.actionColumnName') }}</th>
                </tr>
                </thead>
                <tbody id="options_block">
                @php $countOptions = 0; $optionNameEn = null; $optionDescEn = null;
                $optionNameRu = null; $optionDescRu = null; $optionPrice = null; @endphp
                @if (isset($existBuilding) && count($existBuilding->buildingOptions) > 0)
                    @foreach($existBuilding->buildingOptions as $option)
                        @if ($option->hasTranslation('en'))
                            @php $optionNameEn = $option->getTranslation('en')->name;
                            $optionDescEn = $option->getTranslation('en')->description; @endphp
                        @elseif($option->hasTranslation('ru'))
                            @php $optionNameRu = $option->getTranslation('ru')->name;
                            $optionDescRu = $option->getTranslation('ru')->description; @endphp
                        @endif
                        @php $countOptions++; $optionPrice = $option->price; @endphp
                        @include('manager.owner.templates.grid.option_line')
                    @endforeach
                @else
                    @include('manager.owner.templates.grid.option_line')
                @endif
                {!! Form::hidden("count_options", $countOptions, ['id' => 'count_options']) !!}
                </tbody>
            </table>
            {!! Form::button(trans('common_elements.btnAddOption'), [
            'class' => 'btn btn-raised btn-success m-t-15 waves-effect pull-right',
            'id' => 'add_option']) !!}
        </div>
    </div>

    <div class="row">
        {{-- Building type block--}}
        @if (isset($buildingTypeList))
            <div class="col-xs-12">
                <h2 class="card-inside-title">{{ trans('common_elements.buildingTypes') }} {!! $star !!}:</h2>
                @foreach ($buildingTypeList as $buildingType)
                    @php $checked = false; @endphp
                    @if (isset($existBuilding))
                        @if ($existBuilding->building_type_id == $buildingType->id)
                            @php $checked = true; @endphp
                        @endif
                    @endif
                    {!!  Form::radio('building_type_id', $buildingType->id, $checked,
                    ['class' => "filled-in chk-col-yellow", 'id' => 'md_checkbox_building_type_' . $buildingType->id]) !!}
                    {!!  Form::label('md_checkbox_building_type_' . $buildingType->id, $buildingType->name) !!}</br>
                @endforeach
            </div>
        @endif

        {{-- Amenity block--}}
        @if (isset($amenityList))
            <div class="col-xs-12">
                <h2 class="card-inside-title">{{ trans('common_elements.amenities') }}:</h2>
                @foreach ($amenityList as $amenity)
                    @php $checked = false; @endphp
                    @if (isset($existBuilding) && count($existBuilding->amenities) > 0)
                        @foreach($existBuilding->amenities as $amenityData)
                            @if ($amenity->id == $amenityData->id)
                                @php $checked = true; @endphp
                            @endif
                        @endforeach
                    @endif
                    {!!  Form::checkbox('amenity[]', $amenity->id, $checked,
                    ['class' => "filled-in chk-col-orange", 'id' => 'md_checkbox_amenity_' . $amenity->id]) !!}
                    {!!  Form::label('md_checkbox_amenity_' . $amenity->id, $amenity->name) !!}</br>
                @endforeach
            </div>
        @endif
    </div>

    {!! Form::submit($btn, ['class' => 'btn btn-raised btn-info m-t-15 waves-effect pull-right']) !!}
    @if ($user->hasRole('owner'))
        {!! Form::submit($btnWithSendApp, ['class' => 'btn btn-raised btn-success m-t-15 waves-effect pull-right',
        'name' => 'btn_with_send_app']) !!}
        {!! Form::close() !!}
    @endif
@endsection

@section('additional_scripts')
    <script src="{{ asset('theme/assets/plugins/dropzone/dropzone.js') }}"></script> <!-- Dropzone Plugin Js -->
    <script src="{{ asset('theme/assets/js/pages/images.js') }}"></script>
    <script src="{{ mix('js/add_element.js') }}" type="text/javascript"></script>
@endsection