@extends('layouts.manager_auth')

@section('additional_styles')
    <link href="{{ asset('theme/assets/css/login.css') }}" rel="stylesheet">
@endsection

@section('auth_form')
    <div class="bg-white">
        {!! Form::open([ 'route' => 'manager_register', 'class' => 'col-md-12', 'id' => 'sign_in']) !!}
        <div class="form-group">
            <div class="form-line form-float">
                {!! Form::text('first_name', null, ['class' => 'form-control', 'required', 'autofocus']) !!}
                {!! Form::label('first_name', trans('auth.first_name'), ['class' => 'form-label']) !!}
            </div>
        </div>
        <div class="form-group form-float">
            <div class="form-line">
                {!! Form::text('last_name', null, ['class' => 'form-control', 'required']) !!}
                {!! Form::label('last_name', trans('auth.last_name'), ['class' => 'form-label']) !!}
            </div>
        </div>
        <div class="form-group form-float">
            <div class="form-line">
                {!! Form::email('email', null, ['class' => 'form-control', 'required']) !!}
                {!! Form::label('email', trans('auth.email'), ['class' => 'form-label']) !!}
            </div>
        </div>
        <div class="form-group form-float">
            <div class="form-line">
                {!! Form::text('phone', null, ['class' => 'form-control', 'required']) !!}
                {!! Form::label('phone', trans('auth.phone'), ['class' => 'form-label']) !!}
            </div>
        </div>
        <div class="form-group form-float">
            <div class="form-line">
                {!! Form::password('password', ['class' => 'form-control', 'required']) !!}
                {!! Form::label('password', trans('auth.password'), ['class' => 'form-label']) !!}
            </div>
        </div>
        <div class="form-group form-float">
            <div class="form-line">
                {!! Form::password('password_confirmation', ['class' => 'form-control', 'required']) !!}
                {!! Form::label('password_confirmation', trans('auth.confirm_password'), ['class' => 'form-label']) !!}
            </div>
        </div>
        <div class="form-group">
            <img src="{{ captcha_src() }}" alt="captcha" class="captcha-img" data-refresh-config="default">
            <a href="#" id="refresh"><span class="glyphicon glyphicon-refresh"></span></a>
        </div>
        <div class="form-group form-float">
            <div class="form-line">
                {!! Form::text('captcha', null, ['class' => 'form-control', 'required']) !!}
                {!! Form::label('captcha', trans('auth.captcha'), ['class' => 'form-label']) !!}
            </div>
        </div>
        {!! Form::submit(trans('auth.btn_signup'), ['class' => 'btn btn-raised waves-effect bg-red']) !!}
        <div class="m-t-10 m-b--5 form-group">
            <a href="{{ route('manager_login_page') }}">{{ trans('auth.have_account') }}</a>
        </div>
        {!! Form::close() !!}
        <div class="col-md-12">
            {{-- Errors block --}}
            @if (count($errors) > 0)
                <div class="col-xs-12 alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li><h4>{{ $error }}</h4></li>
                        @endforeach
                    </ul>
                </div>
            @endif {{-- end errors block --}}

            {{-- Message block --}}
            @if (session()->has('flash_notification.message'))
                <div class="col-xs-12 alert alert-{{ session('flash_notification.level') }}">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>

                    <h4>{!! session('flash_notification.message') !!}</h4>
                </div>
            @endif {{-- end message block --}}
        </div>
    </div>
@endsection

@section('additional_scripts')
    <script type="text/javascript">
        $(function () {
            $('#sign_up').validate({
                highlight: function (input) {
                    $(input).parents('.form-line').addClass('error');
                },
                unhighlight: function (input) {
                    $(input).parents('.form-line').removeClass('error');
                },
                errorPlacement: function (error, element) {
                    $(element).parents('.form-group').append(error);
                }
            });
        });
    </script>
    <script src="{{ mix('js/captcha.js') }}" type="text/javascript"></script>
@endsection
