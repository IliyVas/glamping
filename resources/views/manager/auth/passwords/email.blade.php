@extends('layouts.manager_auth')

@section('additional_styles')
    <link href="{{ asset('theme/assets/css/login.css') }}" rel="stylesheet">
@endsection

@section('auth_form')

    @if (session('status'))
        <div class="alert alert-success">
            {{ session('status') }}
        </div>
    @endif
    <h3>Reset password</h3>
    <form class="col-md-12" role="form" method="POST" action="{{ url('/manager/password/email') }}">
        {{ csrf_field() }}

        <div class="form-group form-float">
            <div class="form-line">
                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>
                <label class="form-label">{{ trans('auth.email') }}</label>
            </div>
        </div>

        <button class="btn btn-raised waves-effect bg-red" type="submit">Send</button>
    </form>


@endsection

@section('additional_scripts')
    <script type="text/javascript">
        $(function () {
            $('#sign_in').validate({
                highlight: function (input) {
                    $(input).parents('.form-line').addClass('error');
                },
                unhighlight: function (input) {
                    $(input).parents('.form-line').removeClass('error');
                },
                errorPlacement: function (error, element) {
                    $(element).parents('.form-group').append(error);
                }
            });
        });
    </script>
@endsection
