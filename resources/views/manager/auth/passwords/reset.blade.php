@extends('layouts.manager_auth')

@section('additional_styles')
    <link href="{{ asset('theme/assets/css/login.css') }}" rel="stylesheet">
@endsection

@section('auth_form')
    <h3>New password</h3>
    <form role="form" method="POST" action="{{ url('/manager/password/reset') }}">
        {{ csrf_field() }}

        <input type="hidden" name="token" value="{{ $token }}">

        <div class="form-group form-float">
            <div class="form-line">
                <input id="email" type="email" class="form-control" name="email" value="{{ $email or old('email') }}" required autofocus>
                <label class="form-label">{{ trans('auth.email') }}</label>
            </div>
        </div>
        <div class="form-group form-float">
            <div class="form-line">
                <input id="password" type="password" class="form-control" name="password">

                <label class="form-label">password</label>
            </div>
        </div>
        <div class="form-group form-float">
            <div class="form-line">
                <input id="password-confirm" type="password" class="form-control" name="password_confirmation">
                <label class="form-label">Password Confirmation</label>
            </div>
        </div>

        <button class="btn btn-raised waves-effect bg-red" type="submit">Reset Password</button>

    </form>
@endsection

@section('additional_scripts')
    <script type="text/javascript">
        $(function () {
            $('#sign_in').validate({
                highlight: function (input) {
                    $(input).parents('.form-line').addClass('error');
                },
                unhighlight: function (input) {
                    $(input).parents('.form-line').removeClass('error');
                },
                errorPlacement: function (error, element) {
                    $(element).parents('.form-group').append(error);
                }
            });
        });
    </script>
@endsection
