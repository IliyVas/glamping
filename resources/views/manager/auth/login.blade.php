@extends('layouts.manager_auth')

@section('additional_styles')
    <link href="{{ asset('theme/assets/css/login.css') }}" rel="stylesheet">
@endsection

@section('auth_form')
    <div class="card">
        <form class="col-md-12" id="sign_in" method="POST" action="{{ route('manager_login') }}">
            {{ csrf_field() }}
            <div class="form-group form-float">
                <div class="form-line">
                    <input id="email" type="email" class="form-control" name="email" required autofocus>
                    <label class="form-label">{{ trans('auth.email') }}</label>
                </div>
            </div>
            <div class="form-group form-float">
                <div class="form-line">
                    <input id="password" type="password" class="form-control" name="password" required>
                    <label class="form-label">{{ trans('auth.password') }}</label>
                </div>
            </div>
            <div>
                <input type="checkbox" name="rememberme" id="rememberme" class="filled-in chk-col-cyan">
                <label for="rememberme">{{ trans('auth.remember') }}</label>
            </div>
            <button class="btn btn-raised waves-effect bg-red" type="submit">{{ trans('auth.btn_signin') }}</button>
            <a href="{{ route('manager_register_page') }}" class="btn btn-raised waves-effect" type="submit">{{ trans('auth.btn_signup') }}</a>
            <div class="text-left">
                <a href="{{ url('/manager/password/reset') }}">
                    {{ trans('auth.forgot_pass') }}
                </a>
            </div>
        </form>
        <div class="col-md-12">
            {{-- Errors block --}}
            @if (count($errors) > 0)
                <div class="col-xs-12 alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li><h4>{{ $error }}</h4></li>
                        @endforeach
                    </ul>
                </div>
            @endif {{-- end errors block --}}

            {{-- Message block --}}
            @if (session()->has('flash_notification.message'))
                <div class="col-xs-12 alert alert-{{ session('flash_notification.level') }}">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>

                    <h4>{!! session('flash_notification.message') !!}</h4>
                </div>
            @endif {{-- end message block --}}
        </div>
    </div>
@endsection

@section('additional_scripts')
    <script type="text/javascript">
        $(function () {
            $('#sign_in').validate({
                highlight: function (input) {
                    $(input).parents('.form-line').addClass('error');
                },
                unhighlight: function (input) {
                    $(input).parents('.form-line').removeClass('error');
                },
                errorPlacement: function (error, element) {
                    $(element).parents('.form-group').append(error);
                }
            });
        });
    </script>
@endsection
