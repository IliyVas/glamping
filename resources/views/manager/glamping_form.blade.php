@if ($user->hasRole('admin'))
    @php $role = 'admin'; @endphp
@else
    @php $role = 'owner'; @endphp
@endif

@extends('layouts.' . $role . '_dashboard')

@section('additional_styles')
    <!-- Dropzone Css -->
    <link href="{{ asset('theme/assets/plugins/dropzone/dropzone.css') }}" rel="stylesheet">

    <!-- Bootstrap Material Datetime Picker Css -->
    <link href="{{ asset('theme/assets/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css') }}" rel="stylesheet" />
@endsection

@section('glamping_form_page_status') @if (!isset($glamping))active @endif @endsection

@section('page_name')
    @if (isset($glamping))
        {{ trans('common_elements.btnEdit') . ' ' . mb_strtolower(trans('common_elements.glamping')) }}
    @else
        {{ trans('common_elements.createGlamping') }}
    @endif
@endsection

@section('page_information') @if (isset($glamping)) {{ $glamping->name }} @endif @endsection

@section('bread_crumbs')
    @if ($user->hasRole('admin'))
        <li><a href="{{ route('manager.admin_dashboard') }}">{{ trans('common_elements.home') }}</a></li>
        <li><a href="{{ route('manager.glampings.index') }}">{{ trans('common_elements.glampings') }}</a></li>
    @else
        <li><a href="{{ route('manager.owner_dashboard') }}">{{ trans('common_elements.home') }}</a></li>
        @if (isset($glamping))
            <li><a href="{{ route('manager.glampings.index') }}">{{ trans('common_elements.showUsersGlampings') }}</a></li>
        @endif
    @endif
    @if (isset($glamping))
        <li><a href="{{ route('manager.glampings.show', ['id' => $glamping->id]) }}">{{ $glamping->name }}</a></li>
        <li class="active">
            {{ trans('common_elements.btnEdit') . ' ' . mb_strtolower(trans('common_elements.glamping')) }}
        </li>
    @else
        <li class="active">
            {{ trans('common_elements.createGlamping') }}
        </li>
    @endif
@endsection

@section('content_name')
    <div class="header">
        <h2>
            @if (isset($glamping))
                {{ trans('common_elements.editGlampingForm') }}
            @else
                {{ trans('common_elements.createGlampingForm') }}
            @endif
        </h2>
    </div>
@endsection

@section('content')
    {{--Open form--}}
    @php
    $route = 'manager.glampings.store';
    $method = 'POST';
    $required = 'required';
    $btn = trans('common_elements.btnCreate');
    $maxChildAge = null;
    $lat = null;
    $long = null;
    $arrivalTime = null;
    $checkOutTime = null;
    $freeReturn = null;
    $commission = null;
    @endphp

    @if (isset($glamping))
        @php
        $route = ['manager.glampings.update', $glamping->id];
        $method = 'PATCH';
        $required = null;
        $btn = trans('common_elements.btnSave');
        $maxChildAge = $glamping->max_child_age;
        $lat = $glamping->latitude;
        $long = $glamping->longitude;
        $arrivalTime = $glamping->arrival_time;
        $checkOutTime = $glamping->check_out_time;
        $freeReturn = $glamping->free_return;
        $commission = $glamping->commission;
        @endphp
    @endif

    @php $star = '<strong class="text-danger">*</strong>'; @endphp

    {!! Form::open([ 'route' => $route, 'enctype' => 'multipart/form-data', 'class' => 'add-item']) !!}

    {{ method_field($method) }}

    {{-- Images --}}
    @if (isset($glamping))
        @foreach ($glamping->media()->get() as $image)
            <img class="glamping-image" id="{{ $image->id }}" src="{{ $image->getUrl() }}"/>
            <label class="glamping-image-label">
                <strong onclick="deleteOldImg({{ $image->id }})" class="delete_img" id="{{ $image->id }}-del">&#215;</strong>
                <strong onclick="reestablishOldImg({{ $image->id }})" class="reestablish_img" id="{{ $image->id }}-rees">&#8635;</strong>
            </label>
            <input type="hidden" name="delImages[]" id="{{ $image->id }}-del-input">
            <input type="hidden" name="oldImages[]" id="{{ $image->id }}-input" value="{{ $image->id }}">
        @endforeach
    @endif
    <div id="frmFileUpload" class="dropzone">
        {{ csrf_field() }}
        <div class="dz-message">
            <div class="drag-icon-cph">
                <i class="material-icons">touch_app</i>
            </div>
            <h3>{{ trans('common_elements.dropzoneMessage') }}</h3>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12">
            {{--Lang tabs--}}
            <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="active">
                    <a href="#profile_with_icon_title" data-toggle="tab">RU</a>
                </li>
                <li role="presentation">
                    <a href="#home_with_icon_title" data-toggle="tab">EN</a>
                </li>
            </ul>
        </div>
        <div class="col-sm-6">

            <!-- Tab panes -->
            <div class="tab-content">
                <!--Tabs  ENGLISH --------------------------->
                @php $name = null; $description = null; $otherInformation = null;
                $country = null; $region = null; $glampingAddress = null; @endphp

                {{--Get en-translations--}}
                @if (isset($glamping) && $glamping->hasTranslation('en'))
                    @php $name = $glamping->getTranslation('en')->name;
                    $description = $glamping->getTranslation('en')->description;
                    $otherInformation = $glamping->getTranslation('en')->other_information;
                    $country = $glamping->getTranslation('en')->country;
                    $region = $glamping->getTranslation('en')->region;
                    $glampingAddress = $glamping->getTranslation('en')->glamping_address; @endphp
                @endif

                <div role="tabpanel" class="tab-pane fade" id="home_with_icon_title">
                    {{-- Name --}}
                    <div class="form-group">
                        {!! Form::label('name_en', trans('common_elements.nameInputLabel')) !!}
                        {!! $star !!}
                        <div class="form-line">
                            {!! Form::text('name:en', $name, ['class' => 'form-control', 'id' => 'name_en']) !!}
                        </div>
                    </div>
                    {{-- Country --}}
                    <div class="form-group">
                        {!! Form::label('country_input_en', trans('common_elements.countryInputLabel') . ':') !!}
                        {!! $star !!}
                        <div class="form-line">
                            {!!  Form::text('country:en', $country, ['class' => 'form-control completeWithMap', 'id' => 'country_input_en']) !!}
                        </div>
                    </div>
                    {{-- Region --}}
                    <div class="form-group">
                        {!! Form::label('region_input_en', trans('common_elements.regionInputLabel')) !!}
                        {!! $star !!}
                        <div class="form-line">
                            {!!  Form::text('region:en', $region, ['class' => 'form-control  completeWithMap',
                            'id' => 'region_input_en']) !!}
                        </div>
                    </div>
                    {{-- Glamping adress --}}
                    <div class="form-group">
                        {!! Form::label('glamping_address_en', trans('common_elements.glampingAddressInputLabel')) !!}
                        <div class="form-line">
                            {!!  Form::textarea('glamping_address:en', $glampingAddress, ['class' => 'form-control',
                            'id' => 'glamping_address_en', 'rows' => 3]) !!}
                        </div>
                    </div>
                    {{-- Descriprion --}}
                    <div class="form-group">
                        {!! Form::label('description_en', trans('common_elements.descriptionInputLabel')) !!}
                        {!! $star !!}
                        <div class="form-line">
                            {!!  Form::textarea('description:en', $description, ['class' => 'form-control',
                            'id' => 'description_en', 'rows' => 3]) !!}
                        </div>
                    </div>
                    {{-- Other information --}}
                    <div class="form-group">
                        {!! Form::label('other_information_en', trans('common_elements.otherInformationInputLabel')) !!}
                        <div class="form-line">
                            {!!  Form::textarea('other_information:en', $otherInformation, ['class' => 'form-control',
                            'id' => 'other_information_en', 'rows' => 3]) !!}
                        </div>
                    </div>
                </div>

                <!--Tabs RUSSIAN----------------------------->
                @php $name = null; $description = null; $otherInformation = null;
                $country = null; $region = null; $glampingAddress = null; @endphp

                {{--Get ru-translations--}}
                @if (isset($glamping) && $glamping->hasTranslation('ru'))
                    @php $name = $glamping->getTranslation('ru')->name;
                    $description = $glamping->getTranslation('ru')->description;
                    $otherInformation = $glamping->getTranslation('ru')->other_information;
                    $country = $glamping->getTranslation('ru')->country;
                    $region = $glamping->getTranslation('ru')->region;
                    $glampingAddress = $glamping->getTranslation('ru')->glamping_address; @endphp
                @endif

                <div role="tabpanel" class="tab-pane fade in active" id="profile_with_icon_title">
                    {{-- Name --}}
                    <div class="form-group">
                        {!! Form::label('name_ru', trans('common_elements.nameInputLabel')) !!}
                        {!! $star !!}
                        <div class="form-line">
                            {!! Form::text('name:ru', $name, ['class' => 'form-control', 'id' => 'name_ru']) !!}
                        </div>
                    </div>
                    {{-- Country --}}
                    <div class="form-group">
                        {!! Form::label('country_input_ru', trans('common_elements.countryInputLabel')  . ':') !!}
                        {!! $star !!}
                        <div class="form-line">
                            {!!  Form::text('country:ru', $country, ['class' => 'form-control completeWithMap', 'id' => 'country_input_ru']) !!}
                        </div>
                    </div>
                    {{-- Region --}}
                    <div class="form-group">
                        {!! Form::label('region_input_run', trans('common_elements.regionInputLabel')) !!}
                        {!! $star !!}
                        <div class="form-line">
                            {!!  Form::text('region:ru', $region, ['class' => 'form-control completeWithMap',
                            'id' => 'region_input_ru']) !!}
                        </div>
                    </div>
                    {{-- Glamping adress --}}
                    <div class="form-group">
                        {!! Form::label('glamping_address_ru', trans('common_elements.glampingAddressInputLabel')) !!}
                        <div class="form-line">
                            {!!  Form::textarea('glamping_address:ru', $glampingAddress, ['class' => 'form-control',
                            'id' => 'glamping_address_ru', 'rows' => 3]) !!}
                        </div>
                    </div>
                    {{-- Descriprion --}}
                    <div class="form-group">
                        {!! Form::label('description_ru', trans('common_elements.descriptionInputLabel')) !!}
                        {!! $star !!}
                        <div class="form-line">
                            {!!  Form::textarea('description:ru', $description, ['class' => 'form-control',
                            'id' => 'description_ru', 'rows' => 3]) !!}
                        </div>
                    </div>
                    {{-- Other information --}}
                    <div class="form-group">
                        {!! Form::label('other_information_ru', trans('common_elements.otherInformationInputLabel')) !!}
                        <div class="form-line">
                            {!!  Form::textarea('other_information:ru', $otherInformation, ['class' => 'form-control',
                            'id' => 'other_information_ru', 'rows' => 3]) !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-6">
            {{-- Latitude && Longtitude fields --}}
            <div class="row latLangBlock">
                <div class="col-xs-6">
                    <div class="form-group">
                        {!! Form::label('latitude_input', trans('common_elements.latitudeInputLabel')) !!}
                        {!! $star !!}
                        <div class="form-line">
                            {!!  Form::text('latitude', $lat, ['class' => 'form-control completeWithMap',
                            'id' => 'latitude_input', 'required']) !!}
                        </div>
                    </div>
                </div>
                <div class="col-xs-6">
                    <div class="form-group">
                        {!! Form::label('longitude_input', trans('common_elements.longitudeInputLabel')) !!}
                        {!! $star !!}
                        <div class="form-line">
                            {!!  Form::text('longitude', $long, ['class' => 'form-control completeWithMap',
                            'id' => 'longitude_input', 'required']) !!}
                        </div>
                    </div>
                </div>
            </div>
            <div id="map" style="height:400px; width: 100%" ></div>
        </div>
    </div>

    {{-- Commission block --}}
    @if ($user->hasRole('admin'))
        <div class="row">
            <div class="form-group">
                <div class="col-md-3 col-sm-4 col-xs-8">
                    {!! Form::label('commission', trans('common_elements.commissionInputLabel')) !!}:
                </div>
                <div class="col-sm-2 col-xs-4">
                    <div class="form-line">
                        {!!  Form::number('commission', $commission, ['class' => 'form-control', 'max' => 100, 'min' => 0]) !!}
                    </div>
                </div>
            </div>
        </div>
    @endif

    {{-- The maximum age of free child block --}}
    <div class="row">
        <div class="form-group">
            <div class="col-md-3 col-sm-4 col-xs-8">
                {!! Form::label('max_child_age', trans('common_elements.maxChildAgeInputLabel')) !!}
                {!! $star !!}
            </div>
            <div class="col-sm-2 col-xs-4">
                <div class="form-line">
                    {!!  Form::number('max_child_age', $maxChildAge, ['class' => 'form-control', 'id' => 'max_child_age',
                    'required', 'max' => 18, 'min' => 0]) !!}
                </div>
            </div>
        </div>
    </div>

    {{-- Days of bookings free return --}}
    <div class="row">
        <div class="form-group">
            <div class="col-md-3 col-sm-4 col-xs-8">
                {!! Form::label('free_return', trans('common_elements.freeReturnDays')) !!}:
                {!! $star !!}
            </div>
            <div class="col-sm-2 col-xs-4">
                <div class="form-line">
                    {!!  Form::number('free_return', $freeReturn, ['class' => 'form-control', 'id' => 'free_return',
                    'required', 'max' => 100, 'min' => 1]) !!}
                </div>
            </div>
        </div>
    </div>

    {{--Arrival and check out time block--}}
    <div class="row">
        <div class="form-group">
            <div class="col-xs-4">
                <div class="row">
                    <div class="col-sm-6">
                        {!! Form::label('arrival_time', trans('common_elements.arrivalTimeInputLabel')) !!}
                    </div>
                    <div class="col-sm-5">
                        <div class="form-line">
                            {!!  Form::text('arrival_time', $arrivalTime, ['class' => 'form-control timepicker', 'id' => 'arrival_time']) !!}
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-4">
                <div class="col-sm-6">
                    {!! Form::label('check_out_time', trans('common_elements.checkOutTimeInputLabel')) !!}
                </div>
                <div class="col-sm-5">
                    <div class="form-line">
                        {!!  Form::text('check_out_time', $checkOutTime, ['class' => 'form-control timepicker', 'id' => 'check_out_time']) !!}
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        {{-- Glamping group block--}}
        @if (isset($glampingGroupList))
            <div class="col-xs-12">
                <h2 class="card-inside-title">{{ trans('common_elements.glampingGroups') }}:</h2>
                @foreach ($glampingGroupList as $glampingGroup)
                    @php $checked = false; @endphp
                    @if (isset($glamping) && count($glamping->glampingGroups) > 0)
                        @foreach($glamping->glampingGroups as $glampingGroupData)
                            @if ($glampingGroup->id == $glampingGroupData->id)
                                @php $checked = true; @endphp
                            @endif
                        @endforeach
                    @endif
                    {!!  Form::checkbox('glamping_group[]', $glampingGroup->id, $checked,
                    ['class' => "filled-in chk-col-yellow", 'id' => 'md_checkbox_glamping_group_' . $glampingGroup->id]) !!}
                    {!!  Form::label('md_checkbox_glamping_group_' . $glampingGroup->id, $glampingGroup->name) !!}<br>
                @endforeach
            </div>
        @endif

        {{-- Impression block--}}
        @if (isset($impressions))
            <div class="col-xs-12">
                <h2 class="card-inside-title">{{ trans('common_elements.impressions') }}:</h2>
                @foreach ($impressions as $impression)
                    @php $checked = false; @endphp
                    @if (isset($glamping) && count($glamping->impressions) > 0)
                        @foreach($glamping->impressions as $impressionData)
                            @if ($impression->id == $impressionData->id)
                                @php $checked = true; @endphp
                            @endif
                        @endforeach
                    @endif
                    {!!  Form::checkbox('impression[]', $impression->id, $checked,
                    ['class' => "filled-in chk-col-green", 'id' => 'md_checkbox_impression_' . $impression->id]) !!}
                    {!!  Form::label('md_checkbox_impression_' . $impression->id, $impression->name) !!}<br>
                @endforeach
            </div>
        @endif

        {{-- Amenity block--}}
        @if (isset($amenities))
            <div class="col-xs-12">
                <h2 class="card-inside-title">{{ trans('common_elements.amenities') }}:</h2>
                @foreach ($amenities as $amenity)
                    @php $checked = false; @endphp
                    @if (isset($glamping) && count($glamping->amenities) > 0)
                        @foreach($glamping->amenities as $amenityData)
                            @if ($amenity->id == $amenityData->id)
                                @php $checked = true; @endphp
                            @endif
                        @endforeach
                    @endif
                    {!!  Form::checkbox('amenity[]', $amenity->id, $checked,
                    ['class' => "filled-in chk-col-orange", 'id' => 'md_checkbox_amenity_' . $amenity->id]) !!}
                    {!!  Form::label('md_checkbox_amenity_' . $amenity->id, $amenity->name) !!}<br>
                @endforeach
            </div>
        @endif

        {{-- Service block--}}
        @if (isset($services))
            <div class="col-xs-12">
                <h2 class="card-inside-title">{{ trans('common_elements.services') }}:</h2>
                @foreach ($services as $service)
                    @php $checked = false; @endphp
                    @if (isset($glamping) && count($glamping->services) > 0)
                        @foreach($glamping->services as $serviceData)
                            @if ($service->id == $serviceData->id)
                                @php $checked = true; @endphp
                            @endif
                        @endforeach
                    @endif
                    {!!  Form::checkbox('service[]', $service->id, $checked,
                    ['class' => "filled-in chk-col-purple", 'id' => 'md_checkbox_service_' . $service->id]) !!}
                    {!!  Form::label('md_checkbox_service_' . $service->id, $service->name) !!}<br>
                @endforeach
            </div>
        @endif
    </div>

    {{--Buttons--}}
    {!! Form::submit($btn, ['class' => 'btn btn-raised btn-info m-t-15 waves-effect pull-right']) !!}
    @if ($user->hasRole('owner') && isset($glamping) && count($glamping->buildings) > 0 ||
     isset($glamping) && !is_null($glamping->glamping_id) && $glamping->accepted == false && $user->hasRole('owner'))
        {!! Form::submit(trans('common_elements.btnSaveAndSend'), ['class' => 'btn btn-raised btn-success m-t-15 waves-effect pull-right',
        'name' => 'btn_with_send_app']) !!}
    @endif

    {!! Form::close() !!}
@endsection

@section('additional_scripts')
    <script src="//api-maps.yandex.ru/2.1/?lang={{ trans('common_elements.langValue') }}" type="text/javascript"></script>
    <script src="{{ mix('js/map.js') }}" type="text/javascript"></script>
    <script src="{{ asset('theme/assets/plugins/dropzone/dropzone.js') }}"></script> <!-- Dropzone Plugin Js -->
    <script src="{{ asset('theme/assets/js/pages/images.js') }}"></script>

    <!-- Bootstrap Material Datetime Picker Plugin Js -->
    <script src="{{ asset('theme/assets/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js') }}"></script>
    <script src="{{ asset('theme/assets/js/pages/forms/basic-form-elements.js') }}"></script>
@endsection