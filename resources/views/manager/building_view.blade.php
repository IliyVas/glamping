@if ($user->hasRole('admin'))
    @php $role = 'admin'; @endphp
@else
    @php $role = 'owner'; @endphp
@endif

@extends('layouts.' . $role . '_dashboard')

@section('additional_styles')
    <!-- JQuery DataTable Css -->
    <link href="{{ asset('theme/assets/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css') }}" rel="stylesheet">
@endsection

@section('page_name')
    @if (isset($glamping) && isset($building))
        {{ $building->name . ' (' . $glamping->name . ') '}}
    @endif
@endsection

@section('page_information')
    @if (isset($glamping))
        {{ $glamping->country . ', '  . $glamping->region}}
    @endif
@endsection

@section('bread_crumbs')
    @if ($user->hasRole('admin'))
        <li><a href="{{ route('manager.admin_dashboard') }}">{{ trans('common_elements.home') }}</a></li>
        <li><a href="{{ route('manager.glampings.index') }}">{{ trans('common_elements.glampings') }}</a></li>
    @else
        <li><a href="{{ route('manager.owner_dashboard') }}">{{ trans('common_elements.home') }}</a></li>
        <li><a href="{{ route('manager.glampings.index') }}">{{ trans('common_elements.showUsersGlampings') }}</a></li>
    @endif
    @if (isset($glamping) && isset($building))
        <li><a href="{{ route('manager.glampings.show', ['id' => $glamping->id]) }}">{{ $glamping->name }}</a></li>
        <li><a href="{{ route('manager.buildings.show', ['id' => $glamping->id]) }}">{{ trans('common_elements.buildings') }}</a></li>
        <li class="active">{{ $building->name }}</li>
    @endif
@endsection

@section('content_name')
    <div class="header">
        <h2>
            @if (isset($building))
                {{ $building->name }}
            @endif
        </h2>
    </div>
@endsection

@section('content')
    @if (isset($glamping) && isset($building))
        <div class="row">
            <div class="col-xs-12">
                {{--Lang tabs--}}
                <ul class="nav nav-tabs" role="tablist">
                    @php $enActive=''; $ruActive='';@endphp
                    @if ($building->hasTranslation('en') && !$building->hasTranslation('ru'))
                        <li role="presentation" class="active">
                            <a href="#home_with_icon_title" data-toggle="tab">EN</a>
                        </li>
                        @php $enActive='in active'; @endphp
                    @endif
                    @if ($building->hasTranslation('ru') && !$building->hasTranslation('en'))
                        @php $ruActive='in active'; @endphp
                        <li role="presentation" class="active">
                            <a href="#profile_with_icon_title" data-toggle="tab">RU</a>
                        </li>
                    @endif
                    @if ($building->hasTranslation('en') && $building->hasTranslation('ru'))
                        @php $ruActive='in active'; @endphp
                        <li role="presentation" class="active">
                            <a href="#profile_with_icon_title" data-toggle="tab">RU</a>
                        </li>
                        <li role="presentation">
                            <a href="#home_with_icon_title" data-toggle="tab">EN</a>
                        </li>
                    @endif
                </ul>
            </div>
            <div class="col-sm-6">
                <!-- Tab panes -->
                <div class="tab-content">
                    <!--Tabs  ENGLISH --------------------------->
                    @php $name = null; $description = null; $otherInformation = null; @endphp

                    {{--Get en-translations--}}
                    @if ($building->hasTranslation('en'))
                        @php $name = $building->getTranslation('en')->name;
                        $description = $building->getTranslation('en')->description;
                        $otherInformation = $building->getTranslation('en')->other_information; @endphp
                    @endif

                    <div role="tabpanel" class="tab-pane fade {{ $enActive }}" id="home_with_icon_title">
                        {{-- Name --}}
                        {{ trans('common_elements.nameInputLabel') }}
                        <strong> {{ $name }}</strong><br>
                        {{-- Descriprion --}}
                        {{ trans('common_elements.descriptionInputLabel') }}
                        <strong> {{ $description }}</strong><br>
                        {{-- Other information --}}
                        @if (!empty( $otherInformation))
                            {{ trans('common_elements.otherInformationInputLabel') }}
                            <strong> {{ $otherInformation }}</strong><br>
                        @endif
                    </div>

                    <!--Tabs RUSSIAN----------------------------->
                    @php $name = null; $description = null; $otherInformation = null; @endphp

                    {{--Get ru-translations--}}
                    @if ($building->hasTranslation('ru'))
                        @php $name = $building->getTranslation('ru')->name;
                        $description = $building->getTranslation('ru')->description;
                        $otherInformation = $building->getTranslation('ru')->other_information; @endphp
                    @endif

                    <div role="tabpanel" class="tab-pane fade {{ $ruActive }}" id="profile_with_icon_title">
                        {{-- Name --}}
                        {{ trans('common_elements.nameInputLabel') }}
                        <strong> {{ $name }}</strong><br>
                        {{-- Descriprion --}}
                        {{ trans('common_elements.descriptionInputLabel') }}
                        <strong> {{ $description }}</strong><br>
                        {{-- Other information --}}
                        @if (!empty( $otherInformation))
                            {{ trans('common_elements.otherInformationInputLabel') }}
                            <strong> {{ $otherInformation }}</strong><br>
                        @endif
                    </div>
                </div>

                {{-- Building type block--}}
                @if ($building->buildingType()->first())
                    {{ trans('common_elements.buildingType') }}:
                    <strong> {{ $building->buildingType()->first()->name }}</strong><br>
                @endif

                {{-- Amenity block--}}
                @if (count($building->amenities()->get()) > 0)
                    {{ trans('common_elements.amenities') }}:
                    @foreach($building->amenities()->get() as $amenity)
                        <strong> {{ $amenity->name }};</strong>
                    @endforeach
                    <br>
                @endif

                {{--Capacity block--}}
                {{ trans('common_elements.bookTableCapacity') }}:
                <strong> {{ $building->capacity }}</strong><br>

                {{--Count block--}}
                {{ trans('common_elements.bookTableCount') }}:
                <strong> {{ $building->count }}</strong><br>

                {{--Min nights count block--}}
                @if ($building->min_nights_count)
                    {{ trans('common_elements.minNightsCountInputLabel') }}:
                    <strong> {{ $building->min_nights_count }}</strong><br>
                @endif

                {{--Main price block--}}
                {{ trans('common_elements.mainPriceBuildingInputLabel') }}:
                <strong> {{ $building->main_price }}</strong><br>

                {{--Additional price block--}}
                @if (count($building->prices) > 0)
                    @foreach($building->prices as $price)
                        {{ trans('common_elements.bookTablePrice') }}:
                        <strong> {{ $price->price }}</strong>
                        (<span class="text-danger">{{ trans('common_elements.wordFrom') }}</span>
                        {{ $price->from }}
                        <span class="text-danger">{{ mb_strtolower(trans('common_elements.wordTo')) }}</span>
                        {{ $price->to }})
                        <br>
                    @endforeach
                @endif

                {{--Options block--}}
                @if (count($building->buildingOptions) > 0)
                    <div class="row">
                        <div class="header">
                            <strong>{{ trans('common_elements.options') }}:</strong>
                        </div>
                    </div>

                    <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                        <thead>
                        <tr>
                            <th>{{ trans('common_elements.nameColumnName') }}</th>
                            <th class="sortable-false">{{ trans('common_elements.descriptionColumnName') }}</th>
                            <th>{{ trans('common_elements.bookTablePrice') }}</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($building->buildingOptions as $option)
                            <tr>
                                <td>
                                    {{ $option->name }}
                                </td>
                                <td>
                                    {{ $option->description }}
                                </td>
                                <td>
                                    {{ $option->price }}
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                @endif
            </div>

            {{-- Image block --}}
            <div class="col-sm-6">
                @if (count($building->media()->get()) > 0)
                    @foreach($building->media()->get() as $image)
                        <img class="glamping-image" src="{{ asset($image->getUrl()) }}">
                    @endforeach
                @endif
            </div>
        </div>
        @if (!count($glamping->applications) > 0 || $user->hasRole('admin') ||
         $glamping->lastApplicationStatus() !== ApplicationStatus::PENDING && !$building->changedBuilding)
            <a  class="btn btn-raised btn-primary m-t-15 waves-effect pull-right"
                href="{{ route('manager.buildings.edit', ['building_id' => $building->id]) }}"
                title="{{ trans('common_elements.btnEdit') }}">
                {{ trans('common_elements.btnEdit') . ' "' . $building->name . '"' }}
            </a>
        @endif
    @endif
@endsection

@section('additional_scripts')
    <!-- Jquery DataTable Plugin Js -->
    <script src="{{ asset('theme/assets/plugins/jquery-datatable/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('theme/assets/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js') }}"></script>
    <script src="{{ asset('theme/assets/plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('theme/assets/plugins/jquery-datatable/extensions/export/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('theme/assets/plugins/jquery-datatable/extensions/export/jszip.min.js') }}"></script>
    <script src="{{ asset('theme/assets/plugins/jquery-datatable/extensions/export/pdfmake.min.js') }}"></script>
    <script src="{{ asset('theme/assets/plugins/jquery-datatable/extensions/export/vfs_fonts.js') }}"></script>
    <script src="{{ asset('theme/assets/plugins/jquery-datatable/extensions/export/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('theme/assets/plugins/jquery-datatable/extensions/export/buttons.print.min.js') }}"></script>
    <script src="{{ asset('theme/assets/js/pages/tables/jquery-datatable.js') }}"></script>
@endsection