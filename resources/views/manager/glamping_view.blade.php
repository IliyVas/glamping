@if ($user->hasRole('admin'))
    @php $role = 'admin'; @endphp
@else
    @php $role = 'owner'; @endphp
@endif

@extends('layouts.' . $role . '_dashboard')

@section('page_name')
    @if (isset($glamping))
        {{ $glamping->name }}
    @endif
@endsection

@section('page_information')
    @if (isset($glamping))
        {{ $glamping->country . ', '  . $glamping->region}}
    @endif
@endsection

@section('bread_crumbs')
    @if ($user->hasRole('admin'))
        <li><a href="{{ route('manager.admin_dashboard') }}">{{ trans('common_elements.home') }}</a></li>
        <li><a href="{{ route('manager.glampings.index') }}">{{ trans('common_elements.glampings') }}</a></li>
    @else
        <li><a href="{{ route('manager.owner_dashboard') }}">{{ trans('common_elements.home') }}</a></li>
        <li><a href="{{ route('manager.glampings.index') }}">{{ trans('common_elements.showUsersGlampings') }}</a></li>
    @endif
    @if (isset($glamping))
        <li class="active">{{ $glamping->name }}</li>
    @endif
@endsection

@section('content_name')
    <div class="header">
        <h2>
            @if (isset($glamping))
                {{ $glamping->name }}
                <a href="{{ route('manager.glampings.statistics', ['id' => $glamping->id]) }}"
                   title="{{ trans('common_elements.btnViewStatistics') }}">
                    <i class="material-icons text-pink">assessment</i>
                </a>
            @endif
        </h2>
    </div>
@endsection

@section('content')
    @if (isset($glamping))
        {{-- Image block --}}
        @if (count($glamping->media()->get()) > 0)
            <div class="row">
                <div class="col-xs-12">
                    @foreach($glamping->media()->get() as $image)
                        <img class="glamping-image" src="{{ asset($image->getUrl()) }}">
                    @endforeach
                </div>
            </div>
        @endif

        <div class="row">
            <div class="col-xs-12">
                {{--Lang tabs--}}
                <ul class="nav nav-tabs" role="tablist">
                    @php $enActive=''; $ruActive='';@endphp
                    @if ($glamping->hasTranslation('en') && !$glamping->hasTranslation('ru'))
                        <li role="presentation" class="active">
                            <a href="#home_with_icon_title" data-toggle="tab">EN</a>
                        </li>
                        @php $enActive='in active'; @endphp
                    @endif
                    @if ($glamping->hasTranslation('ru') && !$glamping->hasTranslation('en'))
                        @php $ruActive='in active'; @endphp
                        <li role="presentation" class="active">
                            <a href="#profile_with_icon_title" data-toggle="tab">RU</a>
                        </li>
                    @endif
                    @if ($glamping->hasTranslation('en') && $glamping->hasTranslation('ru'))
                        @php $ruActive='in active'; @endphp
                        <li role="presentation" class="active">
                            <a href="#profile_with_icon_title" data-toggle="tab">RU</a>
                        </li>
                        <li role="presentation">
                            <a href="#home_with_icon_title" data-toggle="tab">EN</a>
                        </li>
                    @endif
                </ul>
            </div>
            <div class="col-sm-4">
                <!-- Tab panes -->
                <div class="tab-content">
                    <!--Tabs  ENGLISH --------------------------->
                    @php $name = null; $description = null; $otherInformation = null;
                    $country = null; $region = null; $glampingAddress = null; @endphp

                    {{--Get en-translations--}}
                    @if ($glamping->hasTranslation('en'))
                        @php $name = $glamping->getTranslation('en')->name;
                        $description = $glamping->getTranslation('en')->description;
                        $otherInformation = $glamping->getTranslation('en')->other_information;
                        $country = $glamping->getTranslation('en')->country;
                        $region = $glamping->getTranslation('en')->region;
                        $glampingAddress = $glamping->getTranslation('en')->glamping_address; @endphp
                    @endif

                    <div role="tabpanel" class="tab-pane fade {{ $enActive }}" id="home_with_icon_title">
                        {{-- Name --}}
                        {{ trans('common_elements.nameInputLabel') }}
                        <strong> {{ $name }}</strong><br>
                        {{-- Descriprion --}}
                        {{ trans('common_elements.descriptionInputLabel') }}
                        <strong> {{ $description }}</strong><br>
                        {{-- Other information --}}
                        {{ trans('common_elements.otherInformationInputLabel') }}
                        <strong> {{ $otherInformation or '&mdash;' }}</strong><br>
                        {{-- Country --}}
                        {{ trans('common_elements.countryInputLabel') }}:
                        <strong>{{ $country }}</strong><br>
                        {{-- Region --}}
                        {{ trans('common_elements.regionInputLabel') }}
                        <strong> {{ $region }}</strong><br>
                        {{-- Glamping adress --}}
                        {{ trans('common_elements.glampingAddressInputLabel') }}
                        <strong> {{ $glampingAddress or '&mdash;' }}</strong><br>
                    </div>

                    <!--Tabs RUSSIAN----------------------------->
                    @php $name = null; $description = null; $otherInformation = null;
                    $country = null; $region = null; $glampingAddress = null; @endphp

                    {{--Get ru-translations--}}
                    @if ($glamping->hasTranslation('ru'))
                        @php $name = $glamping->getTranslation('ru')->name;
                        $description = $glamping->getTranslation('ru')->description;
                        $otherInformation = $glamping->getTranslation('ru')->other_information;
                        $country = $glamping->getTranslation('ru')->country;
                        $region = $glamping->getTranslation('ru')->region;
                        $glampingAddress = $glamping->getTranslation('ru')->glamping_address; @endphp
                    @endif

                    <div role="tabpanel" class="tab-pane fade {{ $ruActive }}" id="profile_with_icon_title">
                        {{-- Name --}}
                        {{ trans('common_elements.nameInputLabel') }}
                        <strong> {{ $name }}</strong><br>
                        {{-- Descriprion --}}
                        {{ trans('common_elements.descriptionInputLabel') }}
                        <strong> {{ $description }}</strong><br>
                        {{-- Other information --}}
                        {{ trans('common_elements.otherInformationInputLabel') }}
                        <strong> {{ $otherInformation or '&mdash;' }}</strong><br>
                        {{-- Country --}}
                        {{ trans('common_elements.countryInputLabel') }}:
                        <strong>{{ $country }}</strong><br>
                        {{-- Region --}}
                        {{ trans('common_elements.regionInputLabel') }}
                        <strong> {{ $region }}</strong><br>
                        {{-- Glamping adress --}}
                        {{ trans('common_elements.glampingAddressInputLabel') }}
                        <strong> {{ $glampingAddress or '&mdash;' }}</strong><br>
                    </div>

                    <input type="hidden" id="longitude_input" value="{{ $glamping->longitude }}">
                    <input type="hidden" id="latitude_input" value="{{ $glamping->latitude }}">

                    {{-- Latitude --}}
                    {{ trans('common_elements.latitudeInputLabel') }}
                    <strong> {{ $glamping->latitude }}</strong><br>

                    {{-- Longtitude --}}
                    {{ trans('common_elements.longitudeInputLabel')}}
                    <strong> {{ $glamping->longitude }}</strong><br>

                    {{-- Free retutn --}}
                    {{ trans('common_elements.freeReturnDays')}}:
                    <strong> {{ $glamping->free_return }}</strong><br>

                    {{-- Max age free child --}}
                    {{ trans('common_elements.maxChildAgeInputLabel') }}
                    <strong> {{ $glamping->max_child_age or '&mdash;' }}</strong><br>

                    {{-- Arrival time --}}
                    {{ trans('common_elements.arrivalTimeInputLabel') }}
                    <strong> {{ $glamping->arrival_time or '&mdash;' }}</strong><br>

                    {{-- Check out time --}}
                    {{ trans('common_elements.checkOutTimeInputLabel') }}
                    <strong> {{ $glamping->check_out_time or '&mdash;' }}</strong><br>
                </div>
            </div>

            <div class="col-sm-8">
                <div id="map" style="height:300px; width: 100%" ></div>
            </div>
        </div>

        {{-- Glamping group block--}}
        @if (count($glamping->glampingGroups()->get()) > 0)
            {{ trans('common_elements.glampingGroups') }}:
            @foreach($glamping->glampingGroups()->get() as $glampingGroup)
                <strong> {{ $glampingGroup->name }};</strong>
            @endforeach
            <br>
        @endif

        {{-- Impression block--}}
        @if (count($glamping->impressions()->get()) > 0)
            {{ trans('common_elements.impressions') }}:
            @foreach($glamping->impressions()->get() as $impression)
                <strong> {{ $impression->name }};</strong>
            @endforeach
            <br>
        @endif

        {{-- Amenity block--}}
        @if (count($glamping->amenities()->get()) > 0)
            {{ trans('common_elements.amenities') }}:
            @foreach($glamping->amenities()->get() as $amenity)
                <strong> {{ $amenity->name }};</strong>
            @endforeach
            <br>
        @endif

        {{-- Service block--}}
        @if (count($glamping->services()->get()) > 0)
            {{ trans('common_elements.services') }}:
            @foreach($glamping->services()->get() as $service)
                <strong> {{ $service->name }};</strong>
            @endforeach
            <br>
        @endif

        @if (count($glamping->buildings()->get()) > 0)
            <a title="{{ trans('common_elements.viewAll') }}"
               href="{{ route('manager.buildings.show', ['id' => $glamping->id]) }}">
                {{ trans('common_elements.buildings') }}
            </a>
            ({{ trans('tables.glampingsListGlampingsSum') . ': ' . count($glamping->buildings()->get()) }})
            <ul>
                @foreach($glamping->buildings()->get() as $build)
                    <li>
                        <a href="{{ route('manager.buildings.view', ['glamping_id' => $glamping->id,
                         'building_id' => $build->id]) }}" title="{{ trans('common_elements.btnView') }}">
                            <strong>{{ $build->name }}</strong>
                        </a>
                        @if (!count($glamping->applications) > 0 || $user->hasRole('admin') ||
                        $glamping->lastApplicationStatus() !== ApplicationStatus::PENDING)
                            <a href="{{ route('manager.buildings.edit', ['glamping_id' => $glamping->id,
                         'building_id' => $build->id]) }}" title="{{ trans('common_elements.btnEdit') }}">
                                <i class="material-icons text-info">edit</i>
                            </a>
                        @endif
                    </li>
                @endforeach
            </ul>
        @endif
        @if (!count($glamping->applications) > 0 || $user->hasRole('admin') ||
        $glamping->lastApplicationStatus() !== ApplicationStatus::PENDING && !$glamping->changedGlamping)
            @if (is_null($glamping->glamping) || $glamping->glamping->lastApplicationStatus() !== ApplicationStatus::PENDING ||
            $user->hasRole('admin'))
                <a class="btn btn-raised btn-primary m-t-15 waves-effect pull-right" href="{{ route('manager.glampings.edit', ['id' => $glamping->id]) }}">
                    {{ trans('common_elements.btnEdit') . ' "' . $glamping->name . '"' }}
                </a>
                <a class="btn btn-raised btn-primary m-t-15 waves-effect pull-right" href="{{ route('manager.glampings.busyness', ['id' => $glamping->id]) }}">
                    {{ trans('common_elements.editBusyness') }}
                </a>
            @endif
        @endif
    @endif
@endsection

@section('additional_scripts')
    <script src="//api-maps.yandex.ru/2.1/?lang={{ trans('common_elements.langValue') }}" type="text/javascript"></script>
    <script src="{{ mix('js/map.js') }}" type="text/javascript"></script>
@endsection