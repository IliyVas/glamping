@extends('layouts.owner_dashboard')

@section('page_name')
    {{ trans('common_elements.userProfile') }}
@endsection

@section('page_information')
    {{ Auth::user()->email }}
@endsection

@section('account_page_status')active @endsection

@section('bread_crumbs')
    <li><a href="{{ route('manager.owner_dashboard') }}">{{ trans('common_elements.home') }}</a></li>
    <li class="active">{{ trans('common_elements.home') }}</li>
@endsection

@section('content')
    <div class="member-card verified">
        <div class="col-md-2 col-sm-2 col-xs-12">
            <div class="thumb-xl member-thumb">
                @if (!is_null(Auth::user()->media()->first()))
                    <img src="{{ Auth::user()->media()->first()->getUrl() }}"
                         class="img-circle" alt="profile-image">
                @else
                    <img src="{{ asset('images/account.png') }}"
                         class="img-circle" alt="profile-image">
                    <i class="zmdi zmdi-info" title="Permanent"></i>
                @endif
            </div>
        </div>
        <div class="col-md-10 col-sm-10 col-xs-12">
            <div class="s-profile">
                {{--Personal Data--}}
                <div class="row"><div class="row header"><h2>{{ trans('auth.personalData') }}:</h2></div></div>
                <h4 class="m-b-5">
                    {{ Auth::user()->first_name . ' ' . Auth::user()->patronymic . ' ' . Auth::user()->last_name }}
                </h4>
                {{ Auth::user()->email }}<br>
                {{ Auth::user()->phone }}<br>

                {{--Company--}}
                @if (!is_null(Auth::user()->company))
                    <div class="row"><div class="header"><h2>{{ trans('auth.companyData') }}:</h2></div></div>
                    {{ trans('common_elements.nameInputLabel') . ' ' . Auth::user()->company->name }}<br>
                    {{ trans('common_elements.mainStateRegisterNumber') . ': ' . Auth::user()->company->registration_number }}<br>
                @endif

                <a href="{{ route('manager.account.edit') }}" class="btn btn-raised btn-sm btn-info">{{ trans('common_elements.editProfileButton') }}</a>
            </div>
        </div>
    </div>
@endsection