@extends('layouts.owner_dashboard')

@section('additional_styles')
    <!-- JQuery DataTable Css -->
    <link href="{{ asset('theme/assets/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css') }}" rel="stylesheet">
@endsection

@section('page_name')
    {{ Auth::user()->first_name . ' ' .  Auth::user()->last_name}}
@endsection

@section('page_information')
    {{ Auth::user()->email }}
@endsection

@section('bread_crumbs')
    <li class="active">{{ trans('common_elements.home') }}</li>
@endsection

@section('content_name')
    <div class="header">
        @if (isset($gridData))<h2>{{ trans('common_elements.nearestBookingInf') }}</h2>
        @else<h2>{{ trans('messages.noBookings') }}</h2>@endif
    </div>
@endsection

@section('content')
    @if(isset($gridData))
        @include('templates.grid')
    @endif
@endsection

@section('additional_scripts')
    <!-- Jquery DataTable Plugin Js -->
    <script src="{{ asset('theme/assets/plugins/jquery-datatable/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('theme/assets/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js') }}"></script>
    <script src="{{ asset('theme/assets/plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('theme/assets/plugins/jquery-datatable/extensions/export/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('theme/assets/plugins/jquery-datatable/extensions/export/jszip.min.js') }}"></script>
    <script src="{{ asset('theme/assets/plugins/jquery-datatable/extensions/export/pdfmake.min.js') }}"></script>
    <script src="{{ asset('theme/assets/plugins/jquery-datatable/extensions/export/vfs_fonts.js') }}"></script>
    <script src="{{ asset('theme/assets/plugins/jquery-datatable/extensions/export/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('theme/assets/plugins/jquery-datatable/extensions/export/buttons.print.min.js') }}"></script>
    <script src="{{ asset('theme/assets/js/pages/tables/jquery-datatable.js') }}"></script>
    <script src="{{ mix('js/confirm_alert.js') }}"></script>
@endsection