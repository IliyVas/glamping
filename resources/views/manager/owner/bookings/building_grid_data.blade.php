@php $cur_building = 0; @endphp
@if (isset($buildings))
    @foreach ($buildings as $building)
        @if ($building->count > 0 ) {{-- we need to replace it to counter of free places--}}
        @php $cur_building++; $op = 0; @endphp
        @include('manager.owner.bookings.building_line')
        @if (count($building->buildingOptions) > 0)
            @foreach($building->buildingOptions as $option)
                @php $op++; @endphp
                @include('manager.owner.bookings.building_line')
            @endforeach
        @endif
        @endif
    @endforeach
@endif
