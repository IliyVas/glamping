<table  class="table table-bordered table-striped table-hover js-basic-example dataTable">
    <thead>
    <tr>
        <th> {{ trans('common_elements.bookTableCaption') }} </th>
        <th class="sortable-false"> {{ trans('common_elements.imageColumnName') }} </th>
        <th> {{ trans('common_elements.amenities') }} </th>
        <th> {{ trans('common_elements.options') }} </th>
        <th> {{ trans('common_elements.buildingType') }} </th>
        <th> {{ trans('common_elements.bookTableCapacity') }} </th>
        <th> {{ trans('common_elements.bookTablePrice') }} </th>
        <th class="sortable-false"> {{ trans('common_elements.bookTableCount') }} </th>
    </tr>
    </thead>
    <tbody id="buildingData">
    @if (isset($glamping) && $glamping !== null || isset($glampingList) && count($glampingList) > 0)
        @include('manager.owner.bookings.building_grid_data')
    @else
        {{-- Message block --}}
        <div class="alert alert-warning">
            <h4>{{ trans('messages.nothingFoundFilterResult') }}</h4>
        </div>
    @endif
    </tbody>
</table>