<tr>
    <td>{{$building->name}}</td>
    <td>
        @if( $building->media()->first() !== null)
            <img class="glamping-image" src="{{ $building->media()->first()->getUrl() }}">
        @else
            <img class="glamping-image" src="http://vk.com/images/camera_a.gif">
        @endif
    </td>
    <td>
        @if(count($building->amenities()->get()) > 0)
            <ul>
                @foreach($building->amenities()->get() as $amenity)
                    <li>{{ $amenity->name }}</li>
                @endforeach
            </ul>
        @else
            <ul>&mdash;</ul>
        @endif
    </td>
    <td>
        @if (count($building->buildingOptions) > 0 && $op !== 0)
            {{ $option->name }}
            @else &mdash; @endif
    </td>
    <td>
        @if($building->building_type_id !== null)
            {{ $building->buildingType->name or '&mdash;'}}
            @else &mdash; @endif
    </td>
    <td>
        {{ $building->capacity }}
    </td>
    <td>
        @if (count($building->buildingOptions) > 0 && $op !== 0)
            {{ $building->period_price + $option->period_price }}
        @else
            {{ $building->period_price or '&mdash;' }}
        @endif
    </td>
    @php $buildingCount = 0 @endphp
    @if (isset($bookBuildings))
        @foreach($bookBuildings as $bookBuilding)
            @if($bookBuilding->building_id == $building->id)
                @php $buildingCount = $bookBuilding->count @endphp
            @endif
        @endforeach
    @endif
    <td>
        <input type="hidden" name="buildings_id[]" value="{{ $building->id }}"/>
        <input type="number" name="buildings_count[]"
               value="{{ $buildingCount or '0' }}"
               min="0" max="{{ $building->count() }}"
               class="buildings-count" id="{{ $building->id }}count"/>
    </td>
</tr>