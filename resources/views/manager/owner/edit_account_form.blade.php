@extends('layouts.owner_dashboard')

@section('bread_crumbs')
    <li><a href="{{ route('manager.owner_dashboard') }}">{{ trans('common_elements.home') }}</a></li>
    <li><a href="{{ route('manager.owner.profile') }}">{{ trans('common_elements.userProfile') }}</a></li>
    <li class="active">{{ trans('common_elements.editProfileButton') }}</li>
@endsection

@section('page_name'){{ trans('common_elements.editProfileButton') }}@endsection

@section('page_description'){{ trans('common_elements.userAccountMessage') }}@endsection

@section('content_name')
    @if (!is_null(Auth::user()->lastApplicationAnswer(ApplicationStatus::REJECTED)))
        <div class="alert alert-danger">
            <h2> {{ Auth::user()->lastApplicationAnswer(ApplicationStatus::REJECTED) }}:</h2>
        </div>
    @endif
@endsection

@section('content')
    @php $star = '<strong class="text-danger">*</strong>'; @endphp
    <div class="row">
        <div class="col-sm-6">
            <div class="row">
                <div class="header">
                    <h2> {{ trans('auth.personalData') }}:</h2>
                </div>
            </div>
            {!! Form::model(Auth::user(), ['route' => 'manager.account.update', 'enctype' => 'multipart/form-data',]) !!}
            {{-- Image block --}}
            <div class="row form-group">
                <div class="col-xs-6">
                    <div class="thumb-xl member-thumb">
                        <img class="img-circle" id="mainImg" alt="Image" src="
                        @if(!is_null(Auth::user()->media()->first())) {{ Auth::user()->media()->first()->getUrl() }} @else
                        {{ asset('images/account.png') }} @endif ">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    {!! Form::file('image', ['id' => 'image']) !!}
                </div>
            </div>
            {{-- First name --}}
            <div class="form-group">
                {!! Form::label('first_name', trans('auth.first_name')) !!}:
                {!! $star !!}
                <div class="form-line">
                    {!! Form::text('first_name', null, ['class' => 'form-control', 'required']) !!}
                </div>
            </div>
            {{-- Patronymic --}}
            <div class="form-group">
                {!! Form::label('patronymic', trans('auth.patronymic')) !!}:
                <div class="form-line">
                    {!! Form::text('patronymic', null, ['class' => 'form-control']) !!}
                </div>
            </div>
            {{-- Last name --}}
            <div class="form-group">
                {!! Form::label('last_name', trans('auth.last_name')) !!}:
                {!! $star !!}
                <div class="form-line">
                    {!! Form::text('last_name', null, ['class' => 'form-control', 'required']) !!}
                </div>
            </div>
            {{-- Email --}}
            <div class="form-group">
                {!! Form::label('email', trans('auth.email')) !!}:
                {!! $star !!}
                <div class="form-line">
                    {!! Form::email('email', null, ['class' => 'form-control', 'required']) !!}
                </div>
            </div>
            {{-- Phone --}}
            <div class="form-group">
                <div class="row">
                    <div class="col-xs-5">
                        {!! Form::label('phone', trans('auth.phone')) !!}:
                        {!! $star !!}
                    </div>
                    <div class="col-xs-7">
                        <div class="form-line">
                            {!! Form::text('phone', null, ['class' => 'form-control', 'required']) !!}
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="header">
                    <h2> {{ trans('auth.companyData') }}:</h2>
                </div>
            </div>
            @if (!is_null(Auth::user()->company))
                @php $ds='disabled';
                $name = Auth::user()->company->name;
                $numb = Auth::user()->company->registration_number;
                @endphp
            @else
                @php $ds ='';
                $name = null;
                $numb = null;
                @endphp
            @endif
             {{--Company name--}}
            <div class="form-group">
                {!! Form::label('company_name', trans('common_elements.nameInputLabel')) !!}
                <div class="form-line">
                    {!! Form::text('company_name', $name, ['class' => 'form-control', $ds]) !!}
                </div>
            </div>
            {{-- Main state registration number --}}
            <div class="form-group">
                {!! Form::label('registration_number', trans('common_elements.mainStateRegisterNumber')) !!}:
                <div class="form-line">
                    {!! Form::text('registration_number', $numb, ['class' => 'form-control', $ds]) !!}
                </div>
            </div>

            {!! Form::submit(trans('common_elements.btnSave'), ['class' => 'btn btn-raised btn-info m-t-15 waves-effect pull-right']) !!}
            {!! Form::close() !!}
        </div>

        <div class="col-sm-6">
            <div class="row">
                <div class="header">
                    <h2> {{ trans('auth.changePassword') }}:</h2>
                </div>
            </div>
            {!! Form::open(['route' => 'manager.password.change']) !!}
            {{-- Old Password --}}
            <div class="form-group">
                <div class="row">
                    <div class="col-xs-6">
                        {!! Form::label('old_pass', trans('auth.password')) !!}:
                        {!! $star !!}
                    </div>
                    <div class="col-xs-6">
                        <div class="form-line">
                            {!! Form::password('old_pass', ['class' => 'form-control', 'required']) !!}
                        </div>
                    </div>
                </div>
            </div>

            {{-- New Password --}}
            <div class="form-group">
                <div class="row">
                    <div class="col-xs-6">
                        {!! Form::label('password', trans('auth.new_password')) !!}:
                        {!! $star !!}
                    </div>
                    <div class="col-xs-6">
                        <div class="form-line">
                            {!! Form::password('password', ['class' => 'form-control', 'required']) !!}
                        </div>
                    </div>
                </div>
            </div>

            {{-- Confirm new Password --}}
            <div class="form-group">
                <div class="row">
                    <div class="col-xs-6">
                        {!! Form::label('password_confirmation', trans('auth.confirm_new_password')) !!}:
                        {!! $star !!}
                    </div>
                    <div class="col-xs-6">
                        <div class="form-line">
                            {!! Form::password('password_confirmation', ['class' => 'form-control', 'required']) !!}
                        </div>
                    </div>
                </div>
            </div>

            {!! Form::submit(trans('common_elements.btnSave'), ['class' => 'btn btn-raised btn-info m-t-15 waves-effect pull-right']) !!}
            {!! Form::close() !!}
        </div>
    </div>
@endsection

@section('additional_scripts')
    <script src="{{ mix('js/icon.js') }}" type="text/javascript"></script>
@endsection