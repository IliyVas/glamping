@extends('layouts.owner_dashboard')

@section('page_name')
    {{ trans('common_elements.editBusynessPage') }}
@endsection

@section('bread_crumbs')
    <li><a href="{{ route('manager.owner_dashboard') }}">{{ trans('common_elements.home') }}</a></li>
    <li><a href="{{ route('manager.glampings.index') }}">{{ trans('common_elements.showUsersGlampings') }}</a></li>
    @if (isset($glamping))
        <li><a href="{{ route('manager.glampings.show', ['id' => $glamping->id]) }}">{{ $glamping->name }}</a></li>
        <li class="active">{{ trans('common_elements.editBusynessPage') }}</li>
    @endif
@endsection

@section('page_information')
    {{ trans('common_elements.editBusyness') }}
@endsection

@section('content_name')
    <div class="header">
        <h2>{{ trans('common_elements.busynessCalendar') }}</h2>
    </div>
@endsection

@section('additional_styles')
    <link href="{{ asset('theme/assets/plugins/slickgrid/slick.grid.css') }}" rel="stylesheet">
    <link href="{{ asset('theme/assets/plugins/slickgrid/css/examples.css') }}" rel="stylesheet">
    <link href="{{ asset('theme/assets/plugins/bootstrap-select/css/bootstrap-select.css') }}" rel="stylesheet">
    <link href="{{ asset('/css/all.css') }}" rel="stylesheet">
@endsection

@section('additional_scripts')
    <script src="{{ asset('theme/assets/plugins/slickgrid/lib/jquery.event.drag-2.3.0.js') }}"></script>
    <!-- SlickGrid Plugin Js -->
    <script src="{{ asset('js/slick.customselectionmodel.js') }}"></script>
    <script src="{{ asset('js/slick.customrangeselector.js') }}"></script>
    <script src="{{ asset('theme/assets/plugins/slickgrid/slick.core.js') }}"></script>
    <script src="{{ asset('theme/assets/plugins/slickgrid/slick.grid.js') }}"></script>
    <script src="{{ asset('theme/assets/plugins/slickgrid/plugins/slick.cellrangedecorator.js') }}"></script>
    <script src="{{ asset('theme/assets/plugins/slickgrid/plugins/slick.cellrangeselector.js') }}"></script>
    <script src="{{ asset('theme/assets/plugins/slickgrid/plugins/slick.rowselectionmodel.js') }}"></script>
    <script src="{{ asset('theme/assets/plugins/slickgrid/plugins/slick.cellselectionmodel.js') }}"></script>
    <script src="{{ asset('theme/assets/plugins/slickgrid/plugins/slick.checkboxselectcolumn.js') }}"></script>
    <script src="{{ asset('theme/assets/plugins/slickgrid/slick.formatters.js') }}"></script>
    <script src="{{ asset('theme/assets/plugins/slickgrid/slick.editors.js') }}"></script>
    <script src="{{ asset('theme/assets/plugins/slickgrid/lib/select2.js') }}"></script>
    @include('templates.page_elements.busyness_calendar')
@endsection

@section('content')
    <div class="row">
        <div class="col-sm-4 pull-right">
            <select class="selectpicker month-select" title="{{ trans('common_elements.wordMonth') }}..">
                <option value="01">{{ trans('common_elements.january') }}</option>
                <option value="02">{{ trans('common_elements.february') }}</option>
                <option value="03">{{ trans('common_elements.march') }}</option>
                <option value="04">{{ trans('common_elements.april') }}</option>
                <option value="05">{{ trans('common_elements.may') }}</option>
                <option value="06">{{ trans('common_elements.june') }}</option>
                <option value="07">{{ trans('common_elements.july') }}</option>
                <option value="08">{{ trans('common_elements.august') }}</option>
                <option value="09">{{ trans('common_elements.september') }}</option>
                <option value="10">{{ trans('common_elements.october') }}</option>
                <option value="11">{{ trans('common_elements.november') }}</option>
                <option value="12">{{ trans('common_elements.december') }}</option>
            </select>
            <select class="selectpicker year-select" title="{{ trans('common_elements.wordYear') }}..">
                @for ($year = date('Y'); $year < date('Y') + 5; $year++)
                    <option value="{{ $year }}" @if ($year == date('Y')) selected @endif>{{ $year }}</option>
                @endfor
            </select>
        </div>
    </div>
    <div class="col-sm-12 pull-right">
        <input type="hidden" value="{{ $glamping->id }}" id="glamping-id" />
        <div id="calendar" style="height: {{ 25 * 2 * sizeof($dates) + 45 }}px"></div>
    </div>
    <div class="row">
        <div class="col-xs-8">
            <div class="col-xs-3">
                <label for="from">{{ trans('common_elements.wordFrom') }}:</label>
                <div class="form-line">
                    <input class="form-control" name="from_date" id="from_date" type="date">
                </div>
            </div>
            <div class="col-xs-3">
                <label for="to">{{ trans('common_elements.wordTo') }}:</label>
                <div class="form-line">
                    <input class="form-control" name="to_date" id="to_date" type="date">
                </div>
            </div>
            <div class="col-xs-3">
                <label for="value">{{ trans('common_elements.wordValue') }}:</label>
                <div class="form-line">
                    <input class="form-control" min="0" name="value" id="value" type="number" value="0">
                </div>
            </div>
            <div class="col-xs-3">
                <button class="btn btn-raised btn-success m-t-15 waves-effect pull-right" id="set_buildings_count" type="button">
                    {{ trans('common_elements.setValue') }}
                </button>
            </div>
            <input id="edit_type" type="hidden">
            <input id="building_id" type="hidden">
        </div>
    </div>
@endsection