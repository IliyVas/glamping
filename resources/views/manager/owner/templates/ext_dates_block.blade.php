<div class="form-group">
    <div class="col-xs-4">
        {!! Form::label('from', trans('common_elements.wordFrom') . ':') !!}
        <div class="form-line">
            {!! Form::date('from_dates[]', null, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-xs-4">
        {!! Form::label('to', trans('common_elements.wordTo') . ':') !!}
        <div class="form-line">
            {!! Form::date('to_dates[]', null, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-xs-3">
        {!! Form::label('to', trans('common_elements.wordPrice') . ':') !!}
        <div class="form-line">
            {!! Form::number('price_dates[]', null, ['class' => 'form-control', 'min' => '0']) !!}
        </div>
    </div>
    <i class="material-icons text-danger close-button">clear</i>
</div>