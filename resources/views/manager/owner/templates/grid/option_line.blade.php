<tr>
    @if (!isset($existBuilding))
        @php $countOptions = 0; $optionNameEn = null; $optionDescEn = null;
        $optionNameRu = null; $optionDescRu = null; $optionPrice = null; @endphp
    @endif
    <td>
        <!-- Tab panes -->
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane fade home_with_icon_title">
                <div class="form-line">
                    {!! Form::text("option_name:en[]", $optionNameEn, [
                    'class' => 'form-control']) !!}
                </div>
            </div>
            <div role="tabpanel" class="tab-pane fade profile_with_icon_title in active">
                <div class="form-line">
                    {!! Form::text("option_name:ru[]", $optionNameRu, [
                    'class' => 'form-control']) !!}
                </div>
            </div>
        </div>
    </td>
    <td>
        <!-- Tab panes -->
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane fade home_with_icon_title">
                <div class="form-line">
                    {!! Form::text("option_description:en[]", $optionDescEn, [
                    'class' => 'form-control']) !!}
                </div>
            </div>
            <div role="tabpanel" class="tab-pane  in active fade profile_with_icon_title">
                <div class="form-line">
                    {!! Form::text("option_description:ru[]", $optionDescRu, [
                    'class' => 'form-control']) !!}
                </div>
            </div>
        </div>
    </td>
    <td>
        <div class="form-line">
            {!! Form::number("option_price[]", $optionPrice, [
            'class' => 'form-control', 'min' => '0']) !!}
        </div>
    </td>
    <td class="del-btn">
        @if (isset($existBuilding))
            <span class="text-danger table-close-button"><i class="material-icons">clear</i></span>
        @endif
    </td>
</tr>