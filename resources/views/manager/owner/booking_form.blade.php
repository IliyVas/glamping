@extends('layouts.owner_dashboard')

@section('additional_styles')
    <!-- Bootstrap Select Css -->
    <link href="{{ asset('theme/assets/plugins/bootstrap-select/css/bootstrap-select.css') }}" rel="stylesheet" />
    <!-- Bootstrap Material Datetime Picker Css -->
    <link href="{{ asset('theme/assets/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css') }}" rel="stylesheet" />
    <!-- Wait Me Css -->
    <link href="{{ asset('theme/assets/plugins/waitme/waitMe.css') }}" rel="stylesheet" />
    <!-- JQuery DataTable Css -->
    <link href="{{ asset('theme/assets/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css') }}" rel="stylesheet">
@endsection

@section('page_name')
    @if (isset($booking) && !is_null($booking))
        {{ trans('booking.editFormName') }}
    @else
        {{ trans('booking.createFormName') }}
    @endif
@endsection

@section('page_information')
    @if (isset($booking) && !is_null($booking))
        {{ trans('common_elements.wordFrom') . ' ' . $booking->from_date . ' ' .
         mb_strtolower(trans('common_elements.wordTo')) . ' ' . $booking->to_date }}
    @elseif (isset($glampingList) && sizeof($glampingList) > 0)
        {{ trans('tables.glampingsListGlampingsSum') . ' ' . mb_strtolower(trans('common_elements.myGlampings')) .
         ': ' . sizeof($glampingList) }}
    @endif
@endsection

@section('bread_crumbs')
    <li><a href="{{ route('manager.owner_dashboard') }}">{{ trans('common_elements.home') }}</a></li>
    <li><a href="{{ route('manager.bookings.index') }}">{{ trans('common_elements.ownerBookings') }}</a></li>
    <li class="active">
        @if (isset($booking) && !is_null($booking))
            {{ trans('booking.editFormName') }}
        @else
            {{ trans('booking.createFormName') }}
        @endif
    </li>
@endsection

@section('content_name')
    <div class="header">
        <h2>
            @if (isset($booking) && !is_null($booking))
                {{ trans('booking.editFormName') }}
            @else
                {{ trans('booking.createFormName') }}
            @endif
        </h2>
    </div>
@endsection

@section('content')
    @if (isset($glampingList) && count($glampingList) > 0)
        {{--Open form--}}
        @php
        $route = 'manager.bookings.store';
        $btn =trans('common_elements.bookBookButton');
        $star = '<strong class="text-danger">*</strong>';
        $method = 'POST';
        $name = null;
        $fromDate = date('d.m.Y', time());
        $toDate = date('d.m.Y', time() + (24*60*60));
        $bookingId = 0;
        $userId = null;
        @endphp

        @if (isset($booking) && !is_null($booking))
            @php
            $route = ['manager.bookings.update', $booking->id];
            $btn = trans('common_elements.btnSave');
            $method = 'PATCH';
            $name = $booking->name;
            $fromDate = date_create($booking->from_date)->Format('d.m.Y');
            $toDate = date_create($booking->to_date)->Format('d.m.Y');
            $bookingId = $booking->id;
            $userId = $booking->user_id;
            @endphp
        @endif

        {!! Form::open([ 'route' => $route, 'enctype' => 'multipart/form-data']) !!}
        {!! Form::hidden('error', trans('messages.dangerMessage'), ['id' => 'error']) !!}
        {{ method_field($method) }}
        <div class="col-xs-6 col-xs-offset-3" id="ajaxError"></div>


        {{--Glamping--}}
        <div class="row">
            <div class="col-sm-6">
                <div class="row">
                    <div class="form-group">
                        <div class="col-sm-3">
                            {!! Form::label('glamping_id', trans('common_elements.glamping')) !!}
                            {!! $star !!}:
                        </div>
                        <div class="col-sm-9">
                            <div class="form-line">
                                @php $options[0] =  '-- ' . trans('common_elements.nothingSelected') . ' --' @endphp
                                @php $selected = '' @endphp
                                @foreach ($glampingList as $listItem)
                                    @if(isset($booking) && $booking->glamping_id == $listItem->id || old('glamping_id') ==  $listItem->id)
                                        @php $selected = $listItem->id; @endphp
                                    @endif
                                    @php $options[$listItem->id] =  $listItem->name @endphp
                                @endforeach
                                {!! Form::select('glamping_id', $options, $selected, ['class' => 'form-control  show-tick',
                                'id' => 'glamping_id', 'onchange' => "activateBlock()" ]) !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {{--Name--}}
            <div class="col-sm-6">
                <div class="row">
                    <div class="form-group">
                        <div class="col-sm-3">
                            {!! Form::label('name', trans('auth.name')) !!}
                            {!! $star !!}:
                        </div>
                        <div class="col-sm-9">
                            <div class="form-line">
                                {!! Form::text('name', $name, ['class' => 'form-control', 'id' => 'name', 'required']) !!}
                            </div>
                            {!! Form::hidden('user_id', $userId) !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>

        {{--Dates--}}
        <div class="row clearfix">
            <div class="col-sm-12">
                <label>
                    {{ trans('common_elements.bookBookDates') }}:
                    <strong class="text-danger">*</strong>
                </label>
            </div>
            {{--From date--}}
            <div class="col-sm-4">
                <div class="form-group">
                    <div class="form-line">
                        {!! Form::text('from_date', $fromDate, ['class' => 'datepicker form-control',
                        'placeholder' => trans('common_elements.bookDateIn'), 'id' => 'from_date']) !!}
                    </div>
                </div>
            </div>
            {{--To date--}}
            <div class="col-sm-4">
                <div class="form-group">
                    <div class="form-line">
                        {!! Form::text('to_date', $toDate, ['class' => 'datepicker form-control',
                        'placeholder' => trans('common_elements.bookDateOut'), 'id' => 'to_date']) !!}
                    </div>
                </div>
            </div>
        </div>

        {{--Table with buildings--}}
        <div class="row" id="disable-block">
            <div class="col-xs-12">
                <label>{{ trans('common_elements.buildings') }}:<strong class="text-danger">*</strong></label>
                <div  id="ajax-content">
                    @include('manager.owner.bookings.full_table')
                </div>
            </div>
        </div>

        <input type="hidden" name="book_id" id="book_id" value="{{ $bookingId }}">

        {{--Button block--}}
        <div id="book-button-block">
            {!! Form::submit($btn, ['class' => 'btn btn-raised btn-primary m-t-15 waves-effect pull-right',
            'id' => 'book-button']) !!}
        </div>

        {!! Form::close() !!}
    @else
        <h2>{{ trans('message.noGlampingList') }}</h2>
    @endif
@endsection

@section('additional_scripts')
    <script type="text/javascript" src="{{ mix('js/booking.js') }}"></script>
    <!-- Bootstrap Material Datetime Picker Plugin Js -->
    <script src="{{ asset('theme/assets/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js') }}"></script>
    <script src="{{ asset('theme/assets/js/pages/forms/basic-form-elements.js') }}"></script>
    <!-- Jquery DataTable Plugin Js -->
    <script src="{{ asset('theme/assets/plugins/jquery-datatable/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('theme/assets/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js') }}"></script>
    <script src="{{ asset('theme/assets/plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('theme/assets/plugins/jquery-datatable/extensions/export/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('theme/assets/plugins/jquery-datatable/extensions/export/jszip.min.js') }}"></script>
    <script src="{{ asset('theme/assets/plugins/jquery-datatable/extensions/export/pdfmake.min.js') }}"></script>
    <script src="{{ asset('theme/assets/plugins/jquery-datatable/extensions/export/vfs_fonts.js') }}"></script>
    <script src="{{ asset('theme/assets/plugins/jquery-datatable/extensions/export/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('theme/assets/plugins/jquery-datatable/extensions/export/buttons.print.min.js') }}"></script>
    <script src="{{ asset('theme/assets/js/pages/tables/jquery-datatable.js') }}"></script>
@endsection