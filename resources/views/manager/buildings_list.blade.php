@if ($user->hasRole('admin'))
    @php $role = 'admin'; @endphp
@else
    @php $role = 'owner'; @endphp
@endif

@extends('layouts.' . $role . '_dashboard')

@section('additional_styles')
    <!-- JQuery DataTable Css -->
    <link href="{{ asset('theme/assets/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css') }}" rel="stylesheet">
@endsection

@section('page_name'){{ trans('common_elements.buildings') }}@endsection

@section('page_information')
    @if (isset($glamping))
        @php $atAllBuild = 0; @endphp
        @foreach ($glamping->buildings()->get() as $build)
            @php $atAllBuild += $build->count; @endphp
        @endforeach
        {{ trans('tables.glampingsListGlampingsSum') . ': ' .  $atAllBuild}} </br>
        {{ trans('common_elements.glamping') . ' "' . $glamping->name . '", ' .
         $glamping->country  . ', ' . $glamping->region }}
    @endif
@endsection

@section('bread_crumbs')
    @if ($user->hasRole('admin'))
        <li><a href="{{ route('manager.admin_dashboard') }}">{{ trans('common_elements.home') }}</a></li>
        <li><a href="{{ route('manager.glampings.index') }}">{{ trans('common_elements.glampings') }}</a></li>
    @else
        <li><a href="{{ route('manager.owner_dashboard') }}">{{ trans('common_elements.home') }}</a></li>
        <li><a href="{{ route('manager.glampings.index') }}">{{ trans('common_elements.showUsersGlampings') }}</a></li>
    @endif
    @if (isset($glamping))
        <li><a href="{{ route('manager.glampings.show', ['id' => $glamping->id]) }}">{{ $glamping->name }}</a></li>
    @endif
    <li class="active">{{ trans('common_elements.buildings') }}</li>
@endsection

@section('content_name')
    <div class="header">
        <h2>{{ trans('common_elements.buildings') }}</h2>
    </div>
@endsection

@section('content')
    @if (isset($glamping) && count($glamping->buildings()->get()) > 0)
        <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
            <thead>
            <tr>
                @include('manager.owner.templates.grid.buildings_list_column_names')
            </tr>
            </thead>
            <tfoot>
            <tr>
                @include('manager.owner.templates.grid.buildings_list_column_names')
            </tr>
            </tfoot>
            <tbody>
            @foreach ($glamping->buildings()->get() as $build)
                @if (!$build->changedBuilding || $build->accepted)
                    <tr>
                        <td>
                            <a href="{{ route('manager.buildings.view', ['glamping_id' => $glamping->id,
                         'building_id' => $build->id]) }}" title="{{ trans('common_elements.btnView') }}"
                            @if($build->accepted == false) class="text-info" @endif>
                                {{ $build->name }}
                            </a>
                        </td>
                        <td>{{ $build->capacity }}</td>
                        {{--<td></td>--}}
                        <td>{{ $build->count }}</td>
                        {{--<td></td>--}}
                        <td>
                            <a href="{{ route('manager.buildings.view', ['glamping_id' => $glamping->id,
                         'building_id' => $build->id]) }}" title="{{ trans('common_elements.btnView') }}">
                                <i class="material-icons text-warning">visibility</i>
                            </a>
                            @if (!count($glamping->applications) > 0 || $user->hasRole('admin') ||
                            $glamping->lastApplicationStatus() !== ApplicationStatus::PENDING)
                                <a href="{{ route('manager.buildings.edit', ['building' => $build->id]) }}"
                                   title="{{ trans('common_elements.btnEdit') . ' ' . trans('common_elements.building') }}">
                                    <i class="material-icons text-info">create</i>
                                </a>
                            @endif
                        </td>
                    </tr>
                @endif
            @endforeach
            </tbody>
        </table>
    @else
        <di class="col-sm-6 col-sm-offset-3">
            <div class="alert alert-info text-center">
                {{ trans('messages.emptyBuildingList') }}<br>
                <a class="btn btn-raised m-t-15 waves-effect" href="{{ route('manager.buildings.create', ['glamping_id' => $glamping->id]) }}">
                    {{ trans('tables.glampingsListAddBuilding') }}?
                </a>
            </div>
        </di>
    @endif
@endsection

@section('additional_scripts')
    <!-- Jquery DataTable Plugin Js -->
    <script src="{{ asset('theme/assets/plugins/jquery-datatable/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('theme/assets/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js') }}"></script>
    <script src="{{ asset('theme/assets/plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('theme/assets/plugins/jquery-datatable/extensions/export/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('theme/assets/plugins/jquery-datatable/extensions/export/jszip.min.js') }}"></script>
    <script src="{{ asset('theme/assets/plugins/jquery-datatable/extensions/export/pdfmake.min.js') }}"></script>
    <script src="{{ asset('theme/assets/plugins/jquery-datatable/extensions/export/vfs_fonts.js') }}"></script>
    <script src="{{ asset('theme/assets/plugins/jquery-datatable/extensions/export/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('theme/assets/plugins/jquery-datatable/extensions/export/buttons.print.min.js') }}"></script>
    <script src="{{ asset('theme/assets/js/pages/tables/jquery-datatable.js') }}"></script>
@endsection