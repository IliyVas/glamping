@extends('layouts.admin_dashboard')

@section('additional_styles')
    <!-- JQuery DataTable Css -->
    <link href="{{ asset('theme/assets/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css') }}" rel="stylesheet">
@endsection

@section('page_name'){{ trans('common_elements.' . $breadCrumbsActive) }}@endsection

@section('bread_crumbs')
    <li><a href="{{ route('manager.admin_dashboard') }}">{{ trans('common_elements.home') }}</a></li>
    <li class="active">{{ trans('common_elements.' . $breadCrumbsActive) }}</li>
@endsection

@section('page_information')
    {{ trans('tables.glampingsListGlampingsSum') }}: {{sizeof($gridData)}}
@endsection

@section($active) class = "active" @endsection

@section('content_name')
    <div class="header">
        <h2>{{ $pageName or NULL }}</h2>
    </div>
@endsection

@section('content')

    <table class="table table-bordered table-striped table-hover js-basic-example">
        <thead>
            <tr>
                <th class="text-center sorting_asc">Name</th>
                <th class="text-center sorting">E-Mail Address</th>
                <th class="text-center sortable-false sorting_disabled">Message</th>
                <th class="text-center sortable-false sorting_disabled" >Action</th>
                <th class="text-center sortable-false sorting_disabled" >Viewed</th>
            </tr>
        </thead>
        <tbody>
            @forelse($gridData as $data)
                <tr {{!$data->viewed ? 'class=non-viewed':''}}>
                    <td>{{$data->name}}</td>
                    <td>{{$data->email}}</td>
                    <td>{{str_limit($data->message, 70, "...")}}</td>
                    <td class="text-center">
                        <a href="{{route('manager.showMessage', ['id' => $data->id])}}">
                            <i class="material-icons text-info">visibility</i>
                        </a>
                    </td>
                    <td>
                        <div class="switch">
                            <label>
                                <input type="checkbox" value="{{$data->viewed ? 0 : 1}}" onchange="switchViewed('{{$data->id}}', this)" {{$data->viewed ? 'checked' : ''}}>
                                <span class="lever switch-col-light-blue"></span></label>
                        </div>
                    </td>
                </tr>
            @empty
                <tr>
                    <td colspan="5" class="text-center">Нет сообщений</td>
                </tr>
            @endforelse
        </tbody>
    </table>
@endsection

@section('additional_scripts')
    <!-- Jquery DataTable Plugin Js -->
    <script src="{{ asset('theme/assets/plugins/jquery-datatable/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('theme/assets/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js') }}"></script>
    <script src="{{ asset('theme/assets/plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('theme/assets/plugins/jquery-datatable/extensions/export/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('theme/assets/plugins/jquery-datatable/extensions/export/jszip.min.js') }}"></script>
    <script src="{{ asset('theme/assets/plugins/jquery-datatable/extensions/export/pdfmake.min.js') }}"></script>
    <script src="{{ asset('theme/assets/plugins/jquery-datatable/extensions/export/vfs_fonts.js') }}"></script>
    <script src="{{ asset('theme/assets/plugins/jquery-datatable/extensions/export/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('theme/assets/plugins/jquery-datatable/extensions/export/buttons.print.min.js') }}"></script>
    <script src="{{ asset('theme/assets/js/pages/tables/jquery-datatable.js') }}"></script>
    <script src="{{ mix('js/confirm_alert.js') }}"></script>
    <script src="{{ asset('js/message/index.js') }}"></script>
@endsection
