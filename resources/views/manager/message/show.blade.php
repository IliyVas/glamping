@extends('layouts.admin_dashboard')

@section('additional_styles')
    <!-- JQuery DataTable Css -->
    <link href="{{ asset('theme/assets/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css') }}" rel="stylesheet">
@endsection

@section('page_name'){{ trans('common_elements.' . $breadCrumbsActive) }}@endsection

@section('bread_crumbs')
    <li><a href="{{ route('manager.admin_dashboard') }}">{{ trans('common_elements.home') }}</a></li>
    <li class="active">{{ trans('common_elements.' . $breadCrumbsActive) }}</li>
@endsection

@section('page_information')
    {{ trans('tables.glampingsListGlampingsSum') }}
@endsection

@section($active) class = "active" @endsection

@section('content_name')
    <div class="header">
        <h2>{{ $pageName or NULL }}</h2>
    </div>
@endsection

@section('content')
    <div class="wrapper-message">
        {!!  $message->message !!}
    </div>
    <div class="wrapper-form">
        <form action="{{route('manager.answerMessage')}}" method="post">
            {{csrf_field()}}
            <input type="hidden" name="message" value="{{$message->id}}">
            <div class="form-group form-float">
                <div class="form-line">
                    <textarea class="form-control" name="answer" required></textarea>
                    <label class="form-label">{{trans('messages.ur_answer')}}</label>
                </div>
            </div>
            <div>
                <button class="btn btn-raised waves-effect bg-indigo" type="submit">{{ trans('messages.btn_answer') }}</button>
            </div>
        </form>
    </div>
@endsection

@section('additional_scripts')
    <!-- Jquery DataTable Plugin Js -->
    <script src="{{ asset('theme/assets/plugins/jquery-datatable/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('theme/assets/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js') }}"></script>
    <script src="{{ asset('theme/assets/plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('theme/assets/plugins/jquery-datatable/extensions/export/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('theme/assets/plugins/jquery-datatable/extensions/export/jszip.min.js') }}"></script>
    <script src="{{ asset('theme/assets/plugins/jquery-datatable/extensions/export/pdfmake.min.js') }}"></script>
    <script src="{{ asset('theme/assets/plugins/jquery-datatable/extensions/export/vfs_fonts.js') }}"></script>
    <script src="{{ asset('theme/assets/plugins/jquery-datatable/extensions/export/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('theme/assets/plugins/jquery-datatable/extensions/export/buttons.print.min.js') }}"></script>
    <script src="{{ asset('theme/assets/js/pages/tables/jquery-datatable.js') }}"></script>
    <script src="{{ mix('js/confirm_alert.js') }}"></script>
@endsection
