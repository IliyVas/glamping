@extends(Auth::guard('manager')->user()->hasRole('admin')?'layouts.admin_dashboard':'layouts.owner_dashboard')

@section('page_name')
    {{ trans('auth.nonActiveTitle') }}
@endsection

@section('page_information')
    {{ Auth::user()->email }}
@endsection

@section('content_name')
    <div class="header">
        <h2>{{ trans('messages.welcome') . ', ' . Auth::user()->first_name }}!</h2>
        @if (session('success'))
            <div class="alert alert-success">{{ session()->get('success') }}</div>
        @endif
        <small>{{ trans('auth.emailChangedSuccess', ['email' => htmlspecialchars( Auth::user()->email)]) }}</small>
        <small><a href="{{route('manager.resend')}}">{{ trans('auth.resubmit') }}</a></small>
    </div>
@endsection
