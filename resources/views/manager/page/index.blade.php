@extends('layouts.admin_dashboard')

@section('additional_styles')
    <!-- JQuery DataTable Css -->
    <link href="{{ asset('theme/assets/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css') }}" rel="stylesheet">
@endsection

@section('pages_status')active @endsection

@section('page_name'){{ trans('common_elements.' . $breadCrumbsActive) }}@endsection

@section('bread_crumbs')
    <li><a href="{{ route('manager.admin_dashboard') }}">{{ trans('common_elements.home') }}</a></li>
    <li class="active">{{ trans('common_elements.' . $breadCrumbsActive) }}</li>
@endsection


@section('content_name')
    <div class="header">
        <h2>{{ $pageName or NULL }}</h2>
    </div>
@endsection

@section('content')

    <table class="table table-bordered table-striped table-hover js-basic-example">
        <thead>
        <tr>
            <th class="text-center sorting">#</th>
            <th class="text-center sorting">Name</th>
            <th class="text-center sortable-false">Description</th>
            <th class="text-center sortable-false" >Action</th>
        </tr>
        </thead>
        <tbody>
        @forelse($pages as $page)
            <tr>
                <td>{{$page->id}}</td>
                <td>{{$page->name}}</td>
                <td>{!! str_limit($page->description, 70, "...") !!}</td>
                <td class="text-center">
                    <a href="{{route('manager.page', ['id' => $page->id])}}">
                        <i class="material-icons text-info">edit</i>
                    </a>
                </td>
            </tr>
        @empty
            <tr>
                <td colspan="4" class="text-center">Нет страниц</td>
            </tr>
        @endforelse
        </tbody>
    </table>
@endsection

@section('additional_scripts')
    <!-- Jquery DataTable Plugin Js -->
    <script src="{{ asset('theme/assets/plugins/jquery-datatable/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('theme/assets/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js') }}"></script>
    <script src="{{ asset('theme/assets/plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('theme/assets/plugins/jquery-datatable/extensions/export/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('theme/assets/plugins/jquery-datatable/extensions/export/jszip.min.js') }}"></script>
    <script src="{{ asset('theme/assets/plugins/jquery-datatable/extensions/export/pdfmake.min.js') }}"></script>
    <script src="{{ asset('theme/assets/plugins/jquery-datatable/extensions/export/vfs_fonts.js') }}"></script>
    <script src="{{ asset('theme/assets/plugins/jquery-datatable/extensions/export/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('theme/assets/plugins/jquery-datatable/extensions/export/buttons.print.min.js') }}"></script>
    <script src="{{ asset('theme/assets/js/pages/tables/jquery-datatable.js') }}"></script>
    <script src="{{ mix('js/confirm_alert.js') }}"></script>

@endsection
