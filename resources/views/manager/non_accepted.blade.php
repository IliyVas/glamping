@extends(Auth::guard('manager')->user()->hasRole('admin')?'layouts.admin_dashboard':'layouts.owner_dashboard')

@section('page_name')
    {{ trans('auth.nonAcceptedTitle') }}
@endsection

@section('page_information')
    {{ Auth::user()->email }}
@endsection

@section('content_name')
    <div class="header">
        <h2>{{ trans('messages.welcome') . ', ' . Auth::user()->first_name }}!</h2>
        <small>{{ trans('auth.nonAcceptedTitle') }}</small>
    </div>
@endsection
