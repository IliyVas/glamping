@extends('layouts.user_dashboard')

@section('page_name'){{ trans('common_elements.home') }}@endsection

@section('content_name')
    <div class="header">
        @if (isset($nearestBooking))<h2>{{ trans('common_elements.nearestBookingInf') }}</h2>
        @else<h2>{{ trans('messages.noBookings') }}</h2>@endif
    </div>
@endsection

@section('content')
    @if (isset($nearestBooking))
        <div class="row">
            {{--Glamping images block--}}
            <div class="col-sm-3">
                @if (count($nearestBooking->glamping->media()->get()) > 0)
                    @foreach($nearestBooking->glamping->media()->get() as $image)
                        <a href="{{ $nearestBooking->getPdfUrl() }}" target="blank">
                            <img class="img-responsive" src="{{ asset($image->getUrl()) }}">
                        </a>
                    @endforeach
                @endif
            </div>
            <div class="col-sm-9">
                {{--From date block--}}
                <br><strong>{{ trans('common_elements.bookDateIn') . ': ' }}</strong>
                {{ date_create($nearestBooking->from_date)->format('d.m.Y') }}
                {{--To date block--}}
                <br><strong>{{ trans('common_elements.bookDateOut') . ': ' }}</strong>
                {{ date_create($nearestBooking->to_date)->format('d.m.Y') }}
                {{--Price block--}}
                <br><strong >{{ trans('common_elements.bookTablePrice') . ': ' }}</strong>{{ $nearestBooking->price() }}
                {{--Glamping name, country, region block--}}
                <br><strong >{{ trans('common_elements.glamping') . ': ' }}</strong>
                {{ $nearestBooking->glamping->name . ' (' . $nearestBooking->glamping->country .
                 ', ' . $nearestBooking->glamping->region . ')' }}
                <br><a href="{{ $nearestBooking->getPdfUrl() }}" target="blank"
                   class="btn btn-raised btn-success m-t-15 waves-effect">{{ trans('common_elements.btnView') }}</a>
            </div>
        </div>
    @endif
@endsection