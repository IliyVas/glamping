@extends('layouts.user_dashboard')

@section('bread_crumbs')
    <li><a href="{{ route('user.dashboard') }}">{{ trans('common_elements.home') }}</a></li>
    <li class="active">{{ trans('common_elements.editProfileButton') }}</li>
@endsection

@section('page_name'){{ trans('common_elements.editProfileButton') }}@endsection

@section('page_description'){{ trans('common_elements.userAccountMessage') }}@endsection

@section('content')
    @php $star = '<strong class="text-danger">*</strong>'; @endphp
    <div class="row">
        <div class="col-sm-6">
            <div class="row">
                <div class="header">
                    <h2> {{ trans('common_elements.basicInformationFormBlock') }}:</h2>
                </div>
            </div>
            {!! Form::model(Auth::user(), ['route' => 'user.account.update', 'enctype' => 'multipart/form-data',]) !!}
            {{-- Image block --}}
            <div class="row form-group">
                <div class="col-xs-6">
                    <div class="thumb-xl member-thumb">
                        <img class="img-circle" id="mainImg" alt="Image" src="
                        @if(!is_null(Auth::user()->media()->first())) {{ Auth::user()->media()->first()->getUrl() }} @else
                            {{ asset('images/account.png') }} @endif ">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    {!! Form::file('image', ['id' => 'image']) !!}
                </div>
            </div>

            {{-- First name --}}
            <div class="form-group">
                {!! Form::label('first_name', trans('auth.first_name')) !!}:
                {!! $star !!}
                <div class="form-line">
                    {!! Form::text('first_name', null, ['class' => 'form-control', 'required']) !!}
                </div>
            </div>

            {{-- Patronymic --}}
            <div class="form-group">
                {!! Form::label('patronymic', trans('auth.patronymic')) !!}:
                <div class="form-line">
                    {!! Form::text('patronymic', null, ['class' => 'form-control']) !!}
                </div>
            </div>

            {{-- Last name --}}
            <div class="form-group">
                {!! Form::label('last_name', trans('auth.last_name')) !!}:
                {!! $star !!}
                <div class="form-line">
                    {!! Form::text('last_name', null, ['class' => 'form-control', 'required']) !!}
                </div>
            </div>

            {{-- Birth_date --}}
            <div class="form-group">
                <div class="row">
                    <div class="col-xs-5">
                        {!! Form::label('birth_date', trans('auth.birth_date')) !!}:
                        {!! $star !!}
                    </div>
                    <div class="col-xs-7">
                        <div class="form-line">
                            {!! Form::date('birth_date', null, ['class' => 'form-control', 'required', 'max' => date('Y-m-d', time() - (5*365*24*60*60))]) !!}
                        </div>
                    </div>
                </div>
            </div>

            {{-- Phone --}}
            <div class="form-group">
                <div class="row">
                    <div class="col-xs-5">
                        {!! Form::label('phone', trans('auth.phone')) !!}:
                        {!! $star !!}
                    </div>
                    <div class="col-xs-7">
                        <div class="form-line">
                            {!! Form::text('phone', null, ['class' => 'form-control', 'required']) !!}
                        </div>
                    </div>
                </div>
            </div>

            {!! Form::submit(trans('common_elements.btnSave'), ['class' => 'btn btn-raised btn-info m-t-15 waves-effect pull-right']) !!}
            {!! Form::close() !!}
        </div>

        <div class="col-sm-6">
            <div class="row">
                <div class="header">
                    <h2> {{ trans('auth.changePassword') }}:</h2>
                </div>
            </div>
            {!! Form::open(['route' => 'user.password.change']) !!}
            {{-- Old Password --}}
            <div class="form-group">
                <div class="row">
                    <div class="col-xs-6">
                        {!! Form::label('old_pass', trans('auth.password')) !!}:
                        {!! $star !!}
                    </div>
                    <div class="col-xs-6">
                        <div class="form-line">
                            {!! Form::password('old_pass', ['class' => 'form-control', 'required']) !!}
                        </div>
                    </div>
                </div>
            </div>

            {{-- New Password --}}
            <div class="form-group">
                <div class="row">
                    <div class="col-xs-6">
                        {!! Form::label('password', trans('auth.new_password')) !!}:
                        {!! $star !!}
                    </div>
                    <div class="col-xs-6">
                        <div class="form-line">
                            {!! Form::password('password', ['class' => 'form-control', 'required']) !!}
                        </div>
                    </div>
                </div>
            </div>

            {{-- Confirm new Password --}}
            <div class="form-group">
                <div class="row">
                    <div class="col-xs-6">
                        {!! Form::label('password_confirmation', trans('auth.confirm_new_password')) !!}:
                        {!! $star !!}
                    </div>
                    <div class="col-xs-6">
                        <div class="form-line">
                            {!! Form::password('password_confirmation', ['class' => 'form-control', 'required']) !!}
                        </div>
                    </div>
                </div>
            </div>

            {!! Form::submit(trans('common_elements.btnSave'), ['class' => 'btn btn-raised btn-info m-t-15 waves-effect pull-right']) !!}
            {!! Form::close() !!}
        </div>
    </div>

    <div class="row">
        <div class="col-xs-6">
            <div class="row">
                <div class="header">
                    <h2> {{ trans('auth.changeEmail') }}:</h2>
                </div>
            </div>
            {!! Form::open(['route' => 'user.email.change']) !!}
            {{-- Email --}}
            <div class="form-group">
                {!! Form::label('email', trans('auth.new_email')) !!}:
                {!! $star !!}
                <div class="form-line">
                    {!! Form::email('email', null, ['class' => 'form-control', 'required']) !!}
                </div>
            </div>

            {!! Form::submit(trans('common_elements.btnSave'), ['class' => 'btn btn-raised btn-info m-t-15 waves-effect pull-right']) !!}
            {!! Form::close() !!}
        </div>
    </div>
@endsection

@section('additional_scripts')
    <script src="{{ mix('js/icon.js') }}" type="text/javascript"></script>
@endsection