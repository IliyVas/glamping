@extends('layouts.user_dashboard')

@section('content')
    <div class="col-sm-12 col-sm-offset-1">
        @include('templates.user_update_booking')
    </div>
    {{-- Scripts --}}
    <script src="{{ mix('js/book_buildings_updater.js') }}" type="text/javascript"></script>
@endsection