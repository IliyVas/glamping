@extends('layouts.app')

@section ('title'){{ $glamping->name or config('app.name', 'Laravel') }}@endsection

@section('content')
    {{--Glamping block--}}
    @include('templates.glamping_page')

    {{--Exist comments block--}}
    @if (count($comments) > 0)
        <div class="col-sm-8">
            <div class="page-header"><h3>{{ trans('common_elements.comments') }}</h3></div>
            @foreach($comments as $comment)
                <div class="alert alert-info">
                <span class="pull-right">
                    {{{ $comment->user->first_name . ' ' . $comment->user->last_name . ' (' .
                    date_create($comment->created_at)->format('d.m.Y H:i') . ') ' }}}
                </span>
                    <br>{{{ $comment->comment }}}
                    <br>{{{ trans('tables.rating') . ' - ' . $comment->rating }}}
                    @if ($comment->canBeDeleted())
                        <a class="pull-right text-danger" href="#" onclick="event.preventDefault(); if(confirm('Confirm the deletion?'))
                                document.getElementById('delete').submit();">{{ trans("common_elements.btnDelete") }}
                        </a>
                        <form id="delete" method="POST" action="{{ route('comments.destroy', ['id' => $comment->id]) }}">
                            {{ csrf_field() }}
                            {{ method_field('DELETE') }}
                        </form>
                    @endif
                </div>
            @endforeach
        </div>
    @endif

    {{--Add new comment block--}}
    @if (MultiAuth::check() && !$manager && $user->isGlampingVisited($glamping))
        <div class="col-sm-8">
            <div class="page-header"><h4>{{ trans('common_elements.addComment') }}</h4></div>
            {!! Form::open(['route' => 'user.comments.store']) !!}

            <div class="form-group">
                {!! Form::label('rating', trans('tables.rating'), ['class' => 'control-label h4']) !!}
                {!!  Form::select('rating', [0, 1, 2, 3, 4, 5]) !!}
            </div>

            <div class="form-group">
                {!! Form::textarea('comment', null, ['class' => 'form-control', 'required', 'rows' => 5,
                 'placeholder' => trans('common_elements.comment')]) !!}
                {!!  Form::hidden('glamping_id', $glamping->id) !!}
            </div>

            <div class="form-group text-right">
                {!! Form::submit(trans('common_elements.btnSend'), ['class' => 'btn btn-success']) !!}
            </div>

            {!! Form::close() !!}
        </div>
    @endif

    {{-- Scripts --}}
    <script src="{{ mix('js/book_buildings_updater.js') }}" type="text/javascript"></script>
    <script src="//api-maps.yandex.ru/2.1/?lang={{ trans('common_elements.langValue') }}" type="text/javascript"></script>
    <script src="{{ mix('js/map.js') }}" type="text/javascript"></script>
@endsection