@extends('layouts.user_dashboard')

@section('content')
    <div class="col-sm-6">
        <div class="alert alert-danger">
            {{ trans('messages.confirmCancelBooking') }}
        </div>
        <a href="{{ route('user.userCancelBooking', $id) }}">
            <button class="btn  btn-raised btn-success waves-effect">
                {{ trans('common_elements.wordYes') }}
            </button>
        </a>
        <a href="{{ route('user.bookings') }}">
            <button class="btn  btn-raised btn-danger waves-effect">
                {{ trans('common_elements.wordNo') }}
            </button>
        </a>
    </div>
@endsection