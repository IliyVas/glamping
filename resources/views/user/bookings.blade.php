@extends('layouts.user_dashboard')

@section('bread_crumbs')
    <li><a href="{{ route('user.dashboard') }}">{{ trans('common_elements.home') }}</a></li>
    <li class="active">{{ trans('common_elements.userYourBookings') }}</li>
@endsection

@section('user_bookings_page_status')
    active
@endsection

@section('page_name'){{ trans('common_elements.userYourBookings') }}@endsection
@section('page_description'){{ trans('common_elements.userYourBookingsMessage') }}@endsection

@section('content_name')
    <div class="header">
        <h2>{{ trans('common_elements.userYourBookings') }}</h2>
    </div>
@endsection

@section('content')
    {{-- Information about bookings --}}
    <div class="table-responsive">
        @include('templates.bookings_table')
    </div>
@endsection