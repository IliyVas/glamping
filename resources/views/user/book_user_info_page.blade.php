@extends('layouts.app')

@section ('title'){{ config('app.name', 'Laravel') }}@endsection

@section('content')
    <form class="col-md-12" id="sign_in" action="{{ route('createBookingFromUserForm') }}" method="POST" enctype="multipart/form-data">
        {{ csrf_field() }}
        @if (is_null($user))@php $name = old('name'); $email = old('email'); $surname = old('surname'); @endphp
        @else @php $name = $user->first_name; $email = $user->email; $surname =  $user->last_name; @endphp @endif
        {{--First name block--}}
        <div class="form-group form-float">
            <div class="form-line">
                <input id="name" type="text" class="form-control" name="name" value="{{ $name }}" required autofocus>
                <label class="form-label">{{ trans('auth.first_name') }}</label>
            </div>
            @if ($errors->has('name'))
                <label id="name-error" class="error" for="name">{{ $errors->first('name') }}</label>
            @endif
        </div>
        {{--Last name block--}}
        <div class="form-group form-float">
            <div class="form-line">
                <input id="surname" type="text" class="form-control" name="surname" value="{{ $surname }}" required autofocus>
                <label class="form-label">{{ trans('auth.last_name') }}</label>
            </div>
            @if ($errors->has('surname'))
                <label id="surname-error" class="error" for="surname">{{ $errors->first('surname') }}</label>
            @endif
        </div>
        {{--Email block--}}
        <div class="form-group form-float">
            <div class="form-line">
                <input id="email" type="email" class="form-control" name="email" value="{{  $email }}" required>
                <label class="form-label">{{ trans('auth.email') }}</label>
            </div>
            @if ($errors->has('email'))
                <label id="email-error" class="error" for="email">{{ $errors->first('email') }}</label>
            @endif
        </div>
        {{--Booking data--}}
        <input type="hidden" name="book_data" value="{{ $book_data }}">
        {{--Password block--}}
        @if (is_null($user))
            <div class="form-group form-float">
                <div class="form-line">
                    <input id="password" type="password" class="form-control" name="password">
                    <label class="form-label">{{ trans('auth.password') }}</label>
                    <p>{{ trans('auth.not necessary') }}</p>
                </div>
                @if ($errors->has('password'))
                    <label id="password-error" class="error" for="password">{{ $errors->first('password') }}</label>
                @endif
            </div>
        @endif
        {{--Button--}}
        <div class="text-left"><button class="btn btn-raised waves-effect bg-red" type="submit">
                {{ trans('common_elements.btnConfirm') }}</button></div>
    </form>
@endsection
