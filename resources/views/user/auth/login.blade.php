@extends('layouts.app')
@section('additional_styles')
    <link rel="stylesheet" type="text/css" href="{{ mix('css/auth.css') }}">
@endsection
@section ('title'){{ config('app.name', 'Laravel') }}@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">{{ trans('common_elements.login') }}</div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ route('user_login') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">{{ trans('auth.email') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" autofocus>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">{{ trans('auth.password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password">

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="remember"> {{ trans('auth.remember') }}
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ trans('auth.btn_signin') }}
                                </button>

                                <a class="btn btn-link" href="{{ route('user_register') }}"> {{ trans('common_elements.register') }}</a>
                            </div>
                        </div>

                        <div class="form">
                            <div class="col-md-8 col-md-offset-4">
                                <a class="btn btn-link" href="{{ url('/user/password/reset') }}">
                                    {{ trans('auth.forgot_pass') }}
                                </a>
                            </div>
                        </div> 
                    </form>
                    <div>
                        <a href="{{ route('socialLogin', 'vkontakte') }}"><i class="zmdi zmdi-vk"></i></a>
                        <a href="{{ route('socialLogin', 'google') }}"><i class="zmdi zmdi-google"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
