@extends('layouts.user_dashboard')

@section('bread_crumbs')
    <li><a href="{{ route('user.dashboard') }}">{{ trans('common_elements.home') }}</a></li>
    <li class="active">{{ trans('common_elements.userAccount') }}</li>
@endsection

@section('user_account_page_status')
    active
@endsection

@section('page_name'){{ trans('common_elements.userAccount') }}@endsection

@section('page_description'){{ trans('common_elements.userAccountMessage') }}@endsection

@section('content_name')
    <div class="header">
        <h2>{{ Auth::user()->first_name . ' ' . Auth::user()->patronymic . ' ' . Auth::user()->last_name }}</h2>
    </div>
@endsection

@section('content')
    <div class="thumb-xl member-thumb">
        @if (!is_null(Auth::user()->media()->first()))
            <img src="{{ Auth::user()->media()->first()->getUrl() }}"
                 class="img-circle" alt="profile-image">
        @else
            <img src="{{ asset('images/account.png') }}" class="img-circle" alt="profile-image">
        @endif
    </div>
    <p class="text-muted">{{ date_create(Auth::user()->birth_date)->format('d.m.Y') }}</p>
    <p class="text-muted">{{ Auth::user()->email }}</p>
    <p class="text-muted">{{ Auth::user()->phone }}</p>
    <a href="{{ route('user.account.edit') }}" class="btn btn-raised btn-sm btn-info">{{ trans('common_elements.editProfileButton') }}</a>
@endsection