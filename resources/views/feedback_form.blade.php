@extends('layouts.app')

@section ('title'){{ trans('common_elements.feedbackFormName') }}@endsection

@section('content')
    <div class="col-sm-10 col-sm-offset-1">
        <div class="col-xs-8 col-xs-offset-2">
            <h2 class="page-header">{{ trans('common_elements.feedbackFormName') }}</h2>
        </div>

        {!! Form::open(['route' => 'setFeedbackMessage']) !!}

        <div class="col-xs-8 col-xs-offset-2 form-group">
            {!! Form::label('name', trans('auth.name') . ':', ['class' => 'control-label h4']) !!}
            {!! Form::text('name', null, ['class' => 'form-control', 'required']) !!}
        </div>

        <div class="col-xs-8 col-xs-offset-2 form-group">
            {!! Form::label('email', trans('auth.email') . ':', ['class' => 'control-label h4']) !!}
            {!! Form::email('email', null, ['class' => 'form-control', 'required']) !!}
        </div>

        <div class="col-xs-8 col-xs-offset-2 form-group">
            {!! Form::label('message', trans('messages.message') . ':', ['class' => 'control-label h4']) !!}
            {!! Form::textarea('message', null, ['class' => 'form-control', 'required']) !!}
        </div>

        <div class="col-xs-8 col-xs-offset-2 form-group">
            {!! Form::submit(trans('common_elements.btnSend'), ['class' => 'btn btn-success btn-block']) !!}
        </div>

        {!! Form::close() !!}
    </div>
@endsection