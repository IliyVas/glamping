@extends('layouts.app')

@section ('title'){{ config('app.name', 'Laravel') }}@endsection

@section('content')
    <div class="col-sm-10 col-sm-offset-1">
        <div class="col-xs-8 col-xs-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">{{$page->name}}</div>
                <div class="panel-body">
                    {{$page->description}}
                </div>
            </div>
        </div>

    </div>
@endsection
