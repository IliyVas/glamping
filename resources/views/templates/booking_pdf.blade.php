<html>

<head>
    <meta http-equiv=Content-Type content="text/html; charset=utf-8">
    <style>
        /* Style Definitions */
        p.MsoNormal, li.MsoNormal, div.MsoNormal
        {
            margin: 0 .0001pt;
            line-height:115%;
            mso-pagination:widow-orphan;
            font-size:11.0pt;
            font-family: DejaVu Sans, sans-serif;

            color:black;}
        @page WordSection1 {
            size:595.3pt 841.9pt;
            margin:28.3pt 28.3pt 28.3pt 28.3pt;
        }
        div.WordSection1 {
            page:WordSection1;
        }
        .pdf-usual-list {
            font-size: 8.0pt;
            line-height: 115%;
        }
        .pdf-big-list {
            font-size: 9.0pt;
            line-height: 115%;
        }
        .pdf-small-list {
            font-size:6.0pt;
            line-height:115%;
        }
        .pdf-hr {
            border-top: 1px solid #8c8b8b;
        }
    </style>
</head>

<body bgcolor=white lang=RU style='tab-interval:36.0pt'>

<div class=WordSection1>

    <p class=MsoNormal>
        <span class=SpellE>
            <span style='font-size:19.0pt;line-height:115%'>
                Glampster
            </span>
            <b>
                <span class="pdf-small-list">
                    {{ trans('pdf.bookingConfirmation') }}
                </span>
            </b>
        </span>
    </p>

    <hr class="pdf-hr">

    <p class=MsoNormal><b><i style='font-style:normal'><span class="pdf-usual-list">{{ trans('pdf.welcome') }}, {{ $data['name'] }}.</span></i></b></p>

    <p class=MsoNormal><span class="pdf-usual-list">{{ trans('pdf.welcomeMessage') }}</span></p>

    <hr class="pdf-hr">

    <p class=MsoNormal><b><span class="pdf-big-list">{{ trans('pdf.yourBooking') }}:</span></b></p>

    <p class=MsoNormal>
        <span class="pdf-usual-list">
            <b>{{ trans('pdf.bookNumber') }}: </b>
            {{ $booking->id }}
        </span>
    </p>

    <p class=MsoNormal>
        <b>
            <span class="pdf-usual-list">{{ trans('pdf.bookDate') }}:</span>
        </b>
        <span class="pdf-usual-list">{{ $booking->from_date . ' - ' . $booking->to_date }}</span>
    </p>

    <p class=MsoNormal>
        <span class=GramE>
            <b>
                <span class="pdf-usual-list">{{ trans('pdf.bookFullName') }}:</span>
            </b>
        </span>
        <span class="pdf-usual-list">
            {{ $data['name'] }}
        </span>
    </p>

    <p class=MsoNormal>
        <span class=SpellE>
            <span class=GramE>
                <b>
                    <span class="pdf-usual-list">{{ trans('pdf.bookEmail') }}: </span>
                </b>
            </span>
        </span>
        <span style='font-size:8.0pt;line-height:115%;color:#1155CC'>
            {{ $data['email'] }}
        </span>
    </p>

    {{--<p class=MsoNormal>
        <span class=GramE>
            <b>
                <span class="pdf-usual-list">
                    {{ trans('pdf.bookPhoneNumber') }}:
                </span>
            </b>
        </span>
        <span class="pdf-usual-list">
            +7(905)343-65-60
        </span>
    </p>--}}

    <hr class="pdf-hr">

    <p class=MsoNormal>
        <b>
            <span class="pdf-big-list">
                {{ trans('pdf.selectedGlamping') }}:
            </span>
        </b>
    </p>

    <p class=MsoNormal>
        <b>
            <i style='font-style:normal'>
                <span class="pdf-usual-list">
                    {{ $glamping->name }}
                </span>
            </i>
        </b>
    </p>

    <p class=MsoNormal>
        <i style='font-style:normal'>
            <span class="pdf-usual-list">
                {{ $glamping->country . ', ' . $glamping->region }}
                {{ (!is_null($glamping->address))?(', ' . $glamping->address):'' }}
                {{ '(' . $glamping->latitude . ', ' . $glamping->longitude . ')' }}
            </span>
        </i>
    </p>

    <p class=MsoNormal>
        <span class=GramE>
            <b>
                <span class="pdf-usual-list">
                    {{ trans('pdf.bookPhoneNumber') }}:
                </span>
            </b>
        </span>
        <span class="pdf-usual-list">
            <i style='font-style:normal'>
                {{ $glamping->managers()->first()->phone or '-' }}
            </i>
        </span>
    </p>

    <p class=MsoNormal>
        <span class=GramE>
            <b>
                <span class="pdf-usual-list">
                    {{ trans('pdf.bookEmail') }}:
                </span>
            </b>
        </span>
        <span class="pdf-usual-list">
            <i style='font-style:normal'>
                {{ $glamping->managers()->first()->email or '-' }}
            </i>
        </span>
    </p>

    <p class=MsoNormal>
        <span class=GramE>
            <b>
                <span class="pdf-usual-list">
                    {{ trans('pdf.timeIn') }}:
                </span>
            </b>
        </span>
        <span class="pdf-usual-list">
            <i style='font-style:normal'>
                {{ $glamping->arrival_time . ':00' }}
            </i>
        </span>
    </p>

    <hr class="pdf-hr">

    <p class=MsoNormal>
        <b>
            <span class="pdf-big-list">
                {{ trans('pdf.buildingsInfo') }}
            </span>
        </b>
    </p>

    @foreach ($booking->bookBuildings()->get() as $key => $book_building)
        @php $building = $book_building->building @endphp
        <p class=MsoNormalCxSpMiddle style='margin-left:36.0pt;text-indent:-18.0pt;
            mso-list:l0 level1 lfo1'>
            <b>
                <i style='font-style:normal'>
                    <span class="pdf-usual-list">
                        <span style='mso-list:Ignore'>
                            {{ $key + 1 }}.
                        </span>
                    </span>
                </i>
            </b>
            <b>
                <i style='font-style:normal'>
                    <span class="pdf-usual-list">
                        {{ $building->name }}
                    </span>
                </i>
            </b>
            <br>
            <b style='margin-left:-20.0pt;font-weight:normal'>
                <span class="pdf-usual-list">
                        {{ trans('pdf.buildingNumber') }}:
                </span>
            </b>
            <span class="pdf-usual-list">{{ $building->id }}</span>
        </p>

        <p class=MsoNormal>
            <b>
                <span class="pdf-usual-list">
                        {{ trans('pdf.buildingGuests') }}:
                </span>
            </b>
            <span class="pdf-usual-list">{{ $booking->adults }}</span>
        </p>
        <p class=MsoNormal>
            <b>
                <span class="pdf-usual-list">
                        {{ trans('pdf.buildingChildren') }}:
                </span>
            </b>
            <span class="pdf-usual-list">{{ $booking->children }}</span>
        </p>
        <p class=MsoNormal>
            <b>
                <span class="pdf-usual-list">
                        {{ trans('pdf.arrival') }} / {{ trans('pdf.departure') }}:
                </span>
            </b>
            <span class="pdf-usual-list">{{ $booking->from_date . ' / ' . $booking->to_date }}</span>
        </p>
        <p class=MsoNormal>
            <b>
                <span class="pdf-usual-list">
                        {{ trans('pdf.buildingPrice') }}:
                </span>
            </b>
            <span class="pdf-usual-list">{{ $booking->price() . ' руб.' }}</span>
        </p>
    @endforeach

    <hr class="pdf-hr">

    <p class=MsoNormal>
        <b>
                <span class="pdf-usual-list">
                        {{ trans('pdf.bookType') }}:
                </span>
        </b>
        <span class="pdf-usual-list">{{ trans('pdf.bookTypeGuarantee' )}}</span>
    </p>

    <p class=MsoNormal>
        <b>
                <span class="pdf-usual-list">
                        {{ trans('pdf.bookPayMethod') }}:
                </span>
        </b>
        <span class="pdf-usual-list">{{ trans('pdf.payCard' )}}</span>
    </p>

    <hr class="pdf-hr">

    <p class=MsoNormal>
        <span class="pdf-usual-list">
            {{ trans('pdf.taxesIn') }}
        </span>
        <i style='font-style:normal'>
            <span style='font-size:16.0pt;line-height:115%'>
                <span style='mso-spacerun:yes'> </span>
                {{ $booking->price() }} руб.
            </span>
        </i>
    </p>

    <hr class="pdf-hr">

    <p class=MsoNormal>
        <b>
            <span class="pdf-big-list">
                {{ trans('pdf.amenities') }}
            </span>
        </b>
    </p>

    <p class=MsoNormal>
        <span class="pdf-usual-list">
            @php $count = 0 @endphp
            @foreach ($glamping->amenities as $amenity)
                @php $count++ @endphp
                @if ($count > 1)/ @endif
                {{ $amenity->name }}
            @endforeach
        </span>
    </p>

    <p class=MsoNormal>
        <b>
            <span class="pdf-big-list">
                {{ trans('pdf.services') }}
            </span>
        </b>
    </p>

    <p class=MsoNormal>
        <span class="pdf-usual-list">
            @php $count = 0 @endphp
            @foreach ($glamping->services as $service)
                @php $count++ @endphp
                @if($count > 1)/ @endif
                {{ $service->name }}
            @endforeach
        </span>
    </p>

    <p class=MsoNormal>
        <b>
            <span class="pdf-big-list">
                {{ trans('pdf.impressions') }}
            </span>
        </b>
    </p>

    <p class=MsoNormal>
        <span class="pdf-usual-list">
            @php $count = 0 @endphp
            @foreach ($glamping->impressions as $impression)
                @php $count++ @endphp
                @if ($count > 1)/ @endif
                {{ $impression->name }}
            @endforeach
        </span>
    </p>

    <hr class="pdf-hr">

    <p class=MsoNormal>
        <span class="pdf-small-list">
            {{ trans('pdf.finalMessage') }}
        </span>
    </p>

    <p class=MsoNormal>
        <span class="pdf-small-list">
            © glampster.ru
        </span>
    </p>

</div>

</body>

</html>