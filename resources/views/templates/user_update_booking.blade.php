<form method="post" action="{{ route('userUpdateBooking', ['id'=>$booking->id]) }}" enctype="multipart/form-data">
    {{ csrf_field() }}

    {{-- Basic information block --}}
    <div class="row">
        <div class="col-xs-6">
            <h3 class="page-header">{{ trans('common_elements.bookEditing') }}</h3>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-10">
            <h4 class="page-header">
                {{ trans('common_elements.bookBookDates') }}
                <strong class="text-danger">*</strong>
            </h4>
            {{-- Dates --}}
            <section class="form-group">
                <div class="dates-block">
                    <div>
                        <p>
                            {{ trans('common_elements.bookDateIn') }}:
                            <input type="date" name="from_date" value="{{ $booking->from_date }}">
                            {{ trans('common_elements.bookDateOut') }}:
                            <input type="date" name="to_date" value="{{ $booking->to_date }}">
                        </p>
                    </div>
                </div>
            </section>
        </div>
    </div>

    {{-- Table with buildings --}}
    <div class="row">
        <div class="col-xs-10">
            <h4 class="page-header">{{ trans('common_elements.buildings') }}</h4>
            <table class="table">
                <tr>
                    <th> {{ trans('common_elements.bookTableCaption') }} </th>
                    <th> {{ trans('common_elements.bookTableCapacity') }} </th>
                    <th> {{ trans('common_elements.bookTablePrice') }} </th>
                    <th> {{ trans('common_elements.bookTableCount') }} </th>
                    <th> {{ trans('common_elements.bookConfirmation') }}</th>
                </tr>
                @php $cur_building = 0 @endphp
                @foreach ($booking->bookBuildings()->get() as $book_building)
                    @php
                        $cur_building++;
                        $building = $book_building->building;
                    @endphp
                    <tr>
                        <td>{{$building->name}}</td>
                        <td>
                            @for ($i = 0; $i < $building->capacity; $i++)
                                <img class="glamping-capacity-man-img"
                                     src="http://s1.iconbird.com/ico/2013/8/429/w512h5121377939745185082manpeoplestreamlineuser.png"/>
                            @endfor
                        </td>
                        <td id="{{ $building->id }}price">
                            @php $date_prices = $building->datePrice(new DateTime('now')) @endphp
                            @if (!array_key_exists('date_prices', $date_prices) ||
                                !sizeof($date_prices['date_prices']))
                                <span class="price-num">
                                {{ $building->main_price }}
                            </span>
                            @else
                                @foreach ($date_prices['date_prices'] as $date)
                                    <span class="price-num">
                                    {{$date->price}}
                                </span>
                                    {{ $date->from }} - {{$date->to}}
                                    <br>
                                @endforeach
                                <span class="price-num">
                                {{ $building->main_price }}
                            </span>
                                {{ trans('common_elements.bookTableStandard') }}
                            @endif
                        </td>
                        <td>
                            <input type="hidden" name="buildings_id[]" value="{{ $building->id }}"/>
                            <input type="number" name="buildings_count[]"
                                   value="{{ $book_building->count }}"
                                   min="0" max="{{ $building->count + $book_building->count }}"
                                   class="buildings-count" id="{{ $building->id }}count"/>
                            {{-- we need to replace max to counter of free places--}}
                        </td>
                        <td>
                            @if ($cur_building == 1)
                                <input type="submit" id="book-submit-button" class="btn btn-success btn-lg"
                                       value="{{ trans('common_elements.bookBookButton') }}"/>
                            @endif
                        </td>
                    </tr>
                @endforeach
            </table>
        </div>
        <input type="hidden" name="booking_id" value="{{ $booking->id }}"/>
    </div>
</form>
