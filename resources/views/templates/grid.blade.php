<table class="table table-bordered table-striped table-hover js-basic-example">
    <thead>
    <tr>
        @foreach ($columns as $columnName => $columnData)
            @php $class = ''; @endphp
            @if ($columnName == 'action' || $columnName == 'description' || $columnName == 'icon' ||
             $columnData['type'] == 'fontIcon'|| $columnName == 'message')
                @php $class = 'sortable-false'; @endphp
            @endif
            <th class="text-center {{ $class }}">{{ $columnData['name'] }}</th>
        @endforeach
    </tr>
    </thead>
    <tbody>
    @foreach ($gridData as $gridItem)
        <tr>
            {{--Columns generator--}}
            @foreach($columns as $columnName => $columnData)
                <?php
                if (isset($arrayGridData)) {
                    $item = null;
                    if (isset($gridItem[$columnName])) {
                        $item = $gridItem[$columnName];
                    }
                    $id = $gridItem['id'];
                } else {
                    $item = $gridItem->$columnName;
                    $id = $gridItem->id;
                }
                switch ($columnData['type']) {
                case 'fontIcon':
                $columnName = $columnData['dbName'];
                $item = $gridItem->$columnName;
                echo '<td class="icon-td"><i class="img-thumbnail ' . $item . '"></i></td>'; break;
                case 'icon':
                echo '<td class="icon-td"><img style="height: 30px" src="' . $item . '"></td>'; break;
                case 'text':
                    if ($columnData['name'] != trans('common_elements.statusColumnName'))
                        echo '<td>' . $item . '</td>';
                    else {
                        echo '<td><div style="padding: 5px" class="';
                        if ($item == trans('booking.confirmedStatus') || $item == trans('applications.accepted'))
                            echo 'bg-green">';
                        else if ($item == trans('booking.notConfirmedStatus')|| $item == trans('applications.pending'))
                            echo 'bg-info">';
                        else
                            echo 'bg-red">';
                        echo $item . '</div></td>';
                    } break;
                case 'action':
                    echo '<td>';
                    if (isset ($columnData['elem']['edit'])) {
                        echo '<a href="' . route($editRoute, ['id' => $id]) . '" title="' .
                                trans("common_elements.btnEdit") . '"><i class="material-icons text-info">create</i></a>';
                    }
                    if (isset ($columnData['elem']['delete'])) {?>
                        <a href="#" onclick="event.preventDefault();
                                            if(confirm('Confirm the deletion?'))
                                                document.getElementById('delete-form-{{ $id }}').submit();"
                                    title="{{ trans("common_elements.btnDelete") }}">
                            <i class="material-icons text-danger">close</i>
                        </a>

                        <form id="delete-form-{{ $id }}" method="POST" action="{{ route($deleteRoute, ['id' => $id]) }}" class="hidden">
                            {{ csrf_field() }}
                            {{ method_field('DELETE') }}
                        </form> <?php
                    }
                    if (isset ($columnData['elem']['view'])) {
                        echo '<a href="' . route($viewRoute, ['id' => $id]) . '" title="' .
                                trans("common_elements.btnView") . '"><i class="material-icons text-info">visibility</i></a>';
                    }
                    if (isset ($columnData['elem']['confirm'])) {
                        echo '<a href="' . route($confirmRoute, ['id' => $id]) .
                                '" title="' . trans("common_elements.btnConfirm") .
                                '"><i class="material-icons text-success">check</i></a>';
                    }
                    if (isset ($columnData['elem']['cancel'])) {
                        echo '<a href="' . route($cancelRoute, ['id' => $id]) .
                                '" title="' . trans("common_elements.btnCancel") .
                                '"><i class="material-icons text-danger">close</i></a>';
                    }
                    echo '</td>'; break;
                }
                ?>
            @endforeach {{--end columns generator--}}
        </tr>
    @endforeach
    </tbody>
</table>
