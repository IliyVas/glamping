{{-- Table with bookings --}}

<table class="table table-striped">
    <tr>
        <th>{{ trans('common_elements.bookTableCaption') }}</th>
        <th>{{ trans('common_elements.bookDateIn') }}</th>
        <th>{{ trans('common_elements.bookDateOut') }}</th>
        <th>{{ trans('common_elements.statusColumnName') }}</th>
        <th>{{ trans('common_elements.bookTablePrice') }}</th>
        <th>{{ trans('common_elements.actionColumnName') }}</th>
    </tr>
    @foreach ($bookings as $booking)
        @php
            if ($booking->status == BookingStatus::NOT_CONFIRMED)
                $status_class = 'info';
            else if (date($booking->to_date) >= date("Y-m-d") && $booking->status !== BookingStatus::CANCELED)
                $status_class = 'success';
            else $status_class = '';
        @endphp
        <tr>
            <td> {{ $booking->bookBuildings()->first()->building->name }} </td>
            <td> {{ $booking->from_date }} </td>
            <td> {{ $booking->to_date }} </td>
            <td> @if ($booking->isPayed() && $booking->status !== BookingStatus::CANCELED)
                    <span class="label label-success">{{ trans('common_elements.wordPayed') }}</span>
                @elseif ($booking->status == BookingStatus::CANCELED)
                    <span class="label label-danger">{{ trans('booking.canceledStatus') }} </span>
                @else
                    <span class="label label-danger">{{ trans('common_elements.wordNotPayed') }} </span>
                @endif
            </td>
            <td>
            <span class="@if ($booking->isPayed()) text-success @else text-danger @endif">
                {{ $booking->price() }} руб.
            </span>
            </td>
            <td>
                @if ($booking->isDate() && !$booking->isPayed() && $booking->status !== BookingStatus::CANCELED)
                    <p>
                        <a href="{{ route('user.userPayBooking', $booking->id) }}" class="label label-danger">
                            {{ trans('common_elements.bookPay') }} </a>
                    </p>
                @endif
                @if ($booking->isFreeCancelable())
                    <p>
                        <a href="{{ route('user.userCancelBooking', $booking->id) }}" class="label label-danger">
                            {{ trans('common_elements.bookCancel') }} </a>
                    </p>
                @elseif ($booking->isCancelable())
                    <p>
                        <a href="{{ route('user.userConfirmCancelBooking', $booking->id) }}" class="label label-danger">
                            {{ trans('common_elements.bookCancel') }} </a>
                    </p>
                @endif
                <p>
                    <a href="{{ $booking->getPdfUrl() }}" class="label label-info" target="blank">
                        {{ trans('common_elements.btnView') }}</a>
                </p>
            </td>
            {{--<td>
                @if ($booking->isActive())
                    <a href="/my/bookings/delete/{{ $booking->id }}">
                        <img class="user_book_edit"
                             src="https://www.shareicon.net/data/2015/12/19/690107_button_512x512.png" />
                    </a>
                @else
                    <button class="pay-button btn btn-success btn-lg">
                        {{ trans('common_elements.wordPay') }}
                    </button>
                @endif
            </td>--}}
        </tr>
        @if (isset($should_show_buildings) && $should_show_buildings)
            @foreach ($booking->bookBuildings as $building)
                <tr>
                    <td> {{ $building->building->name }} ({{ $building->count }}) </td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
            @endforeach
        @endif
    @endforeach
</table>