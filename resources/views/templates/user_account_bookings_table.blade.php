{{-- Information about bookings --}}
<div class="row">
    <div class="col-xs-8">
        <h2 class="page-header">
            {{ trans('common_elements.userYourBookings') }}
        </h2>
        @include('templates.bookings_table')
    </div>
</div>
