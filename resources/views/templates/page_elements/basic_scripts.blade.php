<script src="{{ asset('theme/assets/bundles/libscripts.bundle.js') }}"></script> <!-- Lib Scripts Plugin Js -->
<script src="{{ asset('theme/assets/bundles/vendorscripts.bundle.js') }}"></script> <!-- Lib Scripts Plugin Js -->
<script src="{{ asset('theme/assets/plugins/autosize/autosize.js') }}"></script> <!-- Autosize Plugin Js -->
<script src="{{ asset('theme/assets/plugins/momentjs/moment.js') }}"></script> <!-- Moment Plugin Js -->
<script src="{{ asset('theme/assets/bundles/mainscripts.bundle.js') }}"></script>
<script src="{{ asset('theme/assets/plugins/jquery-sparkline/jquery.sparkline.js') }}"></script> <!-- Sparkline Plugin Js -->
<script src="{{ asset('theme/assets/js/pages/charts/sparkline.js') }}"></script>