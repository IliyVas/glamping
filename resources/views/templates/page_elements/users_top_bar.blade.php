<nav class="navbar">
    <div class="container-fluid">
        <div class="navbar-header">
            <a href="javascript:void(0);" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false"></a>
            <a href="javascript:void(0);" class="bars"></a>
            <a class="navbar-brand" href="{{ route('homepage') }}">{{ config('app.name', 'glamping') }}</a>
        </div>
        <div class="collapse navbar-collapse" id="navbar-collapse">
            <ul class="nav navbar-nav navbar-right">
                {{-- Notifications block--}}
                {{--<li class="dropdown">--}}
                    {{--<a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button">--}}
                        {{--<i class="zmdi zmdi-notifications"></i>--}}
                        {{--<div class="notify"><span class="heartbit"></span><span class="point"></span></div>--}}
                    {{--</a>--}}
                    {{--<ul class="dropdown-menu">--}}
                        {{--<li class="header">{{ mb_strtoupper(trans('common_elements.notifications')) }}</li>--}}
                        {{--<li class="body">--}}
                            {{--<ul class="menu">--}}
                                {{--<li>--}}
                                    {{--<a href="javascript:void(0);">--}}
                                        {{--<div class="icon-circle bg-light-green">--}}
                                            {{--<i class="material-icons">person_add</i>--}}
                                        {{--</div>--}}
                                        {{--<div class="menu-info">--}}
                                            {{--<h4>{{ trans('common_elements.notificationExample') }}</h4>--}}
                                            {{--<p>--}}
                                                {{--<i class="material-icons">access_time</i>--}}
                                                {{--14 {{ trans('common_elements.minsAgo') }}--}}
                                            {{--</p>--}}
                                        {{--</div>--}}
                                    {{--</a>--}}
                                {{--</li>--}}
                            {{--</ul>--}}
                        {{--</li>--}}
                        {{--<li class="footer">--}}
                            {{--<a href="javascript:void(0);">--}}
                                {{--{{ trans('common_elements.viewAll') . ' ' . trans('common_elements.notifications') }}--}}
                            {{--</a>--}}
                        {{--</li>--}}
                    {{--</ul>--}}
                {{--</li>--}}
                {{-- end notifications block--}}

                {{-- Logout block --}}
                <li>
                    <a href="/logout" class="mega-menu" data-close="true"
                       onclick="event.preventDefault();
                                                         document.getElementById('logout-form').submit();">
                        <i class="zmdi zmdi-power"></i>
                    </a>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="hidden">
                        {{ csrf_field() }}
                    </form>
                </li>{{-- end logout block --}}

                {{-- Settings block--}}
                <li class="pull-right">
                    <a href="javascript:void(0);" class="js-right-sidebar" data-close="true">
                        <i class="zmdi zmdi-sort-amount-desc"></i>
                    </a>
                </li>
            </ul>
        </div>
    </div>
</nav>