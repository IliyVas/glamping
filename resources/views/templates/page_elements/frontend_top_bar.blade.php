<div class="wrapper-border">
    <div class="header-top">
        <!-- Branding Image -->
        <div class="logo">
            <a class="default-link" href="{{ route('homepage') }}">
                <img src="{{ asset('images/Glampster_logo_horizontal.png') }}" alt="Glampster">
            </a>
        </div>
        <div class="menu">
            <ul class="menu-list">
                {{--Filter page--}}
                <li><a href="{{ route('filterPage') }}" class="default-link">
                        {{ trans('common_elements.findOffers') }}</a></li>
                {{--Countries block--}}
                <li class="dropdown-open">
                    <a href="#" class="default-link link-arrow">{{ trans('common_elements.destinations') }}</a>
                    <ul class="dropdown">
                        @php $countries = GlampingController::getGlampingsCountriesList(); @endphp
                        @if (!is_null($countries) && count($countries) > 0)
                            @foreach($countries as $country)
                                <li class="drop-elem">
                                    <a href="{{ route('filterPage', ['country' => $country]) }}" class="default-link">
                                        {{ $country }}
                                    </a>
                                </li>
                            @endforeach
                        @endif
                    </ul>
                </li>
                {{--Building types block--}}
                <li class="dropdown-open">
                    <a href="#" class="default-link link-arrow">{{ trans('common_elements.kindOfGlampings') }}</a>
                    <ul class="dropdown">
                        @if (count($buildingTypes) > 0)
                            @foreach($buildingTypes as $buildingType)
                                <li class="drop-elem">
                                    <a href="{{ route('filterPage', ['building_types' => [$buildingType->id]]) }}" class="default-link">
                                        {{{ $buildingType->name }}}
                                    </a>
                                </li>
                            @endforeach
                        @endif
                    </ul>
                </li>
                {{--Pet-friendly glampings block--}}
                <img src="{{ asset('images/animal-prints.png') }}" alt="animal" class="animal-hands">
                <li class="green">
                    <a href="{{ route('filterPage', ['glamping_groups' => 1]) }}" class="default-link">
                        {!! trans('common_elements.petFriendlyGlampings') !!}
                    </a>
                </li>
                {{--Feedback--}}
                <li><a href="{{ route('feedbackForm') }}" class="default-link">{{ trans('common_elements.support') }}</a></li>
                {{--Owner registration--}}
                @if (!MultiAuth::check())
                    <li><a href="{{ route('manager_register_page') }}" class="default-link">
                            {{ trans('common_elements.owner_register') }}</a></li>
                @endif
            </ul>
        </div>
        <div class="auth">
            @if (!MultiAuth::check())
                <a href="{{ route('user_login_page') }}" class="auth-btn">{{ trans('common_elements.login') }}</a>
                <a href="{{ route('user_register') }}" class="auth-btn">{{ trans('common_elements.register') }}</a>
            @else
                <li><a class="auth-btn" href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                        {{ trans('common_elements.logout') }}
                    </a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                    </form>
                </li>
            @endif
        </div>
        <!-- Lang block -->
        <div class="select-money">
            @foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties)
                @if (config('app.locale') !== $localeCode)
                    <a rel="alternate" hreflang="{{ $localeCode }}"
                       href="{{ LaravelLocalization::getLocalizedURL($localeCode, null, [], true) }}"
                       class="default-link">{{ mb_strtoupper($localeCode) }}</a>
                @endif
            @endforeach
        </div>
    </div>
</div>