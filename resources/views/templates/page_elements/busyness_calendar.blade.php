<script type="text/javascript">
    function dateToInputFormat(date) {
        var day = ("0" + date.getDate()).slice(-2);
        var month = ("0" + (date.getMonth() + 1)).slice(-2);

        return date.getFullYear()+"-"+(month)+"-"+(day) ;
    }

    function addCalendar() {

        //this method paints rows in two colors (building prices and counts)
        function statusFormatter(row, cell, value, columnDef, dataContext) {
            var rtn = { text: value, removeClasses: 'red orange green' };
            if (row % 2 == 0)
                rtn.addClasses = 'calendar-count-row';
            else
                rtn.addClasses = 'calendar-price-row';
            return rtn;
        }

        var grid;
        var year = (new Date).getFullYear();
        var pub_ranges, pub_row;
        var data = [];
        //generate columns names
        var columns = [
            {
                id: 'building_names',
                name: '{{ trans('common_elements.buildingNames') }}',
                field: 'building_names',
                width: 130
            },
            @foreach ($dates[0] as $date_count)
            {
                id: '{{ $date_count['id'] }}',
                name: '{{ str_replace('-', ' ', substr($date_count['date'], 5)) }}',
                field: '{{ str_replace('-', '', $date_count['date']) }}',
                width: 43,
                formatter: statusFormatter
            },
            @endforeach
        ];
        var options = {
            enableCellNavigation: true,
            enableColumnReorder: false,
            multiSelect: false
        };
        $(function () {
            @for ($i = 0; $i < sizeof($dates); $i++)
            data[{{ 2 * $i }}] = {
                building_names: '{{ $buildings[$i]->name }}',
                @foreach ($dates[$i] as $date_count)
                {{ str_replace('-', '', $date_count['date']) }}: '{{ $date_count['count'] }}',
                @endforeach
                building_id: '{{ $dates[$i][0]['building_id'] }}',
                edit_type: 'count'
            };
            data[{{ 2 * $i + 1 }}] = {
                building_names: '',
                @foreach ($dates[$i] as $date_count)
                {{ str_replace('-', '', $date_count['date']) }}: '{{ $date_count['price'] }}',
                @endforeach
                building_id: '{{ $dates[$i][0]['building_id'] }}',
                edit_type: 'price'
            };
            @endfor

            grid = new Slick.Grid("#calendar", data, columns, options);

            grid.setSelectionModel(new Slick.CustomSelectionModel());
            grid.onSelectedRowsChanged.subscribe(function () {
                var ranges = grid.getSelectionModel().getSelectedRanges()[0],
                    row_number = ranges.fromRow,
                    first_column = grid.getColumns()[ranges.fromCell],
                    second_column = grid.getColumns()[ranges.toCell],
                    row = grid.getData()[row_number],
                    building_id = row['building_id'],
                    edit_type = row['edit_type'];

                pub_ranges = ranges;
                pub_row = row_number;

                var from_date = year + '-' + first_column.name.split(' ').join('-'),
                    to_date = year + '-' + second_column.name.split(' ').join('-');
                $('#from_date').val(from_date);
                $('#to_date').val(to_date);
                $('#building_id').val(building_id);
                $('#edit_type').val(edit_type);
            });
        });

        $('#set_buildings_count').click(function() {
            var from_date = $('#from_date').val();
            var to_date = $('#to_date').val();
            var value = $('#value').val();
            var building_id = $('#building_id').val();
            var edit_type = $('#edit_type').val();
            if (edit_type == 'count')
                $.ajax({
                    url: '{{ route('manager.glampings.building_counts_update') }}',
                    type: 'POST',
                    dataType: "json",
                    data: {
                        from_date: from_date,
                        to_date: to_date,
                        count: value,
                        building_id: building_id,
                        _token: '{{ csrf_token() }}'
                    },
                    success: function (ajax_data, textStatus) {
                        if (ajax_data == 0)
                            console.log('Error!')
                        else {
                            var columns = grid.getColumns()
                            for (var i = pub_ranges.fromCell; i <= pub_ranges.toCell; i++)
                                data[pub_ranges.toRow][columns[i].field] = value;
                            grid.invalidateRow(pub_ranges.toRow);
                            grid.render();
                        }
                    },
                    error: function(error) {
                        console.log('Error!');
                    }
                });
            else
                $.ajax({
                    url: '{{ route('manager.glampings.building_prices_update') }}',
                    type: 'POST',
                    dataType: "json",
                    data: {
                        from_date: from_date,
                        to_date: to_date,
                        price: value,
                        building_id: building_id,
                        _token: '{{ csrf_token() }}'
                    },
                    success: function (ajax_data, textStatus) {
                        if (ajax_data == 0)
                            console.log('Error!')
                        else {
                            var columns = grid.getColumns()
                            for (var i = pub_ranges.fromCell; i <= pub_ranges.toCell; i++)
                                data[pub_ranges.toRow][columns[i].field] = value;
                            grid.invalidateRow(pub_ranges.toRow);
                            grid.render();
                        }
                    },
                    error: function(error) {
                        console.log('Error!');
                    }
                });

        });
    }

    $(document).ready(function() {
        // page is now ready, initialize the calendar...
        addCalendar();
    });
</script>