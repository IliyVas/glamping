<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<!-- Font Icon -->
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">
<!-- Custom Css -->
<link href="{{ asset('theme/assets/css/main.css') }}" rel="stylesheet">
<!-- themes Css -->
<link href="{{ asset('theme/assets/css/themes/all-themes.css') }}" rel="stylesheet" />