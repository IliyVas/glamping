{{-- Errors block --}}
@if (count($errors) > 0)
    <div class="col-xs-6 col-xs-offset-3 alert alert-danger">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <ul>
            @foreach ($errors->all() as $error)
                <li><h4>{{ $error }}</h4></li>
            @endforeach
        </ul>
    </div>
@endif {{-- end errors block --}}

{{-- Message block --}}
@if (session()->has('flash_notification.message'))
    <div class="col-xs-6 col-xs-offset-3 alert alert-{{ session('flash_notification.level') }}">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>

        <h4>{!! session('flash_notification.message') !!}</h4>
    </div>
@endif {{-- end message block --}}