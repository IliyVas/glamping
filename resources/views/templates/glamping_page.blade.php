{{-- Basic information block --}}
<div class="row">
    <div class="col-xs-6">
        <h3 class="page-header glamping-name">{{ $glamping->name }}</h3>
        <h5>{{ $glamping->country }}, {{ $glamping->region }}</h5>
        <input type="hidden" id="longitude_input" value="{{ $glamping->longitude }}">
        <input type="hidden" id="latitude_input" value="{{ $glamping->latitude }}">
        <div id="map"></div>
    </div>
    <div class="col-xs-6 glamping-image-block">
        @foreach ($glamping->media()->get() as $image)
            <img src="{{ $image->getUrl() }}">
        @endforeach
    </div>
</div>

{{-- Aditional information--}}
<div class="row">
    <div class="col-sm-2 col-md-1">
        {{ trans('common_elements.descriptionInputLabel') }}
    </div>
    <strong class="col-sm-9 col-md-10"> {{ $glamping->description }}</strong></br>
    @if(count($glamping->glampingGroups()->get()) > 0)
        <div class="col-sm-2 col-md-1">{{ trans('common_elements.groups') }}:</div>
        <strong class="col-sm-9 col-md-10">
            @foreach($glamping->glampingGroups()->get() as $group)
                {{ $group->name }};
            @endforeach
        </strong></br>
    @endif
    @if(count($glamping->impressions()->get()) > 0)
        <div class="col-sm-2 col-md-1">{{ trans('common_elements.impressions') }}:</div>
        <strong class="col-sm-9 col-md-10">
            @foreach($glamping->impressions()->get() as $impression)
                {{ $impression->name }};
            @endforeach
        </strong></br>
    @endif
    @if(count($glamping->services()->get()) > 0)
        <div class="col-sm-2 col-md-1">{{ trans('common_elements.services') }}:</div>
        <strong class="col-sm-9 col-md-10">
            @foreach($glamping->services()->get() as $service)
                {{ $service->name }};
            @endforeach
        </strong></br>
    @endif
</div>

<form method="post" action="{{ route('bookAdditionalInfo') }}" enctype="multipart/form-data">
    {{ csrf_field() }}
    <input type="hidden" name="glamping_id" value="{{ $glamping->id }}">

    <div class="row form-group glamping-image-block">
        {{-- arrival date block --}}
        @if(isset($filter) && isset($filter['from']))
            @php $fromDate = $filter['from']; @endphp
        @else
            @php $fromDate = old('from_date'); @endphp
        @endif
        <div class="col-xs-3">
            {{ trans('common_elements.bookDateIn') }}:
            <input id="arrival_date" min="{{ date('Y-m-d', time()) }}"
                   name="from_date" type="date" value="{{ $fromDate or date('Y-m-d', time())}}">
        </div>

        {{-- county date block --}}
        @if(isset($filter) && isset($filter['to']))
            @php $toDate = $filter['to']; @endphp
        @else
            @php $toDate = old('to_date'); @endphp
        @endif
        <div class="col-xs-3">
            {{ trans('common_elements.bookDateOut') }}:
            <input min="{{ date('Y-m-d', time() + (24*60*60)) }}" id="county_date"
                   name="to_date" type="date" value="{{ $toDate or date('Y-m-d', time() + 24*60*60) }}">
        </div>

        {{--Adults--}}
        @if(isset($filter) && isset($filter['adults']))
            @php $adults = $filter['adults']; @endphp
        @else
            @php $adults = old('adults'); @endphp
        @endif
        <div class="col-xs-2">
            {{ trans('common_elements.bookAdultsCount') }}:
            <input name="adults" type="number" value="{{ $adults }}" min="0" max="100">
        </div>

        {{--Children--}}
        @if(isset($filter) && isset($filter['children']))
            @php $children = $filter['children']; @endphp
        @else
            @php $children = old('children'); @endphp
        @endif
        <div class="col-xs-2">
            {{ trans('common_elements.bookChildrenCount') }}:
            <input name="children" type="number" value="{{ $children }}" min="0" max="100">
        </div>

        {{--Buildings--}}
        @if(isset($filter) && isset($filter['count']))
            @php $count = $filter['count']; @endphp
        @else
            @php $count = old('buildings'); @endphp
        @endif
        <div class="col-xs-2 form-container form-group">
            {{ trans('common_elements.bookCountOfBuildings') }}:
            <input name="buildings" type="number" value="{{ $count}}"
                   min="0" max="100">
        </div>

        <div class="col-xs-12">
            <div class="col-xs-6 form-container">
                <h5>
                    <strong class="text-danger">***</strong>{{ trans('booking.maxChildAgeInf') }}: {{ $glamping->max_child_age }}
                </h5>
            </div>
            <div class="col-xs-6 text-right">
                <span class="btn btn-primary" id="filter-btn">{{ trans('common_elements.btnApply') }}</span>
            </div>
        </div>
    </div>

    {{-- Table with buildings --}}
    <div class="row">
        <div class="col-xs-10">
            <h4 class="page-header">{{ trans('common_elements.buildings') }}</h4>
            <div id="ajax-content">
                @include('manager.owner.bookings.full_table')
            </div>
        </div>
        <input type="hidden" name="glamping_id" value="{{ $glamping->id }}"/>
    </div>
    {{--Button block--}}
    <div class="row" id="book-button-block">
        <div class="col-xs-10 form-group">
            <input type="submit" class="btn btn-success btn-lg pull-right" id="book-submit-button"
                   value="{{ trans('common_elements.bookBookButton') }}"/>
        </div>
    </div>
</form>
