<div class="col-md-8 col-md-offset-4 social-block">
    <a href="{{ route('social_login', 'vkontakte') }}" title="VK ">
        <img class="social-image" src="https://www.footboom.kz/img/new-images/icons/social/vk.png" />
    </a>
    <a href="{{ route('social_login', 'google') }}">
        <img class="social-image" src="https://s-media-cache-ak0.pinimg.com/originals/99/de/58/99de5884d9fa270bdeabd623c7659684.png" />
    </a>
</div>