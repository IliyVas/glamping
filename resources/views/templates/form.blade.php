@php
$required = 'required';
$btn = trans('common_elements.btnCreate');
$star = '<strong class="text-danger">*</strong>';
@endphp

@if (isset($model))
    @php
    $required = null;
    $btn = trans('common_elements.btnSave');
    @endphp
@endif

{!! Form::open([ 'route' => $actionRoute, 'id' => 'form1', 'runat' => 'server',
'enctype' => 'multipart/form-data', 'role' => 'form']) !!}

{{ method_field($method) }}

@if (!empty($transFields))
    <div class="row">
        {{--Lang tabs--}}
        <div class="col-xs-12">
            <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="active">
                    <a href="#profile_with_icon_title" data-toggle="tab">RU</a>
                </li>
                <li role="presentation">
                    <a href="#home_with_icon_title" data-toggle="tab">EN</a>
                </li>
            </ul>
        </div>

        <div class="col-sm-8">
            <!-- Tab panes -->
            <div class="tab-content">
                <!--Tabs RUSSIAN----------------------------->
                <div role="tabpanel" class="tab-pane fade in active" id="profile_with_icon_title">
                    @foreach ($transFields as $fieldName => $fieldData)
                        @php $fieldType = $fieldData['type']; @endphp
                        <?php switch ($fieldType) {
                            case 'text':
                            case 'textarea':
                                if (isset($model) && $model->hasTranslation('ru')) {
                                    $value = $model->getTranslation('ru')->$fieldName;
                                } else {
                                    $value = null;
                                }
                                echo '<div class="form-group">' . Form::label($fieldName, $fieldData['label']) . $star;
                                echo '<div class="form-line">' .
                                        Form::$fieldType($fieldName . ":ru", $value, ['class' => 'form-control', 'rows' => '3']) .
                                        '</div></div>'; break;
                        }?>
                    @endforeach
                </div><!-- #Tabs RUSSIAN -->

                <!--Tabs  ENGLISH --------------------------->
                <div role="tabpanel" class="tab-pane fade" id="home_with_icon_title">
                    @foreach ($transFields as $fieldName => $fieldData)
                        @php $fieldType = $fieldData['type']; @endphp
                        <?php switch ($fieldType) {
                            case 'text':
                            case 'textarea':
                                if (isset($model) && $model->hasTranslation('en')) {
                                    $value = $model->getTranslation('en')->$fieldName;
                                } else {
                                    $value = null;
                                }
                                echo '<div class="form-group">' . Form::label($fieldName, $fieldData['label']) . $star;
                                echo '<div class="form-line">' .
                                    Form::$fieldType($fieldName . ":en", $value, ['class' => 'form-control', 'rows' => '5']) .
                                    '</div></div>'; break;
                        }?>
                    @endforeach
                </div> <!--#Tabs  ENGLISH -->
            </div><!-- #Tab panes -->
        </div>
    </div>
@endif

<div class="row">
    @foreach ($fields as $fieldName => $fieldData) {{-- $fieldData contains type, label, and, if $fieldName = 'fontIcon', contain name--}}
    @php $fieldType = $fieldData['type']; @endphp
    {{--Label and icons list--}}
    @if ($fieldName == 'fontIcon')
        <div class="col-xs-12">
            {!!  Form::label($fieldName, $fieldData['label']) . $star !!}
            <div class="font-icons">
                @include('templates.icons_list')
            </div>
        </div>
    @endif

    {{--Labels and preview block--}}
    @if (!in_array($fieldType, ['hidden', 'button', 'submit']))
        <div class="col-xs-6">
            {{--Preview for icon of image type--}}
            @if ($fieldName == 'icon')
                <div class="form-group">
                    {!!  Form::label($fieldName, $fieldData['label']) !!} </br>
                    <img id="iconImg" class="img-thumbnail icon" alt="Icon"
                         src="@if(isset($model)){{ asset($model->icon) }}@else # @endif">
                </div>
            @endif
            {{--Preview for image--}}
            @if ($fieldName == 'image')
                <div class="form-group">
                    {!!  Form::label($fieldName, $fieldData['label']) !!} </br>
                    <img id="mainImg" class="img-circle" alt="Image"
                         src="@if(isset($model)){{ asset($model->image) }}@else # @endif">
                </div>
            @endif
            @endif

            {{--Fields generator--}}
            <?php
            switch ($fieldType) {
                case 'hidden':
                    $val = null;
                    if (isset($model))
                        $val = $model->icon;

                    echo Form::$fieldType($fieldData['name'], $val); break;
                case 'color':
                case 'date':
                case 'datetime':
                case 'datetimeLocal':
                case 'time':
                case 'url':
                case 'email':
                case 'number':
                case 'password':
                case 'tel':
                    echo '<div class="form-line">' .
                            Form::$fieldType($fieldName, null, ['class' => 'form-control']) .
                            '</div>'; break;
                case 'file':
                    echo  Form::$fieldType($fieldName, ['accept' => 'img/jpeg, img/png', $required]); break;
                case 'submit':
                case 'button':
                    echo '<div class="col-xs-12">' .
                            Form::$fieldType($btn, [
                                    'class' => 'btn btn-raised btn-primary m-t-15 waves-effect pull-right']); break;
            }?>
        </div>
        @endforeach
</div>
{!! Form::close() !!}

@section('additional_scripts')
    @if (!isset($model))
        <script>
            $('#mainImg').hide();
            $('#iconImg').hide();
        </script>
    @endif
    <script src="{{ mix('js/icon.js') }}" type="text/javascript"></script>
@endsection