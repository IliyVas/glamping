@if ($glampings !== null)
    @foreach($glampings as $glamping)
        @if (count($glamping->buildings()->get()) > 0)
            <div class="col-xs-12 glamping-block">
                <h2 class="page-header text-center">
                    <a href="{{ route('viewGlamping', ['id' => $glamping->id, 'count' => $buildings,
                    'children' => $children, 'adults' => $adults, 'from' => $fromDate, 'to' => $toDate]) }}">
                        {{ $glamping->name }}
                    </a>
                </h2>
                @if (!is_null($glamping->media()->first()))
                    <img class="glamping-image"
                         src="{{ $glamping->media()->first()->getUrl() }}">
                @endif
                @php $discountData = $glamping->getDiscountData(); @endphp
                @if (is_null($discountData))
                    <label class="text-danger pull-right min-glamping-price">
                        {{ trans('common_elements.wordFrom') . ' ' . $glamping->min_price }}
                    </label>
                @else
                    <section class="pull-right">
                        <label class="text-danger min-glamping-price">
                            {{ trans('common_elements.wordFrom') }}</label>
                        <label style="text-decoration: line-through" class="min-glamping-price">
                            {{ ' ' . $discountData['price'] }}</label>
                        <label class="text-danger min-glamping-price">
                            {{ ' ' . $glamping->min_price }}</label><br>
                        <label class="text-danger min-glamping-price">
                            {{ trans('common_elements.sale') . ' ' . $discountData['sale'] . '%' }}</label>
                    </section>
                @endif
                <br>
                <p class="control-label pull-right min-glamping-price">
                    <a href="{{ route('viewGlamping', ['id' => $glamping->id, 'count' => $buildings,
                    'children' => $children, 'adults' => $adults, 'from' => $fromDate, 'to' => $toDate]) }}">
                        {{ trans('common_elements.readMore') }}
                    </a>
                </p>
            </div>
        @endif
    @endforeach
@else
    {{-- Message block --}}
    <div class="col-xs-12 alert alert-warning">
        <h4>{{ trans('messages.nothingFoundFilterResult') }}</h4>
    </div>
@endif