$.ajaxSetup({
    headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') }
});

function switchViewed(message_id, obj) {

    $.ajax({
        method:'POST',
        url: '/manager/admin/messages/viewed',
        data:{
            mes_id: message_id,
            val : $(obj).val()
        },
        success: function(data){
            if(data.status){
                $(obj).closest('tr').toggleClass('non-viewed');
            }
        }
    });
}
