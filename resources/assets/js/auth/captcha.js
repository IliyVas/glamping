$('#refresh').on('click',function(){
    var captcha = $('img.captcha-img');
    var config = captcha.data('refresh-config');
    $.ajax({
        method: 'GET',
        url: '/' + $('html').attr('lang') + '/get_captcha/' + config
    }).done(function (response) {
        captcha.prop('src', response);
    });
});
