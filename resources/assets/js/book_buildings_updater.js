$( window ).load(function() {
    var fromDate = $('#arrival_date');
    fromDate.change(onDatesChange);

    var toDate = $('#county_date');
    toDate.change(onDatesChange);

    if (fromDate !== '' && toDate !== '') {
        applyFilterData();
    }

    $('#filter-btn').click(function() {
        if (fromDate !== '' && toDate !== '') {
            applyFilterData();
        }
    });

    function applyFilterData() {
        var glampingId = $('input[name="glamping_id"]').val();
        var adults = $('input[name="adults"]').val();
        var count = $('input[name="buildings"]').val();
        var children = $('input[name="children"]').val();

        var ajaxContent = $('#ajax-content');
        ajaxContent.addClass('ajax-loader');
        jQuery.ajax({
            type: 'POST',
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            url: '/' + $('html').attr('lang') + '/glamping/' + glampingId + '/ajax',
            data: {
                _token: $('input[name="_token"]').val(),
                from_date: fromDate.val(),
                to_date: toDate.val(),
                adults: adults,
                buildings: count
            },
            success: function(html){
                ajaxContent.html(html);
                ajaxContent.removeClass('ajax-loader');

                setLocation('?count=' + count + '&children=' + children + '&adults=' + adults +
                '&from=' + fromDate.val() + '&to=' + toDate.val());
            },

            error: function(){
                ajaxContent.removeClass('ajax-loader');
            }
        });
    }

    function setLocation(curLoc){
        try {
            history.pushState(null, null, curLoc);
            return;
        } catch(e) {}
        location.hash = '#' + curLoc;
    }

    function onDatesChange() {
        var arrivalDate = fromDate.val().trim();
        var countyDate = toDate.val().trim();

        if (countyDate !== '') {
            fromDate.attr('max', countyDate)
        }

        if (arrivalDate !== '') {
            toDate.attr('min', arrivalDate)
        }
    }

    function updateBuildingData(){
        var to = toDate.val();
        var from = fromDate.val();

        //make request if from and to is ok
        if (from != '' && to != '') {
            var request = new XMLHttpRequest();
            request.open('GET', '/glamping/get_prices/' + $('input[name="glamping_id"]').val() + '/' + from + '/' + to);
            request.onreadystatechange = function () {
                if (request.readyState == 4) {
                    if (request.status == 200) {
                        var parse = JSON.parse(request.responseText);
                        var word_standard = parse['translations']['standard'];
                        var info = parse['info'];
                        var i, j, id, standard_price, build, el, added_prices;

                        for (i = 0; i < info.length; i++) {
                            build = info[i];
                            id = build['id'];
                            el = $('#' + id + 'price')[0];
                            $(el).html('');
                            standard_price = build['prices']['main_price']; //standard price
                            added_prices = build['prices']['date_prices'];

                            //additional prices
                            if (added_prices.length > 0) {
                                for (j = 0; j < added_prices.length; j++)
                                    $(el).append('<span class="price-num">' + added_prices[j]['price'] + '</span> ' +
                                    added_prices[j]['from'] + ' - ' + added_prices[j]['to'] + '<br>');
                                $(el).append('<span class="price-num">' +
                                standard_price + '</span> ' + word_standard);
                            }
                            else
                                $(el).append('<span class="price-num">' +
                                standard_price + '</span>');
                        }
                    } else {
                        console.log('Error: ' + request.status);
                    }
                }
            };
            request.send(null);
        }
    }
});