$( window ).on('load', function() {
    $('strong[class="reestablish_img"]').hide(); //Hide recovery arrows
});

function deleteOldImg(id) {
    $('#' + id).addClass('inactive-block');
    $('#' + id + '-del').hide(); //hide delete button
    $('#' + id + '-rees').show(); //show recovery arrow
    $('#' + id + '-input').val(''); //unset image $id in exist image input
    $('#' + id + '-del-input').val(id); //set image $id in deleted image input
}

function reestablishOldImg(id) {
    $('#' + id).removeClass('inactive-block');
    $('#' + id + '-rees').hide(); //hide recovery arrow
    $('#' + id + '-del').show(); //show delete button
    $('#' + id + '-input').val(id); //set image $id in exist image input
    $('#' + id + '-del-input').val(''); //unset image $id in deleted image input
}

//A global variable where the data of number of glamping image inputs will be located.
var imageCount = 0;

function uploadGlampingImg(input) {
    //A variable where the data of image will be located.
    var files = input.files;
    //Get general value for common elements (div(id), input(data-img), img(src)).
    var divName = input.getAttribute('data-img');

    //Check whether $files is empty.
    if (files.length === 0) {
        //Clear of the content of the div.
        $('div[id="'+divName+'"]').html('');

        return;
    }

    //Create the form data and add the image data to them from $files.
    var fd = new FormData();
    $.each( files, function( key, value ){
        fd.append( key, value );
    });

    //Send request.
    $.ajax({
        url: '/glamping-owner/glamping/create/ajax-upload',
        type: 'POST',
        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
        data: fd,
        processData: false, //Don't process the files
        contentType: false,
        success:function(uploadedImagePath){

            // Set new value to the input attribute('data-img'). It's general value for common elements (div(id), input(data-img), img(src)).
            input.setAttribute('data-img', uploadedImagePath);
            var parent = $(input).parent();
            var preview_img = '<img src="' + uploadedImagePath + '">';
            var delete_button ='<label class="glamping-image-label" id="' + parent.attr('id') + '-del">' +
                '<strong onclick="deleteImgById(\'' + parent.attr('id') + '\')" class="delete_img">&#215;</strong>' +
                '</label>';

            //Insert content in div.
            $(preview_img).insertAfter(input);
            $(delete_button).insertAfter($(parent));

            //Increase the number of image inputs per unit.
            imageCount++;

            //Get the maximum number of image inputs
            var maxCountImg = document.getElementById('add_attr').getAttribute('data-max-count');

            //Detach add_attr button from its place
            var add_img_button = $('#add_attr');
            //add_img_button.detach();

            //If $imageCount exceeds the maximum allowed value, hide the add button.
            if (imageCount <= maxCountImg) {
                //Get general value for common elements (div(id), input(data-img), img(src)).
                var newDivName = Math.random().toString(36);

                var str = '<input id="' + newDivName + '" data-img="' + newDivName + '" onchange="uploadGlampingImg(this)" name="images[]" type="file" ' +
                    'class="form-control glamping-image-input" accept="image/png, image/jpeg">';

                //Get last label block
                var label_block_caption = '.glamping-image-label';
                var label_block = $(label_block_caption);
                label_block = label_block[label_block.length - 1];

                //Insert label block
                var new_label_block = '<label class="glamping-image-label" id="label-' + newDivName + '"></label>';
                $(new_label_block).insertAfter($(label_block));

                //Get added label block
                var label_block = $(label_block_caption);
                label_block = label_block[label_block.length - 1];

                //Insert to added label block
                $(label_block).html(str);
                $(label_block).append(add_img_button);
            }
        }
    });
}

$( window ).load(function() {
    $('.current-image-label').click(function(){
        var exist = $('input[name="glamping_images[]"][value="' + this.id + '"]');
        if (!exist.length) {
            var html = '<input type="hidden" name="glamping_images[]" value="' + this.id + '">';
            $(html).insertAfter(this);
            $(this).css("opacity", "1");
        }
    });
});

function deleteImgById(id) {
    //Show the add button.
    $('#add_attr').show();
    //Check the number of inputs.
    //Reduce the number of image inputs per unit.
    imageCount--;
    //Delete the div and the input.
    $('label[id="'+id+'"]').remove();
    $('label[id="' + id + '-del"]').remove();
}