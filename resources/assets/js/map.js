$( window ).load(function() {
    ymaps.ready(init);
});

function init() {
    var myPlacemark,
        myMap = new ymaps.Map('map', {
            center: [55.753994, 37.622093],
            zoom: 9
        }, {
            searchControlProvider: 'yandex#search'
        });

    var longInput = $('#longitude_input'), latInput = $('#latitude_input');
    var mapInputs = $('.completeWithMap');

    if (longInput.val().trim() !== '' && latInput.val().trim() !== ''){
        var myCoords = [parseFloat(latInput.val().trim()),parseFloat(longInput.val().trim())];
        myMap.setCenter(myCoords);
        mapInputs.attr('disabled', 'disabled');
        getPlacemark(myCoords);
        getAddress(myCoords);
    }

    longInput.change(onCoordinatesChange);
    latInput.change(onCoordinatesChange);

    function onCoordinatesChange() {
        var long = longInput.val().trim();
        var lat = latInput.val().trim();

        if (lat == '' || long == '') {
            return;
        }

        mapInputs.attr('disabled', 'disabled');
        var myCoords = [parseFloat(lat),parseFloat(long)];

        getPlacemark(myCoords);
        getAddress(myCoords);
    }

    function getPlacemark(coords) {
        // Если метка уже создана – просто передвигаем ее.
        if (myPlacemark) {
            myPlacemark.geometry.setCoordinates(coords);
        }
        // Если нет – создаем.
        else {
            myPlacemark = createPlacemark(coords);
            myMap.geoObjects.add(myPlacemark);
            // Слушаем событие окончания перетаскивания на метке.
            myPlacemark.events.add('dragend', function () {
                getAddress(myPlacemark.geometry.getCoordinates());
            });
        }
    }

    myMap.events.add('click', function (e) {
        var coords = e.get('coords');
        mapInputs.attr('disabled', 'disabled');
        getPlacemark(coords);
        getAddress(coords);
    });

    // Создание метки.
    function createPlacemark(coords) {
        return new ymaps.Placemark(coords, {
            iconCaption: 'поиск...'
        }, {
            preset: 'islands#violetDotIconWithCaption',
            draggable: true
        });
    }

    // Определяем адрес по координатам (обратное геокодирование).
    function getAddress(coords) {
        myPlacemark.properties.set('iconCaption', 'поиск...');
        ymaps.geocode(coords).then(function (res) {
            var firstGeoObject = res.geoObjects.get(0);

            myPlacemark.properties
                .set({
                    // Формируем строку с данными об объекте.
                    iconCaption: [
                        // Название населенного пункта или вышестоящее административно-территориальное образование.
                        firstGeoObject.getLocalities().length ? firstGeoObject.getLocalities() : firstGeoObject.getAdministrativeAreas(),
                        // Получаем путь до топонима, если метод вернул null, запрашиваем наименование здания.
                        firstGeoObject.getThoroughfare() || firstGeoObject.getPremise()
                    ].filter(Boolean).join(', '),
                    // В качестве контента балуна задаем строку с адресом объекта.
                    balloonContent: firstGeoObject.getAddressLine()
                });

            setInputsValues(coords, firstGeoObject);
            mapInputs.removeAttr('disabled');
        });
    }
}

function setInputsValues(coords, firstGeoObject) {
    var lat = $('#latitude_input');
    lat.val(coords[0].toFixed(5)); //set latitude

    var long = $('#longitude_input');
    long.val(coords[1].toFixed(5)); //set longitude

    var lang = $('html').attr('lang'); //get active lang

    var langLocale = 'en_US';
    var defaultLang = 'ru';
    var defaultLangLocale = 'ru_RU';

    if (lang == 'ru') {
        defaultLang = 'en';
        defaultLangLocale = 'en_US';
        langLocale = 'ru_RU';
    }

    var url = 'https://geocode-maps.yandex.ru/1.x/?geocode=' + long.val() + ',' + lat.val() +
        '&format=json&lang=';
    //set default lang data
    $.ajax({
        url: url + defaultLangLocale,
        type: 'GET',
        success:function(res){
            var firstGeoObject = res.response.GeoObjectCollection.featureMember[0].GeoObject.metaDataProperty.GeocoderMetaData.Address.Components;
            $('#country_input_' + defaultLang).val(firstGeoObject[0].name);
            $('#region_input_' + defaultLang).val(firstGeoObject[2].name);
        }
    });
    //set active lang data
    $.ajax({
        url: url + langLocale,
        type: 'GET',
        success:function(res){
            var firstGeoObject = res.response.GeoObjectCollection.featureMember[0].GeoObject.metaDataProperty.GeocoderMetaData.Address.Components;
            $('#country_input_' + lang).val(firstGeoObject[0].name);
            $('#region_input_' + lang).val(firstGeoObject[2].name);
        }
    });
}
