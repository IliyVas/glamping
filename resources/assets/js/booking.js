$( window ).load(function() {
    var val = $('#glamping_id option:selected').val();
    var fromDate = $('#from_date');
    var toDate = $('#to_date');

    if (val == "0") {
        $('#disable-block').addClass('inactive-block');
        $('#book-button-block').addClass('inactive-block');
        $('#book-button').attr('disabled', true);
        fromDate.attr('disabled', true);
        toDate.attr('disabled', true);
    } else {
        getBuildingData(val);
    }

    fromDate.on('change', activateBlock);
    toDate.on('change', activateBlock);
});

function activateBlock() {
    var val = $('#glamping_id option:selected').val();
    var fromDate = $('#from_date');
    var toDate = $('#to_date');
    
    if (val == "0") {
        $('#disable-block').removeClass('active-block').addClass('inactive-block');
        $('#book-button-block').removeClass('active-block').addClass('inactive-block');
        $('#book-button').attr('disabled', true);
        $('#buildingData').html('');
        fromDate.attr('disabled', true);
        toDate.attr('disabled', true);
    } else {
        $('#book-button-block').removeClass('inactive-block').addClass('active-block');
        $('#disable-block').removeClass('inactive-block').addClass('active-block');
        $('#book-button').attr('disabled', false);
        fromDate.attr('disabled', false);
        toDate.attr('disabled', false);
        getBuildingData(val);
    }
}

function getBuildingData(val) {
    var fromDate = $('#from_date');
    var toDate = $('#to_date');
    $.ajax({
        url: '/' + $('html').attr('lang') + '/manager/bookings/create/glamping/' + val,
        type: 'GET',
        data: {
            from_date: fromDate.val(),
            to_date: toDate.val(),
            book_id: $('input[name="book_id"]').val()
        },
        success:function(html) {
            $('#buildingData').html(html);
        },
        error: function() {
            $('#ajaxError').addClass('alert alert-danger').html($('#error').val());
        }
    });
}