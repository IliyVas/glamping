$( window ).load(function() {
    var iconSelector, iconClass;
    iconClass = $('input[name="icon"]').val();
    iconSelector = 'div[data-icon="' + iconClass + '"]';
    $(iconSelector).addClass('icon-border');

    $("#image").change(function(){
        $('#mainImg').show();
        readImgURL(this);
    });

    $("#icon").change(function(){
        $('#iconImg').show();
        readIconURL(this);
    });
});

function chooseIcon(elem) {
    $('div[data-icon]').removeClass('icon-border');
    $(elem).addClass('icon-border');

    var iconName;
    iconName = elem.getAttribute('data-icon');
    $('input[name=icon]').val(iconName);
}

function readIconURL(input) {

    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#iconImg').attr('src', e.target.result);
        };

        reader.readAsDataURL(input.files[0]);
    }
}

function readImgURL(input) {

    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#mainImg').attr('src', e.target.result);
        };

        reader.readAsDataURL(input.files[0]);
    }
}