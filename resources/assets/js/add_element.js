$( window ).load(function() {
    $('#add_building_price').click(function() {
        var request = new XMLHttpRequest();
        request.open('GET', '/manager/post-get-ext-prices-form.tmplt');
        request.onreadystatechange = function () {
            if (request.readyState == 4) {
                if (request.status == 200) {
                    $('.extended_prices_block').append(request.responseText);
                } else {
                    console.log('Error: ' + request.status);
                }
            }
        };
        request.send(null);
    });

    $(document).on('click', '.close-button', function() {
        $(this).parent().remove();
    });

    var countOptions = $('#count_options');
    $('#add_option').click(function() {
        var elemCount = countOptions.val();
        elemCount++;
        var request = new XMLHttpRequest();
        request.open('GET', '/manager/option-line.tmplt');
        request.onreadystatechange = function () {
            if (request.readyState == 4) {
                if (request.status == 200) {
                    countOptions.val(elemCount);
                    $('#options_block').append(request.responseText);
                    $('.del-btn').html('<span class="text-danger table-close-button" data-option="0"><i class="material-icons">clear</i></span>')
                } else {
                    console.log('Error: ' + request.status);
                }
            }
        };
        request.send(null);
    });

    $(document).on('click', '.table-close-button', function() {
        var elemCount = countOptions.val();
        var td = $(this).parent();
        var tr = td.parent();
        tr.remove();
        elemCount--;
        countOptions.val(elemCount);
        if (elemCount == 0) {
            $('.del-btn').html('')
        }
    });
});