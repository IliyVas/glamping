function readURL(input) {

    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {

            $('#prev').attr('src', e.target.result);

            if(!$('#prev').hasClass('active')){
                $('#prev').addClass('active');
            }
        }

        reader.readAsDataURL(input.files[0]);
    }
}

$("#imgInp").change(function(){
    readURL(this);
});
