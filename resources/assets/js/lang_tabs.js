$( window ).on('load', function() {
    var defaultLang = getDefaultLang($('html').attr('lang'));
    $('div[data-tab="' + defaultLang + '"]').hide();
});

function getDefaultLang(activeLang) {
    var defaultLang;

    if (activeLang == 'ru') {
        defaultLang = 'en';
    } else {
        defaultLang = 'ru';
    }

    return defaultLang;
}

function showLangBlock(lang) {
    var defaultLang = getDefaultLang(lang);
    $('div[data-tab="' + defaultLang + '"]').hide();
    $('div[data-tab="' + lang + '"]').show();
}