function sendAjaxFilter(event, form) {
    event.preventDefault();
    form = jQuery(form);
    var m_data=form.serialize();

    form.find("input[type=submit]").attr('disabled', true);

    var ajaxContent = $('#ajax-content');
    ajaxContent.addClass('ajax-loader');
    jQuery.ajax({
        type: 'POST',
        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
        url: '/' + $('html').attr('lang') + '/filter',
        data: m_data,
        success: function(html){
            $('#ajax-content').html(html);
            form.find("input[type='submit']").attr('disabled', false);
            ajaxContent.removeClass('ajax-loader');

            setLocation('?' + m_data);
        },
        error: function(){
            form.find("input[type='submit']").attr('disabled', false);
            ajaxContent.removeClass('ajax-loader');
        }
    });
}

function autocompleteFilterInputs(request, response, columns) {
    $.ajax({
        url: '/' + $('html').attr('lang') + '/autocomplete',
        data: {
            term: request.term,
            columns: columns
        },
        dataType: "json",
        success: function( data ) {
            response( $.map( data.myData, function( item ) {
                return {
                    value: item.value,
                    type: item.type
                }
            }));
        }
    });
}

function autocompleteImpressions(request, response) {
    $.ajax({
        url: '/' + $('html').attr('lang') + '/autocomplete/impression',
        data: {
            term: request.term
        },
        dataType: "json",
        success: function( data ) {
            response( $.map( data, function( item ) {
                return {
                    value: item.value,
                    id: item.id
                }
            }));
        }
    });
}

function setLocation(curLoc){
    try {
        history.pushState(null, null, curLoc);
        return;
    } catch(e) {}
    location.hash = '#' + curLoc;
}