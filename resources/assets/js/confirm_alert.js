function confirm_action() {
    var isConfirmed = confirm('Confirm the deletion?');
    if (!isConfirmed) {
        event.preventDefault();
    }
}