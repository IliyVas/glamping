$( window ).on('load', function() {
    $('strong[class="reestablish_img"]').hide(); //Hide recovery arrows
});

function deleteOldImg(id) {
    $('#' + id).addClass('inactive-block');
    $('#' + id + '-del').hide(); //hide delete button
    $('#' + id + '-rees').show(); //show recovery arrow
    $('#' + id + '-input').val(''); //unset image $id in exist image input
    $('#' + id + '-del-input').val(id); //set image $id in deleted image input
}

function reestablishOldImg(id) {
    $('#' + id).removeClass('inactive-block');
    $('#' + id + '-rees').hide(); //hide recovery arrow
    $('#' + id + '-del').show(); //show delete button
    $('#' + id + '-input').val(id); //set image $id in exist image input
    $('#' + id + '-del-input').val(''); //unset image $id in deleted image input
}

Dropzone.autoDiscover = false;

$(document).ready(function(){
    var form = $('form.add-item');
    var file_names = [];

    $('#frmFileUpload').dropzone({
        url: '/manager/glampings/create/ajax-upload',
        'method': 'POST',
        'acceptedFiles': 'image/*',
        maxFilesize: 3, // MB
        maxFiles: 6,
        'params': {
            '_token': $('input[name="_token"]').val()
        },
        'addRemoveLinks': true,
        sending: function(file) {
            if (file.name in file_names) {
                alert('Файл с таким именем уже загружен!');
                this.removeFile(file);
            }
        },
        success: function(file, responseMessage) {
            form.append('<input type="hidden" name="images[]" value="' + responseMessage['image_name'] + '">');
            file_names[file.name] = responseMessage['image_name'];
        },
        error: function(file, responseMessage) {
            alert('Error: ' + responseMessage);
            this.removeFile(file);
        },
        removedfile: function(file) {
            var _ref;
            if (file.name in file_names) {
                var true_file_name = file_names[file.name];
                $('input[value="' + true_file_name + '"]').remove();
                $.ajax({
                    url: '/manager/glampings/create/ajax-remove',
                    type: "POST",
                    data: {
                        "file_name": file_names[file.name],
                        "_token": $('input[name="_token"]').val()
                    }
                });
                delete file_names[file.name];
            }
            return (_ref = file.previewElement) != null ? _ref.parentNode.removeChild(file.previewElement) : void 0;

        }
    });
});

$( window ).load(function() {
    $('.current-image-label').click(function(){
        var exist = $('input[name="glamping_images[]"][value="' + this.id + '"]');
        if (!exist.length) {
            var html = '<input type="hidden" name="glamping_images[]" value="' + this.id + '">';
            $(html).insertAfter(this);
            $(this).removeClass('inactive-block');
        }
    });
});