$(function () {
    //Textare auto growth
    autosize($('textarea.auto-growth'));
    var shortTime = false;

    if($('html').attr('lang') === 'en') {
        shortTime = true
    }

    //Datetimepicker plugin
    $('.datetimepicker').bootstrapMaterialDatePicker({
        format: 'dddd DD MMMM YYYY - HH:mm',
        clearButton: true,
        weekStart: 1
    });

    $('.datepicker').bootstrapMaterialDatePicker({
        format: 'DD.MM.YYYY',
        clearButton: true,
        weekStart: 1,
        time: false
    });

    $('.timepicker').bootstrapMaterialDatePicker({
        format: 'HH:mm',
        clearButton: true,
        date: false,
        shortTime: shortTime
    });
});