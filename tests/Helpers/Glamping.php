<?php

namespace Tests\Helpers;

use App\Models\Building\Building;
use Illuminate\Database\Eloquent\Model;

class Glamping
{
    /**
     * Array with glamping countries.
     */
    const COUNTRIES = ['Russian Federation', 'Republic of Belarus', 'Italy', 'Spain', 'France'];

    /**
     * Array with glamping regions.
     */
    const REGIONS = ['Moscow', 'Mogilev', 'Milan', 'Barcelona', 'Paris'];

    /**
     * Array with glamping names.
     */
    const NAMES = ['Test glamping', 'Farmhouse', 'Loges', 'Eco-house', 'Tents'];

    /**
     *  Create new glampings.
     *
     * @param Model $owner
     *
     * @param int $count
     * $count is number of required glampings.
     *
     * @return mixed
     * Return queryBuilder.
     */
    public static function createGlampings(Model $owner, $count = 1)
   {
       for ($i = 1; $i <= $count; $i++){
           $owner->glampings()->create([
               'latitude' => '55.76638',
               'longitude' => '37.46734',
               'max_child_age' => 2,
               'name:' . config('app.locale') => str_random(5) . '-' . self::NAMES[rand(0, (count(self::NAMES) - 1))],
               'description:'. config('app.locale') => 'Farmhouse',
               'country:'. config('app.locale') => self::COUNTRIES[rand(0, (count(self::COUNTRIES) - 1))],
               'region:'. config('app.locale') => self::REGIONS[rand(0, (count(self::REGIONS) - 1))],
               'arrival_time' => '08:00',
               'check_out_time' => '20:00',
               'free_return' => 3
           ]);
       }

       return $owner->glampings();
   }

    /**
     * @param Model $glamping
     *
     * @param int $count
     * $count is number of required buildings.
     *
     * @return mixed
     * Return buildings collection.
     */
    public static function createBuildingOfGlamping(Model $glamping, $count = 1)
   {
       for ($i = 1; $i <= $count; $i++){
           (new Building)->forceFill([
               'glamping_id' => $glamping->id,
               'building_type_id' => 1,
               'capacity' => 2,
               'count' => 4,
               'main_price' => 800,
               'min_nights_count' => 3,
               'description:'. config('app.locale') => 'Cabin room',
               'name:'. config('app.locale') => 'Cabin room',
           ])->save();
       }

       return $glamping->buildings;
   }
}
