<?php

namespace Tests\Helpers;

class Login
{
    /**
     * @param $email
     * @param $browser
     */
    public static function managerLogin($email, $browser)
    {
        $browser->visit('/manager/login')
            ->type('email', $email)
            ->type('password', 'password')
            ->press(mb_strtoupper(trans('auth.btn_signin')));
    }

    /**
     * @param $email
     * @param $browser
     */
    public static function userLogin($email, $browser)
    {
        $browser->visit('/user/login')
            ->type('email', $email)
            ->type('password', 'password')
            ->press(trans('auth.btn_signin'));
    }

    /**
     * @param $browser
     */
    public static function logout($browser)
    {
        $browser
            ->visit('/')
            ->clickLink(trans('auth.logout'));
    }
}