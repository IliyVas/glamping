<?php

namespace Tests\Helpers;

use App\Models\Auth\Manager;
use App\Models\Auth\Role;
use App\Models\Auth\User;

class CreateUsers
{
    /**
     * @return mixed
     */
    public static function owner()
    {
        $email = 'test@owner.com';

        Manager::create([
            'first_name' => 'Test',
            'last_name' => 'Owner',
            'phone' => '789456123',
            'email_token' => base64_encode($email),
            'email' => $email,
            'password' => bcrypt('password'),
        ]);

        $role = Role::create([
            'name' => 'owner',
            'display_name' => 'Glamping owner',
            'description' => 'User can leave application for create glamping, edit the glamping data.'
        ]);

        $owner = Manager::where('email', $email)->first();

        //attach role
        $owner->attachRole($role->id);

        return $owner;
    }

    /**
     * @return mixed
     */
    public static function user()
    {
        $email = 'test@user.com';

        $user = User::create([
            'first_name' => 'Test',
            'last_name' => 'User',
            'birth_date' => date_create('26-11-1992')->format('Y-m-d'),
            'email_token' => base64_encode($email),
            'email' => $email,
            'phone' => '8(963)995-24-27',
            'password' => bcrypt('password'),
        ]);

        return $user;
    }

    /**
     * @return mixed
     */
    public static function admin()
    {
        $email = 'test@admin.com';

        Manager::create([
            'first_name' => 'Test',
            'last_name' => 'Admin',
            'email_token' => base64_encode($email),
            'email' => $email,
            'password' => bcrypt('password'),
        ])->forceFill(['activated' => true, 'accepted' => true])->update();

        $role = Role::create([
            'name' => 'admin',
            'display_name' => 'Project admin',
            'description' => 'User can create, edit all data.'
        ]);

        $admin = Manager::where('email', $email)->first();

        //attach role
        $admin->attachRole($role->id);

        return $admin;
    }
}