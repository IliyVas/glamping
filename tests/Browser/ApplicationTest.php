<?php

namespace Tests\Browser;

use App\Models\Communication\Application;
use App\Models\Communication\ApplicationStatus;
use App\Models\Communication\ApplicationType;
use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\Helpers\CreateUsers;
use Tests\Helpers\Glamping;
use Tests\Helpers\Login;

class ApplicationTest extends DuskTestCase
{
    use DatabaseMigrations;

    /**
     * @param $browser
     */
    private function sendApplication($browser)
    {
        $browser->visitRoute('manager.glampings.index')
            ->clickLink('send');
    }

    /**
     * Check the sending of the application by the owner and its receipt by the admin.
     */
    public function testSendGlampingApplication()
    {
        //Create database migrations before run test.
        $this->artisan('migrate:rollback');
        $this->runDatabaseMigrations();

        $owner = CreateUsers::owner()->forceFill(['accepted' => true, 'activated' => true]);
        $owner->update();
        Glamping::createBuildingOfGlamping(Glamping::createGlampings($owner)->first());

        $this->browse(function (Browser $browser) use($owner) {
            //login as owner
            Login::managerLogin($owner->email, $browser);
            //send application for creation of glamping
            $this->sendApplication($browser);
            Login::logout($browser); //logout

            //login as admin
            Login::managerLogin(CreateUsers::admin()->email, $browser);
            //check for the submitted application
            $browser->visitRoute('manager.applications.index')
                ->assertSee($owner->glampings()->first()->name);
            Login::logout($browser); //logout
        });
    }

    /**
     * Check action after pressing the 'Accept' button.
     */
    public function testApplyApplication()
    {
        //Create database migrations before run test.
        $this->artisan('migrate:rollback');
        $this->runDatabaseMigrations();

        $glamping = Glamping::createGlampings(CreateUsers::owner())->first(); //create glamping
        Glamping::createBuildingOfGlamping($glamping); //create building for glamping

        //send application for creation of glamping
        $app = (new Application)->forceFill(['type' => ApplicationType::CREATE]);
        $glamping->applications()->save($app);

        $this->browse(function (Browser $browser)  use ($app, $glamping){
            Login::managerLogin(CreateUsers::admin()->email, $browser);//login as admin
            //check for the submitted application
            $browser->visitRoute('manager.applications.show', ['id' => $app->id])
                ->assertSee($glamping->name)
                ->press(trans('applications.accept'))
                ->assertSee(trans('applications.accepted'));
            Login::logout($browser); //logout
        });
    }

    /**
     * Check action after pressing the 'Reject' button.
     */
    public function testRejectApplication()
    {
        //Create database migrations before run test.
        $this->artisan('migrate:rollback');
        $this->runDatabaseMigrations();

        $glamping = Glamping::createGlampings(CreateUsers::owner())->first(); //create glamping
        Glamping::createBuildingOfGlamping($glamping); //create building for glamping

        //send application for creation of glamping
        $app = (new Application)->forceFill(['type' => ApplicationType::CREATE]);
        $glamping->applications()->save($app);

        $this->browse(function (Browser $browser)  use ($app, $glamping){
            Login::managerLogin(CreateUsers::admin()->email, $browser);//login as admin
            //check for the submitted application
            $browser->visitRoute('manager.applications.show', ['id' => $app->id])
                ->assertSee($glamping->name)
                //check the action if you do not write the answer and click Reject
                ->press(trans('applications.reject'))
                ->assertSee(trans('messages.emptyDenyMessage'))
                //check the action if you write the answer and click Reject
                ->type('answer', 'Application reject message.')
                ->press(trans('applications.reject'))
                ->assertSee(trans('applications.rejected'));
            Login::logout($browser); //logout
        });
    }

    /**
     * Check that application for registration of owner was created after save profile.
     */
    public function testSendApplicationAfterOwnerRegistration()
    {
        //Create database migrations before run test.
        $this->artisan('migrate:rollback');
        $this->runDatabaseMigrations();

        $owner = CreateUsers::owner()->forceFill(['activated' => true]);
        $owner->update();

        $this->browse(function (Browser $browser)  use ($owner){
            Login::managerLogin($owner->email, $browser);//login as owner
            //change profile and save this changes
            $browser->visitRoute('manager.account.edit')
                ->type('company_name', 'Test Company')
                ->type('registration_number', 'Test Registration Number')
                ->press(trans('common_elements.btnSave'));
            Login::logout($browser);
            Login::managerLogin(CreateUsers::admin()->email, $browser); //login as admin
            //check that application for registration of owner was created
            $browser->visitRoute('manager.applications.index')
                ->assertSee(trans('applications.registration'))
                ->assertSee($owner->first_name . ' ' . $owner->last_name);
            Login::logout($browser); //logout
        });
    }

    /**
     * Check that Nonaccepted route is available.
     */
    public function testAvailableRouteIfRegistrationApplicationIsNotConsidered()
    {
        //Create database migrations before run test.
        $this->artisan('migrate:rollback');
        $this->runDatabaseMigrations();

        $owner = CreateUsers::owner()->forceFill(['activated' => true]);
        $owner->update();

        //send application for registration of owner
        $owner->applications()->save((new Application)->forceFill(['type' => ApplicationType::REGISTER]));
        $this->browse(function (Browser $browser)  use ($owner){
            Login::managerLogin($owner->email, $browser);//login as owner
            //check Nonaccepted route and message
            $browser->assertRouteIs('manager.nonaccepted')
                ->assertSee(trans('auth.nonAcceptedTitle'));
            Login::logout($browser);
        });
    }

    /**
     * Check that Profile and Edit profile routes are not available.
     */
    public function testInvalidProfileRouteIfRegistrationApplicationIsNotConsidered()
    {
        //Create database migrations before run test.
        $this->artisan('migrate:rollback');
        $this->runDatabaseMigrations();

        $owner = CreateUsers::owner()->forceFill(['activated' => true]);
        $owner->update();

        //send application for registration of owner
        $owner->applications()->save((new Application)->forceFill(['type' => ApplicationType::REGISTER]));
        $this->browse(function (Browser $browser)  use ($owner){
            Login::managerLogin($owner->email, $browser);//login as owner
            //check Profile and Edit profile routes not available.
            $browser->visitRoute('manager.owner.profile')
                ->assertRouteIs('manager.nonaccepted')
                ->assertSee(trans('auth.nonAcceptedTitle'))
                ->visitRoute('manager.account.edit')
                ->assertRouteIs('manager.nonaccepted')
                ->assertSee(trans('auth.nonAcceptedTitle'));
            Login::logout($browser);
        });
    }

    /**
     * Check available owner routes after reject the application for registration.
     */
    public function testAvailableRoutesIfRegistrationApplicationRejected()
    {
        //Create database migrations before run test.
        $this->artisan('migrate:rollback');
        $this->runDatabaseMigrations();

        $owner = CreateUsers::owner()->forceFill(['activated' => true]);
        $owner->update();

        //send application for registration of owner
        $rejectMessage = 'Test application reject message';
        $owner->applications()->save((new Application)->forceFill([
            'type' => ApplicationType::REGISTER, 'is_viewed' => true,
            'status' => ApplicationStatus::REJECTED, 'answer' => $rejectMessage]));
        $this->browse(function (Browser $browser)  use ($owner, $rejectMessage){
            Login::managerLogin($owner->email, $browser);//login as owner
            $browser->assertRouteIs('manager.owner.profile')//check Profile route
                ->visitRoute('manager.account.edit') //check Edit profile route
                ->assertSee($rejectMessage); //check reject message
            Login::logout($browser);
        });
    }
}
