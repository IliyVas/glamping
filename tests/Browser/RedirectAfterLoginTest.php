<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\Helpers\CreateUsers;
use Tests\Helpers\Login;

class RedirectAfterLoginTest extends DuskTestCase
{
    use DatabaseMigrations;

    /**
     * Check redirect link after login as user.
     */
    public function testRedirectAfterUserLogin()
    {
        //Create database migrations before run test.
        $this->artisan('migrate:rollback');
        $this->runDatabaseMigrations();

        $this->browse(function (Browser $browser) {
            Login::userLogin(CreateUsers::user()->email, $browser); //login as user
            $browser->assertRouteIs('user.dashboard'); //check redirect after login
            Login::logout($browser); //logout
        });
    }

    /**
     * Check redirect link after login as glamping owner.
     */
    public function testRedirectAfterOwnerLogin()
    {
        //Create database migrations before run test.
        $this->artisan('migrate:rollback');
        $this->runDatabaseMigrations();

        $this->browse(function (Browser $browser) {
            //create owner
            $owner = CreateUsers::owner()->forceFill(['accepted' => true, 'activated' => true]);
            $owner->update();
            Login::managerLogin($owner->email, $browser); //login as owner
            $browser->assertRouteIs('manager.owner_dashboard'); //check redirect after login
            Login::logout($browser); //logout
        });
    }

    /**
     * Check redirect link after login as admin
     */
    public function testRedirectAfterAdminLogin()
    {
        //Create database migrations before run test.
        $this->artisan('migrate:rollback');
        $this->runDatabaseMigrations();

        $this->browse(function (Browser $browser) {
            Login::managerLogin(CreateUsers::admin()->email, $browser); //login as admin
            $browser->assertRouteIs('manager.admin_dashboard'); //check redirect after login
            Login::logout($browser); //logout
        });
    }
}
