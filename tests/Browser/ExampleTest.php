<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class ExampleTest extends DuskTestCase
{
    use DatabaseMigrations;

    /**
     * A basic browser test example.
     *
     * @return void
     */
    public function testBasicExample()
    {
        //Create database migrations before run test.
        $this->artisan('migrate:rollback');
        $this->runDatabaseMigrations();

        $this->browse(function (Browser $browser) {
            $browser->visitRoute('homepage')
                ->assertSee('glamping');
        });
    }
}
