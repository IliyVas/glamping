<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\Helpers\CreateUsers;
use Tests\Helpers\Glamping;
use Tests\Helpers\Login;

class CommentTest extends DuskTestCase
{
    use DatabaseMigrations;

    /**
     * Check that add comments form is invisible if a user have not been in the glamping.
     *
     * @return void
     */
    public function testIsInvisibleCommentsFormIfUserIsNotGlampingVisited()
    {
        //Create database migrations before run test.
        $this->artisan('migrate:rollback');
        $this->runDatabaseMigrations();

        //create glamping with building
        $glamping = Glamping::createGlampings(CreateUsers::owner())->first();
        Glamping::createBuildingOfGlamping($glamping);

        $this->browse(function (Browser $browser) use ($glamping) {
            Login::userLogin(CreateUsers::user()->email, $browser); //login as user
            $browser->visitRoute('viewGlamping', ['id' => $glamping->id])
                //check that don't see comment form elements
                ->assertDontSee(trans('common_elements.addComment'))
                ->assertMissing('#rating')
                ->assertMissing('#comment')
                ->assertDontSee(trans('common_elements.btnSend'));
            Login::logout($browser);
        });
    }
}
