<?php

namespace Tests\Unit;

use App\Models\Auth\Manager;
use App\Models\Communication\Application;
use App\Models\Communication\ApplicationType;
use Illuminate\Support\Facades\Auth;
use Tests\Helpers\CreateUsers;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class ApplicationTest extends TestCase
{
    use DatabaseMigrations;

    /**
     *  Check the impossibility of changing the profile if the last application is not considered.
     */
    public function testImpossibilityChangeProfileIfLastApplicationIsNotConsidered()
    {
        $this->artisan('migrate:rollback');
        $this->runDatabaseMigrations();
        //create active owner instance
        $owner = CreateUsers::owner()->forceFill(['accepted' => true, 'activated' => true]);
        $owner->update();
        //create application
        $owner->applications()->save((new Application())->forceFill(['type' => ApplicationType::EDIT_PROFILE]));
        //login as owner
        Auth::guard('manager')->login($owner);
        //send application for change profile
        $response = $this->call('POST', route('manager.account.update'), ['first_name' => 'Fail']);
        //check redirect action
        $this->assertEquals('302', $response->getStatusCode());
        $this->assertEquals(route('manager.nonaccepted'), $response->getTargetUrl());
        $response = $this->call('GET', $response->getTargetUrl());
        $this->assertEquals('200', $response->getStatusCode());
        //check information
        $this->assertFalse(Manager::find($owner->id)->first_name === 'Fail');
    }
}
