<?php

namespace Tests\Unit;

use App\Models\Auth\User;
use App\Models\Booking\Booking;
use App\Models\Booking\BookingStatus;
use Illuminate\Support\Facades\Auth;
use Tests\Helpers\CreateUsers;
use Tests\Helpers\Glamping;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class CommentControllerTest extends TestCase
{
    use DatabaseMigrations;

    /**
     * Array, contains data of comment.
     */
    const COMMENT_DATA = ['comment' => 'Test', 'rating' => 5, 'glamping_id' => 1, 'user_id' => 1];

    /**
     * @var Glamping Model.
     */
    protected $glamping;

    /**
     * @var User Model.
     */
    protected $user;

    /**
     * Prepare data for tests.
     */
    private function prepareData() {
        //create database migrations before run test.
        $this->artisan('migrate:rollback');
        $this->runDatabaseMigrations();

        //create user
        $this->user = CreateUsers::user();

        //create glamping
        $this->glamping = Glamping::createGlampings(CreateUsers::owner())->first();
    }

    /**
     * Check store function result if a user haven't been in the glamping.
     *
     * @return void
     */
    public function testStoreIfUserIsNotGlampingVisited()
    {
        $this->prepareData();
        //login as user and check it
        Auth::guard('user')->loginUsingId($this->user->id);
        $this->assertTrue(Auth::guard('user')->check());
        //check that user haven't been in the glamping
        $this->assertFalse(Auth::guard('user')->user()->isGlampingVisited($this->glamping));
        //coll method store
        $response = $this->call('POST', route('user.comments.store'), self::COMMENT_DATA);
        //check that there was a redirect
        $this->assertEquals('302', $response->getStatusCode());
        $this->assertEquals(route('homepage'), $response->getTargetUrl());
        //check that the comment was not created
        $this->assertFalse(count($this->glamping->comments) > 0);
        Auth::logout();
    }

    /**
     * Check store function result if a user have been in the glamping.
     *
     * @return void
     */
    public function testStoreIfUserIsGlampingVisited()
    {
        $this->prepareData();
        //login as user and check it
        Auth::guard('user')->loginUsingId($this->user->id);
        $this->assertTrue(Auth::guard('user')->check());
        //create booking
        (new Booking)->forceFill(['user_id' => $this->user->id, 'from_date' => date('Y-m-d', time() - (24*60*60)),
            'to_date' => date('Y-m-d', time() + (3*24*60*60)), 'adults' => 5, 'country_code' => 'RU',
            'manager_id' => 1, 'glamping_id' => $this->glamping->id, 'status' => BookingStatus::CONFIRMED,
            'children' => 1])->save();
        //check that user have been in the glamping
        $this->assertTrue(Auth::guard('user')->user()->isGlampingVisited($this->glamping));
        //coll method store
        $response = $this->call('POST', route('user.comments.store'), self::COMMENT_DATA);
        //check that there was a redirect
        $this->assertEquals('302', $response->getStatusCode());
        $this->assertEquals(route('homepage'), $response->getTargetUrl());
        //check that the comment was created
        $this->assertTrue(count($this->glamping->comments) > 0);
        Auth::logout();
    }
}
