<?php

namespace Tests\Unit;

use App\Http\Controllers\Glamping\Helpers\GlampingListFilterHelper;
use Illuminate\Database\Eloquent\Builder;
use Tests\Helpers\CreateUsers;
use Tests\Helpers\Glamping;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class GlampingListFilterHelperTest extends TestCase
{
    use DatabaseMigrations;

    /**
     * @var Builder
     */
    protected $glampingsBuilder;

    /**
     * Prepare data for filter.
     *
     * @return mixed
     * Return glampings queryBuilder.
     */
    private function prepareData() {
        //create database migrations before run test.
        $this->artisan('migrate:rollback');
        $this->runDatabaseMigrations();

        //create glampings
        $this->glampingsBuilder = Glamping::createGlampings(CreateUsers::owner(), 10);

        //create buildings
        foreach ($this->glampingsBuilder->get() as $glamping) {
            Glamping::createBuildingOfGlamping($glamping, 3);
        }
    }

    /**
     * Test filterByTransFields function.
     *
     * @return void
     */
    public function testFilterByTransFields()
    {
        $this->prepareData();

        //generate filter data
        $name = Glamping::NAMES[rand(0, (count(Glamping::NAMES) - 1))];
        $country = Glamping::COUNTRIES[rand(0, (count(Glamping::COUNTRIES) - 1))];
        $region = Glamping::REGIONS[rand(0, (count(Glamping::REGIONS) - 1))];

        //check filter by name
        $filterByName = ['name' => $name];
        $result = GlampingListFilterHelper::filterByTransFields($this->glampingsBuilder, $filterByName)->get();

        if (count($result) > 0) {
            foreach ($result as $item) {
                $this->assertEquals(substr($item->name, 6), $name);
            }
        } else {
            $this->assertFalse(count($this->glampingsBuilder->whereTranslationLike('name', '%' . $name . '%')
                    ->get()) > 0);
        }

        //check filter by country
        $filterByCountry = ['country' => $country];
        $result = GlampingListFilterHelper::filterByTransFields($this->glampingsBuilder, $filterByCountry)->get();

        if (count($result) > 0) {
            foreach ($result as $item) {
                $this->assertEquals($item->country, $country);
            }
        } else {
            $this->assertFalse(count($this->glampingsBuilder->whereTranslationLike('country', '%' . $country . '%')
                    ->get()) > 0);
        }

        //check filter by region
        $filterByRegion = ['region' => $region];
        $result = GlampingListFilterHelper::filterByTransFields($this->glampingsBuilder, $filterByRegion)->get();

        if (count($result) > 0) {
            foreach ($result as $item) {
                $this->assertEquals($item->region, $region);
            }
        } else {
            $this->assertFalse(count($this->glampingsBuilder->whereTranslationLike('region', '%' . $region . '%')
                    ->get()) > 0);
        }

        //get filter by all fields
        $this->filter = array_merge($filterByRegion, $filterByCountry, $filterByName);
        $result = GlampingListFilterHelper::filterByTransFields($this->glampingsBuilder, $this->filter)->get();

        if (count($result) > 0) {
            foreach ($result as $item) {
                $this->assertEquals($item->name, $name);
                $this->assertEquals($item->country, $country);
                $this->assertEquals($item->region, $region);
            }
        } else {
            $this->assertFalse(count($this->glampingsBuilder
                    ->whereTranslationLike('name', '%' . $name . '%')
                    ->whereTranslationLike('country', '%' . $country . '%')
                    ->whereTranslationLike('region', '%' . $region . '%')
                    ->get()) > 0);
        }
    }
}
