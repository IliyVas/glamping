<?php

namespace Tests\Unit;

use App\Helpers\ResourceControllerHelper;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Tests\TestCase;

class ResourceControllerHelperTest extends TestCase
{
    /**
     * @var string
     */
    const MODEL_NAME = 'TestModelName';

    /**
     * Array with column/field names.
     */
    protected $basicArray = ['name', 'description'];

    /**
     * Test getGridData function.
     *
     * @return void
     */
    public function testGetGridData()
    {
        //get the function result
        $data = $this->createMock(Collection::class, [], [], '', false);
        $gridData = ResourceControllerHelper::getGridData($this->basicArray, $data, self::MODEL_NAME);

        //check that the function returned an array
        $this->assertTrue(is_array($gridData));

        //check that the returned array contains necessary keys
        $necessaryKeys = array_flip(['columns', 'gridData', 'pageName', 'editRoute', 'active', 'breadCrumbsActive',
            'deleteRoute', 'createRoute', 'createButtonName']);
        $this->assertTrue(empty(array_diff_key($necessaryKeys, $gridData)));
    }

    /**
     * Test getFormData function.
     *
     * @return void
     */
    public function testGetFormData()
    {
        //get the function result
        $model = $this->createConfiguredMock(Model::class, [], [], '', false);
        $formData = ResourceControllerHelper::getFormData($model, ['icon'], self::MODEL_NAME);

        //check that the function returned an array
        $this->assertTrue(is_array($formData));

        //check that the returned array contains necessary keys
        $necessaryKeys = array_flip(['fields', 'transFields', 'model', 'modelIndexPath', 'breadCrumbsActive',
            'actionRoute', 'method', 'pageName']);
        $this->assertTrue(empty(array_diff_key($necessaryKeys, $formData)));
    }

    /**
     * Test getColumnGridData function.
     *
     * @return void
     */
    public function testGetColumnGridData()
    {
        //get the function result
        $columnData = ResourceControllerHelper::getColumnGridData($this->basicArray);

        //check that the function returned an array
        $this->assertTrue(is_array($columnData));

        //check that the returned array contains the required number of  arrays with the necessary keys
        $this->assertTrue(count($this->basicArray) == count($columnData));
        $necessaryKeys = array_flip(['name', 'type']);
        foreach ($columnData as $key => $value) {
            $this->assertTrue(in_array($key, $this->basicArray));
            $this->assertTrue(empty(array_diff_key($necessaryKeys, $value)));
        }
    }

    /**
     * Test getFormFields function.
     *
     * @return void
     */
    public function testGetFormFields()
    {
        $this->checkFields(ResourceControllerHelper::getFormFields($this->basicArray));
    }

    /**
     * Test getTranslationFormFields function.
     *
     * @return void
     */
    public function testGetTranslationFormFields()
    {
        $this->checkFields(ResourceControllerHelper::getTranslationFormFields($this->basicArray));
    }

    /**
     * Helper for testGetTranslationFormFields() and testGetFormFields().
     *
     * @param $fields
     */
    private function checkFields($fields){
        //check that the function returned an array
        $this->assertTrue(is_array($fields));

        //check that the returned array contains the required number of  arrays with the necessary keys
        $this->assertTrue(count($this->basicArray) == count($fields));
        $necessaryKeys = array_flip(['label', 'type']);
        foreach ($fields as $key => $value) {
            $this->assertTrue(in_array($key, $this->basicArray));
            $this->assertTrue(empty(array_diff_key($necessaryKeys, $value)));
        }
    }
}
