<?php

use App\Http\Controllers\Manager;
use App\Http\Controllers\User;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//homepage
Route::get('/', 'GlampingListController@index')->name('homepage');
//filter
Route::get('/filter', 'GlampingListController@showFilterPage')->name('filterPage');
Route::post('/filter', 'GlampingListController@showFilteredGlampings');
Route::any('/autocomplete', 'Glamping\GlampingController@getAutocompleteData');
Route::any('/autocomplete/impression', 'Glamping\ImpressionController@getAutocompleteData');

//feedback messages
Route::get('/feedback', 'MessageController@showFeedbackForm')->name('feedbackForm');
Route::post('/feedback/message', 'MessageController@setMessage')->name('setFeedbackMessage');

//social block
Route::get('/login/social/{provider}', 'Auth\SocialAccountController@login')->name('socialLogin');
Route::get('/login/social/callback/{provider}', 'Auth\SocialAccountController@callback');

//glamping page
Route::get('/glamping/{id}', 'User\UserController@showBookGlampingView')
    ->name('viewGlamping');
Route::post('/glamping/{id}', 'Glamping\BookController@createBook')
    ->name('createBook');
Route::post('/booking/new/info', 'Glamping\BookController@showAdditionalInfoForm')
    ->name('bookAdditionalInfo');
Route::post('booking/new', 'Glamping\BookController@createBookingFromUserForm')
    ->name('createBookingFromUserForm');
Route::post('/glamping/{id}/ajax', 'Glamping\GlampingController@getSimpleFilterResult');
Route::get('/glamping/get_prices/{glamping_id}/{from_date}/{to_date}', 'Glamping\BookController@getBuildingsPrices')
    ->name('newBookDatePrices');

Route::resource('page', 'PageController');

Route::group(['prefix' => 'user', 'namespace' => 'User\Auth'], function () {

    Route::get('/login', 'LoginController@showLoginForm')->name('user_login_page');
    Route::post('/login', 'LoginController@login')->name('user_login');

    Route::get('/register', 'RegisterController@showRegistrationForm')->name('user_register_page');
    Route::post('/register', 'RegisterController@register')->name('user_register');

    Route::get('/verifyemail/{token}', 'RegisterController@verify');

    Route::post('/password/email', 'ForgotPasswordController@sendResetLinkEmail');
    Route::post('/password/reset', 'ResetPasswordController@reset');
    Route::get('/password/reset', 'ForgotPasswordController@showLinkRequestForm');
    Route::get('/password/reset/{token}', 'ResetPasswordController@showResetForm');
});

Route::group(['prefix' => 'manager', 'namespace' => 'Manager\Auth'], function () {

    Route::get('/login', 'LoginController@showLoginForm')->name('manager_login_page');
    Route::post('/login', 'LoginController@login')->name('manager_login');

    Route::get('/register', 'RegisterController@showRegistrationForm')->name('manager_register_page');
    Route::post('/register', 'RegisterController@register')->name('manager_register');

    Route::get('/verifyemail/{token}', 'RegisterController@verify');

    Route::post('/password/email', 'ForgotPasswordController@sendResetLinkEmail');
    Route::post('/password/reset', 'ResetPasswordController@reset');
    Route::get('/password/reset', 'ForgotPasswordController@showLinkRequestForm');
    Route::get('/password/reset/{token}', 'ResetPasswordController@showResetForm');
});

Route::post('/logout', function () {
    if (Auth::guard('manager')->check()) {
        $controller = new Manager\Auth\LoginController();
        return $controller->logout(request());
    }
    if (Auth::guard('user')->check()) {
        $controller = new User\Auth\LoginController();
        return $controller->logout(request());
    }
    return App::make('GlampingListController')->index();
})->name('logout');

Route::get('/get_captcha/{config?}', function (\Mews\Captcha\Captcha $captcha, $config = 'default') {
    return $captcha->src($config);
});

//delete comment
Route::resource('comments', 'CommentController', ['only' => ['destroy']]);

Route::get('/booking', function(){
    return view('booking');
});