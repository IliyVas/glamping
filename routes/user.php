<?php

Route::get('/dashboard', 'User\UserController@showDashboard')->name('dashboard');
Route::get('/account', 'User\UserController@showAccount')->name('account');
Route::get('/bookings', 'User\UserController@showBookings')->name('bookings');
Route::get('/account/edit', 'User\UserController@edit')->name('account.edit');
Route::post('/account/update', 'User\UserController@update')->name('account.update');
Route::post('/account/update/password', 'User\UserController@changePassword')->name('password.change');
Route::post('/account/update/email', 'User\UserController@changeEmail')->name('email.change');

//cancel booking
Route::get('/my/bookings/delete/{id}', 'User\UserController@cancelBooking')
    ->name('userCancelBooking');
Route::get('/my/bookings/pay/{id}', 'Glamping\BookController@confirm')
    ->name('userPayBooking');
Route::get('/my/bookings/delete/{id}/confirm', 'User\UserController@confirmCancelBooking')
    ->name('userConfirmCancelBooking');

//comments
Route::resource('comments', 'CommentController', ['only' => ['store']]);