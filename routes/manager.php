<?php
Route::get('resendActivation', ['as' => 'resend', 'uses' => 'Manager\OwnerController@resend']);

Route::group(['middleware' => ['managerActive']], function (){

Route::group(['middleware' => ['role:owner']], function () {
    Route::get('/dashboard', 'Manager\OwnerController@showDashboard')->name('owner_dashboard');
    Route::group(['prefix' => 'public'], function () {
        Route::get('/profile', 'Manager\OwnerController@showProfile')->name('owner.profile');
        Route::get('/account/edit', 'Manager\OwnerController@edit')->name('account.edit');
        Route::post('/account/update', 'Manager\OwnerController@update')->name('account.update');
        Route::post('/account/update/password', 'Manager\OwnerController@changePassword')->name('password.change');
    });

    //buildings
    Route::get('buildings/create/{glamping_id}', 'Glamping\BuildingController@create')
        ->name('buildings.create');
    //form with external dates
    Route::get('/post-get-ext-prices-form.tmplt', function () {
        return view('manager.owner.templates.ext_dates_block');
    });

    //send application
    Route::get('/applications/send/{glamping_id}', 'Manager\ApplicationController@send')->name('applications.send');

    //bookings
    Route::get('/bookings/confirm/{id}', 'Glamping\BookController@confirm')
        ->name('bookings.confirm');
    Route::get('/bookings/cancel/{id}', 'Glamping\BookController@cancel')
        ->name('bookings.cancel');
    Route::get('/bookings/create/glamping/{id}', 'Glamping\BookController@getSelectedGlampingData');
    Route::resource('bookings', 'Glamping\BookController', ['except' => ['show', 'destroy']]);
});

Route::group(['middleware' => ['role:admin']], function () {
    Route::group(['prefix' => 'public'], function () {
        Route::get('/admin-dashboard', 'Manager\AdminController@showDashboard')->name('admin_dashboard');
        Route::get('/admin-account/edit', 'Manager\AdminController@edit')->name('admin-account.edit');
        Route::post('/admin-account/update', 'Manager\AdminController@update')->name('admin-account.update');
        Route::post('/admin-account/update/email', 'Manager\AdminController@changeEmail')->name('admin-email.change');
    });


    Route::resource('building_types', 'Glamping\BuildingTypeController', ['except' => ['show']]);
    Route::resource('glamping_groups', 'Glamping\GlampingGroupController', ['except' => ['show']]);
    Route::resource('impressions', 'Glamping\ImpressionController', ['except' => ['show']]);
    Route::resource('amenities', 'Glamping\AmenityController', ['except' => ['show']]);
    Route::resource('services', 'Glamping\ServiceController', ['except' => ['show']]);

    Route::get('applications/archive', 'Manager\ApplicationController@archive')->name('applications.archive');
    Route::resource('applications', 'Manager\ApplicationController', ['only' => ['index', 'show', 'update']]);

    Route::resource('news', 'Manager\NewsController');

    //feedback messages
    Route::get('/admin/messages', 'Manager\AdminController@showFeedbackMessages')->name('feedbackMessages');
    Route::get('/admin/message/{id}', 'Manager\AdminController@showMessage')->name('showMessage');
    Route::post('/admin/messages/viewed', 'Manager\AdminController@toggleViewed')->name('toggleViewed');
    Route::post('/admin/messages/answer', 'Manager\AdminController@answerMessage')->name('answerMessage');

    //Pages
    Route::get('/admin/pages', 'Manager\PageController@index')->name('pages');
    Route::get('/admin/page/show/{id}', 'Manager\PageController@show')->name('page');
    Route::put('/admin/page/{id}/update', 'Manager\PageController@update')->name('page.update');
});

    //glampings
    Route::get('/glamping/{id}/busyness', 'Glamping\GlampingController@showBusynessPage')
        ->name('glampings.busyness');
    Route::post('/glamping/edit/ajax-count', 'Glamping\GlampingController@editBuildingsCount')
        ->name('glampings.building_counts_update');
    Route::post('/glamping/edit/ajax-price', 'Glamping\GlampingController@editBuildingsPrice')
        ->name('glampings.building_prices_update');
    Route::post('/glamping/edit/ajax-get-count-by-dates', 'Glamping\GlampingController@getBuildingsCountsByDates')
        ->name('glampings.get_counts_by_date');
    //glamping images
    Route::post('/glampings/create/ajax-upload',
        'Glamping\Helpers\CrudHelper@ajaxUploadImages')->name('ajax_upload');
    Route::post('/glampings/create/ajax-remove',
        'Glamping\Helpers\CrudHelper@ajaxRemoveImages')->name('ajax_remove');
    //glamping statistics
    Route::any('/glampings/{id}/statistics', 'Glamping\GlampingController@showStatistics')
        ->name('glampings.statistics');
    //glamping resource
    Route::resource('glampings', 'Glamping\GlampingController');

    //buildings
    Route::get('/buildings/show/{glamping_id}/{building_id}', 'Glamping\BuildingController@showBuilding')
        ->name('buildings.view');
    Route::resource('buildings', 'Glamping\BuildingController', ['except' => ['index', 'destroy', 'create']]);
    //form with options
    Route::get('/option-line.tmplt', function () {
        return view('manager.owner.templates.grid.option_line');
    });
});

Route::get('nonactive',function(){
    return view('manager.non_active');
})->name('nonactive');
Route::get('nonaccepted',function(){
    return view('manager.non_accepted');
})->name('nonaccepted');

