<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Mail;
use App\Mail\EmailBookingConfirmation;

class SendBookingConfirmationEmail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $booking;
    protected $email;
    protected $pdf_path;

    /**
     * Create a new job instance.
     *
     * @param $email
     * @param $booking
     * @param $pdf_path
     */
    public function __construct($email, $booking, $pdf_path)
    {
        $this->booking = $booking;
        $this->email = $email;
        $this->pdf_path = $pdf_path;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $email = $this->email;
        $pdf_path = $this->pdf_path;
        $booking = $this->booking;

        Mail::send('email.book_confirmation', [],
            function ($message) use ($booking, $pdf_path, $email) {
                $message->to($email, $booking->name)
                    ->attach($pdf_path, [
                        'as' => 'confirmation.pdf',
                        'mime' => 'application/pdf',])
                    ->subject(trans('common_elements.bookConfirmation'));
            });
    }
}
