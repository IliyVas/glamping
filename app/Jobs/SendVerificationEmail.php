<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Mail;
use App\Mail\EmailVerification;

class SendVerificationEmail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $user;
    protected $type;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($user, $type = 'user')
    {
        $this->user = $user;
        $this->type = $type;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $email = new EmailVerification($this->user, $this->type);

        if (!is_null($this->user->new_email) && $this->user->activated)
            $mailTo = $this->user->new_email;
        else
            $mailTo = $this->user->email;

        Mail::to($mailTo)->send($email);
    }
}
