<?php

namespace App\Jobs;

use App\Mail\EmailRegistration;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Mail;

class SendRegistrationEmail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $status;
    protected $manager;

    /**
     * SendRegistrationEmail constructor.
     * @param $manager
     * @param $status
     */
    public function __construct($manager, $status)
    {
        $this->manager = $manager;
        $this->status = $status;

    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $email = new EmailRegistration($this->status);
        Mail::to($this->manager->email)->send($email);
    }
}
