<?php

namespace App\Traits;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Jobs\SendVerificationEmail;
use App\Http\Controllers\Glamping\Helpers\CrudHelper;

trait ProfileEditor
{
    /**
     * @param $request
     * @param string $mediaType
     * @return $this
     */
    protected function saveChanges($request, $mediaType = 'User')
    {
        $data = $request->all();

        if (isset($data['email']))
            unset($data['email']);

        if (isset($data['password']))
            unset($data['password']);

        $result = Auth::user()->fill($data)->update();

        if (!$result)
            return redirect()->back()->withErrors(trans('messages.dangerMessage'));

        if (!empty($request->image)) {
            if (!is_null(Auth::user()->media()->first()))
                Auth::user()->media()->first()->delete();

            $name = CrudHelper::transliterate($request->image->getClientOriginalName());
            $result = Auth::user()->addMedia($request->image)->usingName($name)
                ->usingFileName($name)->toMediaLibrary($mediaType . 'Image');
        }

        if (!$result)
            return redirect()->back()->withErrors(trans('messages.dangerMessage'));

        flash(trans('auth.editSuccess'), 'success');
    }

    /**
     * @param $request
     * @param string $guard
     * @return $this
     */
    protected function savePassword($request, $guard = 'user')
    {
        $this->validate($request, [
            'old_pass' => 'required',
            'password' => 'required|min:6|confirmed',
        ]);

        if (!Auth::guard($guard)->check(['password' => $request->old_pass])) {
            return redirect()->back()->withErrors(trans('auth.failedPassword'));
        }

        $result = Auth::user()->fill(['password' => bcrypt($request->password)])->update();

        if (!$result)
            return redirect()->back()->withErrors(trans('messages.dangerMessage'));

        flash(trans('auth.passwordChangeSuccess'), 'success');
    }

    /**
     * @param Request $request
     * @param string $type
     * @return $this
     */
    protected function saveEmail(Request $request, $type)
    {
        $this->validate($request, [
            'email' => 'required|email|max:255|unique:' . str_plural($type),
        ]);

        $req = $request->all();

        $data['email_token'] = base64_encode($request->email);
        $data['new_email'] = $request->email;

        $result = Auth::user()->fill($data)->update();

        if (Auth::guard('manager')->user()->hasRole('owner'))
            Auth::user()->forceFill(['activated' => false])->update();

        if (!$result)
            return redirect()->back()->withErrors(trans('messages.dangerMessage'));

        dispatch(new SendVerificationEmail(Auth::user(), $type));
        flash(trans('auth.emailChangedSuccess', ['email' => htmlspecialchars($req['email'])]), 'success');
    }
}
