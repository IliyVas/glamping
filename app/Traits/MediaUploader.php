<?php

namespace App\Traits;

use Symfony\Component\Process\Exception\LogicException;
use Spatie\TemporaryDirectory\TemporaryDirectory;

trait MediaUploader
{
    /**
     * @param $model
     * @param $icon
     * @param $modelName
     * @param $name
     * @return mixed
     */
    protected function uploadIcon($model, $icon, $modelName, $name)
    {
        $mediaLibraryName = $this->getMediaLibraryIconName($modelName);
        $result = $model->addMedia($icon)
            ->usingName($name)->usingFileName($name)->toMediaLibrary($mediaLibraryName);

        return $result;
    }

    /**
     * @param $model
     * @param $image
     * @param $modelName
     * @param $name
     * @return mixed
     */
    protected function uploadImage($model, $image, $modelName, $name)
    {
        $mediaLibraryName = $this->getMediaLibraryImageName($modelName);
        $result = $model->addMedia($image)
            ->usingName($name)->usingFileName($name)->toMediaLibrary($mediaLibraryName);

        return $result;
    }

    /**
     * @param $modelName
     * @return string
     */
    private function getMediaLibraryIconName($modelName)
    {
        $this->validateModelName($modelName);
        $mediaLibraryName = $modelName . 'Icon';

        return $mediaLibraryName;
    }

    /**
     * @param $modelName
     * @return string
     */
    private function getMediaLibraryImageName($modelName)
    {
        $this->validateModelName($modelName);
        $mediaLibraryName = $modelName . 'Image';

        return $mediaLibraryName;
    }

    /**
     * @param $modelName
     */
    private function validateModelName($modelName)
    {
        if (empty($modelName)) {
            throw new LogicException('Invalid $modelName');
        }
    }

    /**
     * @param $location
     * @param $name
     * @return $this
     */
    protected function createTemporaryMediaDirectory($location, $name)
    {
        $temporaryDirectory = (new TemporaryDirectory())
            ->location($location)
            ->name($name)
            ->create();

        return $temporaryDirectory;
    }

    /**
     * @param $path
     * @return bool
     */
    protected function deleteTemporaryMediaDirectory($path)
    {
        if (! file_exists($path)) {
            return true;
        }

        if (! is_dir($path)) {
            return unlink($path);
        }

        foreach (scandir($path) as $item) {
            if ($item == '.' || $item == '..') {
                continue;
            }

            if (! $this->deleteTemporaryMediaDirectory($path.DIRECTORY_SEPARATOR.$item)) {
                return false;
            }
        }

        return rmdir($path);
    }
}
