<?php

namespace App\Models\Glamping;

use App\Models\Booking\BookingStatus;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\HasMedia\Interfaces\HasMedia;
use Dimsav\Translatable\Translatable;

class Glamping extends Model implements HasMedia
{
    use HasMediaTrait, Translatable;

    /**
     * @var array
     */
    public $translatedAttributes = [
        'name',
        'description',
        'glamping_address',
        'other_information',
        'country',
        'region'
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'latitude',
        'longitude',
        'max_child_age',
        'arrival_time',
        'check_out_time',
        'free_return',
        'commission'
    ];

    /**
     * @return array|null
     */
    public function glampingImagesUrl()
    {
        $media = $this->getMedia('GlampingImage');
        $images = null;

        foreach ($media as $image) {
            $images[] = $image->getUrl();
        }

        return $images;
    }

    /**
     * Return a status of the last application.
     * Check ApplicationStatus class for more information.
     *
     * @return string
     */
    public function lastApplicationStatus()
    {
        return $this->applications()->first()->status;
    }

    /**
     * Return a Type of the last application.
     * Check ApplicationType class for more information.
     *
     * @return string
     */
    public function lastApplicationType()
    {
        return $this->applications()->first()->type;
    }

    /**
     * Return manager answer on last application.
     *
     * @return string
     */
    public function lastApplicationAnswer()
    {
        return $this->applications()->first()->answer;
    }

    /**
     * Get the managers who can edit the glamping.
     */
    public function managers()
    {
        return $this->belongsToMany('App\Models\Auth\Manager');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function services()
    {
        return $this->belongsToMany('App\Models\Glamping\Service');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function glampingGroups()
    {
        return $this->belongsToMany('App\Models\Glamping\GlampingGroup');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function amenities()
    {
        return $this->belongsToMany('App\Models\Glamping\Amenity', 'glamping_amenity');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function impressions()
    {
        return $this->belongsToMany('App\Models\Glamping\Impression');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function buildings()
    {
        return $this->hasMany('App\Models\Building\Building');
    }

    /**
     * Get all of the glamping's applications (It currently used for
     * creation applications of the glamping).
     */
    public function applications()
    {
        return $this->morphMany('App\Models\Communication\Application', 'applicationable')
                    ->orderBy('created_at', 'desc');
    }

    /**
     * The relations to eager load on every query.
     *
     * @var array
     */
    protected $with = ['translations'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function changedGlamping()
    {
        return $this->hasOne('App\Models\Glamping\Glamping');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function glamping()
    {
        return $this->belongsTo('App\Models\Glamping\Glamping');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function bookings()
    {
        return $this->hasMany('App\Models\Booking\Booking');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function comments()
    {
        return $this->morphMany('App\Models\Communication\Comments', 'model');
    }

    /**
     * @return float
     */
    public function getAverageRating()
    {
        return round($this->comments()->avg('rating'), 1);
    }

    /**
     * @param $fromDate
     * @param $toDate
     * @return int
     */
    public function getReceipts($fromDate, $toDate)
    {
        $receipts = 0;
        $bookings = $this->bookings()->where('status', BookingStatus::CONFIRMED)->get();

        if (count($bookings) > 0) {
            foreach ($bookings as $booking) {
                if ($booking->from_date >= $fromDate && $booking->from_date <= $toDate) {
                    $receipts += $booking->price();
                }
            }
        }

        return $receipts;
    }

    /**
     * @return null
     */
    public function getClientCountries()
    {
        $bookings = $this->bookings()->where('status', '!=', BookingStatus::CANCELED)->get();
        $countries = null;

        if (count($bookings) > 0) {
            foreach ($bookings as $booking) {
                $countries[$booking->country_code] = $booking->clientCountry();
            }

            if (empty($countries))
                $countries = null;
        }

        return $countries;
    }

    /**
     * @param $countryCode
     * @return int
     */
    public function getCountryReceipts($countryCode)
    {
        $bookings = $this->bookings()->where('status', BookingStatus::CONFIRMED)
            ->where('country_code', $countryCode)->get();

        $receipts = 0;

        if (count($bookings) > 0) {
            foreach ($bookings as $booking) {
                $receipts += $booking->price();
            }
        }

        return $receipts;
    }

    /**
     * @param $countryCode
     * @return float|int|string
     */
    public function getCountryTimeFromOrderToArrival($countryCode)
    {
        $bookings = $this->bookings()->where('status', '!=', BookingStatus::CANCELED)
            ->where('country_code', $countryCode)->get();

        $days = null;

        if (count($bookings) > 0) {
            foreach ($bookings as $booking) {
                $days[] = $booking->getTimeFromOrderToArrival();
            }
        }

        if (!is_null($days))
            $result = array_sum($days)/count($days);
        else
            $result = '&mdash;';

        return $result;
    }

    /**
     * @return array|null
     */
    public function getDiscountData()
    {
        $builds = $this->buildings;
        $discountData = null;

        if (!count($builds) > 0) return $discountData;

        foreach ($builds as $build) {
            $data = $build->getPricesData($fromDate = null);

            if ($this->min_price == $data['discount_price'])
                $discountData = ['price' => $data['price'], 'sale' => $build->getSale($data['price'], $this->min_price)];
        }

        return $discountData;
    }
}
