<?php

namespace App\Models\Glamping;

use Illuminate\Database\Eloquent\Model;
use Dimsav\Translatable\Translatable;

class Amenity extends Model
{
    use Translatable;

    /**
     * @var array
     */
    public $translatedAttributes = [
        'name', 'description'
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['icon'];

    /**
     * The relations to eager load on every query.
     *
     * @var array
     */
    protected $with = ['translations'];
}
