<?php

namespace App\Models\Glamping;

use Illuminate\Database\Eloquent\Model;

class GlampingTranslation extends Model
{
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'description',
        'locale',
        'glamping_address',
        'other_information',
        'country',
        'region',
    ];
}
