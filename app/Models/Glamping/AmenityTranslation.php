<?php

namespace App\Models\Glamping;

use Illuminate\Database\Eloquent\Model;

class AmenityTranslation extends Model
{
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'description', 'locale'
    ];
}
