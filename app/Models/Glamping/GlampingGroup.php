<?php

namespace App\Models\Glamping;

use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\HasMedia\Interfaces\HasMedia;
use Dimsav\Translatable\Translatable;

class GlampingGroup extends Model implements HasMedia
{
    use HasMediaTrait, Translatable;
    /**
     * @var array
     */
    public $translatedAttributes = [
        'name', 'description'
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['icon'];

    /**
     * The relations to eager load on every query.
     *
     * @var array
     */
    protected $with = ['translations'];
}
