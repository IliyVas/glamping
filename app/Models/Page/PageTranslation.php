<?php

namespace App\Models\Page;

use Illuminate\Database\Eloquent\Model;

class PageTranslation extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'name', 'title', 'keywords', 'seo_description', 'description', 'locale'
    ];
}
