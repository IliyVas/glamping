<?php

namespace App\Models\Page;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    use Translatable;

    protected $fillable = [
        'slug', 'parent_page'
    ];

    public $translatedAttributes = [
        'name', 'title', 'keywords', 'seo_description', 'description'
    ];

    protected $with = ['translations'];

    public function children()
    {
        return $this->hasMany(Page::class, 'parent_page');
    }


    public function childrenPage()
    {
        return $this->children()->with('childrenPage');
    }
}
