<?php

namespace App\Models\Communication;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Enums;

class ApplicationStatus
{
    const PENDING = "pending";
    const REJECTED = "rejected";
    const ACCEPTED = "accepted";
}

class ApplicationType
{
    const CHANGE = "update";
    const CREATE = "create";
    const REGISTER = "register";
    const EDIT_PROFILE = "editProfile";
}

class Application extends Model
{
    use Enums;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];

    /**
     * Application statuses.
     */
    protected $enumStatuses = [
        ApplicationStatus::PENDING,
        ApplicationStatus::REJECTED,
        ApplicationStatus::ACCEPTED,
    ];

    /**
     * Application types.
     */
    protected $enumTypes = [
        ApplicationType::CREATE,
        ApplicationType::CHANGE,
        ApplicationType::REGISTER,
        ApplicationType::EDIT_PROFILE,
    ];

    /**
     * Default values for model.
     *
     * @var array
     */
    protected $attributes = array(
        'status' => ApplicationStatus::PENDING,
        'is_viewed' => false,
        'type' => ApplicationType::CREATE
    );


    /**
     * @return string
     */
    public function getDescription()
    {
        if ($this->type == ApplicationType::CHANGE)
            $description = trans('applications.glamping_update') . ' ' . $this->applicationable->name;
        elseif ($this->type == ApplicationType::CREATE)
            $description = trans('applications.glamping_create') .  ' ' . $this->applicationable->name;
        elseif ($this->type == ApplicationType::REGISTER)
            $description = trans('applications.owner_register') . ' ' . $this->applicationable->first_name .
                ' ' . $this->applicationable->last_name;
        else
            $description = trans('applications.owner_edit_profile') . ' ' . $this->applicationable->first_name .
                ' ' . $this->applicationable->last_name;

        return $description;
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        return trans('applications.' . $this->status);
    }

    /**
     * @return string
     */
    public function getType()
    {
        if ($this->type == ApplicationType::CHANGE)
            $type = trans('applications.glamping_update_type');
        elseif ($this->type == ApplicationType::CREATE)
            $type = trans('applications.glamping_create_type');
        elseif ($this->type == ApplicationType::REGISTER)
            $type = trans('applications.registration');
        else
            $type = trans('applications.edit_profile');

        return $type;
    }

    /**
     * @return Manager
     */
    public function reviewer()
    {
        return $this->belongsTo('Manager');
    }

    /**
     * Get all of the owning applicationable models
     * (Currently it could be only Glamping).
     */
    public function applicationable()
    {
        return $this->morphTo();
    }
}
