<?php

namespace App\Models\Communication;

use App\Models\Auth\User;
use App\Models\Glamping\Glamping;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Comments extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['rating', 'comment', 'user_id'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function glamping()
    {
        return $this->belongsTo(Glamping::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphTo
     */
    public function ratingModel()
    {
        return $this->morphTo();
    }

    /**
     * Check rights of comment deleting.
     *
     * @return bool
     */
    public function canBeDeleted()
    {
        $isAuthor = Auth::guard('user')->check() && Auth::guard('user')->user()->id == $this->user_id;
        $isAdmin = Auth::guard('manager')->check() && Auth::guard('manager')->user()->hasRole('admin');

        if ($isAuthor ||$isAdmin) $canBeDeleted = true;
        else $canBeDeleted = false;

        return $canBeDeleted;
    }
}
