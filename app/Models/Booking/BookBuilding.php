<?php

namespace App\Models\Booking;

use Illuminate\Database\Eloquent\Model;

class BookBuilding extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['count'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function building()
    {
        return $this->belongsTo('App\Models\Building\Building');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function booking()
    {
        return $this->belongsTo('App\Models\Booking\Booking');
    }

    /**
     * @param $fromDate
     * @param $toDate
     * @return int|mixed
     */
    public function getPrice($fromDate, $toDate)
    {
        return $this->building->getPeriodPrice($fromDate, $toDate);
    }
}
