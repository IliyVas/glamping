<?php

namespace App\Models\Booking;

use App\Models\Communication\Country;
use App\Models\Glamping\Glamping;
use Illuminate\Database\Eloquent\Model;

class BookingStatus
{
    const NOT_CONFIRMED = "not confirmed";
    const CONFIRMED = "confirmed";
    const CANCELED = "canceled";
}
class Booking extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'from_date',
        'to_date',
        'adults',
        'children',
        'status',
        'country_code'
    ];

    /**
     * Default values for model.
     *
     * @var array
     */
    protected $attributes = array(
        'status' => BookingStatus::NOT_CONFIRMED
    );

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function bookBuildings()
    {
        return $this->hasMany('App\Models\Booking\BookBuilding');
    }

    /**
     * @return bool
     */
    public function isPayed()
    {
        if ($this->status !== BookingStatus::CONFIRMED) { //booking was not confirmed
            return false;
        }

        return true;
    }

    /**
     * @return bool
     */
    public function isDate()
    {
        if (date($this['to_date']) < date('Y-m-d')) { // booking is in the past
            return false;
        }

        return true;
    }

    /**
     * Check status and dates' correct
     * @return bool
     */
    public function isActive()
    {
        if (!$this->isPayed()) {
            return false;
        }
        if (!$this->isDate()) {
            return false;
        }

        return true;
    }

    /**
     * Check if user can remove booking (even not for free)
     * @return bool
     */
    public function isCancelable()
    {
        $today_date = date('Y-m-d');
        list($y, $m, $d) = explode("-", $today_date);
        $today_time = mktime(0, 0, 0, $m, $d, $y);

        list($y, $m, $d) = explode("-", $this['from_date']);
        $book_start = mktime(0, 0, 0, $m, $d, $y);

        if ($book_start - $today_time < 0 || $this->status === BookingStatus::CANCELED)
            return false;

        return true;
    }

    /**
     * Check if user can remove booking for free
     * @return boolean
     */
    public function isFreeCancelable()
    {
        $glamping = $this->bookBuildings()->first()->building->glamping()->first();
        $free_return = $glamping->free_return;

        $today_date = date('Y-m-d');
        list($y, $m, $d) = explode("-", $today_date);
        $today_time = mktime(0, 0, 0, $m, $d, $y);

        list($y, $m, $d) = explode("-", $this['from_date']);
        $book_start = mktime(0, 0, 0, $m, $d, $y);

        if ($book_start - $today_time < 0 || $this->status === BookingStatus::CANCELED)
            return false;
        if (($book_start - $today_time) / (24 * 60 * 60) > $free_return)
            return true;
        return false;

    }

    /**
     * Return count of buildings that pupil booked
     * @return int
     */
    public function buildingsCount()
    {
        $buildings_book = $this->bookBuildings()->get();
        $count = 0;
        foreach ($buildings_book as $building_book) {
            $count += $building_book->count;
        }

        return $count;
    }

    /**
     * @return int
     */
    public function price()
    {
        $price = 0;

        foreach ($this->bookBuildings as $bookBuilding) {
            $price += $bookBuilding->getPrice($this['from_date'], $this['to_date']);
        }

        return $price;
    }

    /**
     * @return mixed
     */
    public function clientCountry()
    {
        return Country::where('iso_code', $this->country_code)->first();
    }

    /**
     * @return mixed
     */
    public function getTimeFromOrderToArrival()
    {
        return date_create($this->created_at)->diff(date_create($this->from_date))->days;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function glamping()
    {
        return $this->belongsTo(Glamping::class);
    }

    /**
     * Generate booking pdf url.
     *
     * @return string
     */
    public function getPdfUrl()
    {
        return asset('pdf/' . $this->glamping->id . '/' . $this->id . '-pdf.pdf');
    }
}
