<?php

namespace App\Models\Auth;

use App\Models\Booking\Booking;
use App\Models\Communication\ApplicationStatus;
use App\Models\News\News;
use Zizaco\Entrust\Traits\EntrustUserTrait;
use App\Notifications\OwnerResetPassword;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

class Manager extends Authenticatable
{
    use Notifiable, EntrustUserTrait, HasMediaTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name',
        'last_name',
        'password',
        'company',
        'phone',
        'email',
        'email_token',
        'patronymic',
        'new_email'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Default values for model.
     *
     * @var array
     */
    protected $attributes = array(
       'activated' => false,
    );

    /**
     * Send the password reset notification.
     *
     * @param  string  $token
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new OwnerResetPassword($token));
    }


    /**
     * Get the owner's glampings.
     */
    public function glampings()
    {
        return $this->belongsToMany('Glamping');
    }

    /**
     * Get applications reviewd by the admin.
     */
    public function reviewedApplications()
    {
        return $this->hasMany('Application');
    }

    public function news()
    {
        return $this->hasMany(News::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function company()
    {
        return $this->hasOne(Company::class);
    }

    /**
     * Return a status of the last application.
     * Check ApplicationStatus class for more information.
     *
     * @return string
     */
    public function lastApplicationStatus()
    {
        if (is_null($this->applications()->first()))
            $lastAppStatus = null;
        else $lastAppStatus = $this->applications()->first()->status;

        return $lastAppStatus;
    }

    /**
     * @return bool
     */
    public function hasPendingApplication()
    {
        if (!is_null($this->lastApplicationStatus()) &&
            $this->lastApplicationStatus() === ApplicationStatus::PENDING)
            return true;
        else return false;
    }

    /**
     * Return a Type of the last application.
     * Check ApplicationType class for more information.
     *
     * @return string
     */
    public function lastApplicationType()
    {
        return $this->applications()->first()->type;
    }

    /**
     * Return manager answer on last application.
     *
     * @param $appStatus
     * @return string
     */
    public function lastApplicationAnswer($appStatus)
    {
        if (is_null($this->applications()->first()) || $this->lastApplicationStatus() !== $appStatus)
            $lastAppAnswer = null;
        else $lastAppAnswer = $this->applications()->first()->answer;

        return $lastAppAnswer;
    }

    /**
     * Get all of the owners's applications.
     */
    public function applications()
    {
        return $this->morphMany('App\Models\Communication\Application', 'applicationable')
            ->orderBy('created_at', 'desc');
    }

    /**
     * Create company.
     *
     * @param array $data
     * @return \Illuminate\Database\Eloquent\Model
     * Return Company Model
     */
    public function createCompany(array $data)
    {
        return $this->company()->create($data);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function bookings()
    {
        return $this->hasMany(Booking::class);
    }
}
