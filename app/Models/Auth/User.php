<?php

namespace App\Models\Auth;

use App\Models\Booking\Booking;
use App\Models\Communication\Comments;
use App\Notifications\UserResetPassword;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use App\Models\Booking\BookingStatus;

class User extends Authenticatable
{
    use Notifiable, HasMediaTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name',
        'last_name',
        'password',
        'phone',
        'email',
        'birth_date',
        'email_token',
        'patronymic',
        'new_email'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Default values for model.
     *
     * @var array
     */
    protected $attributes = array(
       'activated' => false,
    );

    /**
     * Send the password reset notification.
     *
     * @param  string  $token
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new UserResetPassword($token));
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function bookings()
    {
        return $this->hasMany(Booking::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getNearestBooking()
    {
        $nearestDate = $this->bookings()->where('status', '!=', BookingStatus::CANCELED)
            ->where('from_date', '>', date('Y-m-d', time()))->get()->min('from_date');

        return $this->bookings()->where('from_date', $nearestDate)->first();
    }

    /**
     * Get all user comments.
     */
    public function comments()
    {
        return $this->hasMany(Comments::class);
    }

    /**
     * Check whether the user visited the glamping.
     *
     * @param $glamping
     * @return bool
     */
    public function isGlampingVisited($glamping)
    {
        if (count($glamping->bookings()->where('status',  BookingStatus::CONFIRMED)->where('user_id', $this->id)
                ->where('from_date', '<', date('Y-m-d', time()))->get()) > 0)
            $isGlampingVisited = true;
        else $isGlampingVisited = false;

        return $isGlampingVisited;
    }
}
