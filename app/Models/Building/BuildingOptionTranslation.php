<?php

namespace App\Models\Building;

use Illuminate\Database\Eloquent\Model;

class BuildingOptionTranslation extends Model
{
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'description', 'locale'
    ];
}
