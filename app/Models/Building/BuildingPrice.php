<?php

namespace App\Models\Building;

use Illuminate\Database\Eloquent\Model;

class BuildingPrice extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'from',
        'to',
        'price'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function buildings()
    {
        return $this->belongsToMany('App\Models\Building\Building', 'building_price_building');
    }

    /**
     * Clean BuildingPrices that lies into this BuildingPrice gap
     * @return bool
     */
    public function cleanBuildingPrices()
    {
        $building = $this->buildings()->first();

        //buildingPrices that starts before our starts and finish after our starts
        $building_prices_left = $building->prices()->where('id', '!=', $this['id'])
            ->whereDate('from', '<=', $this['from'])
            ->whereDate('to', '>=', $this['from']);
        //prices that starts before out finish and finish after our finish
        $building_prices_right = $building->prices()->where('id', '!=', $this['id'])
            ->whereDate('from', '<=', $this['to'])
            ->whereDate('to', '>=', $this['to']);
        //prices that lies inside our gap
        $building_prices_center = $building->prices()->where('id', '!=', $this['id'])
            ->whereDate('from', '>=', $this['from'])
            ->whereDate('to', '<=', $this['to']);

        $building_prices = $building_prices_left
            ->union($building_prices_right)
            ->union($building_prices_center)
            ->get();

        if (sizeof($building_prices) > 0) {
            foreach ($building_prices as $building_price) {
                if ($building_price['from'] >= $this['from'] &&
                    $building_price['to'] <= $this['to'])
                    $building_price->delete();
                else if ($building_price['from'] >= $this['from'] &&
                    $building_price['to'] > $this['to']) {
                    list($y, $m, $d) = explode("-", $this['to']);
                    $new_to = date('Y-m-d', mktime(0, 0, 0, $m, $d, $y) + 24 * 60 * 60);
                    $building_price->update([
                        'from' => $new_to,
                    ]);
                } else if ($building_price['to'] <= $this['to'] &&
                    $building_price['from'] > $this['from']) {
                    list($y, $m, $d) = explode("-", $this['from']);
                    $new_from = date('Y-m-d', mktime(0, 0, 0, $m, $d, $y) - 24 * 60 * 60);
                    $building_price->update([
                        'to' => $new_from,
                    ]);
                } else {
                    list($y, $m, $d) = explode("-", $this['to']);
                    $new_to = date('Y-m-d', mktime(0, 0, 0, $m, $d, $y) + 24 * 60 * 60);
                    list($y, $m, $d) = explode("-", $this['from']);
                    $new_from = date('Y-m-d', mktime(0, 0, 0, $m, $d, $y) - 24 * 60 * 60);
                    $new_building_price = BuildingPrice::create([
                        'from' => $new_to,
                        'to' => $building_price->to,
                        'price' => $building_price->price,
                    ]);
                    $building = $this->buildings()->first();
                    $building->prices()->attach($new_building_price->id);
                    $building_price->update([
                        'to' => $new_from,
                    ]);
                    if (is_null($new_building_price))
                        return false;
                }
            }
        }

        return true;
    }
}
