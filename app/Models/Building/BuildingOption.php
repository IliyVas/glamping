<?php

namespace App\Models\Building;

use Illuminate\Database\Eloquent\Model;
use Dimsav\Translatable\Translatable;

class BuildingOption extends Model
{
    use Translatable;

    /**
     * @var array
     */
    public $translatedAttributes = [
        'name', 'description'
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'price'
    ];

    /**
     * The relations to eager load on every query.
     *
     * @var array
     */
    protected $with = ['translations'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function building()
    {
        return $this->belongsTo('App\Models\Building\Building');
    }
}
