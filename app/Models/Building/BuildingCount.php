<?php

namespace App\Models\Building;

use Illuminate\Database\Eloquent\Model;

class BuildingCount extends Model
{
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'from_date', 'to_date', 'count'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function building()
    {
        return $this->belongsTo('App\Models\Building\Building');
    }

    /**
     * Clean BuildingCounts that lies into this BuildingCount gap
     * @return bool
     */
    public function cleanBuildingCounts()
    {
        $building = $this->building;

        //buildingCounts that starts before our starts and finish after our starts
        $building_counts_left = $building->buildingCounts()->where('id', '!=', $this['id'])
            ->whereDate('from_date', '<=', $this['from_date'])
            ->whereDate('to_date', '>=', $this['from_date']);
        //buildingCounts that starts before out finish and finish after our finish
        $building_counts_right = $building->buildingCounts()->where('id', '!=', $this['id'])
            ->whereDate('from_date', '<=', $this['to_date'])
            ->whereDate('to_date', '>=', $this['to_date']);
        //buildingCounts that lies inside our gap
        $building_counts_center = $building->buildingCounts()->where('id', '!=', $this['id'])
            ->whereDate('from_date', '>=', $this['from_date'])
            ->whereDate('to_date', '<=', $this['to_date']);

        $building_counts = $building_counts_left
            ->union($building_counts_right)
            ->union($building_counts_center)
            ->get();

        if (sizeof($building_counts) > 0) {
            foreach ($building_counts as $building_count) {
                if ($building_count['from_date'] >= $this['from_date'] &&
                    $building_count['to_date'] <= $this['to_date'])
                    $building_count->delete();
                else if ($building_count['from_date'] >= $this['from_date'] &&
                    $building_count['to_date'] > $this['to_date']) {
                    list($y, $m, $d) = explode("-", $this['to_date']);
                    $new_to_date = date('Y-m-d', mktime(0, 0, 0, $m, $d, $y) + 24 * 60 * 60);
                    $building_count->update([
                        'from_date' => $new_to_date,
                    ]);
                } else if ($building_count['to_date'] <= $this['to_date'] &&
                    $building_count['from_date'] > $this['from_date']) {
                    list($y, $m, $d) = explode("-", $this['from_date']);
                    $new_from_date = date('Y-m-d', mktime(0, 0, 0, $m, $d, $y) - 24 * 60 * 60);
                    $building_count->update([
                        'to_date' => $new_from_date,
                    ]);
                } else {
                    list($y, $m, $d) = explode("-", $this['to_date']);
                    $new_to_date = date('Y-m-d', mktime(0, 0, 0, $m, $d, $y) + 24 * 60 * 60);
                    list($y, $m, $d) = explode("-", $this['from_date']);
                    $new_from_date = date('Y-m-d', mktime(0, 0, 0, $m, $d, $y) - 24 * 60 * 60);
                    $new_building_count = BuildingCount::create([
                        'from_date' => $new_to_date,
                        'to_date' => $building_count->to_date,
                        'count' => $building_count->count,
                        'building_id' => $building_count->building_id,
                    ]);
                    $building_count->update([
                        'to_date' => $new_from_date,
                    ]);
                    if (is_null($new_building_count))
                        return false;
                }
            }
        }

        return true;
    }
}
