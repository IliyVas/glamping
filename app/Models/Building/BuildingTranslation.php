<?php

namespace App\Models\Building;

use Illuminate\Database\Eloquent\Model;

class BuildingTranslation extends Model
{
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'description', 'locale', 'other_information',
    ];
}
