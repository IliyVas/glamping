<?php

namespace App\Models\Building;

use App\Models\Booking\Booking;
use App\Models\Booking\BookingStatus;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\HasMedia\Interfaces\HasMedia;
use Dimsav\Translatable\Translatable;

class Building extends Model implements HasMedia
{
    use HasMediaTrait, Translatable;

    /**
     * @var array
     */
    public $translatedAttributes = [
        'name', 'description', 'other_information',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'capacity',
        'count',
        'min_nights_count',
        'main_price',
        'building_type_id'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function amenities()
    {
        return $this->belongsToMany('App\Models\Glamping\Amenity', 'building_amenity');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function prices()
    {
        return $this->belongsToMany('App\Models\Building\BuildingPrice', 'building_price_building');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function glamping()
    {
        return $this->belongsTo('App\Models\Glamping\Glamping');
    }

    /**
     * @return array
     */
    public function allPrices()
    {
        return [
            'main_prices' => $this->main_price,
            'date_prices' => $this->prices()->get(),
        ];
    }

    /**
     * @param $from
     * @param null $to
     * @return array
     */
    public function datePrice($from, $to = null)
    {
        //dates that start before our and continue to our
        $left = $this->prices()->whereDate('from', '<=', $from)->whereDate('to', '>=', $from);
        //if no second date
        if (is_null($to)) {
            return [
                'main_price' => $this->main_price,
                'date_prices' => $left->get(),
            ];
        }

        //dates that start when our starts and continue after
        $right = $this->prices()->whereDate('from', '<=', $to)->whereDate('to', '>=', $to);
        //dates that start after our and stop before
        $center = $this->prices()->whereDate('from', '>=', $from)->whereDate('to', '<=', $to);

        return [
            'main_price' => $this->main_price,
            'date_prices' => $left->union($right)->union($center)->get(),
        ];
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function buildingType()
    {
        return $this->belongsTo('App\Models\Building\BuildingType');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function buildingOptions()
    {
        return $this->hasMany('App\Models\Building\BuildingOption');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function buildingCounts()
    {
        return $this->hasMany('App\Models\Building\BuildingCount');
    }

    /**
     * @param null $date
     * @return mixed
     */
    public function count($date = null)
    {
        if (is_null($date))
            $date = date('Y-m-d', time());

        $building_count = $this->buildingCounts()
            ->whereDate('from_date', '<=', $date)
            ->whereDate('to_date', '>=', $date)->first();

        $count = $this['count'];
        if (!is_null($building_count))
            $count = $building_count->count;

        $building_books = Booking::whereDate('from_date', '<=', $date)
            ->whereDate('to_date', '>=', $date)->get();

        foreach ($building_books as $building_book) {
            $book_buildings = $building_book->bookBuildings()->get();
            foreach ($book_buildings as $book_building)
                if ($book_building->building->id == $this->id)
                    $count -= $book_building->count;
        }

        if ($count < 0)
            return 0;

        return $count;
    }

    /**
     * The relations to eager load on every query.
     *
     * @var array
     */
    protected $with = ['translations'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function changedBuilding()
    {
        return $this->hasOne('App\Models\Building\Building');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function building()
    {
        return $this->belongsTo('App\Models\Building\Building');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function comments()
    {
        return $this->morphMany('App\Models\Communication\Comments', 'model');
    }

    /**
     * @return float
     */
    public function getAverageRating()
    {
        return round($this->comments()->avg('rating'), 1);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function bookBuildings()
    {
        return $this->hasMany('App\Models\Booking\BookBuilding');
    }

    /**
     * @param $fromDate
     * @param $toDate
     * @return int
     */
    public function getBuildingReceipts($fromDate, $toDate)
    {
        $bookBuildings = $this->bookBuildings;
        $receipts = 0;

        if (count($bookBuildings) > 0) {
            foreach ($bookBuildings as $bookBuilding) {
               $booking = $bookBuilding->booking;

               if ($booking->from_date >= $fromDate && $booking->from_date <= $toDate &&
                   $booking->status === BookingStatus::CONFIRMED) {
                   $receipts += $bookBuilding->getPrice($booking->from_date, $booking->to_date);
               }
            }
        }

        return $receipts;
    }

    /**
     * @param $fromDate
     * @param $toDate
     * @return int
     */
    public function getBuildingConfirmedBookingCount($fromDate, $toDate)
    {
        $bookBuildings = $this->bookBuildings;
        $count = 0;

        if (count($bookBuildings) > 0) {
            foreach ($bookBuildings as $bookBuilding) {
                $booking = $bookBuilding->booking;

                if ($booking->from_date >= $fromDate && $booking->from_date <= $toDate &&
                    $booking->status === BookingStatus::CONFIRMED) {
                    $count ++;
                }
            }
        }

        return $count;
    }

    /**
     * @param $fromDate
     * @param $toDate
     * @return int
     */
    public function getBuildingCanceledBookingCount($fromDate, $toDate)
    {
        $bookBuildings = $this->bookBuildings;
        $count = 0;

        if (count($bookBuildings) > 0) {
            foreach ($bookBuildings as $bookBuilding) {
                $booking = $bookBuilding->booking;

                if ($booking->from_date >= $fromDate && $booking->from_date <= $toDate &&
                    $booking->status === BookingStatus::CANCELED) {
                    $count ++;
                }
            }
        }

        return $count;
    }

    /**
     * @param $fromDate
     * @param $toDate
     * @return int|mixed
     */
    public function getPeriodPrice($fromDate, $toDate)
    {
        $dt = $this->datePrice($fromDate, $toDate);
        $price = $this->main_price;

        if (!count($dt['date_prices']) > 0) {
            for ($periodPrice = 0; $fromDate <= $toDate; $fromDate++) {
                $periodPrice += $price;
            }
        } else {
            $specialPeriodDays = 0;
            $p = 0;
            foreach ($dt['date_prices'] as $date) {
                $periodDaysCount = $this->getAdditionalPricePeriodDaysCount(
                    $date->from, $date->to, $fromDate, $toDate);
                $periodDaysCount = $periodDaysCount->days;

                if ($periodDaysCount == 0)
                    $periodDaysCount++;

                $specialPeriodDays += $periodDaysCount;
                $p += $periodDaysCount * $date->price;
            }
            $daysCount = date_create($fromDate)->diff(date_create($toDate));
            $daysCount = $daysCount->days;

            if ($daysCount == 0)
                $daysCount++;

            $daysCount = $daysCount - $specialPeriodDays;

            $standardPeriodPrice = $daysCount * $price;
            $periodPrice = $p + $standardPeriodPrice;
        }

        return $periodPrice;
    }

    /**
     * @param $fromDate
     * @param $toDate
     * @param $specialFromDate
     * @param $specialToDate
     * @return bool|\DateInterval
     */
    private function getAdditionalPricePeriodDaysCount($fromDate, $toDate, $specialFromDate, $specialToDate)
    {
        if ($fromDate < $specialFromDate) {
            $fromDate = $specialFromDate;
        }

        if ($toDate > $specialToDate) {
            $toDate = $specialToDate;
        }

        return date_create($fromDate)->diff(date_create($toDate));
    }

    /**
     * @param null $fromDate
     * @return mixed
     */
    public function getPricesData($fromDate = null)
    {
        //get dates
        if (is_null($fromDate)) $fromDate = date('Y-m-d', time());
        $toDate = date_create($fromDate)->modify('+ 1 day')->format('Y-m-d');

        //$prices - assoc array, contains ['main_price'] and ['date_prices']
        $prices = $this->datePrice($fromDate, $toDate);
        $data['price'] = $prices['main_price']; //set main price
        $data['discount_price'] = null;
        $data['sale'] = null;

        //set discount prices and sale %
        if (sizeof($prices['date_prices'])) {
            //set discount prices
            $discountPrices = $prices['date_prices']->pluck('price', 'from')->all();
            foreach ($discountPrices as $date => $price) {
                if ($price >= $data['price']) unset($discountPrices[$date]);
            }

            if (empty($discountPrices)) return $data;
            else $data['discount_price'] = $this->getDiscountPrice($discountPrices, $fromDate);

            //set sale
            $data['sale'] = $this->getSale($data['price'], $data['discount_price']);
        }

        return $data;
    }

    /**
     * @param array $discountPrices
     * @param $fromDate
     * @return mixed
     */
    private function getDiscountPrice(array $discountPrices, $fromDate)
    {
        if (count($discountPrices) > 1) {
            foreach ($discountPrices as $date => $price) {
                if ($date > $fromDate) unset($discountPrices[$date]);
            }
        }

        return array_first($discountPrices);
    }

    /**
     * @param $mainPrice
     * @param $discountPrice
     * @return float
     */
    public function getSale($mainPrice, $discountPrice)
    {
        return round(100 - 100 * $discountPrice / $mainPrice);
    }
}
