<?php

namespace App\Models\News;

use Illuminate\Database\Eloquent\Model;

class NewsTranslation extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'name', 'description', 'locale', 'title', 'keywords', 'seo_description'
    ];
}
