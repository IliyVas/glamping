<?php

namespace App\Models\News;

use App\Models\Auth\Manager;
use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\HasMedia\Interfaces\HasMedia;

class News extends Model implements HasMedia
{
    use Translatable, HasMediaTrait;

    protected $fillable = [];

    public $translatedAttributes = [
        'name', 'description', 'title', 'keywords', 'seo_description'
    ];

    protected $with = ['translations'];

    public function manager()
    {
        return $this->belongsTo(Manager::class);
    }
}
