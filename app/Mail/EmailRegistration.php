<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class EmailRegistration extends Mailable
{
    use Queueable, SerializesModels;

    protected $status;

    /**
     * EmailRegistration constructor.
     * @param $status
     */
    public function __construct($status)
    {
        $this->status = $status;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject( trans('applications.registration'))
            ->view('email.registration')
            ->with(['status' => $this->status]);
    }
}
