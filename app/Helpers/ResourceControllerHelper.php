<?php

namespace App\Helpers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Collection;

class ResourceControllerHelper
{
    /**
     * Used to generate data of typical grids (Services, Amenities, Impressions,
     * Glamping group, Glamping Type grids).
     *
     * @param $columns
     * $columns is an array, contain the names of the required columns.
     *
     * @param Collection $data
     * @param $modelName
     * $modelName is a string, contain name of Model.
     *
     * @return array
     */
    public static function getGridData($columns, Collection $data, $modelName)
    {
        $fileTranslateName = self::getFileTranslateName($modelName);

        $gridData = [
            'columns' => self::getColumnGridData($columns),
            'gridData' => $data,
            'pageName' => trans($fileTranslateName . '.gridName'),
            'editRoute' => self::getActionRoute('edit', $modelName),
            'active' => $modelName,
            'breadCrumbsActive' => self::getBreadCrumbsActiveName($modelName),
            'deleteRoute' => self::getActionRoute('destroy', $modelName),
            'createRoute' => self::getActionRoute('create', $modelName),
            'createButtonName' => trans($fileTranslateName . '.createButton'),
            'menuItem' => 'lists'
        ];

        return $gridData;
    }

    /**
     * @param $modelName
     * $modelName is a string, contain name of Model
     *
     * @return mixed|string
     */
    public static function getFileTranslateName($modelName)
    {
        return snake_case($modelName);
    }

    /**
     * @param $modelName
     * @return string
     */
    public static function getBreadCrumbsActiveName($modelName)
    {
        return str_plural(camel_case($modelName));
    }

    /**
     * Used to generate data of typical forms (Services, Amenities, Impressions,
     * Glamping group, Glamping Type forms).
     *
     * @param $model
     * $model can be a Model or null. If it create form $model must be null.
     *
     * @param $fieldsNames
     * $fieldsNames is an array, contain the names of the required columns.
     *
     * @param $modelName
     * $modelName is a string, contain name of Model
     *
     * @param $transFieldsNames
     * $transFieldsNames is an array, contain the names of the translated columns.
     *
     * @return array
     */
    public static function getFormData($model, $fieldsNames, $modelName, $transFieldsNames = ['name', 'description'])
    {
        $actionRoute = null;
        $method = null;
        if (is_null($model)) {
            $actionRoute = self::getActionRoute('store', $modelName);
            $method = "POST";
        } else {
            $actionRoute = [self::getActionRoute('update', $modelName), $model->id];
            $method = "PATCH";
        }

        $formData = [
            'fields' => self::getFormFields($fieldsNames),
            'transFields' => self::getTranslationFormFields($transFieldsNames),
            'model' => $model,
            'modelIndexPath' => self::getActionRoute('index', $modelName),
            'breadCrumbsActive' => self::getBreadCrumbsActiveName($modelName),
            'actionRoute' => $actionRoute,
            'method' => $method,
            'pageName' => trans(self::getFileTranslateName($modelName) . '.formName')
        ];

        return $formData;
    }

    /**
     * @param $columns
     * @return mixed
     * @throws LogicException
     */
    public static function getColumnGridData($columns)
    {
        if (empty($columns)) {
            throw new LogicException('Invalid $columns');
        }

        foreach ($columns as $column) {
            switch ($column) {
                case 'fontIcon':
                    $columnData['fontIcon'] = [
                        'type' => 'fontIcon',
                        'dbName' => 'icon',
                        'name' => trans('common_elements.iconColumnName')
                    ]; break;
                case 'icon':
                    $columnData['icon'] = [
                        'type' => 'icon',
                        'name' => trans('common_elements.iconColumnName')
                    ]; break;
                case '№':
                    $columnData['id'] = [
                        'type' => 'text',
                        'name' => '№'
                    ]; break;
                case 'name':
                    $columnData['name'] = [
                        'type' => 'text',
                        'name' => trans('common_elements.nameColumnName')
                    ]; break;
                case 'Name':
                    $columnData['name'] = [
                        'type' => 'text',
                        'name' => trans('auth.name')
                    ]; break;
                case 'email':
                    $columnData['email'] = [
                        'type' => 'text',
                        'name' => trans('auth.email')
                    ]; break;
                case 'message':
                    $columnData['message'] = [
                        'type' => 'text',
                        'name' => trans('messages.message')
                    ]; break;
                case 'type':
                    $columnData['type'] = [
                        'type' => 'text',
                        'name' => trans('common_elements.applicationTypeColumnName')
                    ]; break;
                case 'description':
                    $columnData['description'] = [
                        'type' => 'text',
                        'name' => trans('common_elements.descriptionColumnName')
                    ]; break;
                case 'date-application':
                    $columnData['date-application'] = [
                        'type' => 'text',
                        'name' => trans('common_elements.applicationDateColumnName')
                    ]; break;
                case 'date':
                    $columnData['date'] = [
                        'type' => 'text',
                        'name' => trans('common_elements.dateColumnName')
                    ]; break;
                case 'date-review':
                    $columnData['date-review'] = [
                        'type' => 'text',
                        'name' => trans('common_elements.reviewDateColumnName')
                    ]; break;
                case 'status':
                    $columnData['status'] = [
                        'type' => 'text',
                        'name' => trans('common_elements.statusColumnName')
                    ]; break;
                case 'reviewer':
                    $columnData['reviewer'] = [
                        'type' => 'text',
                        'name' => trans('common_elements.reviewerColumnName')
                    ]; break;
                case 'action-view':
                    $columnData['action'] = [
                        'type' => 'action',
                        'name' => trans('common_elements.actionColumnName'),
                        'elem' => ['view' => true]
                    ]; break;
                case 'action-edit-delete':
                    $columnData['action'] = [
                        'type' => 'action',
                        'name' => trans('common_elements.actionColumnName'),
                        'elem' => ['edit' => true, 'delete' => true]
                    ]; break;
                case 'action-edit-cancel-confirm':
                    $columnData['action'] = [
                        'type' => 'action',
                        'name' => trans('common_elements.actionColumnName'),
                        'elem' => ['edit' => true, 'cancel' => true, 'confirm' => true]
                    ]; break;
            }
        }

        if (empty($columnData)) {
            throw new LogicException('Invalid columns type');
        }

        return $columnData;
    }

    /**
     * @param $action
     * @param $modelName
     * @return string
     */
    public static function getActionRoute($action, $modelName)
    {
        self::validateParam($modelName);
        $prefix = "";
        if (Auth::guard('manager')->check()) {
            $prefix = "manager.";
        }
        $actionRoute = snake_case(str_plural($modelName)) . '.' .$action;

        return $prefix . $actionRoute;
    }

    /**
     * @param $param
     * @throws LogicException
     */
    private static function validateParam($param)
    {
        if (empty($param)) {
            throw new LogicException('Invalid param');
        }
    }

    /**
     * @param $fieldsNames
     * @return mixed
     * @throws LogicException
     */
    public static function getFormFields($fieldsNames)
    {
        self::validateParam($fieldsNames);

        foreach ($fieldsNames as $fieldName) {
            switch ($fieldName) {
                case 'fontIcon':
                    $fields['fontIcon'] = [
                        'type' => 'hidden',
                        'name' => 'icon',
                        'label' => trans('common_elements.iconInputLabel')
                    ]; break;
                case 'icon':
                    $fields['icon'] = [
                        'type' => 'file',
                        'label' => trans('common_elements.iconInputLabel')
                    ]; break;
                case 'image':
                    $fields['image'] = [
                        'type' => 'file',
                        'label' => trans('common_elements.imageInputLabel')
                    ]; break;
                case 'name':
                    $fields['name'] = [
                        'type' => 'text',
                        'label' => trans('common_elements.nameInputLabel')
                    ]; break;
                case 'description':
                    $fields['description'] = [
                        'type' => 'textarea',
                        'label' => trans('common_elements.descriptionInputLabel')
                    ]; break;
                case 'submit':
                    $fields['submit'] = [
                        'type' => 'submit'
                    ]; break;
            }
        }

        if (empty($fields)) {
            throw new LogicException('Invalid fields type');
        }

        return $fields;
    }

    /**
     * @param $fieldsNames
     * @return mixed
     * @throws LogicException
     */
    public static function getTranslationFormFields($fieldsNames)
    {
        self::validateParam($fieldsNames);

        foreach ($fieldsNames as $fieldName) {
            switch ($fieldName) {
                case 'name':
                    $fields['name'] = [
                        'type' => 'text',
                        'label' => trans('common_elements.nameInputLabel')
                    ]; break;
                case 'description':
                    $fields['description'] = [
                        'type' => 'textarea',
                        'label' => trans('common_elements.descriptionInputLabel')
                    ]; break;
            }
        }

        if (empty($fields)) {
            throw new LogicException('Invalid fields type');
        }

        return $fields;
    }
}
