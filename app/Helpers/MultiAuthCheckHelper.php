<?php

namespace App\Helpers;

use Illuminate\Support\Facades\Auth;

class MultiAuthCheckHelper
{
    public static function check()
    {
        return Auth::guard('manager')->check() || Auth::guard('user')->check();
    }
}
