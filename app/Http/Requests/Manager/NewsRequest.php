<?php

namespace App\Http\Requests\Manager;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class NewsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name:en' => 'required|min:3|max:255',
            'title:en' => 'required|min:3|max:255',
            'keywords:en' => 'required|min:3|max:255',
            'name:ru' => 'required|min:3|max:255',
            'title:ru' => 'required|min:3|max:255',
            'keywords:ru' => 'required|min:3|max:255'
        ];
    }
}
