<?php

namespace App\Http\Controllers\Glamping;

use App\Models\Glamping\Impression;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Traits\MediaUploader;
use App\Http\Controllers\Glamping\Helpers\CrudHelper;
use App\Helpers\ResourceControllerHelper as ResourceHelper;

class ImpressionController extends Controller
{
    use MediaUploader;

    /**
     * @var string
     */
    protected $modelName = 'Impression';

    /**
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $columns = ['icon', 'name', 'description', 'action-edit-delete'];
        $gridData = ResourceHelper::getGridData($columns, Impression::all(), $this->modelName);

        return view('manager.abstract_model.grid')->with($gridData);
    }

    /**
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return $this->renderForm();
    }

    /**
     * @param $id
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $impression = Impression::find($id);

        if (!isset($impression)) {
            return redirect()->back()->withErrors(trans('messages.notExistImpressionError'));
        }

        return $this->renderForm($impression);
    }

    /**
     * @param Impression|null $impression
     * @return \Illuminate\View\View
     */
    private function renderForm(Impression $impression = null)
    {
        $fieldsNames = ['icon', 'submit'];

        return view('manager.abstract_model.form')
            ->with(ResourceHelper::getFormData($impression, $fieldsNames, $this->modelName));
    }

    /**
     * @param Request $request
     * @return $this
     */
    public function store(Request $request)
    {
        $additionalRule = ['icon' => 'required|mimes:jpeg,png'];
        CrudHelper::getValidateRules($request->all(), $additionalRule)->validate();

        $impressionModel = new Impression;
        $impressionModel->fill(CrudHelper::getFinalInputData($request->all()))->save();

        $name = CrudHelper::transliterate($request->icon->getClientOriginalName());
        $iconUrl = $this->uploadIcon($impressionModel, $request->icon, $this->modelName, $name)->getUrl();
        $result = $impressionModel->update(['icon' => $iconUrl]);

        if ($result) {
            flash(trans('messages.createImpressionSuccessMessage'), 'success');
        } else {
            flash(trans('messages.dangerMessage'), 'danger');
        }

        return redirect()->back();
    }

    /**
     * @param Request $request
     * @param $id
     * @return $this
     */
    public function update(Request $request, $id)
    {
        CrudHelper::getValidateRules($request->all())->validate();
        $data = $request->all();
        $impressionModel = Impression::where('id', $id)->first();

        if (!empty($data['icon'])) {
            $name = CrudHelper::transliterate($request->icon->getClientOriginalName());
            $iconUrl = $this->uploadIcon($impressionModel, $request->icon, $this->modelName, $name)->getUrl();
        } else {
            $iconUrl = $impressionModel->icon;
        }

        $data['icon'] = $iconUrl;
        $result = $impressionModel->fill($data)->update();

        if ($result) {
            flash(trans('messages.updateImpressionSuccessMessage'), 'success');
        } else {
            flash(trans('messages.dangerMessage'), 'danger');
        }

        return redirect()->back();
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        $result = CrudHelper::delete(Impression::find($id));

        if ($result) {
            flash(trans('messages.deleteImpressionSuccessMessage'), 'success');
        } else {
            flash(trans('messages.dangerMessage'), 'danger');
        }

        return redirect()->back();
    }

    /**
     * @param Request $request
     * @return array
     */
    public function getAutocompleteData(Request $request)
    {
        $result = [];
        $impressions = Impression::whereTranslationLike('name', '%' . $request->term . '%')->get();

        foreach ($impressions as $impression) {
            $result[] = ['value' => $impression->name, 'id' => $impression->id];
        }

        return $result;
    }
}
