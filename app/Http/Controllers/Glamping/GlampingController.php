<?php

namespace App\Http\Controllers\Glamping;

use App\Http\Controllers\Glamping\Helpers\CrudHelper;
use App\Http\Controllers\Glamping\Helpers\GlampingListFilterHelper;
use App\Http\Requests\CheckParamsRequest;
use App\Models\Communication\Application;
use App\Models\Communication\ApplicationStatus;
use App\Models\Communication\ApplicationType;
use App\Models\Building\Building;
use App\Models\Building\BuildingPrice;
use App\Models\Glamping\Glamping;
use App\Models\Glamping\Amenity;
use App\Models\Glamping\GlampingGroup;
use App\Models\Glamping\Impression;
use App\Models\Glamping\Service;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Collection;
use App\Traits\MediaUploader;

class GlampingController extends Controller
{
    use MediaUploader;

    /**
     * @var int
     */
    const DEFAULT_ROLE_MAX_IMAGE_COUNT = 5;

    /**
     * @var int
     */
    const ADMIN_ROLE_MAX_IMAGE_COUNT = 15;

    /**
     * @var int
     */
    const OWNER_ROLE_MAX_IMAGE_COUNT = 10;


    /**
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $user = Auth::guard('manager')->user();

        if ($user->hasRole('admin'))
            $glampings = Glamping::all();
        else
            $glampings = $user->glampings;

        return view('manager.glampings_list')->with([
            'glampings' => $glampings,
            'user' => $user
        ]);
    }

    /**
     * @return \Illuminate\View\View
     */
    public function create()
    {
        if (!Auth::user()->hasRole('owner'))
            return redirect()->back()->withErrors(trans('messages.dangerMessage'));

        return $this->renderGlampingForm();
    }

    /**
     * @param  int  $id
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $glamping = Glamping::with(
            'services',
            'glampingGroups',
            'impressions',
            'amenities')->find($id);

        if (is_null($glamping))
            return redirect()->back()->withErrors(trans('messages.notExistServiceError'));
        
        if (Auth::guest() || !Auth::user()->glampings->contains($glamping->id) && Auth::user()->hasRole('owner'))
            return redirect()->back()->withErrors(trans('messages.noRightsToThisGlamping'));

        return $this->renderGlampingForm($glamping);
    }

    /**
     * Display the glamping form.
     *
     * @param  \App\Models\Glamping\Glamping $glamping
     * @return \Illuminate\View\View
     */
    private function renderGlampingForm($glamping = null)
    {
        if (!is_null($glamping) && count($glamping->applications) > 0 &&
            $glamping->lastApplicationStatus() === ApplicationStatus::PENDING && Auth::user()->hasRole('owner'))
            return redirect()->back()->withErrors(trans('messages.noRightsToEditThisGlamping'));

        return view('manager.glamping_form')->with([
            'glamping' => $glamping,
            'glampingGroups' => GlampingGroup::all(),
            'impressions' => Impression::all(),
            'amenities' => Amenity::all(),
            'services' => Service::all(),
            'maxImgCount' => GlampingController::getMaxImgCount(),
            'user' => Auth::guard('manager')->user()
        ]);
    }

    /**
     * @return int
     */
    public static function getMaxImgCount()
    {
        /** @var Collection $roleCollection */
        $roleCollection = Auth::user()->roles;
        $role = $roleCollection->first();

        $maxImgCount = self::DEFAULT_ROLE_MAX_IMAGE_COUNT;
        if ($role->name == 'admin') {
            $maxImgCount = self::ADMIN_ROLE_MAX_IMAGE_COUNT;
        } elseif ($role->name == 'owner') {
            $maxImgCount = self::OWNER_ROLE_MAX_IMAGE_COUNT;
        }

        return $maxImgCount;
    }

    /**
     * @param $data
     * @return mixed
     */
    private function getCreateGlampingFinalData($data)
    {
        if (empty($data['name:ru'])) {
            $data['region:ru'] = null;
            $data['country:ru'] = null;
        }

        if (empty($data['name:en'])) {
            $data['region:en'] = null;
            $data['country:en'] = null;
        }
        $data = CrudHelper::getFinalInputData($data);

        return $data;
    }

    /**
     * @param Request $request
     * @return $this
     */
    public function store(Request $request)
    {
        if (!Auth::user()->hasRole('owner'))
            return redirect()->back()->withErrors(trans('messages.dangerMessage'));

        $data = $request->all();
        //validate $data
        CrudHelper::getValidateRules($data, $this->getAdditionalValidateRule(['images' => 'required']))
            ->after(function ($validator) use ($data) {
                if (isset($data['images']) && count($data['images']) > self::getMaxImgCount()) {
                    $validator->errors()->add('images', trans('messages.maxImgCountError') . self::getMaxImgCount());
                }
            })->validate();

        if (isset($data['commission']))
            unset($data['commission']);

        $model = $this->saveGlamping($data);
        flash(trans('messages.createGlampingSuccessMessage'), 'success');

        return redirect()->route('manager.buildings.show', [$model->id]);
    }

    /**
     * @param $data
     * @return $this|Glamping
     */
    private function saveGlamping($data)
    {
        $model = new Glamping;
        $data = $this->getCreateGlampingFinalData($data);
        $model->fill($data);
        $result = Auth::guard('manager')->user()->glampings()->save($model);

        if (!$result)
            return redirect()->back()->withErrors(trans('messages.dangerMessage'));

        $this->saveAdditionalData($data, $model);
        $this->loadGlampingImages($model, $data);

        return $model;
    }

    /**
     * @param $glamping
     * @param $data
     */
    private function loadGlampingImages($glamping, $data){
        //add new images
        if (isset($data['images'])) {
            foreach ($data['images'] as $image) {
                $url = public_path('tmp') . '/' . Auth::user()->id . '/' . $image;
                $mediaLibrary = $this->getMediaLibraryImageName('Glamping');
                $glamping->addMediaFromUrl($url)->toMediaLibrary($mediaLibrary);
            }
        }

        $this->deleteTemporaryMediaDirectory(public_path('tmp/' . Auth::user()->id));
    }

    /**
     * @param $glamping
     * @param $data
     */
    private function loadEditedGlampingImages($glamping, $data)
    {
        $images = $glamping->media()->get();
        //delete selected images
        foreach ($images as $imageKey => $image) {
            if (isset($data['delImages']) && !empty($data['delImages'])) {
                foreach ($data['delImages'] as $delImgId) {
                    if ($image->id == $delImgId) {
                        $images[$imageKey]->delete();
                    }
                }
            }
        }

        $this->loadGlampingImages($glamping, $data);
    }

    /**
     * @param array $uniqueRule
     * @return array
     */
    private function getAdditionalValidateRule($uniqueRule = [])
    {
        $rule = [
            'latitude' => 'required|string|max:10',
            'longitude' => 'required|string|max:10',
            'other_information:en' => 'nullable|string',
            'other_information:ru' => 'nullable|string',
            'glamping_address:en' => 'nullable|string',
            'glamping_address:ru' => 'nullable|string',
            'max_child_age' => 'required|numeric|max:18|min:0',
            'free_return' => 'required|numeric|max:100|min:1',
            'arrival_time' => 'nullable|string|min:3|max:5',
            'check_out_time' => 'nullable|string|min:3|max:5',
            'commission' => 'nullable|numeric|min:0|max:100'
        ];

        return array_merge($uniqueRule, $rule);
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();

        //Delete $data['delImages'] and $data['oldImages'] elements with empty values
        $data['delImages'] = array_filter($data['delImages']);
        $data['oldImages'] = array_filter($data['oldImages']);

        CrudHelper::getValidateRules($data, $this->getAdditionalValidateRule())
            ->after(function ($validator) use ($data) {
                //Check for at least one image
                if (empty($data['oldImages']) && empty($data['images']))
                    $validator->errors()->add('images', trans('messages.delImgError'));

                if (isset($data['images']) && (count($data['images']) + count($data['oldImages'])) > GlampingController::getMaxImgCount())
                    $validator->errors()->add('images', trans('messages.maxImgCountError') . GlampingController::getMaxImgCount());

            })->validate();

        if (isset($data['commission']) && !Auth::user()->hasRole('admin'))
            unset($data['commission']);

        $glamping = Glamping::find($id); //get update glamping

        if (is_null($glamping))
            redirect()->back()->withErrors(trans('messages.messages.notExistServiceError'));

        //Check the right to change the glamping
        if (Auth::guest() && !Auth::user()->glampings->contains($glamping->id))
            return redirect()->back()->withErrors([trans('messages.noRightsToThisGlamping')]);

        if (!count($glamping->applications) > 0 || $glamping->lastApplicationStatus() !== ApplicationStatus::PENDING  &&
            $glamping->accepted == false || Auth::user()->hasRole('admin')) {
            $data = $this->getCreateGlampingFinalData($data);
            $result = $glamping->fill($data)->update();

            if (!$result)
                flash(trans('messages.dangerMessage'), 'danger');

            $this->loadEditedGlampingImages($glamping, $data);

            //refresh additional data of glamping
            $this->detachAdditionalData($glamping);
            $this->saveAdditionalData($data, $glamping);
        } else {
            $newGlamping = $this->saveGlamping($data);
            $newGlamping->forceFill(['glamping_id' => $glamping->id])->update();

            if (!$newGlamping)
                flash(trans('messages.dangerMessage'), 'danger');

            $images = $glamping->media()->get();
            $mediaLibrary = $this->getMediaLibraryImageName('Glamping');
            //load old images
            foreach ($images as $image) {
                if (isset($data['oldImages']) && !empty($data['oldImages'])) {
                    if (in_array($image->id, $data['oldImages'])) {
                        $url = public_path() . $image->getUrl();
                        $newGlamping->addMediaFromUrl($url)->toMediaLibrary($mediaLibrary);
                    }
                }
            }
        }

        flash(trans('messages.updateGlampingSuccessMessage'), 'success');

        if (isset($data['btn_with_send_app']) && Auth::user()->hasRole('owner')) {
            $app = new Application;
            $app->forceFill(['type' => ApplicationType::CHANGE]);
            $appResult = $glamping->applications()->save($app);

            if (!$appResult)
                redirect()->back()->withErrors(trans('messages.dangerMessage'));

            return redirect()->route('manager.glampings.index');
        }

        return redirect()->back();
    }

    /**
     * @param $glamping
     */
    private function detachAdditionalData($glamping)
    {
        $glamping->services()->detach();
        $glamping->impressions()->detach();
        $glamping->amenities()->detach();
        $glamping->glampingGroups()->detach();
    }

    /**
     * @param $data
     * @param $model
     */
    private function saveAdditionalData($data, $model)
    {
        if (isset($data['service']))
            $model->services()->attach($data['service']);

        if (isset($data['glamping_group']))
            $model->glampingGroups()->attach($data['glamping_group']);

        if (isset($data['amenity']))
            $model->amenities()->attach($data['amenity']);

        if (isset($data['impression']))
            $model->impressions()->attach($data['impression']);
    }

    /**
     * @param $collection
     * @return array|null
     */
    public static function getAdditionalData($collection)
    {
        $additionalData = null;

        if (!empty($collection)) {
            foreach ($collection as $model) {
                $additionalData[] = $model->name;
            }
        }

        return $additionalData;
    }

    /**
     * @return mixed
     */
    public static function getAllPublishedGlampings()
    {
        //get Glampings Collection, If there are no published objects, then the collection is empty
        $glampings = Glamping::where('active', true)->where('accepted', true)->get();

        if (!count($glampings) > 0) {
            flash(trans('messages.notExistPublishedGlampings'), 'info');
            $glampings = null;
        } else {
            foreach ($glampings as $glamping) {
                $date = date('Y-m-d', time());
                $prices = GlampingController::getGlampingPrices($glamping, $date);
                $glamping->min_price = $prices[0]; //set min price
            }
        }

        return $glampings;
    }

    /**
     * @param Model $glamping
     * @param $fromDate
     * @param null $toDate
     * @return array|null
     */
    public static function getGlampingPrices(Model $glamping, $fromDate, $toDate = null)
    {
        //get all buildings of Glamping
        $buildings = $glamping->buildings()->where('accepted', true)->where('active', true)->get();
        $prices = null;

        if (count($buildings) > 0) {
            foreach ($buildings as $building) {
                //get special prices
                $specialPrice = $building->datePrice($fromDate, $toDate);

                //set prices of all glamping buildings
                if (!sizeof($specialPrice['date_prices'])) {
                    $prices[] = $building->main_price;
                } else {
                    $prices[] = $building->main_price;
                    foreach ($specialPrice['date_prices'] as $date) {
                        $prices[] = $date->price;
                    }
                }
            }

            if (!is_null($prices))
                sort($prices); //sort ascending prices of all glamping buildings
        }

        return $prices;
    }

    /**
     * @param Collection $glampings
     * @param $date
     * @return Collection|null
     */
    private static function setGlampingMinPrice(Collection $glampings, $date)
    {
        foreach ($glampings as $glampingKey => $glamping) {
            //set Glamping min price
            $prices = GlampingController::getGlampingPrices($glamping, $date['from'], $date['to']);

            if (!is_null($prices))
                $glamping->min_price = $prices[0];
            else
                unset($glampings[$glampingKey]);
        }

        if (!count($glampings) > 0)
            $glampings = null;

        return $glampings;
    }

    /**
     * @param $filter
     * $filter is array, contain only only the specified filter values.
     *
     * @return null
     * return null if no glampings found or glampingCollection
     */
    public static function getGlampingFilterResult($filter)
    {
        //get published Glampings, this is result of the first part of the query building
        $glampings = Glamping::where('active', true)->where('accepted', true);

        if (isset($filter['name']) || isset($filter['country']) || isset($filter['region'])) {
            //filter all published glampings by translatable fields
            //$glampings may be null or query builder
            $glampings = GlampingListFilterHelper::filterByTransFields($glampings, $filter);
        }

        if ($glampings !== null) {
            $glampings = $glampings->get();

            //check Glampings Collection
            if (count($glampings) > 0) {
                $date = GlampingListFilterHelper::getFilterDates($filter);
                $glampings = self::setGlampingMinPrice($glampings, $date);

                if (isset($filter['adults']) || isset($filter['buildings']) && !is_null($glampings)) {
                    //$glampings may be null or Glamping collection
                    $glampings = GlampingListFilterHelper::getFilterByAdultsBuildingsCount($date['from'], $glampings, $filter);
                }

                if (isset($filter['building_types']) && !is_null($glampings)) {
                    //$glampings may be null or Glamping collection
                    $glampings = GlampingListFilterHelper::getBuildingTypeFilterResult($glampings, $filter);
                }

                if (isset($filter['glamping_groups']) && !is_null($glampings)) {
                    //$glampings may be null or Glamping collection
                    $glampings = GlampingListFilterHelper::getFilterByAdditionalGlampingDataResult($glampings, $filter['glamping_groups'], true);
                }

                if (isset($filter['impressions']) && !is_null($glampings)) {
                    //$glampings may be null or Glamping collection
                    $glampings = GlampingListFilterHelper::getFilterByAdditionalGlampingDataResult($glampings, $filter['impressions']);
                }

                if (isset($filter['price']) && !is_null($glampings)) {
                    //$glampings may be null or Glamping collection
                    $glampings = GlampingListFilterHelper::getFilterByPrice($glampings, $date, $filter['price']);
                }
            } else {
                $glampings = null;
            }
        }

        return $glampings;
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function getAutocompleteData(Request $request)
    {
        $options['myData'] = [];
        $columns = $request->columns;
        $term = $request->term;

        if (empty(array_diff($columns, ['name', 'country', 'region']))) {
            foreach ($columns as $column) {
                $result = Glamping::where('active', true)->where('accepted', true)
                    ->whereTranslationLike($column, '%' . $term . '%')->get()->pluck($column)->all();

                $data = [];
                foreach (array_unique($result) as $value) {
                    $data[] = ['value' => $value, 'type' => $column];
                }

                $options['myData'] = array_merge($options['myData'], $data);
            }
        }

        return $options;
    }

    /**
     * @param $id
     * @return bool
     * @throws \Exception
     * @throws \Throwable
     */
    public function getSimpleFilterResult($id)
    {
        $filter = array_filter($_POST);
        $date = GlampingListFilterHelper::getFilterDates($filter);

        $glamping = Glamping::find($id);
        if (is_null($glamping) || !$glamping->active || !$glamping->accepted)
            return false;

        $buildings = $glamping->buildings()->where('active', true)->where('accepted',true)->get();
        if (!count($buildings) > 0)
            $glamping = null;

        if (isset($filter['buildings']) && !is_null($glamping)) {
            //get and set free buildings count since $fromDate
            $freeBuildCount = GlampingListFilterHelper::getGlampingFreeBuildingsCount(
                $buildings, $date['from'], intval($filter['buildings']));

            if (is_null($freeBuildCount))
                $glamping = null;
        }

        if (isset($filter['adults']) && !is_null($glamping)) {
            //get glamping adult capacity since $fromDate
            $adults = GlampingListFilterHelper::getGlampingAdults(
                $buildings, $date['from'], intval($filter['adults']));

            //unset glamping if there is no possibility to place the specified number of adults since $fromDate
            if (is_null($adults))
                $glamping = null;
        }

        if (isset($filter['adults']) && isset($filter['buildings'])) {
            $buildingArr = GlampingListFilterHelper::getAllFreeBuildingsData($buildings);
            $c = GlampingListFilterHelper::countGlampingCapacity($buildingArr, $filter['buildings']);

            if ($c < $filter['adults'])
                $glamping = null;
        }

        if (!is_null($glamping) !== null) {
            foreach ($buildings as $buildingKey => $building) {
                if ($building->count() < 1) {
                    unset($buildings[$buildingKey]);
                } else {
                    $building->period_price = $building->getPeriodPrice($date['from'], $date['to']);
                    self::setPeriodOptionPrice($building, $date['from'], $date['to']);
                }
            }

            if (!count($buildings) > 0)
                $glamping = null;
        }

        $filterResult = view('manager.owner.bookings.full_table')->with([
            'glamping' => $glamping,
            'buildings' => $buildings
        ])->render();

        return $filterResult;
    }

    /**
     * @param Model $building
     * @param $fromDate
     * @param $toDate
     * @return Model
     */
    public static function setPeriodOptionPrice(Model $building, $fromDate, $toDate)
    {
        $options = $building->buildingOptions;

        if (count($options) > 0) {
            $daysCount = date_create($fromDate)->diff(date_create($toDate))->days;

            foreach ($options as $option) {
                if ($daysCount !== 0) {
                    $option->period_price = $option->price * $daysCount;
                } else {
                    $option->period_price = $option->price;
                }
            }
        }

        return $building;
    }

    /**
     * Remove the glamping from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * @param $id
     * @return $this
     */
    public function show($id)
    {
        $glamping = Glamping::find($id);

        if (is_null($glamping))
            return redirect()->back()->withErrors(trans('messages.notExistGlampingError'));
        else if (!Auth::user()->glampings->contains($glamping->id) && Auth::user()->hasRole('owner'))
            return redirect()->back()->withErrors(trans('messages.noRightsToThisGlamping'));

        return view('manager.glamping_view')->with([
            'glamping' => $glamping,
            'user' => Auth::guard('manager')->user()
        ]);
    }

    /**
     * @param $buildings
     * @param int $count
     * @param null $today_date
     * @return array
     */
    protected function getDatesInformation($buildings, $count = 31, $today_date = null)
    {
        $dates = [];
        if (is_null($today_date))
            $today_date = date('Y-m-d');
        list($y, $m, $d) = explode("-", $today_date);
        $today_time = mktime(0, 0, 0, $m, $d, $y);

        $sort_times = 1;
        foreach ($buildings as $building) {
            $string = [];
            for ($time = $today_time; $time < $today_time + $count * 24 * 60 * 60; $time += 24 * 60 * 60) {
                $date = date('Y-m-d', $time);
                $sort_time = '00:' . $sort_times;
                if (strlen($sort_time) < 5)
                    $sort_time = '00:0' . $sort_times;
                $prices = $building->datePrice($date);

                if (sizeof($prices['date_prices']))
                    $price = $prices['date_prices'][0]['price'];
                else $price = $prices['main_price'];

                $string[] = [
                    'id' => ($time / (24 * 60 * 60)) . '-' . $building->id,
                    'date' => $date,
                    'count' => $building->count($date),
                    'price' => $price,
                    'time' => $sort_time,
                    'building_id' => $building->id
                ];
            }
            $sort_times++;
            $dates[] = $string;
        }

        return $dates;
    }

    /**
     * @param $id
     * @return $this
     */
    public function showBusynessPage($id)
    {
        $glamping = Glamping::find($id);
        $buildings = $glamping->buildings()->get();

        if (is_null($glamping))
            return redirect()->back()->withErrors(trans('messages.notExistGlampingError'));
        else if (!Auth::user()->glampings->contains($glamping->id))
            return redirect()->back()->withErrors(trans('messages.noRightsToThisGlamping'));

        $dates = $this->getDatesInformation($buildings);
        
        return view('manager.owner.buildings_busyness')->with([
            'glamping' => $glamping,
            'buildings' => $buildings,
            'dates' => $dates,
        ]);
    }

    /**
     * @param Request $request
     * @return int|string
     */
    public function getBuildingsCountsByDates(Request $request)
    {
        $data = $request->all();

        $this->validate($request, [
            'from_date' => 'required|date',
            'count' => 'required|numeric|min:1|max:365',
            'glamping_id' => 'required|numeric'
        ]);

        $user = Auth::guard('manager')->user();
        $glamping = Glamping::find($data['glamping_id']);
        if (is_null($glamping) || is_null($glamping->managers()->where('manager_id', $user->id)))
            return 1;

        $buildings = $glamping->buildings()->get();

        $dates = $this->getDatesInformation($buildings, $data['count'], $data['from_date']);

        return json_encode($dates);
    }

    /**
     * @param Request $request
     * @return int
     */
    public function editBuildingsCount(Request $request)
    {
        $data = $request->all();

        $this->validate($request, [
            'from_date' => 'required|date',
            'to_date' => 'required|date|after_or_equal:' . $data['from_date'],
            'count' => 'required|numeric|min:0',
            'building_id' => 'required|numeric'
        ]);

        $user = Auth::guard('manager')->user();
        $building = Building::find($data['building_id']);
        if (is_null($building))
            return 0;
        $glamping = $building->glamping;
        if (is_null($glamping) || is_null($glamping->managers()->where('manager_id', $user->id)) ||
            is_null($building) || $building->glamping_id != $glamping->id)
            return 0;

        $building_count = $building->buildingCounts()->create([
            'from_date' => $data['from_date'],
            'to_date' => $data['to_date'],
            'count' => $data['count'],
        ]);
        if (is_null($building_count) || !$building_count->cleanBuildingCounts())
            return 0;

        return 1;
    }

    /**
     * @param Request $request
     * @return int
     */
    public function editBuildingsPrice(Request $request)
    {
        $data = $request->all();

        $this->validate($request, [
            'from_date' => 'required|date',
            'to_date' => 'required|date|after_or_equal:' . $data['from_date'],
            'price' => 'required|numeric|min:0',
            'building_id' => 'required|numeric'
        ]);

        $user = Auth::guard('manager')->user();
        $building = Building::find($data['building_id']);
        if (is_null($building))
            return 0;
        $glamping = $building->glamping;
        if (is_null($glamping) || is_null($glamping->managers()->where('manager_id', $user->id)) ||
            is_null($building) || $building->glamping_id != $glamping->id)
            return 0;

        $building_price = BuildingPrice::create([
            'from' => $data['from_date'],
            'to' => $data['to_date'],
            'price' => $data['price'],
        ]);
        $building->prices()->attach($building_price->id);

        if (is_null($building_price) || !$building_price->cleanBuildingPrices())
            return 0;

        return 1;
    }

    /**
     * @param Request $request
     * @param $glampingId
     * @return \Illuminate\View\View
     */
    public function showStatistics(Request $request, $glampingId)
    {
        $glamping = Glamping::find($glampingId);

        if (is_null($glamping))
            return redirect()->back()->withErrors(trans('messages.notExistGlampingError'));
        else if (!Auth::user()->glampings->contains($glamping->id) && Auth::user()->hasRole('owner'))
            return redirect()->back()->withErrors(trans('messages.noRightsToThisGlamping'));

        if (isset($request->from_date) && isset($request->to_date) &&
            $request->from_date < $request->to_date) {
            $fromDate = $request->from_date;
            $toDate = $request->to_date;
        } else {
            $fromDate = date('Y-m-d', time() - 7*24*60*60);
            $toDate = date('Y-m-d', time());
        }

        return view('manager.glamping_statistics')->with([
            'glamping' => $glamping,
            'user' => Auth::user(),
            'fromDate' => $fromDate,
            'toDate' => $toDate
        ]);
    }

    /**
     * @return array with all countries|null
     */
    public static function getGlampingsCountriesList()
    {
        $glampings = GlampingController::getAllPublishedGlampings();

        //get glamping countries
        if (count($glampings) > 0)
            $countries = array_unique($glampings->pluck('country')->all());
        else $countries = null;

        return $countries;
    }
}
