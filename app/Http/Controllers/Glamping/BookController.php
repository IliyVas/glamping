<?php

namespace App\Http\Controllers\Glamping;

use App\Jobs\SendBookingConfirmationEmail;
use App\Models\Booking\BookBuilding;
use App\Models\Booking\Booking;
use App\Models\Booking\BookingStatus;
use App\Models\Glamping\Glamping;
use App\Models\Building\Building;
use App\Models\Auth\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use App\Helpers\ResourceControllerHelper as ResourceHelper;

class BookController extends Controller
{
    const BOOK_VALIDATE_OK = 1;
    const BOOK_VALIDATE_ERROR_SIZES = 0;
    const BOOK_VALIDATE_ERROR_NULL_BUILDING = -1;
    const BOOK_VALIDATE_ERROR_NOT_FROM_THIS_GLAMPING = -2;
    const BOOK_VALIDATE_ERROR_NO_SELECTED_BUILDINGS = -3;
    const BOOK_VALIDATE_ERROR_NULL_GLAMPING = -4;

    /**
     * @return \Illuminate\View\View
     */
    public function index()
    {
        return view('manager.owner.bookings_list')->with([
            'pageName' => trans('booking.gridName'),
            'columns' => ResourceHelper::getColumnGridData(['№', 'name', 'description', 'date',
                'status', 'action-edit-cancel-confirm']),
            'gridData' => self::getOwnerBooking(),
            'editRoute' => 'manager.bookings.edit',
            'cancelRoute' => 'manager.bookings.cancel',
            'confirmRoute' => 'manager.bookings.confirm'
        ]);
    }

    /**
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return $this->renderForm();
    }

    /**
     * Check and send data for the editing form.
     *
     * @param $id
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $booking = Booking::find($id);

        if (is_null($booking))
            return redirect()->back()->withErrors(trans('messages.notExistBookingError'));

        if (Auth::user()->id !== $booking->manager_id)
            return redirect()->back()->withErrors(trans('messages.notEnoughRightsForBooking'));

        return $this->renderForm($booking);
    }

    /**
     * Return create/edit booking form with generated data.
     *
     * @param Booking|null $booking
     * @return \Illuminate\View\View
     */
    public function renderForm(Booking $booking = null)
    {
        $glampings = Auth::user()->glampings()->where('active', true)->where('accepted', true)->get();

        foreach ($glampings as $key => $glamping) {
            $buildings = $glamping->buildings()->where('active', true)->where('accepted', true)->get();

            if ($buildings->isEmpty())
                unset($glampings[$key]);
        }

        return view('manager.owner.booking_form')-> with([
            'glampingList' => $glampings,
            'booking' => $booking,
        ]);
    }

    /**
     * Creates a new booking. If the booking is not created by the glamping owner,
     * generate an information file-pdf and sends it by e-mail.
     *
     * @param Request $request
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $buildingsArr = $this->getBuildingsData($request);
        $glamping = Glamping::find($request->glamping_id);

        //validate booking request data
        if (!Auth::guest() && Auth::guard('manager')->user())
            $this->validateBookingData($this->getBuildingsData($data)['buildings'], $data, $glamping);

        //forms booking data
        $data = $this->getCreateBookData($data, $glamping);
        $data['country_code'] = geoip($_SERVER['REMOTE_ADDR'])->iso_code;
        $data['adults'] = $buildingsArr['adults'];

        //create booking
        $booking = new Booking;
        $booking = $booking->fill($data)->forceFill([
            'glamping_id' => $glamping->id,
            'manager_id' => $data['manager_id'],
            'user_id' => $data['user_id']
        ]);

        $result = $booking->save();
        if (!$result)
            return redirect()->back()->withErrors(trans('messages.dangerMessage'));
        $this->createBookBuilding($booking, $buildingsArr['buildings']);

        flash(trans('messages.createBookingSuccessMessage'), 'success');

        //generate and send the information file-pdf
        if (Auth::guest() || Auth::guard('user')->user()) {
            $pdf_path = $this->generateBookingPDF($request->all(), $glamping, $booking);
            $email = new SendBookingConfirmationEmail($request->all()['email'], $booking, $pdf_path);
            dispatch($email);
        }
        
        return redirect()->route($this->getRedirectRouteAfterCreateBook($booking));
    }

    /**
     * Return a form for entering/confirming customer data.
     * If password entered, the customer will be registered.
     *
     * @param Request $request
     * @return \Illuminate\View\View
     */
    public function showAdditionalInfoForm(Request $request)
    {
        $data = $request->all();

        //validate booking request data
        $this->validateBookingData($this->getBuildingsData($data)['buildings'], $data,
            Glamping::find($request->glamping_id));

        unset($data['_token']);

        return view('user.book_user_info_page')->with([
            'book_data' => json_encode($data),
            'user' => Auth::guard('user')->user()
        ]);
    }

    /**
     * Create booking for users and guests.
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function createBookingFromUserForm(Request $request)
    {
        $data = $request->all();
        $user = User::where('email', $data['email'])->first();

        //validate request data
        $this->bookingFromUserPageFormValidator($data)->after(function ($validator) use ($data, $user) {
            if (isset($data['password']) && !is_null($user))
                $validator->errors()->add('', trans('validation.unique',
                    ['id' => htmlspecialchars('E-mail')]));
            if (!isset($data['book_data']))
                $validator->errors()->add('', trans('messages.dangerMessage'));
        })->validate();

        //get booking data
        $full_data = json_decode($data['book_data'], true);

        //register and login customer if password entered
        if (isset($data['password']) && !is_null($data['password'])) {
            $user = User::create([
                'first_name' => $data['name'],
                'last_name' => $data['surname'],
                'email' => $data['email'],
                'password' => bcrypt($data['password']),
            ]);
            Auth::guard('user')->login($user, true);
        } else {
            $full_data['name'] = $data['name'] . ' ' . $data['surname'];
            $full_data['email'] = $data['email'];
        }

        $request->except(['surname']);

        return $this->createBookFromData($request, $full_data);
    }

    /**
     * Edit existing booking.
     *
     * @param Request $request
     * @param $id
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();
        $buildingsArr = $this->getBuildingsData($request);

        //validate booking data
        $this->validateBookingData($buildingsArr['buildings'], $data, Glamping::find($data['glamping_id']));

        $booking = Booking::find($id);
        //generate create booking data
        $data['from_date'] = date_create($data['from_date'])->Format('Y-m-d');
        $data['to_date'] = date_create($data['to_date'])->Format('Y-m-d');
        $data['adults'] = $buildingsArr['adults'];

        if (!empty($data['user_id']))
            $data['name'] = null;

        unset($data['buildings_id'], $data['buildings_count'], $data['book_id']);

        $result = $booking->fill($data)->forceFill(['glamping_id' => $data['glamping_id']])->update();
        if (!$result)
            return redirect()->back()->withErrors(trans('messages.dangerMessage'));

        //refresh book buildings
        foreach ($booking->bookBuildings as $bookBuilding) {
            $bookBuilding->delete();
        }
        $this->createBookBuilding($booking, $buildingsArr['buildings']);


        flash(trans('messages.updateBookingSuccessMessage'), 'success');

        return redirect()->back();
    }

    /**
     * Change booking status to canceled.
     *
     * @param $id
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function cancel($id)
    {
        $booking = Booking::find($id);

        if (is_null($booking))
            return redirect()->back()->withErrors(trans('messages.notExistBookingError'));

        if (!(Auth::guard('user')->id() === $booking->user_id
            || Auth::guard('manager')->id() === $booking->manager_id))
            return redirect()->back()->withErrors(trans('messages.notEnoughRightsForBooking'));

        $result = $booking->fill(['status' => BookingStatus::CANCELED])->update();

        if ($result)
            flash(trans('messages.cancelBookingSuccessMessage'), 'success');
        else
            flash(trans('messages.dangerMessage'), 'danger');

        if (Auth::guard('user')->check())
            return redirect()->route('user.bookings');
        else return redirect()->back();
    }

    /**
     * @param Request $request
     * @param $data
     * @return mixed
     */
    public function createBookFromData(Request $request, $data)
    {
        foreach ($data as $key => $value) {
            if ($key != '_token') {
                $request->request->add([$key => $value]);
            }
        }

        return $this->store($request);
    }

    /**
     * Change booking status to confirmed.
     *
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function confirm($id)
    {
        $booking = Booking::find($id);

        if (is_null($booking))
            return redirect()->back()->withErrors(trans('messages.notExistBookingError'));

        if (Auth::user()->id !== $booking->manager_id && Auth::user()->id !== $booking->user_id)
            return redirect()->back()->withErrors(trans('messages.notEnoughRightsForBooking'));

        $result = $booking->update(['status' => BookingStatus::CONFIRMED]);

        if ($result) {
            flash(trans('common_elements.ownerBookings') . ' №' . $booking->id . ' ' .
                trans('messages.confirmBookingStatusSuccessMessage'), 'success');
        } else {
            flash(trans('messages.dangerMessage'), 'danger');
        }

        return redirect()->back();
    }

    /**
     * Return owner Bookings Collection.
     * @return mixed
     */
    public static function getOwnerBooking()
    {
        return self::setBookingData(Booking::where('manager_id', Auth::user()->id)->get());
    }

    /**
     * @param $bookings
     * @return mixed
     */
    public static function setBookingData($bookings){
        foreach ($bookings as $booking) {
            $buildingsCollections = $booking->bookBuildings()->get();
            $booking->description = BookController::getBookingDescriptions($buildingsCollections);
            $booking->date = BookController::getFormattedDate($booking);
            $booking->status = BookController::getBookingStatusName($booking->status);
            BookController::getClientName($booking);
        }

        return $bookings;
    }

    /**
     * Creates array with data of buildings prices from date to date.
     *
     * @param int
     * @param DateTime
     * @param DateTime|null
     *
     * @return string
     */
    public function getBuildingsPrices($glamping_id, $from_date, $to_date = null)
    {
        if (is_null($to_date))
            $to_date = $from_date;

        $glamping = Glamping::find($glamping_id);

        $from_date = \DateTime::createFromFormat('Y-m-d', $from_date);
        $to_date = \DateTime::createFromFormat('Y-m-d', $to_date);
        if ($from_date > $to_date || is_null($glamping))
            return '';

        $prices_arr = [];

        foreach ($glamping->buildings as $building) {
            $new_element = [
                'id' => $building->id,
                'prices' => $building->datePrice($from_date, $to_date),
            ];
            //convert price models to prices
            foreach ($new_element['prices']['date_prices'] as $key => $value) {
                $price = [
                    'price' => ($value->price),
                    'from' => $value->from,
                    'to' => $value->to,
                ];
                $new_element['prices'][1][$key] = $price;
            }
            $prices_arr[] = $new_element;
        }

        $translations = [ 'standard' => trans('common_elements.bookTableStandard') ];

        return json_encode([
                'info' => $prices_arr,
                'translations' => $translations]
        );
    }

    /**
     * @param $id
     * @return bool
     * @throws \Exception
     * @throws \Throwable
     */
    public function getSelectedGlampingData($id)
    {
        $glamping = Auth::user()->glampings()->find($id);
        if (is_null($glamping))
            return redirect()->back()->withErrors(trans('messages.notExistGlampingError'));

        $bookBuildings = null;
        $buildings = $glamping->buildings()->where('active', true)->where('accepted', true)->get();

        if (isset($_GET['book_id']) && $_GET['book_id'] !== "0") {
            $bookId = $_GET['book_id'];
            $booking = Booking::find($bookId);

            if (!$booking)
                return false;

            $bookBuildings = $booking->bookBuildings;
        }

        if (isset($_GET['from_date']))
            $fromDate = date_create($_GET['from_date'])->format('Y-m-d');
        else
            $fromDate = date('Y-m-d', time());

        if (isset($_GET['to_date']))
            $toDate = date_create($_GET['to_date'])->format('Y-m-d');
        else
            $toDate = date('Y-m-d', time() + (24*60*60));

        if (count($buildings) > 0) {
            foreach ($buildings as $building) {
                $building->period_price = $building->getPeriodPrice($fromDate, $toDate);
                GlampingController::setPeriodOptionPrice($building, $fromDate, $toDate);
            }
        }

        if (!$glamping)
            return false;

        return view('manager.owner.bookings.building_grid_data')
            ->with(['buildings' => $buildings, 'bookBuildings' => $bookBuildings])->render();
    }

    /**
     * Set the customer's name in the booking
     *
     * @param $booking
     * @return mixed
     */
    public static function getClientName($booking)
    {
        if (!empty($booking->user_id) && empty($booking->name)) {
            $user = User::find($booking->user_id);
            $booking->name = $user->first_name . ' ' . $user->last_name;
        }

        return $booking;
    }

    /**
     * @param $status
     * @return \Illuminate\Contracts\Translation\Translator|string
     */
    public static function getBookingStatusName($status)
    {
        if ($status == BookingStatus::NOT_CONFIRMED) {
            $status = trans('booking.notConfirmedStatus');
        } elseif ($status == BookingStatus::CONFIRMED) {
            $status = trans('booking.confirmedStatus');
        } elseif ($status == BookingStatus::CANCELED) {
            $status = trans('booking.canceledStatus');
        }

        return $status;
    }

    /**
     * @param $data
     * @param $glamping
     * @param $booking
     *
     * @return string $pdf_path
     */
    public function generateBookingPDF($data, $glamping, $booking)
    {
        $pdf_text =  view('templates.booking_pdf')->with([
            'data' => $data,
            'glamping' => $glamping,
            'booking' => $booking,
        ])->render();

        $pdf = \App::make('dompdf.wrapper');
        $pdf->loadHTML($pdf_text);

        $file_name = $booking->id . '-pdf.pdf';
        $dir_to_save = public_path() . '/pdf/' . $glamping->id . '/';
        if (!file_exists($dir_to_save))
            mkdir($dir_to_save);
        $pdf_path = $dir_to_save . $file_name;
        file_put_contents($pdf_path, $pdf->output());

        return $pdf_path;
    }

    /**
     * @param $booking
     * @param $buildings
     * @return $this
     */
    private function createBookBuilding($booking, $buildings)
    {
        foreach ($buildings as $building) {
            if ($building['count']) {
                $bookBuilding = new BookBuilding;
                $result =  $bookBuilding->forceFill([
                    'booking_id' => $booking->id,
                    'building_id' => $building['entity']->id,
                    'count' => $building['count']
                ])->save();

                if (!$result)
                    return redirect()->back()->withErrors(trans('messages.dangerMessage'));
            }
        }
    }

    /**
     * @param $buildings
     * @param $data
     * @param $glamping
     */
    private function validateBookingData($buildings, $data, $glamping)
    {
        $status = $this->getCustomValidateStatus($buildings, $data, $glamping);
        $this->bookingFormValidator($data)->after(function ($validator) use ($status) {
            //escape if status is not ok
            $bookErrorMessages = $this->getBookErrorMessages();
            if ($status !== self::BOOK_VALIDATE_OK) {
                $validator->errors()->add('', $bookErrorMessages[$status]);
            }
        })->validate();
    }

    /**
     * @param $data
     * @param $glamping
     * @return mixed
     */
    private function getCreateBookData($data, $glamping)
    {
        $data['user_id'] = null;
        //get user_id and name values
        if (Auth::guard('user')->check()) {
            $userId = Auth::guard('user')->user()->id;
            if (!Auth::guest() && !Auth::user()->glampings->contains($glamping->id)) {
                $data['user_id'] = $userId;
                $data['name'] = null;
            } elseif(isset($data['name'])) {
                $data['user_id'] = null;
            } else {
                $data['user_id'] = $userId;
            }
        }

        $data['manager_id'] = $glamping->managers->first()->id;

        if (!isset($data['children']))
            $data['children'] = 0;

        $data['from_date'] =  date_create($data['from_date'])->format('Y-m-d');
        $data['to_date'] =  date_create($data['to_date'])->format('Y-m-d');

        return $data;
    }

    /**
     * @param $booking
     * @return string
     */
    private function getRedirectRouteAfterCreateBook($booking)
    {
        $route = 'homepage';

        if (Auth::guard('user')->user())
            $route = 'user.dashboard';
        elseif (Auth::guard('manager')->user())
            $route = 'manager.bookings.index';

        return $route;
    }

    /**
     * @param $data
     * @return mixed
     */
    private function bookingFromUserPageFormValidator($data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'surname' => 'required|string|max:255',
            'email' => 'required|string|max:255',
            'password' => 'nullable|string',
        ]);
    }

    /**
     * @param $data
     * @return mixed
     */
    private function getBuildingsData($data)
    {
        $adults = 0; // it is sum of counts of building * capacity
        $buildings_arr = [];
        foreach ($data['buildings_id'] as $key => $building_id) {
            $building = [
                'entity' => Building::find($building_id),
                'count' => $data['buildings_count'][$key],
            ];
            $buildings_arr[] = $building;
            $adults += $building['entity']->capacity * $building['count'];
        }
        $buildingsData['buildings'] = $buildings_arr;
        $buildingsData['adults'] = $adults;

        return $buildingsData;
    }

    /**
     * @param $data
     * @return mixed
     */
    private function bookingFormValidator($data)
    {
        return Validator::make($data, [
            'glamping_id' => 'required|numeric|min:1',
            'from_date' => 'required|date|after_or_equal:today',
            'to_date' => 'required|date|after_or_equal:' . $data['from_date'],
            'buildings_id' => 'required',
            'buildings_id.*' => 'numeric|min:0',
            'buildings_count' => 'required',
            'buildings_count.*' => 'numeric|min:0',
        ]);
    }

    /**
     * @return array
     */
    private function getBookErrorMessages()
    {
        //messages that will be shown if custom validator will return error
        $bookErrorMessages = [
            BookController::BOOK_VALIDATE_ERROR_SIZES =>
                trans('messages.dangerMessage'),
            BookController::BOOK_VALIDATE_ERROR_NULL_BUILDING =>
                trans('messages.dangerMessage'),
            BookController::BOOK_VALIDATE_ERROR_NOT_FROM_THIS_GLAMPING =>
                trans('messages.dangerMessage'),
            BookController::BOOK_VALIDATE_ERROR_NO_SELECTED_BUILDINGS =>
                trans('messages.noSelectedBuildings'),
            BookController::BOOK_VALIDATE_ERROR_NULL_GLAMPING =>
                trans('messages.notExistGlampingError'),
        ];

        return $bookErrorMessages;
    }

    /**
     * @param $buildings
     * @param $data
     * @param $glamping
     * @return int
     */
    private function getCustomValidateStatus($buildings, $data, $glamping)
    {
        //check if user broke inputs
        $status = $this->customBuildingsValidate($buildings, $data['glamping_id']);
        //check if count of IDs and counts is equal
        if (sizeof($data['buildings_id']) != sizeof($data['buildings_count'])) {
            $status = BookController::BOOK_VALIDATE_ERROR_SIZES;
        }
        //check if glamping is ok
        if (is_null($glamping)) {
            $status = BookController::BOOK_VALIDATE_ERROR_NULL_GLAMPING;
        }

        return $status;
    }

    /**
     * @param array $buildings
     * @param int $glamping_id
     * @return int
     */
    private static function customBuildingsValidate($buildings, $glamping_id)
    {
        $status = BookController::BOOK_VALIDATE_OK;
        $count = 0;

        foreach ($buildings as $building) {
            if (is_null($building['entity'])) {
                $status = BookController::BOOK_VALIDATE_ERROR_NULL_BUILDING;
            } elseif ($building['entity']->glamping_id != $glamping_id) {
                $status = BookController::BOOK_VALIDATE_ERROR_NOT_FROM_THIS_GLAMPING;
            }
            $count += $building['count'];
        }

        if ($count <= 0) {
            $status = BookController::BOOK_VALIDATE_ERROR_NO_SELECTED_BUILDINGS;
        }

        return $status;
    }

    /**
     * @param $bookingModel
     * @return string
     */
    private static function getFormattedDate($bookingModel)
    {
        return '<strong>'. trans('common_elements.wordFrom') . ' </strong>' .
        date_create($bookingModel->from_date)->Format('d.m.Y') . '</br><strong>' .
        trans('common_elements.wordTo') . ' </strong>' . date_create($bookingModel->to_date)->Format('d.m.Y');
    }

    /**
     * @param $buildingsCollections
     * @return string
     */
    private static function getBookingDescriptions($buildingsCollections)
    {
        $buildingName = [];
        $glampingName = null;

        foreach ($buildingsCollections as $bookBuildingModel) {
            $building = $bookBuildingModel->building;
            $glampingName = $building->glamping->name;
            $buildingName[] = $building->name;
        }

        $title = trans('booking.building');
        if (count($buildingName) > 1)
            $title = trans('common_elements.buildings');

        $glamping = '<strong>' . trans('common_elements.glamping') . ': </strong>' . $glampingName . '</br>';
        $building = '<strong>' . $title . ': </strong>' . implode($buildingName, ', ');

        return   $glamping . $building;
    }
}
