<?php

namespace App\Http\Controllers\Glamping;

use App\Models\Glamping\Amenity;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Glamping\Helpers\CrudHelper;
use App\Helpers\ResourceControllerHelper as ResourceHelper;

class AmenityController extends Controller
{
    /**
     * @var string
     */
    protected $modelName = 'Amenity';

    /**
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $columns = ['fontIcon', 'name', 'description', 'action-edit-delete'];
        $gridData = ResourceHelper::getGridData($columns, Amenity::all(), $this->modelName);

        return view('manager.abstract_model.grid')->with($gridData);
    }

    /**
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return $this->renderForm();
    }

    /**
     * @param $id
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $amenity = Amenity::find($id);

        if (!isset($amenity)) {
            return redirect()->back()->withErrors(trans('messages.notExistAmenityError'));
        }

        return $this->renderForm($amenity);
    }

    /**
     * @param Amenity|null $amenity
     * @return \Illuminate\View\View
     */
    private function renderForm(Amenity $amenity = null)
    {
        $fieldsNames = ['fontIcon', 'submit'];

        return view('manager.abstract_model.form')
            ->with(ResourceHelper::getFormData($amenity, $fieldsNames, $this->modelName));
    }

    /**
     * @param Request $request
     * @return $this
     */
    public function store(Request $request)
    {
        $additionalRule = ['icon' => 'required|string'];
        CrudHelper::getValidateRules($request->all(), $additionalRule)->validate();

        $amenityModel = new Amenity;
        $result = $amenityModel->fill(CrudHelper::getFinalInputData($request->all()))->save();

        if ($result) {
            flash(trans('messages.createAmenitySuccessMessage'), 'success');
        } else {
            flash(trans('messages.dangerMessage'), 'danger');
        }

        return redirect()->back();
    }

    /**
     * @param Request $request
     * @param $id
     * @return $this
     */
    public function update(Request $request, $id)
    {
        $additionalRule = ['icon' => 'required|string'];
        CrudHelper::getValidateRules($request->all(), $additionalRule)->validate();

        $amenityModel = Amenity::where('id', $id)->first();
        $result = $amenityModel->fill($request->all())->update();

        if ($result) {
            flash(trans('messages.updateAmenitySuccessMessage'), 'success');
        } else {
            flash(trans('messages.dangerMessage'), 'danger');
        }

        return redirect()->back();
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        $result = CrudHelper::delete(Amenity::find($id));

        if ($result) {
            flash(trans('messages.deleteAmenitySuccessMessage'), 'success');
        } else {
            flash(trans('messages.dangerMessage'), 'danger');
        }

        return redirect()->back();
    }
}
