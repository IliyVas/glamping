<?php

namespace App\Http\Controllers\Glamping;

use App\Http\Controllers\Glamping\Helpers\CrudHelper;
use App\Models\Building\Building;
use App\Models\Building\BuildingType;
use App\Models\Communication\ApplicationStatus;
use App\Models\Communication\ApplicationType;
use App\Models\Glamping\Amenity;
use App\Models\Glamping\Glamping;
use App\Models\Communication\Application;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Collection;
use App\Traits\MediaUploader;
use Illuminate\Http\UploadedFile;

class BuildingController extends Controller
{
    use MediaUploader;

    const DEFAULT_ROLE_MAX_IMAGE_COUNT = 5;
    const ADMIN_ROLE_MAX_IMAGE_COUNT = 15;
    const OWNER_ROLE_MAX_IMAGE_COUNT = 10;

    /**
     * @param $glampingId
     * @return $this
     */
    public function show($glampingId)
    {
        $glamping = Glamping::find($glampingId);

        if (is_null($glamping))
            return redirect()->back()->withErrors(trans('messages.notExistGlampingError'));

        if (!Auth::user()->glampings->contains($glamping->id) && Auth::guard('manager')->user()->hasRole('owner'))
            return redirect()->back()->withErrors(trans('messages.noRightsToThisGlamping'));

        return view('manager.buildings_list')->with([
            'glamping' => $glamping,
            'user' => Auth::guard('manager')->user()
        ]);
    }

    /**
     * @param $glampingId
     * @return \Illuminate\View\View
     */
    public function create($glampingId)
    {
        if (!Auth::user()->hasRole('owner'))
            return redirect()->back()->withErrors(trans('messages.dangerMessage'));

        return $this->renderForm(Glamping::find($glampingId));
    }

    /**
     * @param $buildingId
     * @return \Illuminate\View\View
     */
    public function edit($buildingId)
    {
        $building = Building::find($buildingId);
        if (is_null($building))
            return redirect()->back()->withErrors(trans('messages.notExistBuilding'));

        return $this->renderForm(Glamping::find($building->glamping_id), $building);
    }

    /**
     * @param Building|null $building
     * @param Glamping|null $glamping
     * @return \Illuminate\View\View
     */
    private function renderForm(Glamping $glamping = null, Building $building = null)
    {
        if (is_null($glamping))
            return redirect()->back()->withErrors(trans('messages.notExistGlampingError'));

        if (Auth::guest() || !Auth::user()->glampings->contains($glamping->id) &&
            Auth::user()->hasRole('owner'))
        return redirect()->back()->withErrors(trans('messages.noRightsToThisGlamping'));

        if (count($glamping->applications) > 0 &&
            $glamping->lastApplicationStatus() === ApplicationStatus::PENDING &&
            Auth::user()->hasRole('owner'))
            return redirect()->back()->withErrors(trans('messages.noRightsToEditThisGlamping'));

        return view('manager.building_form')->with([
            'existBuilding' => $building,
            'glamping' => $glamping,
            'amenityList' => Amenity::all(),
            'buildingTypeList' => BuildingType::all(),
            'maxImgCount' => self::getMaxImgCount(),
            'user' => Auth::guard('manager')->user()
        ]);
    }

    /**
     * @param $data
     * @return array
     */
    private function getBuildingOptionValidateRule($data)
    {
        $rule = [];

        if (!empty($data['option_price'][0]) || !empty($data['option_name:en'][0]) ||
            !empty($data['option_name:ru'][0]) || !empty($data['option_description:en'][0]) ||
            !empty($data['option_description:ru'][0])) {

            $requiredLanguage = config('translatable.fallback_locale');
            $additionalLanguage = config('translatable.additional_locale');

            $rule = [
                'option_price.*' =>  'required|numeric|min:0',
                'option_name:' . $requiredLanguage . '.*' => 'required|string|min:1|max:255',
                'option_description:' . $requiredLanguage . '.*' => 'required|string|min:1|max:500',
            ];

            $names = array_filter($data['option_name:' . $additionalLanguage]);
            $descriptions = array_filter($data['option_description:' . $additionalLanguage]);

            if (!empty($names) || !empty($descriptions)) {
                $rule = array_merge($rule, [
                    'option_name:' . $additionalLanguage . '.*' => 'required|string|min:1|max:255',
                    'option_description:' . $additionalLanguage . '.*' => 'required|string|min:1|max:500',
                ]);
            }
        }

        return $rule;
    }

    /**
     * @param Request $request
     * @return $this
     */
    public function store(Request $request)
    {
        if (!Auth::user()->hasRole('owner'))
            return redirect()->back()->withErrors(trans('messages.dangerMessage'));

        //check if some type of images are available
        if (sizeof($request->images) + sizeof($request->glamping_images) == 0)
            return redirect()->back()->withErrors(trans('messages.noImagesFound'));

        $glamping = Glamping::find($request->glamping_id);
        $this->saveBuilding($request, $glamping);

        flash(trans('messages.createBuildingSuccessMessage'), 'success');

        if (isset($request->btn_with_send_app)) {
            if ($glamping->accepted) {
                $app = new Application;
                $app->forceFill(['type' => ApplicationType::CHANGE]);
                $appResult = $glamping->applications()->save($app);
            }else {
                $app = new Application;
                $app->forceFill(['type' => ApplicationType::CREATE]);
                $appResult = $glamping->applications()->save($app);
            }

            if (!$appResult)
                redirect()->back()->withErrors(trans('messages.dangerMessage'));

            return redirect()->route('manager.glampings.index');
        }

        return redirect()->back();
    }

    /**
     * @param $request
     * @param $building
     * @return $this|bool
     */
    private function loadBuildingImages($request, $building)
    {
        //load usual images
        if (count($request->images) > 0) {
            foreach ($request->images as $image) {
                $url = public_path('tmp') . '/' . Auth::user()->id . '/' . $image;
                $mediaLibrary = $this->getMediaLibraryImageName('Building');
                $building->addMediaFromUrl($url)->toMediaLibrary($mediaLibrary);
            }
        }
        //load images that already exist in glampings
        if ($request->has('glamping_images')) {
            $this->uploadExistingImages($building->glamping, $building, $request['glamping_images']);
        }

        $this->deleteTemporaryMediaDirectory(public_path('tmp/' . Auth::user()->id));

        return true;
    }

    /**
     * @param $building
     * @return $this|bool|mixed
     */
    private function deleteBuildingOptions($building)
    {
        $result = true;

        if (count($building->buildingOptions) > 0) {
            foreach ($building->buildingOptions as $option) {
                $result = CrudHelper::delete($option);

                if (!$result)
                    return redirect()->back()->withErrors(trans('messages.dangerMessage'));
            }
        }

        return $result;
    }

    /**
     * @param $data
     * @param $building
     * @return $this|bool
     */
    private function saveBuildingOptions($data, $building)
    {
        foreach ($data['option_price'] as $key => $value) {
            $optionData['price'] = $value;
            $optionData['name:en'] = $data['option_name:en'][$key];
            $optionData['description:en'] = $data['option_description:en'][$key];
            $optionData['name:ru'] = $data['option_name:ru'][$key];
            $optionData['description:ru'] = $data['option_description:ru'][$key];

            $options[] = array_filter($optionData);
        }

        $result = true;
        if (isset($options)) {
            foreach ($options as $option) {
                $result = $building->buildingOptions()->create($option);

                if (!$result)
                    return redirect()->back()->withErrors(trans('messages.dangerMessage'));
            }
        }

        return $result;
    }

    /**
     * @param $data
     * @param $glamping
     * @return $this
     */
    private function validateBuildingData($data, $glamping)
    {
        //validate $data
        $buildingOptionsRules = $this->getBuildingOptionValidateRule($data);
        $additionalRule = array_merge($buildingOptionsRules, $this->getAdditionalValidateRules());

        CrudHelper::getValidateRules($data, $additionalRule)
            ->after(function ($validator) use ($data) {
                $maxImgCount = self::getMaxImgCount();
                if (isset($data['images']) && count($data['images']) > $maxImgCount ||
                    isset($data['glamping_images']) && count($data['glamping_images']) > $maxImgCount ||
                    isset($data['images']) && isset($data['glamping_images']) &&
                    (count($data['glamping_images']) + count($data['images'])) > $maxImgCount) {
                    $validator->errors()->add('images', trans('messages.maxImgCountError') . $maxImgCount);
                }
            })->validate();

        if (is_null($glamping))
            redirect()->back()->withErrors(trans('messages.messages.notExistServiceError'));

        if (Auth::guest() || !Auth::user()->glampings->contains($glamping->id))
            return redirect()->back()->withErrors(trans('messages.noRightsToThisGlamping'));

        if (count($glamping->applications) > 0 &&
            $glamping->lastApplicationStatus() === ApplicationStatus::PENDING)
            return redirect()->back()->withErrors(trans('messages.noRightsToEditThisBuilding'));
    }

    /**
     * @param $request
     * @param $glamping
     * @return $this
     */
    private function saveBuilding($request, $glamping)
    {
        $data = $request->all();
        $this->validateBuildingData($data, $glamping);
        $data = CrudHelper::getFinalInputData($data);

        //create building
        $building = $glamping->buildings()->create($data);
        if (!$building)
            redirect()->back()->withErrors(trans('messages.dangerMessage'));

        if (!empty($this->getBuildingOptionValidateRule($data)))
            $this->saveBuildingOptions($data, $building);

        $this->loadBuildingImages($request, $building);
        $this->saveAdditionalData($data, $building);

        return $building;
    }

    /**
     * @param Request $request
     * @param $id
     * @return $this
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();
        $glamping = Glamping::find($data['glamping_id']);
        $buildingModel = Building::find($id);

        if (!$buildingModel)
            return redirect()->back()->withErrors(trans('messages.notExistBuilding'));

        if ($glamping->id !== $buildingModel->glamping_id)
            return redirect()->back()->withErrors(trans('messages.wrongGlampingOfBuilding'));

        if (Auth::user()->hasRole('admin') || !count($glamping->applications) > 0 &&
            $buildingModel->accepted == false || count($glamping->applications) > 0 &&
            $glamping->lastApplicationStatus() !== ApplicationStatus::PENDING  &&
            $buildingModel->accepted == false) {
            $data = CrudHelper::getFinalInputData($data);
            $result = $buildingModel->fill($data)->update();

            if (!$result)
                redirect()->back()->withErrors(trans('messages.dangerMessage'));

            //refresh building options
            $this->deleteBuildingOptions($buildingModel);
            if (!empty($this->getBuildingOptionValidateRule($data)))
                $this->saveBuildingOptions($data, $buildingModel);

            $this->loadBuildingImages($request, $buildingModel);

            //refresh additional data
            $this->detachAdditionalData($buildingModel);
            $this->saveAdditionalData($data, $buildingModel);
        } else {
            $newBuilding = $this->saveBuilding($request, $glamping);
            $newBuilding->forceFill(['building_id' => $buildingModel->id])->update();

            $images = $buildingModel->media()->get();
            if (count($images) > 0) {
                foreach ($images as $image) {
                    $url = public_path() . $image->getUrl();
                    $mediaLibrary = $this->getMediaLibraryImageName('Building');
                    $newBuilding->addMediaFromUrl($url)->toMediaLibrary($mediaLibrary);
                }
            }
        }

        flash(trans('messages.editBuildingSuccessMessage'), 'success');

        if (isset($data['btn_with_send_app'])) {
            if ($glamping->accepted) {
                $app = new Application;
                $app->forceFill(['type' => ApplicationType::CHANGE]);
                $appResult = $glamping->applications()->save($app);
            }else {
                $app = new Application;
                $app->forceFill(['type' => ApplicationType::CREATE]);
                $appResult = $glamping->applications()->save($app);
            }

            if (!$appResult)
                redirect()->back()->withErrors(trans('messages.dangerMessage'));

            return redirect()->route('manager.buildings.show', [$glamping->id]);
        }

        return redirect()->back();
    }

    /**
     * @return int
     */
    public static function getMaxImgCount()
    {
        /** @var Collection $roleCollection */
        $roleCollection = Auth::user()->roles;
        $role = $roleCollection->first();

        $maxImgCount = self::DEFAULT_ROLE_MAX_IMAGE_COUNT;
        if ($role->name == 'admin') {
            $maxImgCount = self::ADMIN_ROLE_MAX_IMAGE_COUNT;
        } elseif ($role->name == 'owner') {
            $maxImgCount = self::OWNER_ROLE_MAX_IMAGE_COUNT;
        }

        return $maxImgCount;
    }

    /**
     * @return array
     */
    private function getAdditionalValidateRules()
    {
        return $additionalRule = [
            'capacity' => 'required|numeric|max:255|min:0',
            'count' => 'required|numeric|max:255|min:0',
            'min_nights_count' => 'nullable|numeric|max:40|min:0',
            'main_price' => 'required|numeric|min:0',
            'price_dates.*' => 'required|numeric|min:0',
            'from_dates.*' => 'required|date',
            'to_dates.*' => 'required|date',
            'building_type_id' => 'required|integer',
        ];
    }

    /**
     * @param $error_code
     * @return string
     */
    private function getPriceErrorMessage($error_code)
    {
        $price_error_messages = [
            BuildingPriceController::DATES_ERROR_OVERLAY =>
                trans('messages.priceDatesImposed'),
            BuildingPriceController::DATES_ERROR_NEGATIVE_PRICE =>
                trans('messages.priceDatesWrongPrice'),
            BuildingPriceController::DATES_ERROR_BACKWARDS =>
                trans('messages.priceDatesBroken'),
        ];

        if (array_key_exists($error_code, $price_error_messages)) {
            ($price_error_messages[$error_code]);
        }

        return (trans('messages.dangerMessage'));
    }

    /**
     * @param $glamping
     * @param $building
     * @param $glamping_images
     */
    private function uploadExistingImages($glamping, $building, $glamping_images)
    {
        $media = $glamping->media()->get();
        foreach ($glamping_images as $i) {
            $image = $media[$i];
            $file_path = $image->getPath();
            $dot_pos = strripos($file_path, '.');
            if ($dot_pos) {
                $new_file_path = substr_replace($file_path, '-c.', $dot_pos, 1);
                copy($file_path, $new_file_path);
                $file_name = $image->file_name;
                $file_info = new \finfo(FILEINFO_MIME_TYPE);
                $exist_file = new UploadedFile(
                    $new_file_path,
                    $file_name,
                    $file_info->file($file_path),
                    filesize($file_path),
                    0,
                    false
                );
                $name = CrudHelper::transliterate($exist_file->getClientOriginalName());
                $this->uploadImage($building, $exist_file, 'Building', $name);
            }
        }
    }

    /**
     * @param $model
     */
    private function detachAdditionalData($model)
    {
        $model->amenities()->detach();
        $model->prices()->detach();
    }

    /**
     * @param $data
     * @param $model
     */
    private function saveAdditionalData($data, $model)
    {
        if (isset($data['amenity'])) {
            $model->amenities()->attach($data['amenity']);
        }
    }

    /**
     * @param $glampingId
     * @param $buildingId
     * @return $this
     */
    public function showBuilding($glampingId, $buildingId)
    {
        $glamping = Glamping::where('id', $glampingId)->first();

        if (is_null($glamping))
            return redirect()->back()->withErrors(trans('messages.notExistGlamping'));
        else if (!Auth::user()->glampings->contains($glamping->id) && Auth::user()->hasRole('owner'))
            return redirect()->back()->withErrors(trans('messages.noRightsToThisGlamping'));

        $building = Building::find($buildingId);
        if (is_null($building))
            return redirect()->back()->withErrors(trans('messages.notExistBuilding'));
        else if ($building->glamping_id != $glamping->id)
            return redirect()->back()->withErrors(trans('messages.wrongGlampingOfBuilding'));

        return view('manager.building_view')->with([
            'glamping' => $glamping,
            'building' => $building,
            'user' => Auth::guard('manager')->user()
        ]);
    }
}
