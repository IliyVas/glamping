<?php

namespace App\Http\Controllers\Glamping;

use Illuminate\Http\Request;
use App\Models\Building\BuildingType;
use App\Http\Controllers\Controller;
use App\Traits\MediaUploader;
use App\Http\Controllers\Glamping\Helpers\CrudHelper;
use App\Helpers\ResourceControllerHelper as ResourceHelper;

class BuildingTypeController extends Controller
{
    use MediaUploader;

    /**
     * @var string
     */
    protected $modelName = 'BuildingType';

    /**
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $columns = ['icon', 'name', 'description', 'action-edit-delete'];
        $gridData = ResourceHelper::getGridData($columns, BuildingType::all(), $this->modelName);

        return view('manager.abstract_model.grid')->with($gridData);
    }

    /**
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return $this->renderForm();
    }

    /**
     * @param $id
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $buildingType = BuildingType::find($id);

        if (!isset($buildingType)) {
            return redirect()->back()->withErrors(trans('messages.notExistBuildingTypeError'));
        }

        return $this->renderForm($buildingType);
    }

    /**
     * @param BuildingType|null $buildingType
     * @return \Illuminate\View\View
     */
    private function renderForm(BuildingType $buildingType = null)
    {
        $fieldsNames = ['icon', 'image', 'submit'];

        return view('manager.abstract_model.form')
            ->with(ResourceHelper::getFormData($buildingType, $fieldsNames, $this->modelName));
    }

    /**
     * @param Request $request
     * @return $this
     */
    public function store(Request $request)
    {
        $additionalRule = [
            'image' => 'required|mimes:jpeg,png',
            'icon' => 'required|mimes:jpeg,png',
        ];
        CrudHelper::getValidateRules($request->all(), $additionalRule)->validate();

        $buildingTypeModel = new BuildingType;
        $buildingTypeModel->fill(CrudHelper::getFinalInputData($request->all()))->save();

        $iconName = CrudHelper::transliterate($request->icon->getClientOriginalName());
        $imgName = CrudHelper::transliterate($request->image->getClientOriginalName());
        $iconUrl = $this->uploadIcon($buildingTypeModel, $request->icon, $this->modelName, $iconName)->getUrl();
        $imageUrl = $this->uploadImage($buildingTypeModel, $request->image, $this->modelName, $imgName)->getUrl();

        $result = $buildingTypeModel->update(['icon' => $iconUrl, 'image' => $imageUrl]);

        if ($result) {
            flash(trans('messages.createBuildingTypeSuccessMessage'), 'success');
        } else {
            flash(trans('messages.dangerMessage'), 'danger');
        }

        return redirect()->back();
    }

    /**
     * @param Request $request
     * @param $id
     * @return $this
     */
    public function update(Request $request, $id)
    {
        CrudHelper::getValidateRules($request->all())->validate();

        $buildingTypeModel = BuildingType::find($id);
        $data = $request->all();

        if (!empty($data['icon'])) {
            $iconName = CrudHelper::transliterate($request->icon->getClientOriginalName());
            $iconUrl = $this->uploadIcon($buildingTypeModel, $request->icon, $this->modelName, $iconName)->getUrl();
        } else {
            $iconUrl = $buildingTypeModel->icon;
        }

        if (!empty($data['image'])) {
            $imgName = CrudHelper::transliterate($request->image->getClientOriginalName());
            $imageUrl = $this->uploadImage($buildingTypeModel, $request->image, $this->modelName, $imgName)->getUrl();
        } else {
            $imageUrl = $buildingTypeModel->image;
        }

        $data = CrudHelper::getFinalInputData($data);
        $data['icon'] = $iconUrl;
        $data['image'] = $imageUrl;
        $result = $buildingTypeModel->fill($data)->update();

        if ($result) {
            flash(trans('messages.updateBuildingTypeSuccessMessage'), 'success');
        } else {
            flash(trans('messages.dangerMessage'), 'danger');
        }

        return redirect()->back();
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        $result = CrudHelper::delete(BuildingType::find($id));

        if ($result) {
            flash(trans('messages.deleteBuildingTypeSuccessMessage'), 'success');
        } else {
            flash(trans('messages.dangerMessage'), 'danger');
        }

        return redirect()->back();
    }
}
