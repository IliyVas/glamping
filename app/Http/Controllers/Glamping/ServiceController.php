<?php

namespace App\Http\Controllers\Glamping;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Glamping\Service;
use App\Http\Controllers\Glamping\Helpers\CrudHelper;
use App\Helpers\ResourceControllerHelper as ResourceHelper;

class ServiceController extends Controller
{
    /**
     * @var string
     */
    protected $modelName = 'Service';

    /**
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $columns = ['fontIcon', 'name', 'description', 'action-edit-delete'];
        $gridData = ResourceHelper::getGridData($columns, Service::all(), $this->modelName);

        return view('manager.abstract_model.grid')->with($gridData);
    }

    /**
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return $this->renderForm();
    }

    /**
     * @param $id
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $service = Service::find($id);

        if (!isset($service)) {
            return redirect()->back()->withErrors(trans('messages.notExistServiceError'));
        }

        return $this->renderForm($service);
    }

    /**
     * @param Service|null $service
     * @return \Illuminate\View\View
     */
    private function renderForm(Service $service = null)
    {
        $fieldsNames = ['fontIcon', 'submit'];

        return view('manager.abstract_model.form')
            ->with(ResourceHelper::getFormData($service, $fieldsNames, $this->modelName));
    }

    /**
     * @param Request $request
     * @return $this
     */
    public function store(Request $request)
    {
        $additionalRule = ['icon' => 'required|string'];
        CrudHelper::getValidateRules($request->all(), $additionalRule)->validate();

        $serviceModel = new Service;
        $result = $serviceModel->fill(CrudHelper::getFinalInputData($request->all()))->save();

        if ($result) {
            flash(trans('messages.createServiceSuccessMessage'), 'success');
        } else {
            flash(trans('messages.dangerMessage'), 'danger');
        }

        return redirect()->back();
    }

    /**
     * @param Request $request
     * @param $id
     * @return $this
     */
    public function update(Request $request, $id)
    {
        $additionalRule = ['icon' => 'required|string'];
        CrudHelper::getValidateRules($request->all(), $additionalRule)->validate();

        $serviceModel = Service::where('id', $id)->first();
        $result = $serviceModel->fill($request->all())->update();

        if ($result) {
            flash(trans('messages.updateServiceSuccessMessage'), 'success');
        } else {
            flash(trans('messages.dangerMessage'), 'danger');
        }

        return redirect()->back();
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        $result = CrudHelper::delete(Service::find($id));

        if ($result) {
            flash(trans('messages.deleteServiceSuccessMessage'), 'success');
        } else {
            flash(trans('messages.dangerMessage'), 'danger');
        }

        return redirect()->back();
    }
}
