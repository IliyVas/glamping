<?php

namespace App\Http\Controllers\Glamping\Helpers;

use Illuminate\Database\Eloquent\Collection;

class GlampingListFilterHelper
{
    /**
     * @param $glampingsCollections
     * all published Glampings, this is result of the first part of the query building
     *
     * @param $filter
     * $filter is array, contain only only the specified filter values.
     *
     * @return null
     * return null if no glampings found
     */
    public static function filterByTransFields($glampingsCollections, $filter)
    {
        $glampings = null;

        if (isset($filter['name'])) {
            $glampings = $glampingsCollections->whereTranslationLike('name', '%' . $filter['name'] . '%');
        }

        if (isset($filter['country'])) {
            $glampings = $glampingsCollections->whereTranslationLike('country', '%' . $filter['country'] . '%');
        }

        if (isset($filter['region'])) {
            $glampings = $glampingsCollections->whereTranslationLike('region', '%' . $filter['region'] . '%');
        }

        return $glampings;
    }

    /**
     * @param Collection $glampings
     * @param $date
     * @param $price
     * @return Collection|null
     */
    public static function getFilterByPrice(Collection $glampings, $date, $price)
    {
        foreach ($glampings as $glampingKey => $glamping) {
            $periodMaxPrice = intval($price);

            //if min price more than $periodMaxPrice evaluate $periodMinPrices
            if ($glamping->min_price < $periodMaxPrice) {
                $buildings = $glamping->buildings()->where('active', true)->where('accepted', true)->get();

                if (!count(($buildings)) > 0)
                    unset($glampings[$glampingKey]);

                foreach ($buildings as $buildingKey => $building) {
                    $periodPrice = $building->getPeriodPrice($date['from'], $date['to']);

                    if ($periodPrice > $periodMaxPrice)
                        unset($buildings[$buildingKey]);
                }

                if (!count($buildings) > 0)
                    unset($glampings[$glampingKey]);
            } else {
                unset($glampings[$glampingKey]);
            }
        }

        //check Glampings Collection
        if (!count($glampings) > 0)
            $glampings = null;

        return $glampings;
    }

    /**
     * @param $fromDate
     * @param Collection $glampings
     * @param $filter
     * $filter is array, contain only only the specified filter values.
     *
     * @return Collection|null
     * return null if no glampings found or glampingCollection
     */
    public static function getFilterByAdultsBuildingsCount($fromDate, Collection $glampings, $filter)
    {
        foreach ($glampings as $glampingKey => $glamping) {
            $glampingAdults = null;
            $glampingFreeBuildingsCount = null;
            $buildings = $glamping->buildings()->where('active', true)->where('accepted',true)->get();

            if (!count($buildings) > 0)
                unset($glampings[$glampingKey]);

            if (isset($filter['buildings'])) {
                //get and set free buildings count since $fromDate
                $glampingFreeBuildingsCount = self::getGlampingFreeBuildingsCount(
                    $buildings, $fromDate, intval($filter['buildings']));

                //unset glamping if there are no free buildings since $fromDate
                if (is_null($glampingFreeBuildingsCount))
                    unset($glampings[$glampingKey]);
            }

            if (isset($filter['adults'])) {
                //get glamping adult capacity since $fromDate
                $glampingAdults = self::getGlampingAdults(
                    $buildings, $fromDate, intval($filter['adults']));

                //unset glamping if there is no possibility to place the specified number of adults since $fromDate
                if (is_null($glampingAdults))
                    unset($glampings[$glampingKey]);
            }

            if (isset($filter['adults']) && isset($filter['buildings'])) {
                $buildingArr = self::getAllFreeBuildingsData($buildings);
                $c = self::countGlampingCapacity($buildingArr, $filter['buildings']);

                if ($c < $filter['adults'])
                    unset($glampings[$glampingKey]);
            }
        }

        //check Glampings Collection
        if (!count($glampings) > 0)
            $glampings = null;

        return $glampings;
    }

    /**
     * @param $buildingsData
     * @param $buildingCount
     * @return int
     */
    public static function countGlampingCapacity($buildingsData, $buildingCount)
    {
        $buildCount = 0;
        $c = 0;

        while ($buildCount <= 1) {
            $a = array_pop($buildingsData);

            if ($a['freeCount'] >= $buildingCount)
                $c += $a['capacity'] * $buildingCount;
            else
                $c += $a['capacity'] * $a['freeCount'];

            $buildCount += $a['freeCount'];
        }

        return $c;
    }

    /**
     * @param $filter
     * @return mixed
     */
    public static function getFilterDates($filter){
        if (!isset($filter['from_date']))
            $date['from'] = date('Y-m-d', time()); //today
        else
            $date['from'] = $filter['from_date'];
        if (!isset($filter['to_date']))
            $date['to'] = date('Y-m-d', time() + (24*60*60));
        else
            $date['to'] = $filter['to_date'];

        return $date;
    }

    /**
     * @param Collection $buildings
     * @return array
     */
    public static function getAllFreeBuildingsData(Collection $buildings)
    {
        $buildingArr = [];

        foreach ($buildings as $building) {
            $buildingArr[$building->id] = [
                'freeCount' => $building->free_count,
                'capacity' => $building->capacity
            ];
        }

        usort($buildingArr, function ($v1, $v2) {
            if ($v1['capacity'] == $v2['capacity']) {
                return 0;
            }
            return ($v1['capacity'] < $v2['capacity'])? -1: 1;
        });

        return $buildingArr;
    }

    /**
     * Get and set free buildings count since $fromDate.
     *
     * @param Collection $buildings
     * @param $fromDate
     * $fromDate contain estimated date of arrival.
     *
     * @param $buildingsCount
     * $buildingsCount contain desired count of buildings.
     *
     * @return int|null
     */
    public static function getGlampingFreeBuildingsCount(Collection $buildings, $fromDate, $buildingsCount)
    {
        $freeBuildingCount = 0; //contain total number buildings count of Glamping

        foreach ($buildings as $building) {
            $building->free_count = $building->count($fromDate);
            $freeBuildingCount += $building->free_count;
        }

        if ($freeBuildingCount < $buildingsCount)
            $freeBuildingCount = null;

        return $freeBuildingCount;
    }

    /**
     * @param Collection $buildings
     * @param $fromDate
     * $fromDate contain estimated date of arrival.
     *
     * @param $adultsCount
     * $adultsCount contain a specified number of adults
     *
     * @return int
     */
    public static function getGlampingAdults(Collection $buildings, $fromDate, $adultsCount)
    {
        $glampingAdults = 0; //contain total glamping capacity

        foreach ($buildings as $building) {
            $freeBuildingCount = $building->count($fromDate);
            $glampingAdults += $building->capacity * $freeBuildingCount;
        }

        if ($glampingAdults < $adultsCount)
            $glampingAdults = null;

        return $glampingAdults;
    }

    /**
     * Filter by impressions or groups.
     *
     * @param Collection $glampings
     * @param $group
     * @param $filter
     * $filter is array, contains impressions/groups id or int.
     *
     * @return Collection|null
     */
    public static function getFilterByAdditionalGlampingDataResult(Collection $glampings, $filter, $group = false)
    {
        foreach ($glampings as $glampingKey => $glamping) {
            //$filteredCollection is array, contains id of all glamping impressions/groups
            if (!$group) $filteredCollection = $glamping->impressions->pluck('id')->all();
            else $filteredCollection = $glamping->glampingGroups->pluck('id')->all();

            //if $filter is int, create array with this int
            if (!is_array($filter)) $filter = [$filter];

            //check that all the necessary values are in the $filteredCollection
            $result = array_diff($filter, $filteredCollection);
            if (!is_array($result) && !$result || is_array($result) && !empty($result)) unset($glampings[$glampingKey]);
        }

        //check Glampings Collection
        if (!count($glampings) > 0) $glampings = null;

        return $glampings;
    }

    /**
     * @param Collection $glampings
     * @param $filter
     * $filter is array, contain only only the specified filter values.
     *
     * @return Collection|null
     */
    public static function getBuildingTypeFilterResult(Collection $glampings, $filter)
    {
        foreach ($glampings as $glampingKey => $glamping) {
            $buildings = $glamping->buildings()->where('accepted', true)->where('active', true)
                ->whereIn('building_type_id', $filter['building_types'])->get();

            if (!count($buildings) > 0)
                unset($glampings[$glampingKey]);
        }

        //check Glampings Collection
        if (!count($glampings) > 0)
            $glampings = null;

        return $glampings;
    }
}
