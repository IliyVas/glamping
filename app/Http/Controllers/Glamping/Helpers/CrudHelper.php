<?php

namespace App\Http\Controllers\Glamping\Helpers;

use Illuminate\Contracts\Auth\Guard;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpFoundation\Request;
use App\Traits\MediaUploader;
use Illuminate\Support\Facades\File;

class CrudHelper
{
    use MediaUploader;
    /**
     * @param Model $model
     * @return mixed
     */
    public static function delete(Model $model)
    {
        CrudHelper::deleteTranslations($model); //delete all translations for $model
        $result = $model->delete();

        return $result;
    }

    /**
     * @param Model $model
     * @return mixed
     */
    private static function deleteTranslations(Model $model)
    {
        $result = $model->deleteTranslations();

        return $result;
    }

    /**
     * @param $data
     * $data must contain result Request $request->all()
     *
     * @param array $additionalRule
     * $additionalRule is array, contain pairs 'field name' => 'validate rule'
     * for example $additionalRule = ['country' => 'required|string|max:255']
     *
     * @return mixed
     */
    public static function getValidateRules($data, $additionalRule = [])
    {
        $requiredLanguage = config('translatable.fallback_locale');
        $additionalLanguage = config('translatable.additional_locale');

        $standardRule = [
            'name:' . $requiredLanguage => 'required|string|max:255',
            'description:' . $requiredLanguage => 'required|string',
        ];

        if (isset($data['country:' . $requiredLanguage]) || isset($data['country:' . $additionalLanguage]) ||
            isset($data['region:' . $requiredLanguage]) || isset($data['region:' . $additionalLanguage])) {
            $standardRule = [
                'country:' . $requiredLanguage => 'required|string|max:255',
                'region:' . $requiredLanguage => 'required|string|max:255',
                'name:' . $requiredLanguage => 'required|string|max:255',
                'description:' . $requiredLanguage => 'required|string',
            ];

            if (!empty($data['name:' . $additionalLanguage]) || !empty($data['description:' . $additionalLanguage])) {
                $standardRule = [
                    'name:' . $requiredLanguage => 'required|string|max:255',
                    'description:' . $requiredLanguage => 'required|string',
                    'name:' . $additionalLanguage => 'required|string|max:255',
                    'description:' . $additionalLanguage => 'required|string',
                    'country:' . $requiredLanguage => 'required|string|max:255',
                    'region:' . $requiredLanguage => 'required|string|max:255',
                    'country:' . $additionalLanguage => 'required|string|max:255',
                    'region:' . $additionalLanguage => 'required|string|max:255',
                ];
            }
        } elseif (!empty($data['name:' . $additionalLanguage]) || !empty($data['description:' . $additionalLanguage])) {
            $standardRule = [
                'name:' . $requiredLanguage => 'required|string|max:255',
                'description:' . $requiredLanguage => 'required|string',
                'name:' . $additionalLanguage => 'required|string|max:255',
                'description:' . $additionalLanguage => 'required|string',
            ];
        }

        return Validator::make($data, array_merge($additionalRule, $standardRule));
    }

    /**
     * Unset all empty fields
     *
     * @param $data
     * $data must contain result Request $request->all()
     *
     * @return mixed
     */
    public static function getFinalInputData($data)
    {
        foreach ($data as $key => $value) {
            if (empty($value)) {
                $value = false;
            }
            $data[$key] = $value;
        }

        return array_filter($data);
    }

    /**
     * @param $string
     * @return string
     */
    public static function transliterate($string)
    {
        $string = (string) $string;
        $string = strip_tags($string); //remove html-tags
        $string = trim($string);
        $string = function_exists('mb_strtolower') ? mb_strtolower($string) : strtolower($string);
        $string = strtr($string, array('а'=>'a','б'=>'b','в'=>'v','г'=>'g','д'=>'d','е'=>'e','ё'=>'e','ж'=>'j','з'=>'z','и'=>'i','й'=>'y','к'=>'k','л'=>'l','м'=>'m','н'=>'n','о'=>'o','п'=>'p','р'=>'r','с'=>'s','т'=>'t','у'=>'u','ф'=>'f','х'=>'h','ц'=>'c','ч'=>'ch','ш'=>'sh','щ'=>'shch','ы'=>'y','э'=>'e','ю'=>'yu','я'=>'ya','ъ'=>'','ь'=>''));
        $string = str_replace(" ", "", $string); //remove spaces

        return $string;
    }

    /**
     * @param Request $request
     * @param Guard $auth
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse
     */
    public function ajaxUploadImages(Request $request, Guard $auth)
    {
        if ($auth->guest())
            return redirect()->back();
        $directory = public_path('tmp') . '/' . $auth->id();
        $image = $request->file('file');

        if (!is_dir($directory)) {
            $directory = $this->createTemporaryMediaDirectory(public_path('tmp/'), Auth::user()->id)->path();
        }

        $imageName = time(). CrudHelper::transliterate($image->getClientOriginalName());
        $image->move($directory, $imageName);

        return response()->json([
            'success' => true,
            'image_name' => $imageName
        ]);
    }

    /**
     * @param Request $request
     * @param Guard $auth
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse
     */
    public function ajaxRemoveImages(Request $request, Guard $auth)
    {
        if ($auth->guest())
            return redirect()->back();
        $directory = public_path('tmp') . '/' . $auth->id() . '/';
        $file_name = $request['file_name'];

        File::delete($directory . $file_name);

        return response()->json([
            'success' => true,
            'path' => $directory . $file_name,
        ]);
    }
}
