<?php

namespace App\Http\Controllers\Glamping;

use App\Models\Glamping\GlampingGroup;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Traits\MediaUploader;
use App\Http\Controllers\Glamping\Helpers\CrudHelper;
use App\Helpers\ResourceControllerHelper as ResourceHelper;

class GlampingGroupController extends Controller
{
    use MediaUploader;

    /**
     * @var string
     */
    protected $modelName = 'GlampingGroup';

    /**
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $columns = ['icon', 'name', 'description', 'action-edit-delete'];
        $gridData = ResourceHelper::getGridData($columns, GlampingGroup::all(), $this->modelName);

        return view('manager.abstract_model.grid')->with($gridData);
    }

    /**
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return $this->renderForm();
    }

    /**
     * @param $id
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $glampingGroup = GlampingGroup::find($id);

        if (!isset($glampingGroup)) {
            return redirect()->back()->withErrors(trans('messages.notExistGlampingGroupError'));
        }

        return $this->renderForm($glampingGroup);
    }

    /**
     * @param GlampingGroup|null $glampingGroup
     * @return \Illuminate\View\View
     */
    private function renderForm(GlampingGroup $glampingGroup = null)
    {
        $fieldsNames = ['icon', 'submit'];

        return view('manager.abstract_model.form')
            ->with(ResourceHelper::getFormData($glampingGroup, $fieldsNames, $this->modelName));
    }

    /**
     * @param Request $request
     * @return $this
     */
    public function store(Request $request)
    {
        $additionalRule = ['icon' => 'required|mimes:jpeg,png'];
        CrudHelper::getValidateRules($request->all(), $additionalRule)->validate();

        $glampingGroupModel = new GlampingGroup;
        $glampingGroupModel->fill(CrudHelper::getFinalInputData($request->all()))->save();

        $name = CrudHelper::transliterate($request->icon->getClientOriginalName());
        $iconUrl = $this->uploadIcon($glampingGroupModel, $request->icon, $this->modelName, $name)->getUrl();

        $result = $glampingGroupModel->update(['icon' => $iconUrl]);

        if ($result) {
            flash(trans('messages.createGlampingGroupSuccessMessage'), 'success');
        } else {
            flash(trans('messages.dangerMessage'), 'danger');
        }

        return redirect()->back();
    }

    /**
     * @param Request $request
     * @param $id
     * @return $this
     */
    public function update(Request $request, $id)
    {
        CrudHelper::getValidateRules($request->all())->validate();
        $glampingGroupModel = GlampingGroup::where('id', $id)->first();
        $data = $request->all();

        if (!empty($data['icon'])) {
            $name = CrudHelper::transliterate($request->icon->getClientOriginalName());
            $iconUrl = $this->uploadIcon($glampingGroupModel, $request->icon, $this->modelName, $name)->getUrl();
        } else {
            $iconUrl = $glampingGroupModel->icon;
        }

        $data['icon'] = $iconUrl;
        $result = $glampingGroupModel->fill($data)->update();

        if ($result) {
            flash(trans('messages.updateGlampingGroupSuccessMessage'), 'success');
        } else {
            flash(trans('messages.dangerMessage'), 'danger');
        }

        return redirect()->back();
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        $result = CrudHelper::delete(GlampingGroup::find($id));

        if ($result) {
            flash(trans('messages.deleteGlampingGroupSuccessMessage'), 'success');
        } else {
            flash(trans('messages.dangerMessage'), 'danger');
        }

        return redirect()->back();
    }
}
