<?php

namespace App\Http\Controllers\Glamping;

use App\Models\Building\BuildingPrice;
use App\Models\Building\Building;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BuildingPriceController extends Controller
{

    //errors while loading images
    /**
     * One image overlay on another
     * @var int
     */
    const DATES_ERROR_OVERLAY = -1;
    /**
     * "To" is smaller than "From"
     * @var int
     */
    const DATES_ERROR_BACKWARDS = -2;
    /**
     * Price is negative
     * @var int
     */
    const DATES_ERROR_NEGATIVE_PRICE = -3;
    /**
     * Price is OK
     * @var int
     */
    const DATES_OK = 1;

    /**
     * @param array $prices
     * @return $this
     */
    public function createPrices($prices)
    {
        $building = null;
        foreach ($prices as $price) {
            if (is_null($building) || $price["building_id"] != $building->id) {
                $building = Building::find($price["building_id"]);
            }

            $priceModel = new BuildingPrice();
            $priceModel->fill($price)->save();

            if ($building) {
                $building->prices()->attach($priceModel->id);
            }
        }
    }

    /**
     * @param Request $request
     * @param $id
     * @return int
     */
    public function generatePricesFromRequest(Request $request, $id)
    {
        $data = $request->all();
        $from_dates = $data['from_dates'];
        $to_dates = $data['to_dates'];
        $price_dates = $data['price_dates'];

        $arr = array();
        
        // generate readable format
        for ($i = 0; $i < sizeof($from_dates); $i++) {
            array_push($arr, [
                'from' => $from_dates[$i],
                'to' => $to_dates[$i],
                'price' => $price_dates[$i],
                'building_id' => $id
            ]);
        }

        $status = $this->validatePriceDates($arr);
        if ($status != BuildingPriceController::DATES_OK) {
            return $status;
        } else {
            $this->createPrices($arr);
            return BuildingPriceController::DATES_OK;
        }
    }

    /**
     * @param $a
     * @param $b
     * @return int
     */
    private static function datesSort($a, $b)
    {
        if ($a['from'] < $b['from']) {
            return -1;
        } else {
            return 1;
        }
    }

    /**
     * @param $dates
     * @return int
     */
    public function validatePriceDates($dates)
    {
        usort($dates, array($this, 'datesSort'));
        for ($i = 0; $i < sizeof($dates); $i++) {
            if ($i < sizeof($dates) - 1 && $dates[$i]['to'] > $dates[$i + 1]['from']) {
                return BuildingPriceController::DATES_ERROR_OVERLAY;
            }
            if ($dates[$i]['to'] < $dates[$i]['from']) {
                return BuildingPriceController::DATES_ERROR_BACKWARDS;
            }
            if ($dates[$i]['price'] < 0) {
                return BuildingPriceController::DATES_ERROR_NEGATIVE_PRICE;
            }
        }
        return BuildingPriceController::DATES_OK;
    }
}
