<?php

namespace App\Http\Controllers;

use App\Models\Communication\Comments;
use App\Models\Glamping\Glamping;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CommentController extends Controller
{
    /**
     * Create new comment.
     *
     * @param Request $request
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'comment' => 'required|string|max:500',
            'rating' => 'required|numeric|max:5|min:0',
            'glamping_id' => 'required|numeric|min:1'
        ]);

        $glamping = Glamping::find($request->glamping_id);

        //check glamping
        if (is_null($glamping))
            return redirect()->back()->withErrors(trans('messages.notExistGlampingError'));

        //check right of create comment
        $isOwner = Auth::guard('manager')->check() && Auth::user()->hasRole('owner') &&
            Auth::user()->glampings->contains($glamping->id);
        $isValidUser = Auth::guard('user')->check() && Auth::guard('user')->user()
                ->isGlampingVisited($glamping);
        if (Auth::guest() || !$isOwner && !$isValidUser )
            return redirect()->back()->withErrors(trans('messages.dangerMessage'));

        //create comment
        $result = $glamping->comments()->create(array_merge($request->all(), ['user_id' => Auth::user()->id]));
        if (!$result) flash(trans('messages.dangerMessage'), 'danger');

        return redirect()->back();
    }

    /**
     * Delete comment and rating.
     *
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        $comment = Comments::find($id);
        //validate comment and right for delete
        if (is_null($comment) || !$comment->canBeDeleted()) return redirect()->back();
        //delete comment
        $result = $comment->delete();

        if (!$result) flash(trans('messages.dangerMessage'), 'danger');

        return redirect()->back();
    }
}
