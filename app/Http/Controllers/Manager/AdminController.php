<?php

namespace App\Http\Controllers\Manager;

use App\Traits\ProfileEditor;
use App\Http\Controllers\Controller;
use App\Http\Requests\Manager\AnswerRequest;
use App\Models\Communication\Message;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class AdminController extends Controller
{
    use ProfileEditor;
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showDashboard()
    {
        return view('manager.admin.dashboard');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit()
    {
        return view('manager.admin.edit_account_form');
    }

    /**
     * @param Request $request
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function update(Request $request)
    {
        $this->validate($request, [
            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'patronomic' => 'nullable|string|max:255'
        ]);

        $this->saveChanges($request, 'Admin');

        return redirect()->back();
    }

    /**
     * @param Request $request
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function changeEmail(Request $request)
    {
        $this->saveEmail($request, 'manager');

        return redirect()->back();
    }

    /**
     * @return $this
     */
    public function showFeedbackMessages()
    {
        return view('manager.message.index',[
            'gridData' => Message::all(),
            'breadCrumbsActive' => 'feedbackMessages',
            'active' => 'Feedback',
            'pageName' => trans('messages.feedbackMessagesGrid')
        ]);
    }


    /**
     * Show message
     *
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showMessage($id)
    {
        $message = Message::find($id);

        if(!$message->viewed){

            $message->update([
                'viewed' => 1
            ]);
        }
        return view('manager.message.show', [
            'breadCrumbsActive' => 'feedbackMessages',
            'active' => 'Feedback',
            'pageName' => $message->name,
            'message' => $message
        ]);
    }

    /**
     * Toggle viewed option
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function toggleViewed(Request $request)
    {
        $message = Message::find($request->mes_id);
        $message->update([
            'viewed' => $request->val
        ]);

        return response()->json(['status' => true]);
    }

    /**
     * Sending answer on user question
     *
     * @param AnswerRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function answerMessage(AnswerRequest $request)
    {

        $mess = Message::find($request->message);

        Mail::send('manager.emails.answer', ['ask' => $mess->message, 'answer' => $request->answer], function ($message)  use ($mess){

            $message->to($mess->email)->subject('Answer ur question!');
        });

        return redirect()->route('manager.feedbackMessages');
    }
}
