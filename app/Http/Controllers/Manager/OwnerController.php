<?php

namespace App\Http\Controllers\Manager;

use App\Helpers\ResourceControllerHelper;
use App\Http\Controllers\Glamping\BookController;
use App\Models\Booking\BookingStatus;
use App\Models\Communication\Application;
use App\Models\Communication\ApplicationType;
use App\Traits\ProfileEditor;
use App\Http\Controllers\Controller;
use App\Jobs\SendVerificationEmail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class OwnerController extends Controller
{
    use ProfileEditor;
    /**
     * @return \Illuminate\View\View
     */
    public function showDashboard()
    {
        $owner =  Auth::guard('manager')->user();
        //get nearest data
        $nearestDate = $owner->bookings()->where('status', '!=', BookingStatus::CANCELED)
            ->where('from_date', '>', date('Y-m-d', time()))->get()->min('from_date');

        return view('manager.owner.dashboard')->with([
            'columns' => ResourceControllerHelper::getColumnGridData(['№', 'name', 'description', 'date', 'status']),
            'gridData' => BookController::setBookingData($owner->bookings()->where('from_date', $nearestDate)->get()),
            ]);
    }

    /**
     * @return \Illuminate\View\View
     */
    public function showProfile()
    {
        return view('manager.owner.profile');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit()
    {
        return view('manager.owner.edit_account_form');
    }

    /**
     * Edit owner data.
     *
     * @param Request $request
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function update(Request $request)
    {
        $this->validate($request, [
            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'patronomic' => 'nullable|string|max:255',
            'phone' => 'required|string|max:255',
            'email' => 'required|string|max:255',
            'company_name' => 'nullable|string|max:255',
            'registration_number' => 'nullable|string|max:255',
        ]);

        $owner = Auth::guard('manager')->user();

        if ($request->email !== $owner->email)
            $this->saveEmail($request, 'manager');

        if ($owner->company)
            unset($request->company_name, $request->registration_number);
        else
            $owner->createCompany(['name' => $request->company_name,
                'registration_number' => $request->registration_number]);

        $this->saveChanges($request, 'Owner');

        //send application
        $owner->forceFill(['accepted' => false])->update();

        if (!count($owner->applications) > 0)
            $type = ApplicationType::REGISTER;
        else
            $type = ApplicationType::EDIT_PROFILE;

        $owner->applications()->save((new Application)->forceFill(['type' => $type]));

        return redirect()->back();
    }

    /**
     * @param Request $request
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function changePassword(Request $request)
    {
        $this->savePassword($request, 'manager');

        return redirect()->back();
    }

    /**
     * Запрос на отправку письма с активацией ещё раз
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function resend()
    {
        $user = Auth::guard('manager')->user();
        dispatch(new SendVerificationEmail($user, 'manager'));

        Session::flash('success', trans('auth.sendVerifyMessage'));
        return redirect()->route('manager.nonactive');
    }
}
