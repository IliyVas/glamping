<?php

namespace App\Http\Controllers\Manager\Traits;

use Illuminate\Support\Facades\Auth;

trait RedirectToDashboard
{
    /**
     * Where to redirect users after login / registration.
     *
     * @return string
     */
    protected function redirectTo()
    {
        $user = Auth::guard('manager')->user();

        if ($user->hasRole('owner')) {
            return route('manager.owner_dashboard');
        } elseif ($user->hasRole('admin')) {
            return route('manager.admin_dashboard');
        }
        return '/';
    }
}
