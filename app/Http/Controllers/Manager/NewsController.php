<?php

namespace App\Http\Controllers\Manager;

use App\Http\Requests\Manager\NewsRequest;
use App\Models\News\News;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class NewsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::guard('manager')->user();
        $news = $user->news()->get();

        return view('manager.admin.news.index',['news' => $news]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('manager.admin.news.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(NewsRequest $request)
    {
        $user = Auth::guard('manager')->user();
        $data = $request->all();

        $news = $user->news()->create([
               'name:ru' => $data['name:ru'],
               'name:en' => $data['name:en'],
               'title:ru' => $data['title:ru'],
               'title:en' => $data['title:en'],
               'keywords:ru' => $data['keywords:ru'],
               'keywords:en' => $data['keywords:en'],
               'description:ru' => $data['description:ru'],
               'description:en' => $data['description:en'],
               'seo_description:ru' => $data['seo_description:ru'],
               'seo_description:en' => $data['seo_description:en']
            ]);

        if(isset($request->mainImg)){
            $news->addMedia($request->mainImg)->toMediaLibrary('NewsImages');
        }

        return redirect()->route('manager.news.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $news = News::find($id);
        $media = $news->media()->first();

        return view('manager.admin.news.edit', ['news' => $news, 'thumb' => $media]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(NewsRequest $request, $id)
    {
        $data = $request->all();

        $news = News::find($id);

        $news->translate('en')->update([
            'name' => $data['name:en'],
            'title' => $data['title:en'],
            'keywords' => $data['keywords:en'],
            'description' => $data['description:en'],
            'seo_description' => $data['seo_description:en'],
        ]);

        $news->translate('ru')->update([
            'name' => $data['name:ru'],
            'title' => $data['title:ru'],
            'keywords' => $data['keywords:ru'],
            'description' => $data['description:ru'],
            'seo_description' => $data['seo_description:ru'],
        ]);


        if(isset($request->mainImg)){

            if($news->hasMedia('NewsImages')){
                $images = $news->media()->get();
                foreach ($images as $img){
                    $news->deleteMedia($img->id);
                }

                $news->addMedia($request->mainImg)->toMediaLibrary('NewsImages');
            }else{
                $news->addMedia($request->mainImg)->toMediaLibrary('NewsImages');
            }
        }

        return redirect()->route('manager.news.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $news = News::find($id);

        $news->delete();

        return redirect()->route('manager.news.index');
    }
}
