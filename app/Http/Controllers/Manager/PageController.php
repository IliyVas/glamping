<?php

namespace App\Http\Controllers\Manager;

use App\Http\Requests\Manager\PageRequest;
use App\Models\Page\Page;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PageController extends Controller
{
    /**
     * Show listing pages
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $pages = Page::all();

        return view('manager.page.index',[
            'pages' => $pages,
            'breadCrumbsActive' => 'pages',
            'pageName' => trans('common_elements.pages')
        ]);
    }

    /**
     * Show information about page by id
     *
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($id)
    {
        $page = Page::find($id);
        return view('manager.page.show',[
            'page' => $page,
            'breadCrumbsActive' => 'pages',
            'pageName' => $page->name
        ]);
    }


    /**
     * Updating page
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(PageRequest $request, $id)
    {
        $data = $request->all();

        $page = Page::find($id);
        $page->translate('en')->update([
            'name' => $data['name:en'],
            'title' => $data['title:en'],
            'keywords' => $data['keywords:en'],
            'description' => $data['description:en'],
            'seo_description' => $data['seo_description:en'],
        ]);

        $page->translate('ru')->update([
            'name' => $data['name:ru'],
            'title' => $data['title:ru'],
            'keywords' => $data['keywords:ru'],
            'description' => $data['description:ru'],
            'seo_description' => $data['seo_description:ru'],
        ]);

        return redirect()->route('manager.pages');
    }
}
