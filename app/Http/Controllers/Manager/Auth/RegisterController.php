<?php

namespace App\Http\Controllers\Manager\Auth;

use App\Jobs\SendVerificationEmail;
use App\Models\Auth\Manager;
use App\Models\Auth\Role;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Manager\Traits\RedirectToDashboard;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Config;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers, RedirectToDashboard;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('manager.guest');
        Log::info(Config::get('entrust.user_foreign_key'));
    }


    /**
     * Confirm email address with subsequent activation of the user or changing the email address
     *
     * @param $token
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function verify($token)
    {
        $user = Manager::where('email_token', $token)->first();
        Auth::guard('manager')->login($user);

        if ($user->activated && !is_null($user->new_email)) {
            $result = $user->fill(['email' => $user->new_email, 'new_email' => null])->update();
            $tr = 'Changed';
        } else {
            $tr = 'Verified';
            $result = $user->forceFill(['activated' => true])->update();
        }

        if($result)
            flash(trans('auth.email' . $tr,['email' => htmlspecialchars($user->email)]), 'success');

        if ($user->hasRole('admin'))
            $red = 'admin';
        else
            $red = 'owner';
        return redirect()->route('manager.' . $red . '_dashboard');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'phone' => 'required|string|max:255',
            'email' => 'required|email|max:255|unique:managers',
            'password' => 'required|string|min:6|confirmed',
            'captcha' => 'required|captcha',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return Manager
     */
    protected function create(array $data)
    {
        $data['password'] = bcrypt($data['password']);
        $data['email_token'] = base64_encode($data['email']);

        $manager = Manager::create($data);
        dispatch(new SendVerificationEmail($manager, 'manager'));

        if (isset($manager)) {
            $ownerRole = Role::where('name', 'owner')->first();
            $manager->attachRole($ownerRole->id);
        }

        return $manager;
    }

    /**
     * Show the application registration form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showRegistrationForm()
    {
        return view('manager.auth.register');
    }

    /**
     * Get the guard to be used during registration.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard('manager');
    }
}
