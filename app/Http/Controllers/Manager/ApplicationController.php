<?php

namespace App\Http\Controllers\Manager;

use App\Http\Controllers\Controller;
use App\Jobs\SendRegistrationEmail;
use App\Models\Auth\Manager;
use App\Models\Communication\ApplicationType;
use App\Models\Glamping\Glamping;
use App\Models\Communication\Application;
use App\Models\Communication\ApplicationStatus;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Helpers\ResourceControllerHelper as ResourceHelper;
use Illuminate\Support\Facades\Validator;

class ApplicationController extends Controller
{
    /**
     * @return ApplicationController
     */
    public function index()
    {
        $columns = ['type', 'description', 'date-application', 'action-view'];

        return $this->renderGrid($columns);
    }

    /**
     * @return ApplicationController
     */
    public function archive()
    {
        $columns = ['type', 'description', 'date-application', 'date-review',
            'status', 'action-view'];

        return $this->renderGrid($columns, true);
    }

    /**
     * @param $columns
     * @param bool $archive
     * @return $this
     */
    private function renderGrid($columns, $archive = false) {
        if ($archive) {
            $active = 'ApplicationArchive'; $crumbs = 'archive';
        } else {
            $active = 'Application'; $crumbs = 'new';
        }

        return view('manager.abstract_model.grid')->with([
            'pageName' => trans('applications.gridName'),
            'columns' => ResourceHelper::getColumnGridData($columns),
            'active' => $active,
            'breadCrumbsActive' => $crumbs,
            'gridData' => $this->getApplicationGridData($archive),
            'arrayGridData' => true,
            'viewRoute' => 'manager.applications.show',
            'menuItem' => 'applications'
        ]);
    }

    /**
     * @param $glampingId
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function send($glampingId)
    {
        $glamping = Glamping::find($glampingId);

        if (is_null($glamping))
            return redirect()->back()->withErrors(trans('messages.notExistGlampingError'));

        if (Auth::guest() || !Auth::user()->glampings->contains($glamping->id))
            return redirect()->back()->withErrors(trans('messages.noRightsToThisGlamping'));

        if ($glamping->accepted) {
            $app = new Application;
            $app->forceFill(['type' => ApplicationType::CHANGE]);
            $result = $glamping->applications()->save($app);
        }else {
            $app = new Application;
            $app->forceFill(['type' => ApplicationType::CREATE]);
            $result = $glamping->applications()->save($app);
        }

        if (!$result)
            redirect()->back()->withErrors(trans('messages.dangerMessage'));

        return redirect()->back();
    }

    /**
     * @param bool $archive
     * @return array|null
     */
    private function getApplicationGridData($archive = false)
    {
        $gridData = null;
        $applications = Application::where('is_viewed', $archive)->get();

        foreach ($applications as $app) {
            $gridData[] = [
                'type' => $app->getType(),
                'description' => $app->getDescription(),
                'date-application' => date_create($app->created_at)->format('d.m.Y H:i'),
                'date-review' => date_create($app->updated_at)->format('d.m.Y H:i'),
                'id' => $app->id,
                'status' => $app->getStatus(),
                'reviewer' => $app->reviewer,
                'viewRoute' => 'manager.applications.show',
            ];
        }

        return $gridData;
    }

    /**
     * @param Request $request
     */
    private function validateApplicationData(Request $request)
    {
        Validator::make($request->all(), ['answer' => 'nullable|string|min:3|max:500'])
            ->after(function ($validator) use ($request) {
            if (isset($request->reject) && is_null($request->answer))
                $validator->errors()->add('', trans('messages.emptyDenyMessage'));

            if (!isset($request->reject) && !isset($request->approve))
                $validator->errors()->add('', trans('messages.unknownApplicationStatus'));
        })->validate();
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        $this->validateApplicationData($request);
        $app = Application::find($id);

        if (is_null($app))
            redirect()->back()->withErrors(trans('messages.notExistApplicationError',
                ['id' => htmlspecialchars($id)]));

        $status = ApplicationStatus::REJECTED;
        $message = trans('messages.rejectApplicationMessage');

        if (isset($request->approve)) {
            $message = trans('messages.approveApplicationMessage');
            $status = ApplicationStatus::ACCEPTED;

            if ($app->applicationable_type === 'glamping')
                $this->acceptGlamping($app->applicationable);
            else
                $this->acceptAndNotifyOwner($app->applicationable, $app->type);
        }

        $result = $app->forceFill([ 'status' => $status, 'answer' =>$request->answer,
            'manager_id' => Auth::user()->id, 'is_viewed' => true])->update();

        if ($app->type === ApplicationType::REGISTER)
            dispatch(new SendRegistrationEmail($app->applicationable, ApplicationStatus::REJECTED));

        if (!$result)
            flash(trans('messages.dangerMessage'), 'danger');

        flash($message, 'success');

        return redirect()->back();
    }

    /**
     * @param $owner
     * @param $appType
     * @return $this
     */
    private function acceptAndNotifyOwner($owner, $appType)
    {
        $result = $owner->forceFill(['accepted' => true])->update();
        if (!$result)
            return redirect()->back()->withErrors(trans('messages.dangerMessage'));

        if ($appType === ApplicationType::REGISTER)
            dispatch(new SendRegistrationEmail($owner, ApplicationStatus::ACCEPTED));
    }

    /**
     * @param $glamping
     * @return $this
     */
    private function acceptGlamping($glamping)
    {
        if ($glamping->changedGlamping) {
            $result = $glamping->forceFill(['active' => false, 'accepted' => false])->update();

            if (!$result)
                return redirect()->back()->withErrors(trans('messages.dangerMessage'));

            $result = $glamping->changedGlamping->forceFill([
                'active' => true, 'accepted' => true])->update();

            if (!$result)
                return redirect()->back()->withErrors(trans('messages.dangerMessage'));

            foreach ($glamping->buildings as $building) {
                $this->saveApplicationableBuilding($building,
                    $glamping->changedGlamping->id);
            }
        } else {
            $result = $glamping->forceFill([
                'active' => true, 'accepted' => true])->update();

            if (!$result)
                return redirect()->back()->withErrors(trans('messages.dangerMessage'));

            foreach ($glamping->buildings as $building) {
                $this->saveApplicationableBuilding($building, $glamping->id);
            }
        }
    }

    /**
     * @param $building
     * @param $applicationableId
     * @return $this
     */
    private function saveApplicationableBuilding($building, $applicationableId)
    {
        if ($building->changedBuilding) {
            $result = $building->changedBuilding->forceFill([
                'active' => true,
                'accepted' => true,
                'glamping_id' => $applicationableId])->update();

            if (!$result)
                return redirect()->back()->withErrors(trans('messages.dangerMessage'));

            $result = $building->forceFill([
                'active' => false,
                'accepted' => false
            ])->update();

            if (!$result)
                return redirect()->back()->withErrors(trans('messages.dangerMessage'));
        } else {
            $result = $building->forceFill([
                'active' => true,
                'accepted' => true,
                'glamping_id' => $applicationableId])->update();

            if (!$result)
                return redirect()->back()->withErrors(trans('messages.dangerMessage'));
        }
    }

    /**
     * @param $id
     * @return $this
     */
    public function show($id)
    {
        $app = Application::find($id);

        if (is_null($app))
            return redirect()->back()->withErrors(trans('messages.notExistApplicationError',
                ['id' => htmlspecialchars($id)]));

        return view('manager.admin.application')->with([
            'app' => $app,
            'reviewer' => Manager::find($app->manager_id)
        ]);
    }
}
