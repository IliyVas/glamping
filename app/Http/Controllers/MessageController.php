<?php

namespace App\Http\Controllers;

use App\Models\Communication\Message;
use Illuminate\Http\Request;

class MessageController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showFeedbackForm()
    {
        return view('feedback_form');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function setMessage(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string|max:255',
            'email' => 'required|email',
            'message' => 'required|string|max:500'
        ]);

        $message = new Message();
        $result = $message->fill($request->all())->save();

        if ($result) {
            flash(trans('messages.sendSuccessMessage'), 'success');
        } else {
            flash(trans('messages.dangerMessage'), 'danger');
        }

        return redirect()->back();
    }
}
