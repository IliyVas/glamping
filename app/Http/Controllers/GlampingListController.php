<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Glamping\GlampingController;
use App\Models\Building\Building;
use App\Models\Building\BuildingType;
use App\Models\Glamping\GlampingGroup;
use App\Models\Glamping\Impression;
use App\Models\News\News;
use Illuminate\Http\Request;

class GlampingListController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index() {
        $glampings = GlampingController::getAllPublishedGlampings();

        if (count($glampings) > 0)
            $glampings = $glampings->shuffle()->take(3);

        //get impressions
        $impressions = Impression::all();
        if (count( $impressions ) > 0)
            $impressions = $impressions->shuffle()->take(3);

        return view('homepage')->with([
            'news' => News::all()->take(3),
            'buildingTypes' => BuildingType::all(),
            'maxQuantity' => Building::where('accepted', true)->where('active', true)
                ->get()->pluck('capacity')->max(),
            'popularImpressions' =>  $impressions,
            'glampings' => $glampings,
        ]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|\Illuminate\Http\RedirectResponse
     */
    public function showFilterPage(Request $request)
    {
        $filter = $this->getFilterData($request);
        $glampings = $this->getFilteredGlampings($filter);
        $filterData = $this->generateViewFilterData($filter);

        if (count($glampings) == 1) {
            $glamping = $glampings->first();

            if (isset($filter['name']) && $filter['name'] === $glamping->name)
                return redirect()->route('viewGlamping', ['id' => $glamping->id,
                    'from' => $filterData['fromDate'], 'to' =>$filterData['toDate'], 'adults' => $filterData['adults']]);
        }

        return view('glamping_list')->with(array_merge(['glampings' => $glampings, 'impressions' => Impression::all(),
            'glampingGroups' => GlampingGroup::all(), 'buildingTypes' => BuildingType::all()], $filterData));
    }

    /**
     * Show filter result.
     *
     * @param Request $request
     * @return string
     */
    public function showFilteredGlampings(Request $request)
    {
        $filter = $this->getFilterData($request);

        return view('templates.glampingsData')
            ->with(array_add($this->generateViewFilterData($filter), 'glampings', $this->getFilteredGlampings($filter)))
            ->render();
    }

    /**
     * Return collection of Glampings according to the filtering requirements.
     *
     * @param $filter
     * @return mixed|null
     */
    private function getFilteredGlampings($filter)
    {
        if (empty($filter)) $glampings = GlampingController::getAllPublishedGlampings();
        else $glampings = GlampingController::getGlampingFilterResult($filter);

        return $glampings;
    }

    /**
     * Generate filter array.
     *
     * @return mixed
     */
    private function getFilterData(Request $request)
    {
        $data = $request->all();
        $data = array_filter($data); //remove empty filter fields

        if (isset($data['additional_data']) && isset($data['additional_data_type'])) {
            $data[$data['additional_data_type']] = $data['additional_data'];
            unset ($data['additional_data_type'], $data['additional_data']);
        }

        return $data;
    }

    /**
     * Generate an array depending on the filtering fields.
     *
     * @param $filter
     * @return array
     */
    private function generateViewFilterData($filter)
    {
        //get arrival date
        if (isset($filter['from_date'])) $fromDate = $filter['from_date'];
        else $fromDate = date('Y-m-d', time());

        //get county date
        if (isset($filter['to_date'])) $toDate = $filter['to_date'];
        else $toDate = date('Y-m-d', time() + (2*24*60*60));

        //get adults
        if (isset($filter['adults'])) $adults = $filter['adults'];
        else $adults = null;

        //get name
        if (isset($filter['name'])) $name = $filter['name'];
        else $name = null;

        //get country
        if (isset($filter['country'])) $country = $filter['country'];
        else $country = null;

        //get region
        if (isset($filter['region'])) $region = $filter['region'];
        else $region = null;

        //get buildings
        if (isset($filter['buildings'])) $buildings = $filter['buildings'];
        else $buildings = null;

        //get children
        if (isset($filter['children'])) $children = $filter['children'];
        else $children = null;

        return [
            'fromDate' => $fromDate,
            'toDate' => $toDate,
            'adults' => $adults,
            'name' => $name,
            'country' => $country,
            'region' => $region,
            'buildings' => $buildings,
            'children' => $children
        ];
    }
}
