<?php

namespace App\Http\Controllers\User;

use App\Traits\ProfileEditor;
use App\Http\Controllers\Controller;
use App\Models\Booking\Booking;
use App\Http\Controllers\Glamping\BookController;
use App\Models\Glamping\Glamping;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    use ProfileEditor;

    // const for errors
    const BOOKINGS_RIGHTS_OK = 1;
    const ERROR_NOT_LOGIN = 0;
    const ERROR_NO_RIGHTS_FOR_BOOKING = -1;
    const ERROR_BOOKING_NOT_ACTIVE = -2;
    const ERROR_STRANGE = -3;

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit()
    {
        return view('user.edit_account_form');
    }

    /**
     * @param Request $request
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function update(Request $request)
    {
        $this->validate($request, [
            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'phone' => 'required|max:255',
            'birth_date' => 'required|date',
            'patronomic' => 'nullable|string|max:255'
        ]);

        $this->saveChanges($request);

        return redirect()->back();
    }

    /**
     * @param Request $request
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function changePassword(Request $request)
    {
        $this->savePassword($request);

        return redirect()->back();
    }

    /**
     * @param Request $request
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function changeEmail(Request $request)
    {
        $this->saveEmail($request, 'user');

        return redirect()->back();
    }

    /**
     * @param Guard $auth
     * @return mixed
     */
    public function showDashboard(Guard $auth)
    {
        return view('user.dashboard')->with(['nearestBooking' => $auth->user()->getNearestBooking()]);
    }

    /**
     * @param Guard $auth
     * @return mixed
     */
    public function showAccount(Guard $auth)
    {
        return view('user.account')->with(['user' => $auth->user()]);
    }

    /**
     * @param Guard $auth
     * @return mixed
     */
    public function showBookings(Guard $auth)
    {
        $user = $auth->user();
        $bookings = Booking::where('user_id', $user->id)->get();

        return view('user.bookings')->with([
            'user' => $user,
            'bookings' => $bookings,
            'should_show_buildings' => true,
        ]);
    }

    /**
     * @param Guard $auth
     * @param $id
     * @return int
     */
    private function checkRightsForBooking(Guard $auth, $id)
    {
        if ($auth->guest()) {
            return self::ERROR_NOT_LOGIN;
        }

        $booking = Booking::find($id);

        if (!isset($booking) || $booking->user_id != $auth->id()) {
            return self::ERROR_NO_RIGHTS_FOR_BOOKING;
        }

        if (!$booking->isActive()) {
            return self::ERROR_BOOKING_NOT_ACTIVE;
        }

        return self::BOOKINGS_RIGHTS_OK;
    }

    /**
     * @param Guard $auth
     * @param $id
     * @return $this|\Illuminate\Http\RedirectResponse|int
     */
    private function bookingRightsRedirect(Guard $auth, $id)
    {
        $status = $this->checkRightsForBooking($auth, $id);
        switch ($status):
            case self::ERROR_NOT_LOGIN:
                session()->put('nextUrl', 'userUpdateBookingForm');
        return redirect()->route('login');
        break;
        case self::ERROR_NO_RIGHTS_FOR_BOOKING:
                return redirect()->back()->
                    withErrors(trans('messages.notEnoughRightsForBooking'));
        break;
        case self::ERROR_BOOKING_NOT_ACTIVE:
                return redirect()->back()->
                    withErrors(trans('messages.bookingNotActive'));
        break;
        case self::BOOKINGS_RIGHTS_OK:
                return self::BOOKINGS_RIGHTS_OK;
        break;
        default:
                return redirect()->back()->
                    withErrors(trans('messages.dangerMessage'));
        endswitch;
    }

    /**
     * @param Guard $auth
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|UserController|\Illuminate\Http\RedirectResponse|int
     */
    public function showBookingEditForm(Guard $auth, $id)
    {
        $status = $this->bookingRightsRedirect($auth, $id);
        if (gettype($status) != gettype(self::BOOKINGS_RIGHTS_OK)) {
            return $status;
        }

        $booking = Booking::find($id);

        return view('user.booking_edit_form')->with([
            'booking' => $booking,
        ]);
    }

    /**
     * @param Request $request
     * @param Guard $auth
     * @param $id
     * @return mixed
     */
    public function updateBooking(Request $request, Guard $auth, $id)
    {
        $status = $this->bookingRightsRedirect($auth, $id);
        if (gettype($status) != gettype(self::BOOKINGS_RIGHTS_OK)) {
            return $status;
        }

        $booking = Booking::find($id);
        $glamping_id = $booking->glamping_id;
        $request->request->add(['glamping_id' => $glamping_id]);

        $controller = new BookController;

        return $controller->update($request, $id);
    }

    /**
     * @param Guard $auth
     * @param $id
     * @return mixed
     */
    public function cancelBooking(Guard $auth, $id)
    {
        $status = $this->bookingRightsRedirect($auth, $id);
        $booking = Booking::find($id);

        if (!$booking->isCancelable()) {
            return $status;
        }

        $controller = new BookController;
        return $controller->cancel($id);
    }

    /**
     * @param Guard $auth
     * @param $id
     * @return mixed
     */
    public function confirmCancelBooking(Guard $auth, $id)
    {
        $booking = Booking::find($id);

        if (!is_null($booking) && !$booking->isFreeCancelable() && $booking->isCancelable())
            return view('user.confirm_cancel_booking')
                ->with(['id' => $id]);
        else if (!is_null($booking) && $booking->isCancelable())
            return $this->cancelBooking($auth, $id);
        else return redirect()->back();
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showBookGlampingView(Request $request, $id)
    {
        $glamping = Glamping::find($id);
        $filter = array_filter($request->all());
        if (empty($filter)) $filter = null;

        if (is_null($glamping))
            return redirect()->back()->withErrors(trans('messages.notExistGlampingError'));

        if (Auth::guard('user')->check()) $user = Auth::guard('user')->user();
        elseif (Auth::guard('manager')->check()) $user = Auth::guard('manager')->user();
        else $user = null;

        return view('user.book_glamping')->with([
            'glamping' => $glamping,
            'filter' => $filter,
            'buildings' => $glamping->buildings,
            'comments' => $glamping->comments,
            'user' => $user,
            'manager' => Auth::guard('manager')->check()
        ]);
    }
}
