<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfManager
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = 'manager')
    {
        $isVerifyMethod = str_contains($request->route()->uri(), 'verifyemail/{token}');

        if (Auth::guard($guard)->check() && !$isVerifyMethod) {
            $user = Auth::guard('manager')->user();

            if ($user->hasRole('owner') && !$user->accepted) {
                return redirect()->route('manager.owner.profile');
            } elseif ($user->hasRole('owner') && $user->accepted) {
                return redirect()->route('manager.owner_dashboard');
            } elseif ($user->hasRole('admin')) {
                return redirect()->route('manager.admin_dashboard');
            }
        }

        return $next($request);
    }
}
