<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfUser
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = 'user')
    {
        $isVerifyMethod = str_contains($request->route()->uri(), 'verifyemail/{token}');
        
        if (Auth::guard($guard)->check() && !$isVerifyMethod) {
            return redirect()->route('user.dashboard');
        }

        return $next($request);
    }
}
