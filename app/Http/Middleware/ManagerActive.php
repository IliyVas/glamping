<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class ManagerActive
{
    protected $user;

    /**
     * ManagerActive constructor.
     */
    public function __construct()
    {
        $this->user = Auth::guard('manager')->user();
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $publicPrefix = str_contains($request->route()->getPrefix(), 'public');
        $hasPendingApp = $this->user->hasPendingApplication();

        if($this->user->activated && $this->user->accepted && !$hasPendingApp)
            return $next($request);
        elseif (!$this->user->activated)
            return redirect()->route('manager.nonactive');
        elseif (!$this->user->accepted)
            if ($publicPrefix && !$hasPendingApp)
                return $next($request);
            elseif (str_contains($request->route()->uri(), 'dashboard'))
                return redirect()->route('manager.owner.profile');
            else return redirect()->route('manager.nonaccepted');
        else return redirect()->route('manager.nonaccepted');
    }
}
