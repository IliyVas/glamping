const { mix } = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/js/app.js', 'public/js')
    .scripts(['resources/assets/js/booking.js'], 'public/js/booking.js')
    .scripts(['resources/assets/js/map.js'], 'public/js/map.js')
    .scripts(['resources/assets/js/confirm_alert.js'], 'public/js/confirm_alert.js')
    .scripts(['resources/assets/js/add_element.js'], 'public/js/add_element.js')
    .scripts(['resources/assets/js/icon.js'], 'public/js/icon.js')
    .scripts(['resources/assets/js/book_buildings_updater.js'], 'public/js/book_buildings_updater.js')
    .scripts(['resources/assets/js/slick.customselectionmodel.js'], 'public/js/slick.customselectionmodel.js')
    .scripts(['resources/assets/js/slick.customrangeselector.js'], 'public/js/slick.customrangeselector.js')
    .sass('resources/assets/sass/app.scss', 'public/css')
    .sass('resources/assets/sass/booking.scss', 'public/css')
    .sass('resources/assets/sass/auth.scss', 'public/css')
    .scripts(['resources/assets/js/auth/captcha.js'], 'public/js/captcha.js')
    .styles(['resources/assets/css/flaticon.css'], 'public/css/flaticon.css');

mix.styles([
    'resources/assets/css/flaticon.css',
    'resources/assets/css/style.css'
], 'public/css/all.css');

mix.scripts([
    'resources/assets/js/preview.js',
    'resources/assets/js/glamping_filter.js',
    'resources/assets/js/glamping_images.js'
], 'public/js/all.js');



mix.copy('./resources/assets/js/message/index.js', 'public/js/message/index.js');
mix.copy('./resources/assets/js/page/show.js', 'public/js/page/show.js');
mix.copy('./resources/assets/js/news/create.js', 'public/js/news/create.js');
mix.copyDirectory('./resources/assets/js/components/ckeditor', 'public/js/ckeditor/');

mix.scripts([
    'resources/assets/js/jquery.js',
    'resources/assets/js/jquery-ui.js'
], 'public/js/jQ.js');

mix.copyDirectory('resources/assets/theme', 'public/theme');
mix.copyDirectory('resources/assets/images', 'public/images');
mix.copyDirectory('resources/assets/fonts', 'public/fonts');
mix.copyDirectory('resources/assets/pdf', 'public/pdf');

mix.browserSync('localhost:8000');