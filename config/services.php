<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, SparkPost and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
    ],

    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => 'us-east-1',
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'model' => App\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],

    // social block
    'vkontakte' => [
        'client_id' => env('SOCIAL_VKONTAKTE_KEY'),
        'client_secret' => env('SOCIAL_VKONTAKTE_SECRET'),
        'redirect' => env('SOCIAL_VKONTAKTE_REDIRECT_URI'),
    ],
    'google' => [
        'client_id' => env('SOCIAL_GOOGLE_KEY'),
        'client_secret' => env('SOCIAL_GOOGLE_SECRET'),
        'redirect' => env('SOCIAL_GOOGLE_REDIRECT_URI'),
    ],
];
